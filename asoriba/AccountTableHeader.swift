//
//  MyChurchTableHeader.swift
//  asoriba
//
//  Created by Bright Ahedor on 20/12/2016.
//  Copyright © 2016 Asoriba Inc. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage

class AccountTableHeader: UITableViewHeaderFooterView {
   
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var userBackProfileImageView: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var userStatsLabel: UILabel!
    @IBOutlet weak var userSegmentControl: UISegmentedControl!
    
    var currentUser = DataOperation.getUser()
    
    @IBOutlet weak var followButton: UIButton!
    
    var userData: Member! {
        didSet {
            updateUI()
        }
    }
    
    func updateUI()  {
        
        let path = Bundle.main.path(forResource: "configuration", ofType: "plist")
        let root = NSDictionary(contentsOfFile: path!)
        
        let headerBackground = (root?["headerBackgroundColor"] as! String)
        //let headerYellowBackground = (root?["headerYellowColor"] as! String)
        let tint_white = (root?["tint_white"] as! String)

         self.userNameLabel.text = userData.firstName + " " + userData.lastName
        
        let followsShowable = userData.numberOfFollowing == 1 ? "\(userData.numberOfFollowing) Follower" : "\(userData.numberOfFollowing) Followers";
        
        //let groupsShowable = userData.numberOfGroups == 1 ? "\(userData.numberOfGroups) Group" : "\(userData.numberOfGroups) Groups";
        
        if userData.isFollowing {
            
            self.followButton.setTitle("Unfollow", for: UIControlState.normal)
            self.followButton.layer.cornerRadius = 3
            self.followButton.layer.borderWidth = 2
            self.followButton.backgroundColor = UIColor().HexToColor(hexString: headerBackground)
            self.followButton.layer.borderColor = UIColor().HexToColor(hexString: tint_white).cgColor

        }else {
            self.followButton.setTitle("Follow", for: UIControlState.normal)
            self.followButton.layer.cornerRadius = 3
            self.followButton.layer.borderWidth = 2
            self.followButton.backgroundColor = UIColor().HexToColor(hexString: tint_white)
            self.followButton.layer.borderColor = UIColor().HexToColor(hexString: headerBackground).cgColor
            self.followButton.setTitleColor(UIColor().HexToColor(hexString: headerBackground), for: UIControlState.normal)
        }
        
        self.userSegmentControl.backgroundColor = UIColor().HexToColor(hexString: tint_white)
        self.userSegmentControl.tintColor = UIColor().HexToColor(hexString: headerBackground)

        
        self.userStatsLabel.text = "\(followsShowable)" //• \(groupsShowable)"
        
        //content image
        Alamofire.request((userData.avatar)).responseImage { response in
            if let image = response.result.value {
                self.userBackProfileImageView.image = image
            }else{
                self.userBackProfileImageView.image = UIImage(named: "default")
            }
        }
        
        //user image
        Alamofire.request((userData.avatar)).responseImage { response in
            if let image = response.result.value {
                self.userImageView.image = image
            }else{
                self.userImageView.image = UIImage(named: "default")
            }
        }
        
        if (currentUser.id == userData.id){
           self.followButton.setTitle("Edit", for: UIControlState.normal)
        }
        
        self.userImageView.isUserInteractionEnabled = true
        
    }
    
}

