//
//  Group.swift
//  asoriba
//
//  Created by Bright Ahedor on 15/12/2016.
//  Copyright © 2016 Asoriba Inc. All rights reserved.
//

import Foundation

class Group {
    var id = ""
    var name = ""
    var description = ""
    var image = ""
    var isJoined = true

}
