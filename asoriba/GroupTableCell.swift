//
//  GroupTableCell.swift
//  asoriba
//
//  Created by Bright Ahedor on 15/12/2016.
//  Copyright © 2016 Asoriba Inc. All rights reserved.
//

import UIKit
import AlamofireImage
import LBTAComponents

class GroupTableCell: UITableViewCell {

    @IBOutlet weak var groupImageView: UIImageView!
   
    @IBOutlet weak var groupJoinAction: UIButton!
    @IBOutlet weak var groupDescriptionLabel: UILabel!
    @IBOutlet weak var groupTitleLabel: UILabel!
    private var placeHolderImage = UIImage(named: "default")
    
    let aboutImg : UIImageView = {
        let img = UIImageView()
        img.image = UIImage(named: "about")?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
        img.translatesAutoresizingMaskIntoConstraints = false
        return img
    }()
    
    var feed: Group!{
        didSet{
          updateUI()
        }
    }
    
    func updateUI()   {
        
        let path = Bundle.main.path(forResource: "configuration", ofType: "plist")
        let root = NSDictionary(contentsOfFile: path!)
        
        let buttonColor = (root?["headerBackgroundColor"] as! String)
        let buttonYellow = (root?["headerYellowColor"] as! String)
        
        if (feed.isJoined){
            self.groupJoinAction.backgroundColor = UIColor().HexToColor(hexString: buttonColor)
        }else{
            self.groupJoinAction.backgroundColor = UIColor().HexToColor(hexString: buttonYellow)
        }
                
        
        self.aboutImg.tintColor = UIColor().HexToColor(hexString: buttonColor)
        
        addSubview(aboutImg)
        aboutImg.rightAnchor.constraint(equalTo: rightAnchor, constant: -5).isActive = true
        aboutImg.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -5).isActive = true
        aboutImg.topAnchor.constraint(equalTo: groupJoinAction.topAnchor).isActive = true
        aboutImg.widthAnchor.constraint(equalToConstant: 35).isActive = true
        
        self.groupDescriptionLabel.text = feed.description
        self.groupTitleLabel.text = feed.name
        //content image
        if  feed.image != "" {
         self.groupImageView.af_setImage(
            withURL: URL(string: feed.image)!,
            placeholderImage: placeHolderImage,
            imageTransition: .crossDissolve(0.2)
        )}else {
            self.groupImageView.image = placeHolderImage
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.groupImageView.af_cancelImageRequest()
        self.groupImageView.layer.removeAllAnimations()
        self.groupImageView.image = nil
    }
    
}



//new groupcell
class theNewGroupcell: UICollectionViewCell {
    
    private var placeHolderImage = UIImage(named: "default")

    lazy var backView : UIView = {
        let v = UIView()
        v.layer.cornerRadius = 5
        v.backgroundColor = .white
        v.layer.shadowColor = UIColor.lightGray.cgColor
        v.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        //v.layer.shadowRadius = 2.0
        v.layer.shadowOpacity = 1.0
        v.layer.masksToBounds = false
        return v
    }()
    
    lazy var cView : UIView = {
        let v = UIView()
        v.layer.cornerRadius = 5
        v.backgroundColor = .white
        v.layer.shadowColor = UIColor.lightGray.cgColor
        v.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        //v.layer.shadowRadius = 2.0
        v.layer.shadowOpacity = 1.0
        v.layer.masksToBounds = false
        return v
    }()
    
    let churchImg : UIImageView = {
        let img = UIImageView()
        //img.image = #imageLiteral(resourceName: "postImage1")
        img.contentMode = .scaleAspectFill
        img.layer.masksToBounds = true
        img.layer.cornerRadius = 5
        return img
    }()
    
    let checkImg : UIImageView = {
        let img = UIImageView()
        //img.image = #imageLiteral(resourceName: "check")
        img.contentMode = .scaleAspectFill
        img.layer.masksToBounds = true
        img.layer.cornerRadius = 10
        return img
    }()
    
    let churchNameLbl : UILabel = {
        let lbl = UILabel()
        //lbl.text = "Asoriba Church"
        lbl.textAlignment = .left
        lbl.adjustsFontSizeToFitWidth = true
        lbl.font = UIFont.boldSystemFont(ofSize: 16)
        return lbl
    }()
    
    let aboutChurchTxt : UILabel = {
        let txt = UILabel()
        txt.adjustsFontSizeToFitWidth = true
        //txt.text = "Asoriba church was Formed two Years Ago by the power of the holyspirit to drive the church to an excellent destination."
        txt.numberOfLines = 10
        txt.font = UIFont.systemFont(ofSize: 12)
        return txt
    }()
    
    let followBtn : UIButton = {
        let btn = UIButton(type: UIButtonType.roundedRect)
        btn.setTitle("Join", for: UIControlState.normal)
        btn.setTitleColor(UIColor.white, for: UIControlState.normal)
        btn.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        btn.backgroundColor = UIColor(white: 0.0, alpha: 0.8)
        btn.layer.borderWidth = 2
        btn.layer.cornerRadius = 2
        btn.layer.borderColor = UIColor.clear.cgColor
        
        btn.layer.shadowColor = UIColor.lightGray.cgColor
        btn.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        //v.layer.shadowRadius = 2.0
        btn.layer.shadowOpacity = 1.0
        btn.layer.masksToBounds = false
        
        return btn
    }()

    var feed: Group!{
        didSet{
            updateUI()
        }
    }
    
    func updateUI()   {
        
//        let path = Bundle.main.path(forResource: "configuration", ofType: "plist")
//        let root = NSDictionary(contentsOfFile: path!)
//
//        let buttonColor = (root?["headerBackgroundColor"] as! String)
        
        if (feed.isJoined){

        }else{

        }
        
        
//        self.aboutImg.tintColor = UIColor().HexToColor(hexString: buttonColor)
//
//        addSubview(aboutImg)
//        aboutImg.rightAnchor.constraint(equalTo: rightAnchor, constant: -5).isActive = true
//        aboutImg.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -5).isActive = true
//        aboutImg.topAnchor.constraint(equalTo: groupJoinAction.topAnchor).isActive = true
//        aboutImg.widthAnchor.constraint(equalToConstant: 35).isActive = true
        
        self.aboutChurchTxt.text = feed.description
        self.churchNameLbl.text = feed.name
        //content image
        if  feed.image != "" {
            self.churchImg.af_setImage(
                withURL: URL(string: feed.image)!,
                placeholderImage: placeHolderImage,
                imageTransition: .crossDissolve(0.2)
            )}else {
            self.churchImg.image = placeHolderImage
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.churchImg.af_cancelImageRequest()
        self.churchImg.layer.removeAllAnimations()
        self.churchImg.image = nil
    }

    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(cView)
        addSubview(backView)
        backView.addSubview(churchImg)
        backView.addSubview(followBtn)
        
        
        cView.addSubview(churchNameLbl)
        cView.addSubview(checkImg)
        cView.addSubview(aboutChurchTxt)
        
        backView.anchor(self.topAnchor, left: self.leftAnchor, bottom: self.bottomAnchor, right: nil, topConstant: 8, leftConstant: 8, bottomConstant: 8, rightConstant: 0, widthConstant: self.frame.width / 2 - 10, heightConstant: 0)
        
        let customWidth : CGFloat = backView.frame.width
        churchImg.anchor(backView.topAnchor, left: backView.leftAnchor, bottom: backView.bottomAnchor, right: backView.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: customWidth, heightConstant: 0)
        
        followBtn.anchor(nil, left: churchImg.leftAnchor, bottom: backView.bottomAnchor, right: churchImg.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 40)
        
        
        cView.anchor(backView.topAnchor, left: backView.rightAnchor, bottom: backView.bottomAnchor, right: self.rightAnchor, topConstant: 0, leftConstant: 1, bottomConstant: 0, rightConstant: 8, widthConstant: 0, heightConstant: 0)
        
        churchNameLbl.anchor(cView.topAnchor, left: cView.leftAnchor, bottom: nil, right: cView.rightAnchor, topConstant: 5, leftConstant: 5, bottomConstant: 0, rightConstant: 5, widthConstant: 0, heightConstant: 20)
        
        aboutChurchTxt.anchor(churchNameLbl.bottomAnchor, left: churchNameLbl.leftAnchor, bottom: cView.bottomAnchor, right: cView.rightAnchor, topConstant: 5, leftConstant: 5, bottomConstant: 5, rightConstant: 5, widthConstant: 0, heightConstant: 0)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
