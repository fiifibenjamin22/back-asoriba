//
//  searchUsers.swift
//  asoriba
//
//  Created by Benjamin Acquah on 22/08/2017.
//  Copyright © 2017 Asoriba Inc. All rights reserved.
//

import UIKit
import AlamofireImage

class searchUsersCell: UICollectionViewCell {
    
    let userAva : UIImageView = {
        let img = UIImageView()
        img.image = UIImage(named: "default")?.withRenderingMode(.alwaysOriginal)
        img.translatesAutoresizingMaskIntoConstraints = false
        img.layer.cornerRadius = 37
        img.contentMode = .scaleAspectFill
        img.layer.masksToBounds = true
        return img
    }()
    
    let userName : UILabel = {
        let lbl = UILabel()
        lbl.text = " "
        lbl.font = UIFont.systemFont(ofSize: 16)
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let numberOfFollowers : UILabel = {
        let lbl = UILabel()
        lbl.text = " "
        lbl.font = UIFont.boldSystemFont(ofSize: 12)
        lbl.textColor = UIColor.gray
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let followBtn : UIButton = {
        
        let btn = UIButton(type: UIButtonType.system)
        btn.setTitle("+ Follow", for: .normal)
        btn.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        btn.backgroundColor = UIColor.red
        btn.setTitleColor(UIColor.white, for: .normal)
        btn.layer.cornerRadius = 5
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.layer.shadowColor = UIColor.lightGray.cgColor
        btn.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        btn.layer.shadowRadius = 2.0
        btn.layer.shadowOpacity = 1.0
        btn.layer.masksToBounds = false

        return btn
    }()
    
    let userChurch : UILabel = {
        let lbl = UILabel()
        lbl.text = "ICGC"
        lbl.font = UIFont.boldSystemFont(ofSize: 12)
        lbl.textColor = UIColor.gray
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    private var placeHolderImage = UIImage(named: "default")
    
    let user = DataOperation.getUser()
    
    var noAction : Bool!{
        didSet {
            self.followBtn.isHidden = false
        }
    }
    
    
    var feed: Member!{
        didSet{
            updateUI()
        }
    }
    
    
    func updateUI()   {

        let path = Bundle.main.path(forResource: "configuration", ofType: "plist")
        let root = NSDictionary(contentsOfFile: path!)
        
        let headerBackground = (root?["headerBackgroundColor"] as! String)
        let headerYellowBackground = (root?["tint_white"] as! String)
        
        self.followBtn.layer.borderWidth = 2
        self.followBtn.layer.borderColor = UIColor().HexToColor(hexString: headerBackground).cgColor
        
        
        
        //setting counts
        let followers = feed.numberOfFollowing
        let followingShowable = followers == 1 ? "\(followers) Follower" : "\(followers) Followers"
        self.numberOfFollowers.text = followingShowable
        self.userName.text = feed.lastName + " " + feed.firstName
        
        if (feed.isFollowing){
            self.followBtn.setTitle("Unfollow", for: .normal)
            self.followBtn.backgroundColor = UIColor().HexToColor(hexString: headerBackground)
            self.followBtn.setTitleColor(UIColor().HexToColor(hexString: headerYellowBackground), for: UIControlState.normal)
            
        }else {
            self.followBtn.setTitle("Follow", for: .normal)
            self.followBtn.backgroundColor = UIColor().HexToColor(hexString: headerYellowBackground)
            self.followBtn.setTitleColor(UIColor().HexToColor(hexString: headerBackground), for: UIControlState.normal)
        }
        //content image
        if  feed.avatar != "" {
            self.userAva.af_setImage(
                withURL: URL(string: feed.avatar)!,
                placeholderImage: placeHolderImage,
                imageTransition: .crossDissolve(0.2)
            )
        }else {
            self.userAva.image = placeHolderImage
        }
        
        
        if self.feed.id == self.user.id {
            self.followBtn.isHidden = true
        } else{
            self.followBtn.isHidden = false
        }
        
    }
   
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .white
        
        self.layer.shadowColor = UIColor.lightGray.cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        self.layer.shadowRadius = 2.0
        self.layer.shadowOpacity = 1.0
        self.layer.masksToBounds = false
        self.layer.shadowPath = UIBezierPath(roundedRect: self.bounds, cornerRadius: self.contentView.layer.cornerRadius).cgPath
        
        setUpViews()
        
    }
    
    func setUpViews(){
                
        addSubview(userAva)
        addSubview(userName)
        addSubview(numberOfFollowers)
        addSubview(followBtn)
        addSubview(userChurch)
        
        addConstraintsWithFormat("H:|-8-[v0(74)]", views: userAva)
        addConstraintsWithFormat("V:|-8-[v0(74)]-8-|", views: userAva)
        
        userName.topAnchor.constraint(equalTo: topAnchor, constant: 12).isActive = true
        userName.leftAnchor.constraint(equalTo: userAva.rightAnchor, constant: 8).isActive = true
        userName.rightAnchor.constraint(equalTo: rightAnchor, constant: -8).isActive = true
        userName.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        numberOfFollowers.topAnchor.constraint(equalTo: userName.bottomAnchor, constant: 3).isActive = true
        numberOfFollowers.leftAnchor.constraint(equalTo: userName.leftAnchor).isActive = true
        numberOfFollowers.rightAnchor.constraint(equalTo: userName.rightAnchor).isActive = true
        numberOfFollowers.heightAnchor.constraint(equalToConstant: 10).isActive = true
        
        followBtn.topAnchor.constraint(equalTo: numberOfFollowers.bottomAnchor, constant: 8).isActive = true
        followBtn.rightAnchor.constraint(equalTo: numberOfFollowers.rightAnchor).isActive = true
        followBtn.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -8).isActive = true
        followBtn.widthAnchor.constraint(equalToConstant: 80).isActive = true
        
        userChurch.topAnchor.constraint(equalTo: numberOfFollowers.bottomAnchor, constant: 5).isActive = true
        userChurch.leftAnchor.constraint(equalTo: numberOfFollowers.leftAnchor).isActive = true
        userChurch.rightAnchor.constraint(equalTo: followBtn.leftAnchor, constant: -5).isActive = true
        userChurch.heightAnchor.constraint(equalToConstant: 10).isActive = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
