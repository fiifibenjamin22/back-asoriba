//
//  VersePickerController.swift
//  asoriba
//
//  Created by Bright Ahedor on 06/02/2017.
//  Copyright © 2017 Asoriba Inc. All rights reserved.
//

import UIKit
import LBTAComponents

class VersePickerController: UIViewController,UIPickerViewDataSource,UIPickerViewDelegate,UITextFieldDelegate {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var bookUIPickerView: UIPickerView!
    @IBOutlet weak var chapterTextField: UITextField!
    @IBOutlet weak var fromVerseTextField: UITextField!
    @IBOutlet weak var toVerseTextField: UITextField!
    @IBOutlet weak var setVerseBtn: UIButton!
    @IBOutlet weak var cancelBtn: UIButton!
    
    let pickerData = ["Genesis","Exodus","Leviticus","Numbers","Deuteronomy",
        "Joshua","Judges","Ruth","1 Samuel","2 Samuel","1 Kings","2 Kings","1 Chronicles","2 Chronicles","Ezra","Nehemiah","Esther","Job","Psalm","Proverbs","Ecclesiastes","Song of Solomon","Isaiah","Jeremiah","Lamentations","Ezekiel","Daniel","Hosea","Joel","Amos","Obadiah",
        "Jonah","Micah","Nahum","Habakkuk","Zephaniah","Haggai","Zechariah","Malachi","Matthew",
        "Mark","Luke","John","Acts","Romans","1 Corinthians","2 Corinthians","Galatians",
        "Ephesians","Philippians","Colossians","1 Thessalonians","2 Thessalonians","1 Timothy","2 Timothy","Titus","Philemon","Hebrews","James","1 Peter","2 Peter","1 John","2 John","3 John","Jude","Revelation"]
    var quotation = Quotation()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.8)
        self.showAnimate()
        
        self.containerView.isUserInteractionEnabled = true
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.closePopUp(_:)))
        self.view.addGestureRecognizer(tap)
        self.view.isUserInteractionEnabled = true
        
        //picker delegate
        self.bookUIPickerView.dataSource = self
        self.bookUIPickerView.delegate = self
        
        self.chapterTextField.delegate = self
        self.fromVerseTextField.delegate = self
        self.toVerseTextField.delegate = self
        
        self.quotation.book = self.pickerData[0]
        
        let path = Bundle.main.path(forResource: "configuration", ofType: "plist")
        let root = NSDictionary(contentsOfFile: path!)
        
        let BtnBackground = (root?["headerBackgroundColor"] as! String)
        
        self.setVerseBtn.backgroundColor = UIColor().HexToColor(hexString: BtnBackground)
        
        self.containerView.addSubview(cancelBtn)
        self.cancelBtn.translatesAutoresizingMaskIntoConstraints = false
        self.cancelBtn.anchor(containerView.topAnchor, left: nil, bottom: nil, right: containerView.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 45, heightConstant: 45)
        
        self.cancelBtn.layer.cornerRadius = 15
        self.cancelBtn.layer.masksToBounds = true
        
        self.cancelBtn.addTarget(self, action: #selector(handleCancelBtn), for: UIControlEvents.touchUpInside)
        
    }

    func handleCancelBtn(){
        self.containerView.endEditing(true)
        resignFirstResponder()
        removeAnimate()
    }
        
    //MARK: - Delegates and data sources
    //MARK: Data Sources
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerData.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerData[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.quotation.book = pickerData[row]
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    //end of delegate
    
    //close action on the parent view
    func closePopUp(_ sender: UITapGestureRecognizer) {
        self.chapterTextField.resignFirstResponder()
        self.fromVerseTextField.resignFirstResponder()
        self.toVerseTextField.resignFirstResponder()
    }
    
    //close action on the button
    func cancelAction() {
        self.removeAnimate()
        self.view.removeFromSuperview()
    }
    
    
    func showAnimate()
    {
        self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view.alpha = 0.0;
        UIView.animate(withDuration: 0.25, animations: {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        });
    }
    
    func removeAnimate()
    {
        UIView.animate(withDuration: 0.25, animations: {
            self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view.alpha = 0.0;
        }, completion:{(finished : Bool)  in
            if (finished)
            {
                self.view.removeFromSuperview()
            }
        });
    }

    // MARK: - Textfield delegates
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        self.setVerseBtn.isEnabled = false
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
       self.setVerseBtn.isEnabled = true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true;
    }
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        return true;
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        return true;
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return true;
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder();
        return true;
    }
    
    //end of delegate
    
    @IBAction func setVerseButtonClicked(_ sender: UIButton) {
        resignFirstResponder()
        self.quotation.chapter = self.chapterTextField.text!
        let fromVerse = self.fromVerseTextField.text!
        let toVerse = self.toVerseTextField.text!
        self.quotation.verse = fromVerse + "-" + toVerse
        self.quotation.book =  self.pickerData[self.bookUIPickerView.selectedRow(inComponent: 0)]
        
        if self.quotation.chapter.isEmpty {
          ControllerHelpers.showDialog(targetVC: self, title: "Asoriba", message: "Please set the books chapter")
          return
        }
        
        if fromVerse.isEmpty {
            ControllerHelpers.showDialog(targetVC: self, title: "Asoriba", message: "Tell us the starting verse")
            return
        }
        
        if toVerse.isEmpty {
            ControllerHelpers.showDialog(targetVC: self, title: "Asoriba", message: "Tell us the ending verse")
            return
        }
        
        if let fromVerseInt = Int(fromVerse),let toVerseInt = Int(toVerse){
            if fromVerseInt >= toVerseInt {
              ControllerHelpers.showDialog(targetVC: self, title: "Asoriba", message: "Kindly check the verses again")
              return
            }
        }
        
        
       NotificationCenter.default.post(name: Notification.Name(AllKeys.verseNotification), object: self.quotation)
       self.cancelAction()
    }
    

}
