//
//  CheckoutResponse.swift
//  asoriba
//
//  Created by Bright Ahedor on 20/12/2016.
//  Copyright © 2016 Asoriba Inc. All rights reserved.
//

import Foundation



class CheckoutResponse {
    var statusCode = 0
    var checkoutUrl = ""
    var details = ""
    
}
