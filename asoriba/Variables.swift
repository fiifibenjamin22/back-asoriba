//
//  Variable.swift
//  asoriba
//
//  Created by Bright Ahedor on 15/12/2016.
//  Copyright © 2016 Asoriba Inc. All rights reserved.
//

import Foundation

var MyplistBase : String!
var MyplistAppId : String!
var MyplistSecretToken : String!
var ICGCAPID : String!

struct Variables {
    
    //switching the base Url
    static func baseUrl() -> String {
        
        let path = Bundle.main.path(forResource: "configuration", ofType: "plist")
        let root = NSDictionary(contentsOfFile: path!)
        
        let basePlistData = (root?["base"] as! String)
        MyplistBase = basePlistData
        
        print("print my base \(MyplistBase!)")
        return MyplistBase
    }
    
    //switching the  app Token
    static func appId() -> String {
        
        let path = Bundle.main.path(forResource: "configuration", ofType: "plist")
        let root = NSDictionary(contentsOfFile: path!)
        
        let appTokenPlistData = (root?["appToken"] as! String)
        MyplistAppId = appTokenPlistData
        
        print("print appId \(MyplistAppId!)")
        return MyplistAppId
    }
    
    //switching the secret key
    static func secretKey() -> String {
        
        let path = Bundle.main.path(forResource: "configuration", ofType: "plist")
        let root = NSDictionary(contentsOfFile: path!)
        
        let secretTokenPlistData = (root?["secretToken"] as! String)
        MyplistSecretToken = secretTokenPlistData
        
        print("print secret token\(MyplistSecretToken!)")
        return MyplistSecretToken
    }
    
    //switching the  app ID
    static func icgcAppId() -> String {
        
        let path = Bundle.main.path(forResource: "configuration", ofType: "plist")
        let root = NSDictionary(contentsOfFile: path!)
        
        let icgcID = (root?["APPID"] as! String)
        ICGCAPID = icgcID
        
        print("print icgc app ID \(ICGCAPID!)")
        return ICGCAPID
    }
}
