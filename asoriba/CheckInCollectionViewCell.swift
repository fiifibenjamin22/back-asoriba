//
//  CheckInCollectionViewCell.swift
//  asoriba
//
//  Created by Bright Ahedor on 16/03/2017.
//  Copyright © 2017 Asoriba Inc. All rights reserved.
//

import UIKit

class CheckInCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var checkInMessageLabel: UILabel!
    @IBOutlet weak var checkInTimeLabel: UILabel!

    var feed: CheckIn!{
        didSet{
            updateUI()
        }
    }
    
    
    func updateUI()  {
        self.checkInMessageLabel.text = feed.program
        print("Title \(feed.program)")
        self.checkInTimeLabel.text = feed.device + " @" + Mics.setDate(dateString: feed.date)
    }

}
