//
//  ChatUser.swift
//  asoriba
//
//  Created by Benjamin Acquah on 17/09/2017.
//  Copyright © 2017 Asoriba Inc. All rights reserved.
//

import RealmSwift

//chat user model
class ChatUser : Object {
    
    dynamic var user2_id = ""
    dynamic var user2_firstname = ""
    dynamic var user2_lastname = ""
    dynamic var userName = ""
    @objc dynamic var dateCreated: Double = 0.0
    dynamic var profileImage = ""
    dynamic var lastmessage = ""
    dynamic var numberOfFollowers:Int = 0
    dynamic var numberOfGroups:Int = 0
    
    override class func primaryKey()->String{
        return "user2_id"
    }
}
