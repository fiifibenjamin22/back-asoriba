//
//  searchUserVC.swift
//  asoriba
//
//  Created by Benjamin Acquah on 22/08/2017.
//  Copyright © 2017 Asoriba Inc. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage
import Photos
import SKPhotoBrowser
import Toaster
import Crashlytics

let cellIdentify = "cellID"

class searchUserVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource ,UICollectionViewDelegateFlowLayout, UISearchBarDelegate, ChurchMembersServiceProtocol {

    var titleText = ""

    var api:ApiService!
    
    var positionSelected = 0
    var filteredUsers = [Member]()
    var membersData = [Member]()
    var groupsData = [Group]()
    var feedsData = [Feed]()
    var userData = DataOperation.getUser()
    var churchItem: Church!
    var membersNextUrl = ""
    var loadedPages = [String]()
    var text = ""
    var activityViewIndicator:UIActivityIndicatorView = UIActivityIndicatorView()
    var refreshControl: UIRefreshControl = UIRefreshControl();
    var isRefreshing = false
        
    lazy var searchBar : UISearchBar = {
        let sb = UISearchBar()
        sb.placeholder = "Enter Name to search ..."
        //sb.barTintColor = UIColor.white//(r: 28, g: 90, b: 65)
        sb.translatesAutoresizingMaskIntoConstraints = false
        sb.delegate = self
        UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).backgroundColor = UIColor(r: 28, g: 90, b: 65)
        return sb
    }()
    
    let greenView : UIView = {
        let searchView = UIView()
        searchView.translatesAutoresizingMaskIntoConstraints = false
        return searchView
    }()

    
    lazy var collectionView : UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.delegate = self
        cv.dataSource = self
        cv.alwaysBounceVertical = true
        cv.showsVerticalScrollIndicator = false
        
        return cv
    }()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationItem.backBarButtonItem?.title = ""
        
        //api instance
        api = ApiService()
        self.api.churchMembersDelegate = self
        
        collectionView.register(searchUsersCell.self, forCellWithReuseIdentifier: cellIdentify)
        collectionView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0)
        collectionView.backgroundColor = UIColor.groupTableViewBackground
        
        searchBar.tintColor = .white
        for subView in searchBar.subviews {
            for subView1 in subView.subviews{
                if let textField = subView1 as? UITextField{
                    subView1.backgroundColor = UIColor.black.withAlphaComponent(0.3)
                    //use the code below if you want to change the color of placeholder
                    let textFieldInsideUISearchBarLabel = textField.value(forKey: "placeholderLabel") as? UILabel
                    textFieldInsideUISearchBarLabel?.textColor = .white
                    textField.textColor = .white
                    textField.font = UIFont.boldSystemFont(ofSize: 14)
                }
            }
        }
        
        let path = Bundle.main.path(forResource: "configuration", ofType: "plist")
        let root = NSDictionary(contentsOfFile: path!)
        
        let searchBackColor = (root?["headerBackgroundColor"] as! String)
        greenView.backgroundColor = UIColor().HexToColor(hexString: searchBackColor)//(r: 28, g: 90, b: 65)
        searchBar.barTintColor = UIColor().HexToColor(hexString: searchBackColor)
        
        setUpViews()
        
        self.navigationItem.titleView = searchBar
        
        //refreshing
        refreshControl.addTarget(self, action: #selector(searchUserVC.refresh), for: .valueChanged)
        self.collectionView.addSubview(refreshControl)
        
        //present data
        if !userData.churchId.isEmpty {
            
            self.startProgressBar()

            DispatchQueue.main.async {
                self.getMembers(url: "page=1",churchId: self.userData.churchId)

            }
            
        }

    }
    
    func startProgressBar()  {
        activityViewIndicator.center = self.view.center
        activityViewIndicator.hidesWhenStopped = true
        activityViewIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray
        self.view.addSubview(activityViewIndicator)
        activityViewIndicator.startAnimating()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        DispatchQueue.main.async {

            self.collectionView.reloadData()
        }
    }
    
    func getData(page:String)  {
        self.api.searchChurcheMembers(url: "connections/search/?type=users&\(page)&page_size=500&keyword=\(text)")
    }
    
    
    //set image
    func tappedUserImage()
    {
        ControllerHelpers.presentSingleImage(targetVC: self, url: self.userData.churchLogo)
    }
    
  
    func getMembers(url:String,churchId:String) {
        api.getChurchMembers(branchId: churchId,next:url)
        
    }
    
    func didReceiveChurchMembersSuccess(results: [Member]) {
        self.refreshControl.endRefreshing()
        self.activityViewIndicator.stopAnimating()

        if results.isEmpty {
            Toast(text: "No result found").show()
        }else{
            self.membersData.removeAll()
            self.collectionView.reloadData()
            self.membersData.append(contentsOf: results)
            print("print search results: \(results)")
        }
    }
    
    func getContent(url:String,churchId:String) {
        api.getChurchContent(branchId: churchId, page: url)
    }
    
    
    func didReceiveChurchMembersError(results: String) {
        //members error
        self.activityViewIndicator.stopAnimating()

    }
    
    
    func didReceiveChurchMembersDetail(results: String) {
        //member detail
        self.activityViewIndicator.stopAnimating()

    }
    
    
    func didReceiveChurchMemberNextUrl(results: String) {
        //member next Url
        self.membersNextUrl = results
    }
    
    func setUpViews(){
        
        view.addSubview(collectionView)
        view.addSubview(greenView)
       
        view.addConstraintsWithFormat("H:|[v0]|", views: greenView)
        view.addConstraintsWithFormat("V:[v0(50)]", views: greenView)
        
        view.addConstraintsWithFormat("H:|[v0]|", views: collectionView)
        view.addConstraintsWithFormat("V:|[v0]|", views: collectionView)
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return membersData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentify, for: indexPath) as! searchUsersCell
        
        let feed = membersData[indexPath.row]
        cell.feed = feed
        
        if self.userData.id == feed.id {
            cell.followBtn.isHidden = true
        }else {
            cell.followBtn.isHidden = false
            cell.followBtn.tag = indexPath.row
            
            cell.followBtn.addTarget(self, action: #selector(MyChurchController.followUser(_:)), for: .touchUpInside)
        }
        
        //gesture section image
        cell.contentView.isUserInteractionEnabled = true
        cell.contentView.tag = indexPath.row
        let tapped = UITapGestureRecognizer(target: self, action: #selector(self.startDetail(_:)))
        cell.contentView.addGestureRecognizer(tapped)
        //end of gesture
        
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.view.frame.size.width, height: 90)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        self.searchBar.showsCancelButton = true
        
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.navigationController?.navigationItem.hidesBackButton = false
        self.searchBar.endEditing(true)
        self.searchBar.showsCancelButton = false
        resignFirstResponder()
        self.searchBar.text = ""

        self.membersData.removeAll()
        self.activityViewIndicator.startAnimating()
        self.getMembers(url: "page=1", churchId: userData.churchId)
        self.activityViewIndicator.stopAnimating()


        DispatchQueue.main.async {
            self.collectionView.reloadData()
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        print("searching")
        self.navigationController?.navigationItem.hidesBackButton = true
        
        if searchText.isEmpty {
            self.membersData.removeAll()
            self.getMembers(url: "page=1", churchId: userData.churchId)
        }else{

            if (searchText.count > 2){

                self.membersData.removeAll()
                self.activityViewIndicator.startAnimating()

                self.text = searchText
                self.getData(page: "page=1")
                self.activityViewIndicator.stopAnimating()


                DispatchQueue.main.async {
                    self.collectionView.reloadData()
                    self.activityViewIndicator.isHidden = true
                }
            }

        }
        
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let currentOffset = scrollView.contentOffset.y
        let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height
        
        if (maximumOffset - currentOffset) <= 400 {
            if(!self.membersNextUrl.isEmpty && !loadedPages.contains(self.membersNextUrl)){
                loadedPages.append(self.membersNextUrl)
                getData(page: self.membersNextUrl)
                print("Next Url \(self.membersNextUrl)")
            }
        }
    }
    
    //MARK: - Following user
    @IBAction func followUser(_ sender: UIButton) {
        let path = Bundle.main.path(forResource: "configuration", ofType: "plist")
        let root = NSDictionary(contentsOfFile: path!)
        
        let headerBackground = (root?["headerBackgroundColor"] as! String)
        let headerYellowBackground = (root?["tint_white"] as! String)
        
        titleText = sender.currentTitle!
        let feed = self.membersData[sender.tag]
        if (titleText == "Follow") {
            api.followMeber(userId: feed.id)
            sender.setTitle("Unfollow", for: .normal)
            feed.isFollowing = true
            sender.backgroundColor = UIColor().HexToColor(hexString: headerBackground)
            sender.setTitleColor(UIColor().HexToColor(hexString: headerYellowBackground), for: UIControlState.normal)
        } else {
            api.unFollowMeber(userId: feed.id)
            sender.setTitle("Follow", for: .normal)
            feed.isFollowing = false
            sender.backgroundColor = UIColor().HexToColor(hexString: headerYellowBackground)
            sender.setTitleColor(UIColor().HexToColor(hexString: headerBackground), for: UIControlState.normal)
            
        }
    }

    
    //MARK: -- member profile
    func startDetail(_ sender: UITapGestureRecognizer)  {
        
        let layout = UICollectionViewFlowLayout()
        let vc = profileVC(collectionViewLayout: layout)
        let position = (sender.view?.tag)!
        let member = membersData[position]
        print("print selected member: ", member)
        vc.user = member
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //calling the resfreshing endpoint
    func refresh()  {
        self.getMembers(url: "page=1",churchId: self.userData.churchId)
    }
}
