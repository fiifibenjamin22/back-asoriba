//
//  About.swift
//  asoriba
//
//  Created by Bright Ahedor on 22/12/2016.
//  Copyright © 2016 Asoriba Inc. All rights reserved.
//

import Foundation

class About {
    var id = ""
    var title = ""
    var subTitle = ""
    var content = ""
    var image = ""
    
}
