//
//  ConfigureCell.swift
//  asoriba
//
//  Created by Bright Ahedor on 20/12/2016.
//  Copyright © 2016 Asoriba Inc. All rights reserved.
//

import UIKit


class ConfigureCell {
    

    
    
    static func configureFeedCell(feed:AnyObject,size:Int, contentWidth:CGFloat,lines:Int) -> CGSize {
        
        //moment moment section
        if feed is Feed {
            let currentFeed = feed as! Feed
            var mediaSection = CGFloat(0)
            let userSectionHeight = CGFloat(66)
            var audioSection = CGFloat(0)
            let messageDisplayable = Mics.getAttributeText(feed: currentFeed)
            let contentHeight = Helpers.heightOfLabel(text: (messageDisplayable), fontSize: CGFloat(17), width: contentWidth - CGFloat(16),numberOfLines: lines)
            let countSectionHeight = CGFloat(32)
            let actionHeight = CGFloat(50)
            var micsHeight = CGFloat(25)
            if currentFeed.quotation != nil {
               micsHeight = CGFloat(40)
            }
            if !currentFeed.mediaFile.isEmpty && currentFeed.mediaType == "audio" {
               mediaSection = CGFloat(0)
               audioSection = CGFloat(40)
            }else if (currentFeed.mediaType == "video") {
               mediaSection = CGFloat(300)
            }else if (!currentFeed.image.isEmpty){
               mediaSection = CGFloat(300)
            }else if (currentFeed.gallery.count > 0){
               mediaSection = CGFloat(300)
            }else{
               mediaSection = CGFloat(0)
            }
            
            let height = mediaSection + countSectionHeight + userSectionHeight + actionHeight  + micsHeight + audioSection + contentHeight
            return CGSize(width: size, height: Int(height))
            
        } else {
            let comment = feed as! Comment
            let mics =  CGFloat(50)
            let commentHeight = Helpers.getHeightOfLabel(text: comment.content, fontSize: CGFloat(17), width: UIScreen.main.bounds.width-CGFloat(16),numberOfLines: 5)
            let all = commentHeight + mics
            return CGSize(width: size, height: Int(all))

        }
        
    }

    
    static func getCommmentCellHeight(feed: Comment,screenWidth:CGFloat) -> CGFloat {
        let height =  CGFloat(100);
        return height
    }
 

}
