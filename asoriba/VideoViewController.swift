//
//  VideoViewController.swift
//  asoriba
//
//  Created by Bright Ahedor on 28/12/2016.
//  Copyright © 2016 Asoriba Inc. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation
import Crashlytics


class VideoViewController: UIViewController {
    
    // url for playing video
    var videoUrl: URL!
    var player:AVPlayer!
    


    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tag = "Asoriba - media"
        self.navigationItem.title = tag
    
    
        player = AVPlayer(url: videoUrl!)
        let playerController = AVPlayerViewController()
        
        playerController.player = player
        self.addChildViewController(playerController)
        self.view.addSubview(playerController.view)
        playerController.view.frame = self.view.frame
        
        
        player.play()
        
        
        //tracking analytics
        Answers.logCustomEvent(withName: "VIDEO", customAttributes: nil)

    }

    override func viewWillAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self, selector: Selector(("finishedPlaying:")), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: player)
    }
    
    func finishedPlaying(myNotification:NSNotification) {
        let stopedPlayerItem: AVPlayerItem = myNotification.object as! AVPlayerItem
        stopedPlayerItem.seek(to: kCMTimeZero)
    }


}
