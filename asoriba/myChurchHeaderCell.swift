//
//  profileCell.swift
//  asoriba
//
//  Created by Benjamin Acquah on 26/10/2017.
//  Copyright © 2017 Asoriba Inc. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage

class myChurchHeaderCell: UICollectionViewCell{
    
    let userImageView: UIImageView = {
        let img = UIImageView()
        img.translatesAutoresizingMaskIntoConstraints = false
        img.layer.cornerRadius = 60
        img.layer.masksToBounds = true
        img.contentMode = .scaleAspectFill
        img.image = #imageLiteral(resourceName: "default").withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        return img
    }()
    
    let userBackProfileImageView: UIImageView = {
        let img = UIImageView()
        img.translatesAutoresizingMaskIntoConstraints = false
        img.contentMode = .scaleAspectFill
        img.layer.masksToBounds = true
        img.image = #imageLiteral(resourceName: "default").withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        return img
    }()
    
    let userNameLabel : UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.textColor = .white
        lbl.textAlignment = .center
        lbl.font = UIFont.boldSystemFont(ofSize: 15)
        return lbl
    }()
    
    let userName : UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.textColor = .white
        lbl.textAlignment = .center
        lbl.font = UIFont.boldSystemFont(ofSize: 12)
        return lbl
    }()

    let churchName : UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.textColor = .white
        lbl.textAlignment = .center
        lbl.font = UIFont.boldSystemFont(ofSize: 12)
        return lbl
    }()

    let view : UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor.groupTableViewBackground
        return view
    }()
    
    let callUsBtn : UIButton = {
        let btn = UIButton(type: UIButtonType.roundedRect)
        btn.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        btn.backgroundColor = .white
        return btn
    }()
    
    let findUsBtn : UIButton = {
        let btn = UIButton(type: UIButtonType.roundedRect)
        btn.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        btn.backgroundColor = .white
        return btn
    }()
    
    let chatActionBtn : UIButton = {
        let btn = UIButton(type: UIButtonType.roundedRect)
        btn.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        btn.backgroundColor = .white
        return btn
    }()
    
    let donateBtn : UIButton = {
        let btn = UIButton(type: UIButtonType.roundedRect)
        btn.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        btn.backgroundColor = .white
        return btn
    }()
    
    //stat view
    let statView : UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor.white
        return view
    }()
    
    //stats label
    let followersCountLbl : UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.text = "0"
        lbl.textColor = UIColor(r: 0, g: 165, b: 240)
        lbl.textAlignment = .center
        return lbl
    }()
    
    let followersLbl : UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.text = "Followers"
        lbl.textColor = UIColor(r: 0, g: 165, b: 240)
        lbl.font = UIFont.boldSystemFont(ofSize: 12)
        lbl.textColor = UIColor.lightGray
        lbl.textAlignment = .center
        return lbl
    }()
    
    let groupCountLbl : UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.text = "1"
        lbl.textColor = UIColor(r: 0, g: 165, b: 240)
        lbl.textAlignment = .center
        return lbl
    }()
    
    let groupLbl : UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.text = "Groups"
        lbl.textColor = UIColor(r: 0, g: 165, b: 240)
        lbl.font = UIFont.boldSystemFont(ofSize: 12)
        lbl.textColor = UIColor.lightGray
        lbl.textAlignment = .center
        return lbl
    }()
    
    var currentUser = DataOperation.getUser()
    var api:ApiService!

    var churchItem: Church!{
        didSet {
            self.updateAllData(churchData: churchItem)
        }
    }
    var userData: User! {
        didSet {
            self.updateUI()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        api = ApiService()
        setUpView()
    }
    
    func setUpView(){
        
        addSubview(userBackProfileImageView)
        userBackProfileImageView.addSubview(userImageView)
        userBackProfileImageView.addSubview(userNameLabel)
        userBackProfileImageView.addSubview(userName)
        userBackProfileImageView.addSubview(churchName)
        userBackProfileImageView.addSubview(view)
        userBackProfileImageView.addSubview(statView)
        
        addSubview(followersLbl)
        addSubview(followersCountLbl)
        addSubview(groupLbl)
        addSubview(groupCountLbl)
        
        addSubview(callUsBtn)
        addSubview(findUsBtn)
        addSubview(chatActionBtn)
        addSubview(donateBtn)

        userBackProfileImageView.anchor(topAnchor, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: self.frame.width, heightConstant: self.frame.height)
        
        userImageView.centerXAnchor.constraint(equalTo: userBackProfileImageView.centerXAnchor).isActive = true
        userImageView.topAnchor.constraint(equalTo: userBackProfileImageView.topAnchor, constant: 20).isActive = true
        userImageView.widthAnchor.constraint(equalToConstant: 120).isActive = true
        userImageView.heightAnchor.constraint(equalToConstant: 120).isActive = true
        
        userNameLabel.anchor(userImageView.bottomAnchor, left: userBackProfileImageView.leftAnchor, bottom: nil, right: userBackProfileImageView.rightAnchor, topConstant: 12, leftConstant: 8, bottomConstant: 0, rightConstant: 8, widthConstant: 0, heightConstant: 20)
        
        userName.anchor(userNameLabel.bottomAnchor, left: userNameLabel.leftAnchor, bottom: nil, right: userNameLabel.rightAnchor, topConstant: 8, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 20)
        
        churchName.anchor(userName.bottomAnchor, left: userName.leftAnchor, bottom: nil, right: userName.rightAnchor, topConstant: 8, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 20)

        view.anchor(nil, left: leftAnchor, bottom: statView.topAnchor, right: rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 50)
        
        let width : CGFloat = self.frame.size.width / 4
        
        callUsBtn.anchor(view.topAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: nil, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: width, heightConstant: 0)
        
        findUsBtn.anchor(callUsBtn.topAnchor, left: callUsBtn.rightAnchor, bottom: callUsBtn.bottomAnchor, right: nil, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: width, heightConstant: 0)
        
        chatActionBtn.anchor(findUsBtn.topAnchor, left: findUsBtn.rightAnchor, bottom: findUsBtn.bottomAnchor, right: nil, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: width, heightConstant: 0)
        
        donateBtn.anchor(chatActionBtn.topAnchor, left: chatActionBtn.rightAnchor, bottom: chatActionBtn.bottomAnchor, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        statView.anchor(nil, left: view.leftAnchor, bottom: bottomAnchor, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 50)
        
        let Newwidth : CGFloat = self.frame.width / 2
        
        followersCountLbl.anchor(statView.topAnchor, left: statView.leftAnchor, bottom: statView.bottomAnchor, right: nil, topConstant: 0, leftConstant: 0, bottomConstant: 15, rightConstant: 0, widthConstant: Newwidth, heightConstant: 0)
        
        followersLbl.anchor(followersCountLbl.bottomAnchor, left: followersCountLbl.leftAnchor, bottom: statView.bottomAnchor, right: followersCountLbl.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        

        groupCountLbl.topAnchor.constraint(equalTo: followersCountLbl.topAnchor).isActive = true
        groupCountLbl.leftAnchor.constraint(equalTo: followersCountLbl.rightAnchor).isActive = true
        groupCountLbl.rightAnchor.constraint(equalTo: statView.rightAnchor).isActive = true
        groupCountLbl.bottomAnchor.constraint(equalTo: followersCountLbl.bottomAnchor).isActive = true
        
        groupLbl.anchor(groupCountLbl.bottomAnchor, left: groupCountLbl.leftAnchor, bottom: statView.bottomAnchor, right: groupCountLbl.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
    }
    
    func updateUI(){
        
        backgroundColor = .blue
        
        let path = Bundle.main.path(forResource: "configuration", ofType: "plist")
        let root = NSDictionary(contentsOfFile: path!)
        
        let headerBackground = (root?["headerBackgroundColor"] as! String)
        let headerYellowBackground = (root?["tint_white"] as! String)
        let tint_white = (root?["tint_white"] as! String)
        
        var numberOfFollowers = 0
        var numberOfGroups = 0
        
        if (self.churchItem != nil){
            numberOfFollowers = churchItem.numberOfFollowers
            numberOfGroups = churchItem.numberOfGroups
            
        } else {
            
            numberOfFollowers = userData.numberOfFollowers
            numberOfGroups = userData.numberOfGroups
            

        }
        
        let followsShowable = numberOfFollowers == 1 ? "\(numberOfFollowers)" : "\(numberOfFollowers)";
        let groupsShowable = numberOfGroups == 1 ? "\(numberOfGroups)" : "\(numberOfGroups)";
        
        
        //branch issues must be resolved here//
        
        if userData.churchName.isEmpty {
            self.userNameLabel.text = "----"
            self.churchName.isHidden = true
        }else if churchItem != nil {
            self.churchName.isHidden = false
            self.userName.text = churchItem.branchName
            self.userNameLabel.text = userData.churchName
            
            print("print church details \(userData.churchName) + \(churchItem.branchName)")
            
        }else {
            
            self.userName.text = currentUser.branchName
            self.userNameLabel.text = userData.churchName 
            self.churchName.isHidden = true
            
            print("print church details \(userData.churchName)")
        }
        
        self.followersCountLbl.text = "\(followsShowable)"
        self.groupCountLbl.text = "\(groupsShowable)"
        
        self.chatActionBtn.setTitle("Check In", for: UIControlState.normal)
        self.chatActionBtn.backgroundColor = UIColor().HexToColor(hexString: headerBackground)
        self.chatActionBtn.setTitleColor(UIColor().HexToColor(hexString: headerYellowBackground), for: UIControlState.normal)
        self.chatActionBtn.isEnabled = true
        
        self.callUsBtn.setTitle("Call Us", for: UIControlState.normal)
        self.callUsBtn.backgroundColor = UIColor().HexToColor(hexString: headerBackground)
        self.callUsBtn.setTitleColor(UIColor().HexToColor(hexString: headerYellowBackground), for: UIControlState.normal)
        self.callUsBtn.isEnabled = true
        
        self.findUsBtn.setTitle("Find Us", for: UIControlState.normal)
        self.findUsBtn.backgroundColor = UIColor().HexToColor(hexString: headerBackground)
        self.findUsBtn.setTitleColor(UIColor().HexToColor(hexString: headerYellowBackground), for: UIControlState.normal)
        self.findUsBtn.isEnabled = true
        
        self.donateBtn.setTitle("Donate", for: UIControlState.normal)
        self.donateBtn.backgroundColor = UIColor().HexToColor(hexString: headerBackground)
        self.donateBtn.setTitleColor(UIColor().HexToColor(hexString: headerYellowBackground), for: UIControlState.normal)
        self.donateBtn.isEnabled = true
        
        Alamofire.request((userData.churchBackDrop)).responseImage { response in
            if let image = response.result.value {
                self.userBackProfileImageView.image = image
            }else{
                self.userBackProfileImageView.image = UIImage(named: "default")
            }
        }
        
        //user image
        Alamofire.request((userData.churchLogo)).responseImage { response in
            if let image = response.result.value {
                self.userImageView.image = image
            }else{
                self.userImageView.image = UIImage(named: "default")
            }
        }
        
        self.userImageView.isUserInteractionEnabled = true
        
    }
    
    
    func updateAllData(churchData: Church)  {
        self.userData = User()
        self.userData.churchBackDrop = churchData.image
        self.userData.churchLogo = churchData.churchLogo
        self.userData.numberOfFollowers = churchData.numberOfFollowers
        self.userData.numberOfGroups = churchData.numberOfGroups
        self.userData.churchName = churchData.branchName
        self.userData.churchLogo = churchData.churchLogo
        
        self.updateUI()
    }
        
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

