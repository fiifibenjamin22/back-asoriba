//
//  forumsCell.swift
//  asoriba
//
//  Created by Benjamin Acquah on 06/10/2017.
//  Copyright © 2017 Asoriba Inc. All rights reserved.
//

import UIKit
import LBTAComponents

class forumsCell: UICollectionViewCell {
    
    let backView : UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.shadowColor = UIColor.lightGray.cgColor
        view.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        view.layer.shadowRadius = 2.0
        view.layer.shadowOpacity = 1.0
        view.layer.masksToBounds = false
        view.layer.cornerRadius = 8
        return view
    }()
    
    let titleLabel : UILabel = {
        let lbl = UILabel()
        lbl.text = "Forum Topics Here"
        lbl.numberOfLines = 3
        lbl.font = UIFont.boldSystemFont(ofSize: 17)
        lbl.adjustsFontSizeToFitWidth = true
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let topicDescription : UITextView = {
        let txt = UITextView()
        txt.adjustsFontForContentSizeCategory = true
        txt.font = UIFont.systemFont(ofSize: 14)
        txt.isEditable = false
        txt.textColor = UIColor.darkGray
        txt.contentInset = UIEdgeInsetsMake(5, 0, 0, 0)
        txt.isSelectable = false
        txt.isScrollEnabled = false
        txt.translatesAutoresizingMaskIntoConstraints = false
        return txt
    }()
    
    let notificationIcon : UIImageView = {
        let img = UIImageView()
        img.image = #imageLiteral(resourceName: "notification")
        img.translatesAutoresizingMaskIntoConstraints = false
        return img
    }()
    
    let seperatorLbl : UILabel = {
        let lbl = UILabel()
        lbl.backgroundColor = UIColor.lightGray
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()

    let horizontalSeperatorLbl : UILabel = {
        let lbl = UILabel()
        lbl.backgroundColor = UIColor.lightGray
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()

    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .white
        
        setUpViews()
    }
    
    func setUpViews(){
        
        addSubview(backView)
        backView.addSubview(titleLabel)
        backView.addSubview(topicDescription)
        backView.addSubview(notificationIcon)
        backView.addSubview(seperatorLbl)
        backView.addSubview(horizontalSeperatorLbl)
        
        backView.topAnchor.constraint(equalTo: topAnchor, constant: 5).isActive = true
        backView.leftAnchor.constraint(equalTo: leftAnchor, constant: 8).isActive = true
        backView.rightAnchor.constraint(equalTo: rightAnchor, constant: -8).isActive = true
        backView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -5).isActive = true
        
        titleLabel.topAnchor.constraint(equalTo: backView.topAnchor, constant: 0).isActive = true
        titleLabel.leftAnchor.constraint(equalTo: backView.leftAnchor, constant: 8).isActive = true
        titleLabel.rightAnchor.constraint(equalTo: notificationIcon.leftAnchor, constant: -8).isActive = true
        titleLabel.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        topicDescription.anchor(titleLabel.bottomAnchor, left: titleLabel.leftAnchor, bottom: backView.bottomAnchor, right: backView.rightAnchor, topConstant: 5, leftConstant: 0, bottomConstant: 5, rightConstant: 8, widthConstant: 0, heightConstant: 0)
        
        notificationIcon.anchor(titleLabel.topAnchor, left: nil, bottom: nil, right: backView.rightAnchor, topConstant: 8, leftConstant: 0, bottomConstant: 0, rightConstant: 12, widthConstant: 30, heightConstant: 30)
        
        seperatorLbl.anchor(backView.topAnchor, left: nil, bottom: horizontalSeperatorLbl.topAnchor, right: notificationIcon.leftAnchor, topConstant: 5, leftConstant: 0, bottomConstant: 0, rightConstant: 5, widthConstant: 0.5, heightConstant: 0)
        
        horizontalSeperatorLbl.anchor(titleLabel.bottomAnchor, left: titleLabel.leftAnchor, bottom: nil, right: notificationIcon.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0.5)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
