//
//  Quotation.swift
//  asoriba
//
//  Created by Bright Ahedor on 15/02/2017.
//  Copyright © 2017 Asoriba Inc. All rights reserved.
//

import RealmSwift

class Quotation: Object {
    dynamic var book = ""
    dynamic var chapter = ""
    dynamic var verse = ""
    
}
