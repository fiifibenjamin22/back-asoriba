//
//  AboutDetailViewController.swift
//  asoriba
//
//  Created by Bright Ahedor on 04/05/2017.
//  Copyright © 2017 Asoriba Inc. All rights reserved.
//

import UIKit
import Alamofire

class AboutDetailViewController: UIViewController {

    @IBOutlet weak var contentImageViewConstraint: NSLayoutConstraint!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var contentUIImageView: UIImageView!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var contentLabelConstraint: NSLayoutConstraint!
    @IBOutlet weak var contentViewConstraint: NSLayoutConstraint!
    
    var about: About!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.8)
        self.showAnimate()
        
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.closePopUp(_:)))
        self.view.addGestureRecognizer(tap)
        self.view.isUserInteractionEnabled = true
        
        
        var totalHeight = 400.0
        if self.about.image.isEmpty {
          self.contentUIImageView.isHidden = true
          self.contentImageViewConstraint.constant = CGFloat(0.0)
          totalHeight = totalHeight - 150.0
        } else {
            self.contentUIImageView.isHidden = false
            Alamofire.request((self.about.image)).responseImage { response in
                if let image = response.result.value {
                    self.contentUIImageView.image = image
                } else{
                    self.contentUIImageView.image = UIImage(named: "default")
                }
            }
        }
        
        self.titleLabel.text = about.title
        let content = about.subTitle + "\n" + about.content
        self.descriptionLabel.numberOfLines = 5
        let text = Mics.getAttributeContent(text: content)
        self.contentLabelConstraint.constant = Helpers.heightOfLabel(text: text,fontSize: CGFloat(17), width: UIScreen.main.bounds.width-CGFloat(16),numberOfLines: 5)
        self.descriptionLabel.attributedText = text
     
        
        self.contentViewConstraint.constant = CGFloat(totalHeight)
        
        //gesture section content
        self.contentUIImageView.isUserInteractionEnabled = true
        let tappedImage = UITapGestureRecognizer(target: self, action: #selector(self.showAction(_:)))
        self.contentUIImageView.addGestureRecognizer(tappedImage)
        //end of gesture
        
        
        //gesture section content
        self.descriptionLabel.isUserInteractionEnabled = true
        let tapped = UITapGestureRecognizer(target: self, action: #selector(self.showMoreDescriptionAction(_:)))
        self.descriptionLabel.addGestureRecognizer(tapped)
        //end of gesture
        
    }
    
    func showMoreDescriptionAction(_ sender: UITapGestureRecognizer) {
        ControllerHelpers.showDialog(targetVC: self, title: "...more", message: self.about.content)
    }

    func showAction(_ sender: UITapGestureRecognizer) {
         ControllerHelpers.presentSingleImage(targetVC: self, url: self.about.image)
    }
    
    func endEditing() {
        view.endEditing(true)
    }
    
    
    //close action on the parent view
    func closePopUp(_ sender: UITapGestureRecognizer) {
        self.view.endEditing(true)
        self.removeAnimate()
        self.view.removeFromSuperview()
    }
    
    //close action on the button
    func cancelAction() {
        self.removeAnimate()
        self.view.removeFromSuperview()
    }
    
    
    func showAnimate()
    {
        self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view.alpha = 0.0;
        UIView.animate(withDuration: 0.25, animations: {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        });
    }
    
    func removeAnimate()
    {
        UIView.animate(withDuration: 0.25, animations: {
            self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view.alpha = 0.0;
        }, completion:{(finished : Bool)  in
            if (finished)
            {
                self.view.removeFromSuperview()
            }
        });
    }
    
    @IBAction func cancelButtonClick(_ sender: UIButton) {
        self.cancelAction()
    }
    
}
