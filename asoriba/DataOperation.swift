//
//  DataOperation.swift
//  asoriba
//
//  Created by Bright Ahedor on 14/12/2016.
//  Copyright © 2016 Asoriba Inc. All rights reserved.
//

import RealmSwift
import Realm

class DataOperation {

    //same saving but different approach
    static func saveNewUser(id:String,token:String,email:String,number:String,fName:String,lName:String,oName:String,churchId:String,churchName:String,branchName:String,numberOfGroups:Int,numberOfFollowers:Int,numberOfFollowings: Int,avatar:String,churchLogo:String,churchBackDrop:String,date:String,gender:String) {
        
        let thisUser = User()
        thisUser.id = id
        thisUser.token = token
        thisUser.email = email
        thisUser.number = number
        thisUser.firstName = fName
        thisUser.lastName = lName
        thisUser.churchName = churchName
        thisUser.avatar = avatar
        
        thisUser.churchId = churchId
        thisUser.branchName = branchName
        
        thisUser.otherName = oName
        thisUser.churchLogo = churchLogo
        thisUser.churchBackDrop = churchBackDrop
        thisUser.numberOfGroups = numberOfGroups
        thisUser.numberOfFollowers = numberOfFollowers
        thisUser.numberOfFollowings = numberOfFollowings
        thisUser.dateOfBirth = date
        thisUser.gender = gender
        
        let realm = try! Realm()
        try! realm.write() {
            realm.add(thisUser, update: true)
        }
        
    }
    
    //same saving gateways
    static func saveGateWays(data: PaymentGateWays) {
        let realm = try! Realm()
        try! realm.write() {
            realm.add(data,update: true)
        }
        
    }

    //get user saved into the db
    static func getUser() -> User{
     let realm = try! Realm()
     let user = realm.objects(User.self).first
     return user!
    }
    
    //get user saved into the db
    static func userIsEmpty() -> Bool{
        let realm = try! Realm()
        let user = realm.objects(User.self).count
        return user == 0
    }
    
    //get all giving items
    static func getAllGateWays() -> [PaymentGateWays] {
      var payments = [PaymentGateWays]()
      let realm = try! Realm()
      let results = realm.objects(PaymentGateWays.self)
      payments = Array(results)
      return payments
    }
    
    //get user saved into the db
    static func deleteUser(){
        let realm = try! Realm()
        let thisUser = realm.objects(User.self).first
        try! realm.write() {
            if(thisUser != nil){
                realm.delete(thisUser!)
            }
        }
    }
    
    static func updateUserChurch(id:String,churchId:String,churchName:String,churchLogo:String,backDrop:String){
        let realm = try! Realm()
        try! realm.write {
            realm.create(User.self, value: ["id": id, "churchId":churchId,"churchName": churchName,"churchLogo":churchLogo,"churchBackDrop":backDrop], update: true)
        }
    }

    static func updateUserChurchNumbers(id:String,numberOfFollowers:Int,numberOfGroups:Int){
        let realm = try! Realm()
        try! realm.write {
            realm.create(User.self, value: ["id": id, "numberOfFollowers":numberOfFollowers,"numberOfGroups": numberOfGroups], update: true)
        }
    }
    
    
    static func updateChatUsers(user2_id : String, firstName : String, lastName : String, dateCreated : Double, profileImage : String, lastMessage: String, userName: String, numberOfFollowers: Int, numberOfGroups: Int){
        
        let thisChatUser = ChatUser()
        thisChatUser.user2_id = user2_id
        thisChatUser.user2_firstname = firstName
        thisChatUser.user2_lastname = lastName
        thisChatUser.userName = userName
        thisChatUser.dateCreated = dateCreated
        thisChatUser.profileImage = profileImage
        thisChatUser.lastmessage = lastMessage
        thisChatUser.numberOfFollowers = numberOfFollowers
        thisChatUser.numberOfGroups = numberOfGroups
        
        let realm = try! Realm()
        try! realm.write {
            realm.add(thisChatUser, update: true)
        }
    }
    
    //get all giving items
    static func getChatUserData() -> [ChatUser] {
        var users = [ChatUser]()
        let realm = try! Realm()
        let results = realm.objects(ChatUser.self)
        .sorted(byKeyPath: "dateCreated", ascending: false)
        users = Array(results)
        return users
    }
}
