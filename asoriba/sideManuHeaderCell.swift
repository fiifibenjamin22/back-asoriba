//
//  sideManuHeaderCell.swift
//  asoriba
//
//  Created by Benjamin Acquah on 12/09/2017.
//  Copyright © 2017 Asoriba Inc. All rights reserved.
//

import UIKit
import LBTAComponents

class sideManuHeaderCell: UICollectionViewCell {
    
//    let BackDropImage : UIImageView = {
//        let img = UIImageView()
//        img.image = UIImage(named: "try")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
//        img.contentMode = .scaleAspectFill
//        return img
//    }()
//
//    let userProfileImage : UIImageView = {
//        let img = UIImageView()
//        img.image = UIImage(named: "try")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
//        img.contentMode = .scaleAspectFill
//        img.layer.cornerRadius = 60
//        img.layer.masksToBounds = true
//        img.translatesAutoresizingMaskIntoConstraints = false
//        return img
//    }()
//
//    var blurEffectView : UIVisualEffectView = {
//        let blurry = UIVisualEffectView()
//        return blurry
//    }()
//
//    let userNameLbl: UILabel = {
//        let lbl = UILabel()
//        lbl.text = "International Central Gospel Church"
//        lbl.textColor = .white
//        lbl.font = UIFont.boldSystemFont(ofSize: 15)
//        lbl.adjustsFontSizeToFitWidth = true
//        return lbl
//    }()
//
//    let captionLbl: UILabel = {
//        let lbl = UILabel()
//        lbl.text = "ICGC Christ Temple"
//        lbl.textColor = .white
//        lbl.font = UIFont.systemFont(ofSize: 16)
//        lbl.adjustsFontSizeToFitWidth = true
//        return lbl
//    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        
//        setUpViews()

    }
    
    func setUpViews(){
        
//        addSubview(BackDropImage)
//        
//        BackDropImage.anchor(topAnchor, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
//        
//        //apply blur effect
//        let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.light)
//        blurEffectView = UIVisualEffectView(effect: blurEffect)
//        BackDropImage.addSubview(blurEffectView)
//        
//        blurEffectView.anchor(BackDropImage.topAnchor, left: BackDropImage.leftAnchor, bottom: BackDropImage.bottomAnchor, right: BackDropImage.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
//        
//        BackDropImage.addSubview(userProfileImage)
//        userProfileImage.anchor(blurEffectView.topAnchor, left: blurEffectView.leftAnchor, bottom: nil, right: nil, topConstant: 20, leftConstant: 8, bottomConstant: 0, rightConstant: 0, widthConstant: 120, heightConstant: 120)
//        
//        blurEffectView.addSubview(userNameLbl)
//        userNameLbl.anchor(userProfileImage.bottomAnchor, left: userProfileImage.leftAnchor, bottom: nil, right: rightAnchor, topConstant: 12, leftConstant: 0, bottomConstant: 0, rightConstant: 8, widthConstant: 0, heightConstant: 20)
//        
//        blurEffectView.addSubview(captionLbl)
//        captionLbl.anchor(userNameLbl.bottomAnchor, left: userNameLbl.leftAnchor, bottom: nil, right: userNameLbl.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 20)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
