//
//  ControllerHelpers.swift
//  asoriba
//
//  Created by Bright Ahedor on 05/01/2017.
//  Copyright © 2017 Asoriba Inc. All rights reserved.
//

import UIKit
import Alamofire
import Photos
import SKPhotoBrowser
import MediaPlayer
import Jukebox
import MobilePlayer
import PopupDialog

class ControllerHelpers {

    //MARK: -- show dialog
    static func showDialog(targetVC: UIViewController,title:String,message:String)  {
        let popup = PopupDialog(title: title, message: message, image: nil)
        let buttonOne = CancelButton(title: "CANCEL") {
            print("You canceled the car dialog.")
        }
        
        popup.addButtons([buttonOne])
        targetVC.present(popup, animated: true, completion: nil)
    }
    
    //MARK: -- show dialog
    static func showMessage(targetVC: UIViewController,title:String,message:String)  {
        let popup = PopupDialog(title: title, message: message, image: nil)
        let buttonOne = CancelButton(title: "OK") {
            print("You canceled the Meesage dialog.")
        }
        
        popup.addButtons([buttonOne])
        targetVC.present(popup, animated: true, completion: nil)
    }
  
    //MARK: -- present video player
    static func processVideoPlayer(targetVC: UIViewController,feed:Feed) {
        if (feed.medialUrl.isEmpty){
            //start video player
            startVideoPlayer(targetVC: targetVC,feed: feed)
        }else {
            //start youtube
            startYoutubeVideoPlayer(targetVC: targetVC,videoId: feed.medialUrl)
        }
    }
    
    

    //start video player
   static func startVideoPlayer(targetVC: UIViewController,feed:Feed) {
        let videoURL = URL(string: feed.mediaFile)!
        let playerVC = MobilePlayerViewController(contentURL:videoURL)
        playerVC.title = "Video - \(feed.title)"
        playerVC.activityItems = [videoURL] // Check the documentation for more information.
        targetVC.present(playerVC, animated: true, completion:nil)
    }


    //start video player
    static func startYoutubeVideoPlayer(targetVC: UIViewController,videoId:String) {
        let vc = UIStoryboard(name:"Main", bundle:nil).instantiateViewController(withIdentifier: AllKeys.youtubePlayer) as! YoutubeVideoPlayer
        let videoKey = videoId.substring(from:videoId.index(videoId.endIndex, offsetBy: AllKeys.youtubeVideoIdLength))
        vc.videoId = videoKey
        targetVC.navigationController?.pushViewController(vc, animated: true)
    }

    //MARK: sharing -- present sharing screen
    static func presentSharer(targetVC: UIViewController,feed:Feed){
        let api = ApiService()
        api.addShare(item: feed)
        
        var sharingContent = [AnyObject]()
        var title = ""
        var message = ""
        var image = ""
        let staticText = " download app from: https://itunes.apple.com/gh/app/asoriba/id1198146836?mt=8"
        
        if (feed.title != "" && feed.content == ""){
            title = feed.title
        }else if (feed.title != "" && feed.content != ""){
            message = feed.title + "\n\n" + feed.content
        }else{
            message = feed.content
        }
        sharingContent.append(message as AnyObject)
        sharingContent.append(title as AnyObject)
        
        if (!feed.image.isEmpty || feed.gallery.count > 0){
            
            if (!feed.image.isEmpty){
                image = feed.image
                title = "\(feed.title) \n\n"
                message = feed.content
            }else {
                image = feed.gallery[0].photoUrl
            }
            Alamofire.request(image).responseImage { response in
                if let image = response.result.value {
                    sharingContent.append(image as AnyObject)
                    sharingContent.append(title as AnyObject)
                    sharingContent.append(message as AnyObject)
                    
                }
            }
            
            UIPasteboard.general.string = "\(title)\n\(message)\n\(staticText)"
            
            let vc = UIActivityViewController(activityItems: [title,message,image], applicationActivities: [])
            
            if let popOverController = vc.popoverPresentationController{
                popOverController.sourceView = targetVC.view
                popOverController.sourceRect = targetVC.view.bounds
            }
            targetVC.present(vc, animated: true, completion: nil)
            
        }else{
         
            let vc = UIActivityViewController(activityItems: [image,title,message], applicationActivities: [])
            UIPasteboard.general.string = "\(title)\n\(message)\n\(staticText)"

            if let popOverController = vc.popoverPresentationController{
                popOverController.sourceView = targetVC.view
                popOverController.sourceRect = targetVC.view.bounds
            }
            
            targetVC.present(vc, animated: true, completion: nil)

        }
    }
    
    static func presentImage(targetVC: UIViewController,feed:Feed)  {
        SKPhotoBrowserOptions.backgroundColor = Mics.hexStringToUIColor(hex: AllKeys.primaryHexCode)
        SKPhotoBrowserOptions.textAndIconColor = UIColor.white
        SKPhotoBrowserOptions.toolbarTextShadowColor = UIColor.clear
        SKPhotoBrowserOptions.toolbarFont = UIFont(name: "Futura", size: 16.0)
        SKPhotoBrowserOptions.captionFont = UIFont(name: "Helvetica", size: 18.0)!
        var images = [SKPhoto]()
        if (feed.gallery.count > 1){
            for image in feed.gallery {
                let photo = SKPhoto.photoWithImageURL(image.photoUrl)
                photo.shouldCachePhotoURLImage = true
                images.append(photo)
            }
        }else {
            let photo = SKPhoto.photoWithImageURL(feed.image)
            photo.shouldCachePhotoURLImage = true
            images.append(photo)
        }
        let browser = SKPhotoBrowser(photos: images)
        browser.initializePageIndex(0)
        targetVC.present(browser, animated: true, completion: {})
    }
    
    static func presentSingleImage(targetVC: UIViewController,url:String)  {
        SKPhotoBrowserOptions.backgroundColor = Mics.hexStringToUIColor(hex: AllKeys.primaryHexCode)
        SKPhotoBrowserOptions.textAndIconColor = UIColor.white
        SKPhotoBrowserOptions.toolbarTextShadowColor = UIColor.clear
        SKPhotoBrowserOptions.toolbarFont = UIFont(name: "Futura", size: 16.0)
        SKPhotoBrowserOptions.captionFont = UIFont(name: "Helvetica", size: 18.0)!
        var images = [SKPhoto]()
        
        let photo = SKPhoto.photoWithImageURL(url)
        photo.shouldCachePhotoURLImage = true
        images.append(photo)
        
        let browser = SKPhotoBrowser(photos: images)
        browser.initializePageIndex(0)
        targetVC.present(browser, animated: true, completion: {})
    }
    
    //start video player
    static func startPerformers(targetVC: UIViewController,feedId:String,isAmeners:Bool) {
        let vc = UIStoryboard(name:AllKeys.mainStoryBoard, bundle:nil).instantiateViewController(withIdentifier: AllKeys.actionPerformersController) as! ActionPerformersController
        vc.contentId = feedId
        vc.isAmeners = isAmeners
        targetVC.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    //start video player
    static func startQuote(targetVC: UIViewController,quote:Quotation) {
        let vc = UIStoryboard(name:AllKeys.mainStoryBoard, bundle:nil).instantiateViewController(withIdentifier: AllKeys.bibleVerseViewController) as! BibleVerseViewController
        vc.checkOutVerse = quote
        targetVC.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    //start video player
    static func startDetail(targetVC: UIViewController,feed:Feed) {
        let vc = UIStoryboard(name:AllKeys.mainStoryBoard, bundle:nil).instantiateViewController(withIdentifier: AllKeys.feedDetailController) as! FeedDetailController
        vc.feed = feed
        targetVC.navigationController?.pushViewController(vc, animated: true)
    }
    

    static func roundButton(cornerRadius:CGFloat,button:UIButton){
        
        let borderAlpha : CGFloat = 0.7
        
        // button.frame = CGRect(x: CGFloat(100), y: CGFloat(100), width: CGFloat(200), height: CGFloat(40))
        
        button.setTitleColor(UIColor.gray, for: UIControlState.normal)
        button.backgroundColor = UIColor.clear
        button.layer.borderWidth = 1.0
        button.layer.borderColor = UIColor.lightGray.cgColor
        button.layer.cornerRadius = cornerRadius
    }
    
    
}
