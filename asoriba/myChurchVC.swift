//
//  profileVC.swift
//  asoriba
//
//  Created by Benjamin Acquah on 26/10/2017.
//  Copyright © 2017 Asoriba Inc. All rights reserved.
//

import UIKit
import LBTAComponents
import Alamofire
import AlamofireImage
import SKPhotoBrowser
import Jukebox
import SConnection
import SafariServices
import MobilePlayer
import MediaPlayer
import Toaster


class myChurchVC: UICollectionViewController, UICollectionViewDelegateFlowLayout,FeedServiceProtocol, AboutServiceProtocol {
    
    var titleText = ""
    var api:ApiService!
    var churchItem: Church!
    var membersData = [Member]()
    var memberFollowingData = [Member]()
    var feedsData = [Feed]()
    var feedArray = [Feed]()
    var loadedPages = [String]()
    var feedNextUrl = ""
    var nextUrl = ""
    var followersNextUrl = ""
    var followingNextUrl = ""
    var requestId = ""
    let currentUser = DataOperation.getUser()
    var jukebox : Jukebox!
    var currentPlaying: Feed!
    let numberOfItemsPerRow : CGFloat = 1.0
    var isRefreshing = false
    var churchTitle = ""
    var text = ""
    var searchActive : Bool = false
    var searchUrl = ""
    var isMyChurch = true
    var startSearchProgress = false
    let screenWidth = UIScreen.main.bounds.width
    var refreshControl = UIRefreshControl();
    var activityViewIndicator:UIActivityIndicatorView = UIActivityIndicatorView()
    var header = MyChurchTableHeader()

    
    @IBOutlet weak var playerSectionUIView: UIView!
    @IBOutlet weak var playerSectionLabel: UILabel!
    @IBOutlet weak var playerSectionProgressView: UIProgressView!
    @IBOutlet weak var playerSectionImageView: UIImageView!
    
    @IBAction func playerSectionStopPlayingButton(_ sender: Any) {
        jukebox.stop()
        self.clearPlayerUI()
    }
    @IBOutlet weak var playerSectionButton: UIButton!
    
    @IBAction func searchClickMenuBarItem(_ sender: Any) {
        print("Search action here")
    }
    
    @IBAction func accountClickMenuBarItem(_ sender: Any) {
        print("Accout interaction")
    }
    
    var phoneNumber1 = "+233302688001"
    var phoneNumber2 = "+233302688002"
    var phoneNumber3 = "+233302688003"
    var phoneNumber4 = "+233302688004"
    var phoneNumber5 = "+233302688000"
    var phoneNumber6 = "+233243392422"
    var phoneNumber7 = "+233243392411"
    
    var user:Member!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        requestId  = ""
        let tag = currentUser.branchName
        
        self.navigationItem.title = tag
        self.collectionView?.backgroundColor = .white
        //register cell
        self.collectionView?.register(UINib(nibName: CellIdentifiers.feedCollectionViewCell.rawValue, bundle: nil), forCellWithReuseIdentifier: CellIdentifiers.feedCollectionViewCell.rawValue)
        //end section here

        self.collectionView?.register(myChurchHeaderCell.self, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: profileHeaderID)
    
        setMenuItems()
        api = ApiService()
        self.api.feedDelegate = self
        self.api.aboutDelegate = self

        print("print \(self.currentUser.churchId)")
        
        //chech for network availiability
        if (SConnection.isConnectedToNetwork()){
            
            //call method
            self.getData(page: "?page=1", memberID: self.currentUser.churchId)
            api.getChurchInfo(branchId: currentUser.churchId)

        }else{
            ControllerHelpers.showDialog(targetVC: self, title: "Ooops😭", message: "Your Internet connection is down")
        }

    }
        
    func startProgressBar()  {
        activityViewIndicator.center = self.view.center
        activityViewIndicator.hidesWhenStopped = true
        activityViewIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray
        self.view.addSubview(activityViewIndicator)
        activityViewIndicator.startAnimating()
    }
    
    func startRefreshData()  {
        let url = "page=1"
        loadedPages.append(url)
        isRefreshing = true
        self.api.getFeed(url: "users/get_content/\(url)&page_size=10&feed_type=all&member_id=\(self.currentUser.churchId)&content_category=all&multimedia_type=all&keyword")
    }
    
    func getData(page: String, memberID: String)  {
        loadedPages.append(page)
        self.api.getFeed(url: "users/get_content/\(page)&page_size=10&feed_type=all&member_id=\(memberID)&content_category=all&multimedia_type=all&keyword")
    }

    
    func didReceiveSuccess(results: [Feed]) {
        self.refreshControl.endRefreshing()
        if (isRefreshing == true){
            self.feedArray.removeAll()
        }
        self.isRefreshing = false
        self.activityViewIndicator.stopAnimating()
        feedArray.append(contentsOf: results)
        //print("print all data inside: ", results)
        self.collectionView?.reloadData()
    }
    
    
    func didReceiveDetail(results: String) {
        self.refreshControl.endRefreshing()
        self.activityViewIndicator.stopAnimating()
    }
    
    
    func didReceiveNextUrl(results: String) {
        self.refreshControl.endRefreshing()
        self.nextUrl = results
    }

    func didReceiveError(results: String) {
        self.refreshControl.endRefreshing()
        self.activityViewIndicator.stopAnimating()
    }

    //MARK: - About protocol implementation
    func didReceiveAboutError(result: String) {
        
    }
    
    func didReceiveAboutInfo(result: Church) {
        //update church info
        print("print result: \(result)")
        DataOperation.updateUserChurchNumbers(id: self.currentUser.id, numberOfFollowers: result.numberOfFollowers, numberOfGroups: result.numberOfGroups)
        //update the header stuffs
        header.churchItem = result
    }
    
    func didReceiveAbouts(results: [About]) {
        
    }

    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return feedArray.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CellIdentifiers.feedCollectionViewCell.rawValue, for: indexPath) as! FeedCollectionViewCell
        cell.backgroundColor = UIColor.white
        cell.feed = feedArray[indexPath.row]
        cell.backgroundColor = UIColor.white
        cell.feedContentLabel.textColor = UIColor.lightGray
        cell.feedContentLabel.font = UIFont.systemFont(ofSize: 14)
        
        cell.amenButton.tag = indexPath.row
        cell.amenButton.addTarget(self, action: #selector(self.amenButton(_:)), for: .touchUpInside)
        cell.shareButton.tag = indexPath.row
        cell.shareButton.addTarget(self, action: #selector(self.shareButton(_:)), for: .touchUpInside)
        cell.commentButton.tag = indexPath.row
        cell.commentButton.addTarget(self, action: #selector(self.startComment(_:)), for: .touchUpInside)
        
        cell.audioPlayButton.tag = indexPath.row
        cell.audioPlayButton.addTarget(self, action: #selector(self.playAudioCommand(_:)), for: .touchUpInside)
        
        //gesture section
        cell.feedContentLabel.isUserInteractionEnabled = true
        cell.feedContentLabel.tag = indexPath.row
        let tapped = UITapGestureRecognizer(target: self, action: #selector(self.tappedContent(_:)))
        tapped.cancelsTouchesInView = false
        tapped.numberOfTapsRequired = 1
        cell.feedContentLabel.addGestureRecognizer(tapped)
        
        //extracting urls from feed content label
        cell.feedContentLabel.enabledTypes = [.url]
        cell.feedContentLabel.URLColor = UIColor(red: 18 / 255, green: 165 / 255, blue: 244 / 255, alpha: 1.0)
        cell.feedContentLabel.URLSelectedColor = UIColor.darkText
        cell.feedContentLabel.font = UIFont.systemFont(ofSize: 16)
        cell.feedContentLabel.textColor = UIColor.black
        
        let webStringProtocol = ""
        let input = cell.feedContentLabel.text
        let detector = try! NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue)
        let matches = detector.matches(in: input!, options: [], range: NSRange(location: 0, length: (input?.utf16.count)!))
        
        for match in matches{
            
            let webUrl = (input! as NSString).substring(with: match.range)
            print("link inside post string: \(webStringProtocol)\(webUrl)")
            let combined = URL(string:"\(webStringProtocol)\(webUrl)")
            
            cell.feedContentLabel.handleURLTap({ (webUrl) in
                
                let svc = SFSafariViewController(url: combined!)
                self.present(svc, animated: true, completion: nil)
                
            })
        }
        
        //end of gesture
        
        //gesture account
        let tappedUser = UITapGestureRecognizer(target: self, action: #selector(self.tappedUserLabel(_:)))
        cell.userNameLabel!.isUserInteractionEnabled = true
        cell.userNameLabel!.tag = indexPath.row
        tappedUser.cancelsTouchesInView = false
        tappedUser.numberOfTapsRequired = 1
        cell.userNameLabel!.addGestureRecognizer(tappedUser)
        //end user gesture
        
        //gesture account
        let tappedUserImage = UITapGestureRecognizer(target: self, action: #selector(self.tappedUserLabel(_:)))
        cell.userImageView!.isUserInteractionEnabled = true
        cell.userImageView!.tag = indexPath.row
        tappedUserImage.cancelsTouchesInView = false
        tappedUserImage.numberOfTapsRequired = 1
        cell.userImageView!.addGestureRecognizer(tappedUserImage)
        //end user gesture
        
        //gesture section ameners
        cell.amenCountLabel!.isUserInteractionEnabled = true
        cell.amenCountLabel!.tag = indexPath.row
        let tappedAmenCount = UITapGestureRecognizer(target: self, action: #selector(self.tapPerfomers(_:)))
        tappedAmenCount.cancelsTouchesInView = false
        tappedAmenCount.numberOfTapsRequired = 1
        cell.amenCountLabel!.addGestureRecognizer(tappedAmenCount)
        //end of gesture
        
        //gesture section sharers
        cell.shareCountLabel!.isUserInteractionEnabled = true
        cell.shareCountLabel!.tag = indexPath.row
        let tappedShareCount = UITapGestureRecognizer(target: self, action: #selector(self.tapSharePerfomers(_:)))
        tappedShareCount.cancelsTouchesInView = false
        tappedShareCount.numberOfTapsRequired = 1
        cell.shareCountLabel!.addGestureRecognizer(tappedShareCount)
        //end of ameners gesture
        
        
        //gesture section sharers
        cell.mediaSectionUIView!.isUserInteractionEnabled = true
        cell.mediaSectionUIView!.tag = indexPath.row
        let tappedMedia = UITapGestureRecognizer(target: self, action: #selector(self.tappedMediaSection(_:)))
        tappedMedia.cancelsTouchesInView = false
        tappedMedia.numberOfTapsRequired = 1
        cell.mediaSectionUIView!.addGestureRecognizer(tappedMedia)
        //end of ameners gesture
        
        //gesture for quotation
        cell.quotationLabel!.isUserInteractionEnabled = true
        cell.quotationLabel!.tag = indexPath.row
        let tappedQuote = UITapGestureRecognizer(target: self, action: #selector(self.tapQuote(_:)))
        tappedQuote.cancelsTouchesInView = false
        tappedQuote.numberOfTapsRequired = 1
        cell.quotationLabel!.addGestureRecognizer(tappedQuote)
        //end of gesture
        
        return cell
    }
    
    //MARK: tap a quotation
    func tapQuote(_ sender: UITapGestureRecognizer)
    {
        let feed = feedArray[(sender.view?.tag)!]
        ControllerHelpers.startQuote(targetVC: self, quote: feed.quotation!)
    }

    
    func amenButton(_ sender: UIButton) {
        titleText = sender.currentTitle!
        
        if (titleText == "Amen") {
            
            sender.setTitle("Amened", for: UIControlState.normal)
            let feed = self.feedArray[sender.tag]
            feed.hasAmened = true
            feed.numberOfAmens = feed.numberOfAmens + 1
            self.collectionView?.reloadData()
            self.api.sayAmen(item: feed)
            
        }else{
            sender.setTitle("Amen", for: UIControlState.normal)
            let feed = self.feedArray[sender.tag]
            feed.hasAmened = false
            feed.numberOfAmens = feed.numberOfAmens - 1
            self.collectionView?.reloadData()
            self.api.sayAmen(item: feed)
        }
    }
    
    //send the share signal
    func shareButton(_ sender: UIButton) {
        let feed = self.feedArray[sender.tag]
        
        //let copiedText = "\(feed.title)\n\(feed.content)"
        
        let myAlert = UIAlertController(title: "Notice", message: "If you are sharing on facebook, Text has been coppied to clipboard so paste it and share", preferredStyle: UIAlertControllerStyle.alert)
        let ok = UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel) { (_) in
            ControllerHelpers.presentSharer(targetVC: self, feed: feed)
        }
        
        myAlert.addAction(ok)
        self.present(myAlert, animated: true, completion: nil)
        
    }
    
    //start comment here
    func startComment(_ sender: UIButton) {
        let feed = self.feedArray[sender.tag]
        ControllerHelpers.startDetail(targetVC: self,feed:feed)
        
    }
    
    //play audio here
    func playAudioCommand(_ sender: UIButton) {
        if let item = self.currentPlaying {
            self.jukebox.removeItems(withURL: URL(string: (item.mediaFile))!)
            
        }
        self.currentPlaying = self.feedArray[sender.tag]
//        self.playerSectionUIView.isHidden = false
//        self.playerSectionLabel.text = self.currentPlaying.content
//        self.updateUI()
        
        self.jukebox.append(item: JukeboxItem (URL: URL(string: self.currentPlaying.mediaFile)!), loadingAssets: true)
        jukebox?.stop()
        jukebox?.play()
        
    }
    
    
    //MARK: -- Handling gestures
    func tappedMediaSection(_ sender: UITapGestureRecognizer)
    {
        let currentFeed = feedArray[(sender.view?.tag)!]
        if (currentFeed.mediaType == "video") {
            ControllerHelpers.processVideoPlayer(targetVC: self, feed: currentFeed)
            
            
        }else  {
            ControllerHelpers.presentImage(targetVC: self, feed: currentFeed)
        }
        
    }
    
    
    //MARK: now to user
    func tappedUserLabel(_ sender: UITapGestureRecognizer)
    {
        let feed = feedArray[(sender.view?.tag)!]
        
        //is user
        if (feed.member?.isMobile)!{
            let layout = UICollectionViewFlowLayout()
            let vc = profileVC(collectionViewLayout: layout)
            //let vc = UIStoryboard(name:AllKeys.mainStoryBoard, bundle:nil).instantiateViewController(withIdentifier: AllKeys.userProfileController) as! ProfileViewController
            let member = feed.member
            vc.user = member!
            self.navigationController?.pushViewController(vc, animated: true)
            
            //is church not user
        } else {
            
//            let member = feed.member
//            let church = Church()
//            church.id = (member?.id)!
//            church.name = (member?.firstName)!
//            church.branchName = (member?.lastName)!
//            church.churchLogo = (member?.avatar)!
//
//            let churchController = UIStoryboard(name:AllKeys.mainStoryBoard, bundle:nil).instantiateViewController(withIdentifier: AllKeys.churchProfileController) as! ChurchProfileController
//            churchController.isMyChurch = false
//            churchController.churchItem = church
//
//            self.navigationController?.pushViewController(churchController, animated: true)
            
        }
        
    }
    
    
    //MARK: start detail here
    func tappedContent(_ sender: UITapGestureRecognizer)
    {
        let feed = feedArray[(sender.view?.tag)!]
        ControllerHelpers.startDetail(targetVC: self,feed:feed)
        
    }
    
    //MARK: set performers gesture
    func tapPerfomers(_ sender: UITapGestureRecognizer)
    {
        let feed = feedArray[(sender.view?.tag)!]
        ControllerHelpers.startPerformers(targetVC: self, feedId: feed.id,isAmeners: true)
        
    }
    
    //MARK: set performers gesture
    func tapSharePerfomers(_ sender: UITapGestureRecognizer)
    {
        let feed = feedArray[(sender.view?.tag)!]
        ControllerHelpers.startPerformers(targetVC: self, feedId: feed.id,isAmeners: false)
    }
    
    
    //MARK:- jute box section
    
    func jukeboxStateDidChange(_ jukebox: Jukebox) {
        if jukebox.state == .ready {
            self.playerSectionButton.setTitle("Ready", for: .normal)
        } else if jukebox.state == .loading  {
            self.playerSectionButton.setTitle("Preparing", for: .normal)
        } else if  (jukebox.state == .failed ) {
            self.playerSectionButton.setTitle("Play", for: .normal)
        }else if  (jukebox.state == .playing) {
            self.playerSectionButton.setTitle("Stop", for: .normal)
        }else {
            self.playerSectionButton.setTitle("Playing", for: .normal)
        }
    }
    
    func jukeboxDidLoadItem(_ jukebox: Jukebox, item: JukeboxItem) {
        print("Jukebox did load: \(item.URL.lastPathComponent)")
    }
    
    func jukeboxPlaybackProgressDidChange(_ jukebox: Jukebox) {
        if let currentTime = jukebox.currentItem?.currentTime, let duration = jukebox.currentItem?.meta.duration {
            let value = Float(currentTime / duration)
            if (value >= 1){
                self.clearPlayerUI()
            } else {
                self.playerSectionProgressView.setProgress(value, animated: true)
            }
            
        }
        
    }
    
    func clearPlayerUI() {
        self.playerSectionProgressView.setProgress(0, animated: true)
        self.playerSectionLabel.text = ""
        self.playerSectionUIView.isHidden = true
    }
    
    func populateLabelWithTime(time: Double) -> String {
        let minutes = Int(time / 60)
        let seconds = Int(time) - minutes * 60
        
        return  String(format: "%02d", minutes) + ":" + String(format: "%02d", seconds)
    }
    
    func jukeboxDidUpdateMetadata(_ jukebox: Jukebox, forItem: JukeboxItem) {
        print("Item updated:\n\(forItem)")
    }
    
    
    override func remoteControlReceived(with event: UIEvent?) {
        if event?.type == .remoteControl {
            switch event!.subtype {
            case .remoteControlPlay :
                jukebox.play()
            case .remoteControlPause :
                jukebox.pause()
            case .remoteControlNextTrack :
                jukebox.playNext()
            case .remoteControlPreviousTrack:
                jukebox.playPrevious()
            case .remoteControlTogglePlayPause:
                if jukebox.state == .playing {
                    jukebox.pause()
                } else {
                    jukebox.play()
                }
            default:
                break
            }
        }
    }
    
    //END, gestures
    
    //MARK: - scroll handling section
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let currentOffset = scrollView.contentOffset.y
        let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height
        
        if (maximumOffset - currentOffset) <= 400 {
            if !(self.nextUrl.isEmpty) && !self.loadedPages.contains(self.nextUrl) {
                self.getData(page: self.nextUrl, memberID: self.requestId)
                
            }
        }
    }
    
    func didReceivePostResult(result: Feed) {
        feedArray.insert(result, at: 0)
        //self.feedCollectionView.reloadData()
    }
    
    func didReceivePostError(result: String) {
        print("Posted Erorr \(result)")
    }
    
    func updateUI() {
        if  !((self.currentPlaying.member?.avatar.isEmpty)!) {
            self.playerSectionImageView.af_setImage(
                withURL: URL(string: (self.currentPlaying.member?.avatar)!)!,
                placeholderImage: UIImage(named: "default"),
                imageTransition: .crossDissolve(0.2)
            )
        }else
        {
            self.playerSectionImageView.image = UIImage(named: "default")
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, shouldHighlightItemAt indexPath: IndexPath) -> Bool {
        return false
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return CGFloat(AllKeys.cellSpacing)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return CGFloat(AllKeys.cellSpacing)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: CGFloat(AllKeys.cellSpacing), left: 0.0, bottom: 0.0, right: 0.0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
        let contentWidth = collectionView.bounds.width
        let totalSpace = flowLayout.sectionInset.left
            + flowLayout.sectionInset.right
            + (flowLayout.minimumInteritemSpacing * CGFloat(numberOfItemsPerRow - 1))
        let size = Int((contentWidth - totalSpace) / CGFloat(numberOfItemsPerRow))
        let feed = self.feedArray[indexPath.row]
        return ConfigureCell.configureFeedCell(feed:feed,size:size,contentWidth:contentWidth,lines:5)

        
    }
    
    
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let header = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionHeader, withReuseIdentifier: profileHeaderID, for: indexPath) as! myChurchHeaderCell
        
        header.userData = self.currentUser
        header.userNameLabel.text = currentUser.churchName
        header.backgroundColor = .clear
        header.chatActionBtn.addTarget(self, action: #selector(startCheckIn(_:)), for: UIControlEvents.touchUpInside)
        header.userImageView.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(profileVC.tappedUserImage))
        header.userImageView.addGestureRecognizer(tap)
        
        header.findUsBtn.addTarget(self, action: #selector(handleFindUs), for: UIControlEvents.touchUpInside)
        header.callUsBtn.addTarget(self, action: #selector(handleCallUs), for: UIControlEvents.touchUpInside)
        header.donateBtn.addTarget(self, action: #selector(handleDonate), for: UIControlEvents.touchUpInside)
        
        header.followersCountLbl.isUserInteractionEnabled = true
        header.followersCountLbl.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleChurchmembers)))
        header.groupCountLbl.isUserInteractionEnabled = true
        header.groupCountLbl.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(gotoGroups)))
        
        return header
    }
    
    func gotoGroups(){
        
//        let layout = UICollectionViewFlowLayout()
        let vc = myChurchGroupVC()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func handleChurchmembers(){
        
        self.tabBarController?.selectedIndex = 1
    }
    
    func handleDonate(_ sender: UIButton){
        
        self.tabBarController?.selectedIndex = 3
    }
    
    func handleCallUs(_ sender: UIButton){
        
        let myAlert = UIAlertController(title: "Call Any Of Our Numbers", message: "", preferredStyle: UIAlertControllerStyle.alert)
        let phone1 = UIAlertAction(title: self.phoneNumber1, style: UIAlertActionStyle.default) { (alert) in
            //call function here
            if let phoneCallURL:URL = URL(string: "tel:\(self.phoneNumber1)") {
                let application:UIApplication = UIApplication.shared
                if (application.canOpenURL(phoneCallURL)) {
                    
                    if #available(iOS 10.0, *) {
                        application.open(phoneCallURL, options: [:], completionHandler: nil)
                    } else {
                        UIApplication.shared.openURL(phoneCallURL)
                    }

                }
            }

        }
        let phone2 = UIAlertAction(title: self.phoneNumber2, style: UIAlertActionStyle.default) { (alert) in
            //call function here
            if let phoneCallURL:URL = URL(string: "tel:\(self.phoneNumber2)") {
                let application:UIApplication = UIApplication.shared
                if (application.canOpenURL(phoneCallURL)) {
                    
                    if #available(iOS 10.0, *) {
                        application.open(phoneCallURL, options: [:], completionHandler: nil)
                    } else {
                        UIApplication.shared.openURL(phoneCallURL)
                    }
                    
                }
            }

        }
        let phone3 = UIAlertAction(title: self.phoneNumber3, style: UIAlertActionStyle.default) { (alert) in
            //call function here
            if let phoneCallURL:URL = URL(string: "tel:\(self.phoneNumber3)") {
                let application:UIApplication = UIApplication.shared
                if (application.canOpenURL(phoneCallURL)) {
                    
                    if #available(iOS 10.0, *) {
                        application.open(phoneCallURL, options: [:], completionHandler: nil)
                    } else {
                        UIApplication.shared.openURL(phoneCallURL)
                    }
                    
                }
            }

        }
        let phone4 = UIAlertAction(title: self.phoneNumber4, style: UIAlertActionStyle.default) { (alert) in
            //call function here
            if let phoneCallURL:URL = URL(string: "tel:\(self.phoneNumber4)") {
                let application:UIApplication = UIApplication.shared
                if (application.canOpenURL(phoneCallURL)) {
                    
                    if #available(iOS 10.0, *) {
                        application.open(phoneCallURL, options: [:], completionHandler: nil)
                    } else {
                        UIApplication.shared.openURL(phoneCallURL)
                    }
                    
                }
            }

        }
        let phone5 = UIAlertAction(title: self.phoneNumber5, style: UIAlertActionStyle.default) { (alert) in
            //call function here
            //call function here
            if let phoneCallURL:URL = URL(string: "tel:\(self.phoneNumber5)") {
                let application:UIApplication = UIApplication.shared
                if (application.canOpenURL(phoneCallURL)) {
                    
                    if #available(iOS 10.0, *) {
                        application.open(phoneCallURL, options: [:], completionHandler: nil)
                    } else {
                        UIApplication.shared.openURL(phoneCallURL)
                    }
                    
                }
            }

        }
        let phone6 = UIAlertAction(title: self.phoneNumber6, style: UIAlertActionStyle.default) { (alert) in
            //call function here
            if let phoneCallURL:URL = URL(string: "tel:\(self.phoneNumber6)") {
                let application:UIApplication = UIApplication.shared
                if (application.canOpenURL(phoneCallURL)) {
                    
                    if #available(iOS 10.0, *) {
                        application.open(phoneCallURL, options: [:], completionHandler: nil)
                    } else {
                        UIApplication.shared.openURL(phoneCallURL)
                    }
                    
                }
            }

        }
        let phone7 = UIAlertAction(title: self.phoneNumber7, style: UIAlertActionStyle.default) { (alert) in
            //call function here
            if let phoneCallURL:URL = URL(string: "tel:\(self.phoneNumber7)") {
                let application:UIApplication = UIApplication.shared
                if (application.canOpenURL(phoneCallURL)) {
                    
                    if #available(iOS 10.0, *) {
                        application.open(phoneCallURL, options: [:], completionHandler: nil)
                    } else {
                        UIApplication.shared.openURL(phoneCallURL)
                    }
                    
                }
            }

        }
        
        let cancel = UIAlertAction(title: "cancel", style: UIAlertActionStyle.cancel, handler: nil)
        
        myAlert.addAction(phone1)
        myAlert.addAction(phone2)
        myAlert.addAction(phone3)
        myAlert.addAction(phone4)
        myAlert.addAction(phone5)
        myAlert.addAction(phone6)
        myAlert.addAction(phone7)
        myAlert.addAction(cancel)
        
        self.present(myAlert, animated: true, completion: nil)
    }
    
    func handleFindUs(_ sender: UIButton){
        
        let storyboard = UIStoryboard(name: AllKeys.mainStoryBoard, bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: AllKeys.findUsVC) as! findUsVC
        //vc.user = DataOperation.getUser()
        self.navigationController?.pushViewController(vc, animated: true)

    }
    
    //start editing
    func startCheckIn(_ sender: UIButton) {
        print("print 123")

        let vc = UIStoryboard(name:AllKeys.mainStoryBoard, bundle:nil).instantiateViewController(withIdentifier: AllKeys.scannerViewController) as! ScannerViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: self.view.frame.size.width, height: 310 + 50)
    }
    
    @objc func handlesettingsAndFollow(_ sender: UIButton){
        let text = sender.currentTitle!
        if (text == "settings"){
            //self.startEditing()
        }else{
            
            if text == "Follow" {
                
                //sender.isEnabled = false
                sender.setTitle("Unfollow", for: .normal)
                api.followMeber(userId: self.user.id)
                self.user.isFollowing = true
                //set follow button title to Following
                
            }else{
                //sender.isEnabled = false
                sender.setTitle("Follow", for: .normal)
                api.followMeber(userId: self.user.id)
                self.user.isFollowing = false
                //set follow button title to Following
                
            }
        }
    }
    
    @objc func handleChatAction(_ sender: UIButton){
        
        if user.id == currentUser.id{
            
            let vc = chatVC()
            self.navigationController?.pushViewController(vc, animated: true)
        }else{
            
            let user2_lastname = user.lastName
            let user2_firstname = user.firstName
            let chat2_userName = user.otherName
            let user2_id = user.id
            let user2_profileImage = user.avatar
            
            let chat2_numberOfFollowers : Int = user.numberOfFollowing
            let chat2_numberOfGroups : Int = user.numberOfGroups
            
            let toChatLogCV = chatLogCV(collectionViewLayout: UICollectionViewFlowLayout())
            toChatLogCV.chatUser_2Id = user2_id
            toChatLogCV.chatUser_2firstName = user2_firstname
            toChatLogCV.chatUser_2lastname = user2_lastname
            toChatLogCV.chatuser_2avatar = user2_profileImage
            toChatLogCV.chatUserName = chat2_userName
            
            toChatLogCV.chat2_numberOfFollowers = chat2_numberOfFollowers
            toChatLogCV.chat2_numberOfGroups = chat2_numberOfGroups
            
            self.navigationController?.pushViewController(toChatLogCV, animated: true)

        }
    }
    
    //MARK: - image preview
    @objc func tappedUserImage()
    {
        SKPhotoBrowserOptions.backgroundColor = Mics.hexStringToUIColor(hex: AllKeys.primaryHexCode)
        SKPhotoBrowserOptions.textAndIconColor = UIColor.white
        SKPhotoBrowserOptions.toolbarTextShadowColor = UIColor.clear
        SKPhotoBrowserOptions.toolbarFont = UIFont(name: "Futura", size: 16.0)
        SKPhotoBrowserOptions.captionFont = UIFont(name: "Helvetica", size: 18.0)!
        
        // 1. create URL Array
        var images = [SKPhoto]()
        let photo = SKPhoto.photoWithImageURL(self.user.avatar)
        photo.shouldCachePhotoURLImage = false // you can use image cache by true(NSCache)
        images.append(photo)
        
        // 2. create PhotoBrowser Instance, and present.
        let browser = SKPhotoBrowser(photos: images)
        browser.initializePageIndex(0)
        present(browser, animated: true, completion: {})
        
    }
    
    //setup the menu items for current user
    func setMenuItems() {
        let image = UIImage(named: "info")!.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        let infoBarItem = UIBarButtonItem.init(image: image, style: .plain, target: self, action: #selector(handleClick))
        self.navigationItem.rightBarButtonItem = infoBarItem
    }
    
    func handleClick()  {
        
        let layout = UICollectionViewFlowLayout()
        let vc = aboutThisChurchVC(collectionViewLayout: layout)
        vc.churchId = self.currentUser.churchId
        vc.churchName = self.currentUser.churchName
        self.navigationController?.pushViewController(vc, animated: true)

    }
    
}



