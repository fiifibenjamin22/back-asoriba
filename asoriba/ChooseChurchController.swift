//
//  ChooseChurchController.swift
//  asoriba
//
//  Created by Bright Ahedor on 16/12/2016.
//  Copyright © 2016 Asoriba Inc. All rights reserved.
//

import UIKit
import SwiftSpinner
import Crashlytics

class ChooseChurchController: UIViewController,UITableViewDelegate,UITableViewDataSource,ChurchServiceProtocol,JoinChurchServiceProtocol{
    
    @IBOutlet weak var noContentLabel: UILabel!
    @IBOutlet weak var churchTableView: UITableView!
    var feedsData = [Church]()
    var loadedPages = [String]()
    var api:ApiService!
    let screenWidth = UIScreen.main.bounds.width
    var refreshControl = UIRefreshControl();
    var activityViewIndicator:UIActivityIndicatorView = UIActivityIndicatorView()
    var currentUser = DataOperation.getUser()
    var nextUrl = ""

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.churchTableView.delegate = self
        self.churchTableView.dataSource = self
        self.churchTableView.alwaysBounceVertical = true
        
        //register cells
         self.churchTableView.register(UINib(nibName: CellIdentifiers.churchTableCell.rawValue, bundle: nil), forCellReuseIdentifier: CellIdentifiers.churchTableCell.rawValue)
        
        //calling the protocall function
        self.startProgressBar()
        
        //refreshing
        refreshControl.addTarget(self, action: #selector(ChooseChurchController.startRefreshData), for: .valueChanged)
        churchTableView.addSubview(refreshControl)
        
        //api instance
        api = ApiService()
        self.api.churchDelegate = self
        self.api.joinChurchDelegate = self
        
        
        //call method
        self.getData(page: "page=1")
        
        self.setMenuItems()

        //tracking analytics
    }

    //setup the menu items for current user
    func setMenuItems() {
        let image = UIImage(named: "searching")?.withRenderingMode(.alwaysTemplate)
        let infoBarItem = UIBarButtonItem.init(image: image, style: .plain, target: self, action: #selector(handleClick))
        self.navigationItem.rightBarButtonItem = infoBarItem
    }
    
    func handleClick()  {
        guard let vc = UIStoryboard(name:AllKeys.mainStoryBoard, bundle:nil).instantiateViewController(withIdentifier: AllKeys.searchChurchController) as? SearchChurchController else {
            return
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }

    func startProgressBar()  {
        activityViewIndicator.center = self.view.center
        activityViewIndicator.hidesWhenStopped = true
        activityViewIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray
        self.view.addSubview(activityViewIndicator)
        activityViewIndicator.startAnimating()
    }
    
    
    func startRefreshData()  {
        let url = "page=1"
        loadedPages.append(url)
        self.getData(page: url)
    }
    
    func getData(page:String)  {
        self.api.getChurches(url: "connections/suggest/?type=church&\(page)&page_size=10")
    }
    
    func didReceiveChurchError(results: String) {
        //MARK: Why is this called twice?
        self.activityViewIndicator.stopAnimating()
        self.activityViewIndicator.stopAnimating()
    }
    
    
    func didReceiveChurchDetail(results: String) {
        activityViewIndicator.stopAnimating()
        
    }
    
    func didReceiveChurchNextUrl(results: String) {
        self.nextUrl = results
    }
    
    func didReceiveChurchSuccess(results: [Church]) {
        self.activityViewIndicator.stopAnimating()
        self.feedsData = results
        self.churchTableView.reloadData()
        
        if (self.feedsData.isEmpty){
           self.noContentLabel.isHidden = false
        }else{
           self.noContentLabel.isHidden = true
        }
    }
    
    func didReceiveJoinChurchError(results: String) {
        SwiftSpinner.hide()
        self.showDialog(message: results, isError: true)
    }
    
    
    func didReceiveJoinChurchDetail(results: String) {
        SwiftSpinner.hide()
        self.showDialog(message: results, isError: true)
    }
    
    func didReceiveJoinChurchSuccess(results: Church) {
       SwiftSpinner.hide()
       self.showDialog(message: "Successfully joined \(results.name)", isError: false)
    }
    
    func showDialog(message:String,isError: Bool) {
        let  alert = UIAlertController(title: nil, message: message,preferredStyle: .alert)
        if (isError == true) {
            let cancel = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil)
            alert.addAction(cancel)
        }else{
            
            let action = UIAlertAction(title: "Okay",
                                       style: UIAlertActionStyle.default,
                                       handler: {(alert: UIAlertAction!) in
                                        
               self.restartApp()
            })
            alert.addAction(action)
        }
        self.present(alert,animated: true,completion: nil)
    }
    
    func restartApp(){
        //reload application data (renew root view )
        UIApplication.shared.keyWindow?.rootViewController = storyboard!.instantiateViewController(withIdentifier: AllKeys.mainController)
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
          let feed = feedsData[indexPath.row]
        
          let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.churchTableCell.rawValue, for: indexPath) as! ChurchTableCell
        
             cell.feed = feed
        
             cell.joinChurchButton.tag = indexPath.row
             cell.joinChurchButton.addTarget(self, action: #selector(self.joinChurch(_:)), for: .touchUpInside)
        
             cell.followChurchButton.tag = indexPath.row
             cell.followChurchButton.addTarget(self, action: #selector(self.followChurch(_:)), for: .touchUpInside)
        
        
            //gesture section content
            cell.contentView.isUserInteractionEnabled = true
            cell.contentView.tag = indexPath.row
            let tappedContent = UITapGestureRecognizer(target: self, action: #selector(self.tappedContent(_:)))
            tappedContent.cancelsTouchesInView = false
            tappedContent.numberOfTapsRequired = 1
            cell.contentView.addGestureRecognizer(tappedContent)
            //end of gesture
        
            return cell
    }
    
    
    func tappedContent(_ sender: UITapGestureRecognizer)
    {
        let vc = UIStoryboard(name:AllKeys.mainStoryBoard, bundle:nil).instantiateViewController(withIdentifier: AllKeys.churchProfileController) as! ChurchProfileController
        let churchItem = feedsData[(sender.view?.tag)!]
        vc.churchItem = churchItem
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let number = 250;
        return CGFloat(number)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return feedsData.count
    }

    
    //start comment here
    @IBAction func joinChurch(_ sender: UIButton) {
        let feed = self.feedsData[sender.tag]
        api.followChurch(church: feed,isJoining: true)
        SwiftSpinner.show("Getting church ready...", animated: true)
    }
    
    //play audio here
    @IBAction func followChurch(_ sender: UIButton) {
        let feed = self.feedsData[sender.tag]
        let title = sender.currentTitle
        if (title == "Follow") {
            sender.setTitle("Following", for: .normal)
            api.followChurch(church: feed,isJoining: false)
        } else {
            sender.setTitle("Follow", for: .normal)
            api.unFollowChurch(church: feed,isJoining: false)
        }
    }
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let currentOffset = scrollView.contentOffset.y
        let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height
        
        if (maximumOffset - currentOffset) <= 400 {
            if(!self.nextUrl.isEmpty && !loadedPages.contains(self.nextUrl)){
                loadedPages.append(self.nextUrl)
                getData(page: self.nextUrl)
            }
        }
    }

}
