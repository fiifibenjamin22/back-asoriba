//
//  aboutChurchVC.swift
//  asoriba
//
//  Created by Benjamin Acquah on 06/11/2017.
//  Copyright © 2017 Asoriba Inc. All rights reserved.
//

import UIKit
import LBTAComponents

class aboutThisChurchVC: UICollectionViewController, UICollectionViewDelegateFlowLayout,AboutServiceProtocol {
    
    let emptyLbl : UILabel = {
        let lbl = UILabel()
        lbl.numberOfLines = 1
        lbl.text = "Nothing About Church To Display"
        lbl.font = UIFont.systemFont(ofSize: 15)
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    var activityViewIndicator:UIActivityIndicatorView = UIActivityIndicatorView()
    var churchId: String!
    var churchName: String!
    var aboutArray = [About]()
    var api:ApiService!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.collectionView?.backgroundColor = UIColor.groupTableViewBackground
        self.collectionView?.register(aboutCell.self, forCellWithReuseIdentifier: cellIdentify)
        
        self.view.addSubview(emptyLbl)
        emptyLbl.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        emptyLbl.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        emptyLbl.widthAnchor.constraint(equalTo: self.view.widthAnchor).isActive = true
        emptyLbl.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        //api instance
        api = ApiService()
        self.api.aboutDelegate = self
        
        if (!churchId.isEmpty){
            api.getChurchInfo(branchId: churchId)
            //calling the protocall function
            self.startProgressBar()
            
        }
        if (churchId.isEmpty){
            self.showDialog(message: "You do not have a church, choose one to start")
        }
        
        
        self.emptyLbl.isHidden = true
    }
    
    func startProgressBar()  {
        activityViewIndicator.center = self.view.center
        activityViewIndicator.hidesWhenStopped = true
        activityViewIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray
        self.view.addSubview(activityViewIndicator)
        activityViewIndicator.startAnimating()
    }
    
    
    func showDialog(message:String) {
        let  alert = UIAlertController(title: nil, message: message,preferredStyle: .alert)
        
        let action = UIAlertAction(title: "Okay",style: UIAlertActionStyle.default,
                                   handler: {(alert: UIAlertAction!) in
                                    self.startChosingChurch()
        })
        alert.addAction(action)
        self.present(alert,animated: true,completion: nil)
    }
    
    
    func startChosingChurch() {
        guard let vc = UIStoryboard(name:"Main", bundle:nil).instantiateViewController(withIdentifier: AllKeys.chooseChurchController) as? ChooseChurchController else {
            return
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }

    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return aboutArray.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let feed = aboutArray[indexPath.row]
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentify, for: indexPath) as! aboutCell
        cell.feed = feed
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.view.frame.width, height: 150)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let item = aboutArray[indexPath.row]
        self.showDialog(about: item)

    }
    
    //MARK: - dialog action
    func showDialog(about:About) {
        
        let popOverVC = UIStoryboard(name: AllKeys.mainStoryBoard, bundle: nil).instantiateViewController(withIdentifier: AllKeys.aboutDetailViewController) as! AboutDetailViewController
        popOverVC.about = about
        self.addChildViewController(popOverVC)
        popOverVC.view.frame = self.view.frame
        self.view.addSubview(popOverVC.view)
        popOverVC.didMove(toParentViewController: self)
        
    }
    
    //MARK: - About protocol implementation
    func didReceiveAboutError(result: String) {
        self.activityViewIndicator.stopAnimating()
    }
    
    func didReceiveAboutInfo(result: Church) {
        self.activityViewIndicator.stopAnimating()
        
    }
    
    func didReceiveAbouts(results: [About]) {
        self.activityViewIndicator.stopAnimating()
        aboutArray.append(contentsOf: results)
        
        self.collectionView?.reloadData()
        if(aboutArray.isEmpty){
            self.emptyLbl.isHidden = false
        }
    }

}


//cell
class aboutCell: UICollectionViewCell {
    
    let titleLabel : UILabel = {
        let lbl = UILabel()
        lbl.numberOfLines = 2
        lbl.font = UIFont.boldSystemFont(ofSize: 16)
        lbl.textAlignment = .center
        lbl.text = "Title Here"
        return lbl
    }()
    
    let descriptionLbl : UILabel = {
        let lbl = UILabel()
        lbl.numberOfLines = 10
        lbl.font = UIFont.systemFont(ofSize: 15)
        return lbl
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
        backgroundColor = .white
    }
    
    var feed: About! {
        didSet {
            updateUI()
        }
    }
    
    func updateUI() {
        
        self.titleLabel.text = feed.title
        let message = feed.subTitle + "\n\n" + feed.content
        self.descriptionLbl.text = message
        self.descriptionLbl.numberOfLines = 5
        //self.contentConstraint.constant = Helpers.getHeightOfLabel(text: message, fontSize: CGFloat(17), width: UIScreen.main.bounds.width-CGFloat(17),numberOfLines: 3)
        
    }
    
    func setupViews(){
        addSubview(titleLabel)
        addSubview(descriptionLbl)
        titleLabel.anchor(topAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, topConstant: 5, leftConstant: 8, bottomConstant: 0, rightConstant: 8, widthConstant: 0, heightConstant: 0)
        
        descriptionLbl.anchor(titleLabel.bottomAnchor, left: titleLabel.leftAnchor, bottom: bottomAnchor, right: titleLabel.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 8, rightConstant: 0, widthConstant: 0, heightConstant: 0)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
