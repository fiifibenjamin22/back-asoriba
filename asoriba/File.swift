//
//  File.swift
//  asoriba
//
//  Created by Benjamin Acquah on 19/09/2017.
//  Copyright © 2017 Asoriba Inc. All rights reserved.
//

import UIKit

class <#name#>: <#super class#> {
    <#code#>
}
