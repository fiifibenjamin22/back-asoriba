//
//  NotificationKeys.swift
//  asoriba
//
//  Created by Bright Ahedor on 01/01/2017.
//  Copyright © 2017 Asoriba Inc. All rights reserved.
//

import Foundation

struct NotificationKeys {
    static let feedCategory = "asoribaFeedMessage"
    static let feedReadAction = "feedMore"
}
