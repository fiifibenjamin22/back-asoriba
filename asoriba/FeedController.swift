//
//  FeedController.swift
//  asoriba
//
//  Created by Bright Ahedor on 14/12/2016.
//  Copyright © 2016 Asoriba Inc. All rights reserved.
//   190892

import UIKit
import MediaPlayer
import Jukebox
import MobilePlayer
import Toaster
import SafariServices
import SConnection
import FirebaseAnalytics
import Firebase
import Crashlytics
import LiquidFloatingActionButton
import LBTAComponents
import MRProgress
import SwiftyJSON

class FeedController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,FeedServiceProtocol,JukeboxDelegate,UIGestureRecognizerDelegate,PostServiceProtocol, UISearchBarDelegate {
    
    @IBOutlet weak var feedCollectionView: UICollectionView!
    var api:ApiService!
    let screenWidth = UIScreen.main.bounds.width
    var refreshControl = UIRefreshControl();
    var activityViewIndicator:UIActivityIndicatorView = UIActivityIndicatorView()
    
    var feedArray = [Feed]()
    var memberData = [Member]()
    var myCells = [AnyObject]()
    
    let allFeeds = Feed()
    let allSuggested = Member()
    
    var currentUser: User!
    var nextUrl = ""
    var loadedPages = [String]()
    var jukebox : Jukebox!
    var currentPlaying: Feed!
    let numberOfItemsPerRow : CGFloat = 1.0
    var isRefreshing = false
    var churchTitle = ""

    var text = ""
    
    var searchActive : Bool = false
    var searchUrl = ""
    var startSearchProgress = false
    
    let noResultLabel : UILabel = {
        let lbl = UILabel()
        lbl.text = "No Results Found"
        lbl.font = UIFont.boldSystemFont(ofSize: 20)
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    lazy var searchBar : UISearchBar = {
        let sb = UISearchBar()
        sb.placeholder = "Search Posts"
        sb.barTintColor = UIColor.white//(r: 28, g: 90, b: 65)
        sb.translatesAutoresizingMaskIntoConstraints = false
        sb.delegate = self
        UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).backgroundColor = UIColor(r: 28, g: 90, b: 65)
        return sb
    }()
    
    let greenView : UIView = {
        let searchView = UIView()
        searchView.backgroundColor = .green
        searchView.translatesAutoresizingMaskIntoConstraints = false
        return searchView
    }()
    
    let greenViewLayer : UIView = {
        let searchView = UIView()
        searchView.backgroundColor = .green
        searchView.translatesAutoresizingMaskIntoConstraints = false
        return searchView
    }()

    let noContent : UILabel = {
        let no = UILabel()
        no.backgroundColor = UIColor.clear
        no.text = "Nothing Found"
        no.textColor = UIColor.darkText
        no.font = UIFont.boldSystemFont(ofSize: 16)
        no.translatesAutoresizingMaskIntoConstraints = false
        return no
    }()
    
    @IBOutlet weak var playerSectionUIView: UIView!
    @IBOutlet weak var playerSectionLabel: UILabel!
    @IBOutlet weak var playerSectionProgressView: UIProgressView!
    @IBOutlet weak var playerSectionImageView: UIImageView!
    
    @IBAction func playerSectionStopPlayingButton(_ sender: Any) {
        jukebox.stop()
        self.clearPlayerUI()
    }
    @IBOutlet weak var playerSectionButton: UIButton!
    
    @IBAction func searchClickMenuBarItem(_ sender: Any) {
      print("Search action here")
    }
    
    @IBAction func accountClickMenuBarItem(_ sender: Any) {
      print("Accout interaction")
    }
    
    //upon view didLoad Activity
    override func viewDidLoad() {
        super.viewDidLoad()
        
        feedCollectionView?.register(suggestUserCell.self, forCellWithReuseIdentifier: cellIdentify)
        
        let path = Bundle.main.path(forResource: "configuration", ofType: "plist")
        let root = NSDictionary(contentsOfFile: path!)
        
        let greenBackGroundColor = (root?["headerBackgroundColor"] as! String)
        greenView.backgroundColor = UIColor().HexToColor(hexString: greenBackGroundColor)
        searchBar.barTintColor = UIColor().HexToColor(hexString: greenBackGroundColor)

        
        self.currentUser = DataOperation.getUser()
        
        var tag = ""
        if currentUser.branchName.isEmpty {
            tag = "FEED"
        }else {
            tag = currentUser.branchName
        }
        
        self.navigationItem.title = tag
        
        self.feedCollectionView.delegate = self
        self.feedCollectionView.dataSource = self
        self.searchBar.delegate = self
        self.noResultLabel.isHidden = true
        
        //api instance
        api = ApiService()
        self.api.feedDelegate = self
        self.api.postServiceProtocol = self
        
        //register cell
        self.feedCollectionView.register(UINib(nibName: CellIdentifiers.feedCollectionViewCell.rawValue, bundle: nil), forCellWithReuseIdentifier: CellIdentifiers.feedCollectionViewCell.rawValue)
        //end section here
        
        //register header
        self.feedCollectionView.register(UINib(nibName: CellIdentifiers.feedHeader.rawValue, bundle: nil), forSupplementaryViewOfKind: UICollectionElementKindSectionHeader , withReuseIdentifier: CellIdentifiers.feedHeader.rawValue)
        
        
        //calling the protocall function
        self.startProgressBar()
        
        //refreshing
        self.refreshControl.addTarget(self, action: #selector(self.startRefreshData), for: .valueChanged)
        self.feedCollectionView.addSubview(refreshControl)
        
        //chech for network availiability
        if (SConnection.isConnectedToNetwork()){

            //call method
            self.getData(page: "?page=1")

        }else{
            ControllerHelpers.showDialog(targetVC: self, title: "Ooops😭", message: "Your Internet connection is down")
        }
        
        self.playerSectionUIView.isHidden = true
        jukebox = Jukebox(delegate: self)
        
        
        //
        self.setMenuItems()
        self.setMenuFilterItems()
    
        //tracking analytics
        
        //juke declaration
        UIApplication.shared.beginReceivingRemoteControlEvents()
        
        //adding observers
        NotificationCenter.default.addObserver(self, selector: #selector(FeedController.receivedFeedNotification(notification:)), name: Notification.Name(AllKeys.newPostNotification), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(FeedController.receivedFilterNotification(notification:)), name: Notification.Name(AllKeys.filterNotification), object: nil)
        
        setUpViews()
        
        view.addSubview(noContent)
        
        noContent.isHidden = true
        noContent.centerXAnchor.constraint(equalTo: feedCollectionView.centerXAnchor).isActive = true
        noContent.centerYAnchor.constraint(equalTo: feedCollectionView.centerYAnchor).isActive = true
        noContent.heightAnchor.constraint(equalToConstant: 50).isActive = true
        noContent.widthAnchor.constraint(equalTo: feedCollectionView.widthAnchor).isActive = true
    }
    //End view didLoad
    
    func receivedFilterNotification(notification: Notification){
        let url = notification.object as! String
        //Take Action on Notification
        getFilterData(url:url)

    }
    
    func setUpViews(){
        
        self.feedCollectionView.addSubview(noResultLabel)
        
        noResultLabel.anchor(feedCollectionView.topAnchor, left: feedCollectionView.leftAnchor, bottom: feedCollectionView.bottomAnchor, right: feedCollectionView.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
            
    }
    
    
    var unicount : FeedCollectionViewCell?
    
    override func viewDidAppear(_ animated: Bool) {

        if feedArray.count > 0 {
            let indexPath = NSIndexPath(item: self.feedArray.count - 1, section: 0)
            self.feedCollectionView?.scrollToItem(at: indexPath as IndexPath, at: .top, animated: true)
        }
        
        //self.navigationItem.leftBarButtonItem?.isEnabled = true
        
        //call reload Method
        self.getData(page: "?page=1")
        self.feedCollectionView.reloadData()

        UserDefaults.standard.set(0, forKey: AllKeys.notificationCount)
        startRefreshData()

        print("view refreshed")
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //self.navigationItem.leftBarButtonItem?.isEnabled = true
    }

    func receivedFeedNotification(notification: Notification){
        let feed = notification.object as! Feed

        Toast(text: "Post shared successfully").show()
        
        self.myCells.insert(feed, at: 0)
        self.feedCollectionView.reloadData()
    }
    
    //setup the menu items for current user
    func setMenuItems() {
        
        let chatGate = UIImage(named: "chat")!.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
        let chatBarItem = UIBarButtonItem.init(image: chatGate, style: .plain, target: self, action: #selector(enterForumGate))
        chatBarItem.width = 0
        chatBarItem.imageInsets = UIEdgeInsetsMake(5, -5, 0, 0)
        chatBarItem.titlePositionAdjustment(for: UIBarMetrics.default)
        self.navigationItem.rightBarButtonItem = chatBarItem
    }
    
    func startUserSearch()  {
        let viewController = UIStoryboard(name:AllKeys.mainStoryBoard, bundle:nil).instantiateViewController(withIdentifier: AllKeys.searchViewController) as! SearchViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func enterForumGate(){
        let layout = UICollectionViewFlowLayout()
        let vc = forumsVC(collectionViewLayout: layout)
        self.navigationController?.show(vc, sender: self)
    }

    func startProgressBar()  {
        activityViewIndicator.center = self.view.center
        activityViewIndicator.hidesWhenStopped = true
        activityViewIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray
        self.view.addSubview(activityViewIndicator)
        activityViewIndicator.startAnimating()
    }
    
    func startRefreshData()  {
        let url = "page=1"
        loadedPages.append(url)
        isRefreshing = true
        self.api.getFeed(url: "users/get_content/\(url)&page_size=10&feed_type=all&multimedia_type=all&content_category=all&keyword=")
    }

    
    func getData(page: String)  {
        loadedPages.append(page)
        self.api.getFeed(url: "users/get_content/\(page)&page_size=10&feed_type=all&multimedia_type=all&content_category=all&keyword=")        
    }
    
    //start search and filter
    func getFilteredData(page: String,text:String){
        startProgressBar()
        activityViewIndicator.startAnimating()
        loadedPages.append(page)
        self.api.getFeed(url: "users/get_content/\(page)&page_size=10&feed_type=all&multimedia_type=all&content_category=all&keyword=\(text)")
    }
    
    func getFilterData(page: String,type:String,category:String,multimedia:String)  {
        feedArray.removeAll()
        loadedPages.append(page)
        self.searchUrl = "users/get_content/\(page)&page_size=10&feed_type=\(type)&multimedia_type=\(multimedia)&content_category=\(category)&keyword="
        self.api.getFeed(url: self.searchUrl)
        self.startProgressBar()
    }
    
    //make call
    func getMoreData(url: String)  {
        let firstRange = url.index(of: "?")
        let secondRange = url.index(of: "&")
        let newUrl = self.searchUrl.replacingCharacters(in: firstRange!..<secondRange!, with: self.nextUrl)
        loadedPages.append(url)
        self.api.getFeed(url: newUrl)
    }
    //end search and filter
    
    func getFilterData(url:String)  {
        feedArray.removeAll()
        loadedPages.append(url)
        self.searchUrl = url
        self.api.getFeed(url: self.searchUrl)
        self.startProgressBar()
    }

    //api implemetations
    func didReceiveError(results: String) {
        self.refreshControl.endRefreshing()
        self.activityViewIndicator.stopAnimating()
    }
    
    
    func didReceiveSuccess(results: [Feed]) {
        self.refreshControl.endRefreshing()
        if (isRefreshing == true){
            self.feedArray.removeAll()
        }
        self.isRefreshing = false
        self.activityViewIndicator.stopAnimating()
        feedArray.append(contentsOf: results)
        self.feedCollectionView.reloadData()
    }
    
    
    func didReceiveDetail(results: String) {
        self.refreshControl.endRefreshing()
        self.activityViewIndicator.stopAnimating()
    }
    
    
    func didReceiveNextUrl(results: String) {
       self.refreshControl.endRefreshing()
       self.nextUrl = results
    }
    
    //ends here
    
    // MARK: - Navigation
    func startUserProfile()  {
        let profileViewController = UIStoryboard(name:AllKeys.mainStoryBoard, bundle:nil).instantiateViewController(withIdentifier: AllKeys.userProfileController) as! ProfileViewController
  
        let member = Member()
        member.avatar = currentUser.avatar
        member.firstName = currentUser.firstName
        member.lastName = currentUser.lastName
        member.id = currentUser.id
        member.isFollowing = true
        member.numberOfFollowing = currentUser.numberOfFollowers
        member.numberOfGroups = currentUser.numberOfGroups
        profileViewController.user = member
        
        self.navigationController?.pushViewController(profileViewController, animated: true)
     
    }
    
    //MARK:- collection view delegate
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return feedArray.count
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        self.feedCollectionView.collectionViewLayout.invalidateLayout()
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CellIdentifiers.feedCollectionViewCell.rawValue, for: indexPath) as! FeedCollectionViewCell
        cell.backgroundColor = UIColor.white
        cell.feed = feedArray[indexPath.row] //as? Feed
        
        if cell.feed == nil {
            
            self.noContent.isHidden = false
            
        }else{
            self.noContent.isHidden = true
        }
        
        cell.isDetail = false
        
        cell.amenButton.tag = indexPath.row
        cell.amenButton.addTarget(self, action: #selector(self.amenButton(_:)), for: .touchUpInside)
        cell.shareButton.tag = indexPath.row
        cell.shareButton.addTarget(self, action: #selector(self.shareButton(_:)), for: .touchUpInside)
        cell.commentButton.tag = indexPath.row
        cell.commentButton.addTarget(self, action: #selector(self.startComment(_:)), for: .touchUpInside)
        
        cell.audioPlayButton.tag = indexPath.row
        cell.audioPlayButton.addTarget(self, action: #selector(self.playAudioCommand(_:)), for: .touchUpInside)
        
        //gesture section
        cell.feedContentLabel.isUserInteractionEnabled = true
        cell.feedContentLabel.tag = indexPath.row
        let tapped = UITapGestureRecognizer(target: self, action: #selector(self.tappedContent(_:)))
        tapped.cancelsTouchesInView = false
        tapped.numberOfTapsRequired = 1
        cell.feedContentLabel.addGestureRecognizer(tapped)
        
        //extracting urls from feed content label
        cell.feedContentLabel.enabledTypes = [.url]
        cell.feedContentLabel.URLColor = UIColor(red: 18 / 255, green: 165 / 255, blue: 244 / 255, alpha: 1.0)
        cell.feedContentLabel.URLSelectedColor = UIColor.darkText
        cell.feedContentLabel.font = UIFont.systemFont(ofSize: 16)
        cell.feedContentLabel.textColor = UIColor.black
        
        let webStringProtocol = ""
        let input = cell.feedContentLabel.text
        let detector = try! NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue)
        let matches = detector.matches(in: input!, options: [], range: NSRange(location: 0, length: (input?.utf16.count)!))
        
        for match in matches{
            
            let webUrl = (input! as NSString).substring(with: match.range)
            print("link inside post string: \(webStringProtocol)\(webUrl)")
            let combined = URL(string:"\(webStringProtocol)\(webUrl)")
            
            cell.feedContentLabel.handleURLTap({ (webUrl) in
                
                let svc = SFSafariViewController(url: combined!)
                self.present(svc, animated: true, completion: nil)

            })
        }

        //end of gesture
        
        //gesture account
        let tappedUser = UITapGestureRecognizer(target: self, action: #selector(self.tappedUserLabel(_:)))
        cell.userNameLabel!.isUserInteractionEnabled = true
        cell.userNameLabel!.tag = indexPath.row
        tappedUser.cancelsTouchesInView = false
        tappedUser.numberOfTapsRequired = 1
        cell.userNameLabel!.addGestureRecognizer(tappedUser)
        //end user gesture
        
        //gesture account
        let tappedUserImage = UITapGestureRecognizer(target: self, action: #selector(self.tappedUserLabel(_:)))
        cell.userImageView!.isUserInteractionEnabled = true
        cell.userImageView!.tag = indexPath.row
        tappedUserImage.cancelsTouchesInView = false
        tappedUserImage.numberOfTapsRequired = 1
        cell.userImageView!.addGestureRecognizer(tappedUserImage)
        //end user gesture
        
        //gesture section ameners
        cell.amenCountLabel!.isUserInteractionEnabled = true
        cell.amenCountLabel!.tag = indexPath.row
        let tappedAmenCount = UITapGestureRecognizer(target: self, action: #selector(self.tapPerfomers(_:)))
        tappedAmenCount.cancelsTouchesInView = false
        tappedAmenCount.numberOfTapsRequired = 1
        cell.amenCountLabel!.addGestureRecognizer(tappedAmenCount)
        //end of gesture
        
        //gesture section sharers
        cell.shareCountLabel!.isUserInteractionEnabled = true
        cell.shareCountLabel!.tag = indexPath.row
        let tappedShareCount = UITapGestureRecognizer(target: self, action: #selector(self.tapSharePerfomers(_:)))
        tappedShareCount.cancelsTouchesInView = false
        tappedShareCount.numberOfTapsRequired = 1
        cell.shareCountLabel!.addGestureRecognizer(tappedShareCount)
        //end of ameners gesture
        
        
        //gesture section sharers
        cell.mediaSectionUIView!.isUserInteractionEnabled = true
        cell.mediaSectionUIView!.tag = indexPath.row
        let tappedMedia = UITapGestureRecognizer(target: self, action: #selector(self.tappedMediaSection(_:)))
        tappedMedia.cancelsTouchesInView = false
        tappedMedia.numberOfTapsRequired = 1
        cell.mediaSectionUIView!.addGestureRecognizer(tappedMedia)
        //end of ameners gesture
        
        //gesture for quotation
        cell.quotationLabel!.isUserInteractionEnabled = true
        cell.quotationLabel!.tag = indexPath.row
        let tappedQuote = UITapGestureRecognizer(target: self, action: #selector(self.tapQuote(_:)))
        tappedQuote.cancelsTouchesInView = false
        tappedQuote.numberOfTapsRequired = 1
        cell.quotationLabel!.addGestureRecognizer(tappedQuote)
        //end of gesture
        
        return cell
        //}else{
        //    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentify, for: indexPath) as! suggestUserCell
        //    return cell
        //}
        
    }
    
    func collectionView(_ collectionView: UICollectionView, shouldHighlightItemAt indexPath: IndexPath) -> Bool {
        return false
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return CGFloat(AllKeys.cellSpacing)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return CGFloat(AllKeys.cellSpacing)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: CGFloat(AllKeys.cellSpacing), left: 0.0, bottom: 0.0, right: 0.0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        //let feedObject = self.feedArray[indexPath.row]
        //var height : CGFloat = 0

        //if feedObject is Feed {
            
            let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
            let contentWidth = collectionView.bounds.width
            let totalSpace = flowLayout.sectionInset.left
                + flowLayout.sectionInset.right
                + (flowLayout.minimumInteritemSpacing * CGFloat(numberOfItemsPerRow - 1))
            let size = Int((contentWidth - totalSpace) / CGFloat(numberOfItemsPerRow))
            let feed = self.feedArray[indexPath.row] //as? Feed
            return ConfigureCell.configureFeedCell(feed:feed,size:size,contentWidth:contentWidth,lines:5)
        //}else{
            
            //height = 250
        //}
        
        //return CGSize(width: view.frame.width, height: height)

    }
    
    
    //MARK- header configurations
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionHeader, withReuseIdentifier: CellIdentifiers.feedHeader.rawValue, for: indexPath as IndexPath) as! FeedCollectionHeader
        headerView.backgroundColor = UIColor.white
        headerView.user = self.currentUser
        
        
        headerView.buttonShareWord.addTarget(self, action: #selector(self.tappedHeader(_:)), for: .touchUpInside)
        
        headerView.userImageView.isUserInteractionEnabled = true
        headerView.userImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleUserProfile)))
        
        return headerView
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: CGFloat(collectionView.frame.width), height: CGFloat(60))
    }
    //end of header configuration
    
    func handleUserProfile(){
        //let viewController = UIStoryboard(name:AllKeys.mainStoryBoard, bundle:nil).instantiateViewController(withIdentifier: AllKeys.userProfileController) as! ProfileViewController
        
        let layout = UICollectionViewFlowLayout()
        let viewController = profileVC(collectionViewLayout: layout)
        
        let member = Member()
        member.avatar = currentUser.avatar
        member.firstName = currentUser.firstName
        member.lastName = currentUser.lastName
        member.id = currentUser.id
        member.isFollowing = true
        member.numberOfFollowing = currentUser.numberOfFollowers
        member.numberOfGroups = currentUser.numberOfGroups
        viewController.user = member
        
        self.navigationController?.pushViewController(viewController, animated: true)
    }
 
    //MARK: tap a quotation
    func tapQuote(_ sender: UITapGestureRecognizer)
    {
        let feed = feedArray[(sender.view?.tag)!]
        ControllerHelpers.startQuote(targetVC: self, quote: feed.quotation!)
    }
 
    //MARK: start the header section
    @IBAction func tappedHeader(_ sender: UIButton)
    {
        let vc = UIStoryboard(name:AllKeys.mainStoryBoard, bundle:nil).instantiateViewController(withIdentifier: AllKeys.newFeedController) as! NewFeedController
        let nextViewController = UINavigationController(rootViewController: vc)
        self.present(nextViewController, animated: true, completion: nil)
    }
    
    
    //send the amen signal
    var titleText = ""

    @IBAction func amenButton(_ sender: UIButton) {
        titleText = sender.currentTitle!

        if (titleText == "Amen") {
            
            sender.setTitle("Amened", for: UIControlState.normal)
            let feed = self.feedArray[sender.tag]
            feed.hasAmened = true
            feed.numberOfAmens = feed.numberOfAmens + 1
            self.feedCollectionView.reloadData()
            self.api.sayAmen(item: feed)
            
        }else{
            sender.setTitle("Amen", for: UIControlState.normal)
            let feed = self.feedArray[sender.tag]
            feed.hasAmened = false
            feed.numberOfAmens = feed.numberOfAmens - 1
            self.feedCollectionView.reloadData()
            self.api.sayAmen(item: feed)
        }
    }
    
    //send the share signal
    @IBAction func shareButton(_ sender: UIButton) {
        let feed = self.feedArray[sender.tag]
        
        //let copiedText = "\(feed.title)\n\(feed.content)"
        
        let myAlert = UIAlertController(title: "Notice", message: "If you are sharing on facebook, Text has been coppied to clipboard so paste it and share", preferredStyle: UIAlertControllerStyle.alert)
        let ok = UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel) { (_) in
            ControllerHelpers.presentSharer(targetVC: self, feed: feed)
        }
        
        myAlert.addAction(ok)
        self.present(myAlert, animated: true, completion: nil)
        
    }
    
    //start comment here
    @IBAction func startComment(_ sender: UIButton) {
        let feed = self.feedArray[sender.tag]
        ControllerHelpers.startDetail(targetVC: self,feed:feed)
        
        Answers.logCustomEvent(withName: "content_action", customAttributes: ["click_comments" : (Any).self])
    }
    
    //play audio here
    @IBAction func playAudioCommand(_ sender: UIButton) {
        if let item = self.currentPlaying {
           self.jukebox.removeItems(withURL: URL(string: (item.mediaFile))!)
            
        }
        self.currentPlaying = self.feedArray[sender.tag]
        self.playerSectionUIView.isHidden = false
        self.playerSectionLabel.text = self.currentPlaying.content
        self.updateUI()
        
        self.jukebox.append(item: JukeboxItem (URL: URL(string: self.currentPlaying.mediaFile)!), loadingAssets: true)
        Answers.logCustomEvent(withName: "play_audio", customAttributes: ["Audio" : currentPlaying.id])
        jukebox?.stop()
        jukebox?.play()
        
    }
    

    //MARK: -- Handling gestures
   func tappedMediaSection(_ sender: UITapGestureRecognizer)
    {
        let currentFeed = feedArray[(sender.view?.tag)!]
        if (currentFeed.mediaType == "video") {
           ControllerHelpers.processVideoPlayer(targetVC: self, feed: currentFeed)
            
            Answers.logCustomEvent(withName: "play_video", customAttributes: ["Video" : currentFeed.id])
            
        }else  {
            ControllerHelpers.presentImage(targetVC: self, feed: currentFeed)
        }
        
    }
    
    
    //MARK: now to user
    func tappedUserLabel(_ sender: UITapGestureRecognizer)
    {
        let feed = feedArray[(sender.view?.tag)!]
        
        //is user
        if (feed.member?.isMobile)!{
//            let layout = UICollectionViewFlowLayout()
//            let vc = profileVC(collectionViewLayout: layout)
//            //let vc = UIStoryboard(name:AllKeys.mainStoryBoard, bundle:nil).instantiateViewController(withIdentifier: AllKeys.userProfileController) as! ProfileViewController
//            let member = feed.member
//            vc.user = member!
//            self.navigationController?.pushViewController(vc, animated: true)
            
            //let member = feed.member
            
            let layout = UICollectionViewFlowLayout()
            let vc = profileVC(collectionViewLayout: layout)
            let position = (sender.view?.tag)!
            let member = feedArray[position]
            print("print selected member: ", member)
            vc.user = feed.member
            self.navigationController?.pushViewController(vc, animated: true)
            
        //is church not user
        } else {
            
            let member = feed.member
            let church = Church()
            church.id = (member?.id)!
            church.name = (member?.firstName)!
            church.branchName = (member?.lastName)!
            church.churchLogo = (member?.avatar)!
            
            let layout = UICollectionViewFlowLayout()
            let churchController = myChurchVC(collectionViewLayout: layout)
            churchController.isMyChurch = false
            churchController.churchItem = church
            
            self.navigationController?.pushViewController(churchController, animated: true)
            
        }
       
    }
    
    
    //MARK: start detail here
    func tappedContent(_ sender: UITapGestureRecognizer)
    {
        let feed = feedArray[(sender.view?.tag)!]
        ControllerHelpers.startDetail(targetVC: self,feed:feed)
        
    }
    
    //MARK: set performers gesture
    func tapPerfomers(_ sender: UITapGestureRecognizer)
    {
        let feed = feedArray[(sender.view?.tag)!]
        ControllerHelpers.startPerformers(targetVC: self, feedId: feed.id,isAmeners: true)
        
    }
    
    //MARK: set performers gesture
    func tapSharePerfomers(_ sender: UITapGestureRecognizer)
    {
        let feed = feedArray[(sender.view?.tag)!]
        ControllerHelpers.startPerformers(targetVC: self, feedId: feed.id,isAmeners: false)
    }
    
    
    //MARK:- jute box section
    
    func jukeboxStateDidChange(_ jukebox: Jukebox) {
        if jukebox.state == .ready {
           self.playerSectionButton.setTitle("Ready", for: .normal)
        } else if jukebox.state == .loading  {
          self.playerSectionButton.setTitle("Preparing", for: .normal)
        } else if  (jukebox.state == .failed ) {
           self.playerSectionButton.setTitle("Play", for: .normal)
        }else if  (jukebox.state == .playing) {
            self.playerSectionButton.setTitle("Stop", for: .normal)
        }else {
          self.playerSectionButton.setTitle("Playing", for: .normal)
        }
    }
    
    func jukeboxDidLoadItem(_ jukebox: Jukebox, item: JukeboxItem) {
        print("Jukebox did load: \(item.URL.lastPathComponent)")
    }
    
    func jukeboxPlaybackProgressDidChange(_ jukebox: Jukebox) {
        if let currentTime = jukebox.currentItem?.currentTime, let duration = jukebox.currentItem?.meta.duration {
                let value = Float(currentTime / duration)
                if (value >= 1){
                    self.clearPlayerUI()
                } else {
                    self.playerSectionProgressView.setProgress(value, animated: true)
                }
            
        }
  
    }
    
    func clearPlayerUI() {
        self.playerSectionProgressView.setProgress(0, animated: true)
        self.playerSectionLabel.text = ""
        self.playerSectionUIView.isHidden = true
    }
    
    func populateLabelWithTime(time: Double) -> String {
        let minutes = Int(time / 60)
        let seconds = Int(time) - minutes * 60
        
        return  String(format: "%02d", minutes) + ":" + String(format: "%02d", seconds)
    }
    
    func jukeboxDidUpdateMetadata(_ jukebox: Jukebox, forItem: JukeboxItem) {
        print("Item updated:\n\(forItem)")
    }
    
    
    override func remoteControlReceived(with event: UIEvent?) {
        if event?.type == .remoteControl {
            switch event!.subtype {
            case .remoteControlPlay :
                jukebox.play()
            case .remoteControlPause :
                jukebox.pause()
            case .remoteControlNextTrack :
                jukebox.playNext()
            case .remoteControlPreviousTrack:
                jukebox.playPrevious()
            case .remoteControlTogglePlayPause:
                if jukebox.state == .playing {
                    jukebox.pause()
                } else {
                    jukebox.play()
                }
            default:
                break
            }
        }
    }

    //END, gestures
    
    //MARK: - scroll handling section
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let currentOffset = scrollView.contentOffset.y
        let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height
        
        if (maximumOffset - currentOffset) <= 400 {
            if !(self.nextUrl.isEmpty) && !self.loadedPages.contains(self.nextUrl) {
                    self.getData(page: self.nextUrl)

            }
        }
    }
    
    func didReceivePostResult(result: Feed) {
       feedArray.insert(result, at: 0)
       self.feedCollectionView.reloadData()
    }
    
    func didReceivePostError(result: String) {
       print("Posted Erorr \(result)")
    }
    
    func updateUI() {
        if  !((self.currentPlaying.member?.avatar.isEmpty)!) {
            self.playerSectionImageView.af_setImage(
                withURL: URL(string: (self.currentPlaying.member?.avatar)!)!,
                placeholderImage: UIImage(named: "default"),
                imageTransition: .crossDissolve(0.2)
            )
        }else
        {
            self.playerSectionImageView.image = UIImage(named: "default")
        }
    }
    
    //setup the menu items for current user
    func setMenuFilterItems() {
        
        let filterContent = #imageLiteral(resourceName: "filter").withRenderingMode(.alwaysOriginal)
        let filterButtonItem = UIBarButtonItem(image: filterContent, style: .plain, target: self, action: #selector(tapSearchFilter))
        filterButtonItem.imageInsets = UIEdgeInsetsMake(5, -5, 0, 0)
        navigationItem.leftBarButtonItems = [filterButtonItem]
    }
        
    //MARK: - verse picker
    func tapSearchFilter()
    {
        let popOverVC = UIStoryboard(name: AllKeys.micsStoryBoard, bundle: nil).instantiateViewController(withIdentifier: AllKeys.searchFilterViewController) as! SearchFilterViewController
        self.addChildViewController(popOverVC)
        popOverVC.view.frame = self.view.frame
        self.view.addSubview(popOverVC.view)
        popOverVC.didMove(toParentViewController: self)
        //self.navigationItem.leftBarButtonItem?.isEnabled = false
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        jukebox.stop()
    }

    //search implementation
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        self.searchBar.showsCancelButton = true
        self.navigationItem.rightBarButtonItems?.removeAll()

        searchActive = true
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if searchText == "" {
            self.feedArray.removeAll()
            self.getData(page: "page=1")
            UserDefaults.standard.set(0, forKey: AllKeys.notificationCount)
            startRefreshData()
            self.searchBar.resignFirstResponder()
        }else{
         
            if (searchText.count > 2) {
                
                self.feedArray.removeAll()
                
                self.text = searchText
                self.getFilteredData(page: "?page=1", text: self.text)
                self.startProgressBar()
                self.startSearchProgress = true
                
                DispatchQueue.main.async {
                    self.feedCollectionView.reloadData()
                }
            }
        }
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        self.searchBar.showsCancelButton = false
        setMenuItems()
        searchBar.resignFirstResponder()
        searchActive = false
    }

    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.searchBar.endEditing(true)
        self.searchBar.showsCancelButton = false
        setMenuItems()
        self.searchBar.resignFirstResponder()
        searchActive = false
        self.searchBar.text = ""

        self.getData(page: "page=1")
        DispatchQueue.main.async {
            self.feedCollectionView.reloadData()
        }
    }

    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        searchActive = false
    }
}
