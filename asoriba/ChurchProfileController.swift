//
//  MyChurchController.swift
//  asoriba
//
//  Created by Bright Ahedor on 15/12/2016.
//  Copyright © 2016 Asoriba Inc. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage
import Photos
import SKPhotoBrowser
import Crashlytics



class ChurchProfileController: UIViewController,UITableViewDelegate,UITableViewDataSource,GroupsServiceProtocol,ChurchMembersServiceProtocol,FeedServiceProtocol,AboutServiceProtocol {
    
    @IBOutlet weak var dataTableView: UITableView!
    var headerView:MyChurchTableHeader! = nil
    
    
    var api:ApiService!
    
    var positionSelected = 0
    var membersData = [Member]()
    var groupsData = [Group]()
    var feedsData = [Feed]()
    var userData = User()
    var isMyChurch = true
    var churchItem: Church!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        userData = DataOperation.getUser()
        let tag  = churchItem.name
        
        self.navigationItem.title = tag
        
        dataTableView.delegate = self
        dataTableView.dataSource = self
        dataTableView.alwaysBounceVertical = true
        
        
        //regiser header
        let nib = UINib(nibName: CellIdentifiers.myChurchHeader.rawValue, bundle: nil)
        self.dataTableView.register(nib, forHeaderFooterViewReuseIdentifier: CellIdentifiers.myChurchHeader.rawValue)
        
        
        //register cells
        self.dataTableView.register(UINib(nibName: CellIdentifiers.groupCell.rawValue, bundle: nil), forCellReuseIdentifier: CellIdentifiers.groupCell.rawValue)
        self.dataTableView.register(UINib(nibName: CellIdentifiers.memberCell.rawValue, bundle: nil), forCellReuseIdentifier: CellIdentifiers.memberCell.rawValue)
        self.dataTableView.register(UINib(nibName: CellIdentifiers.feedWithImageTableCell.rawValue, bundle: nil), forCellReuseIdentifier: CellIdentifiers.feedWithImageTableCell.rawValue)
        
        self.dataTableView.register(UINib(nibName: CellIdentifiers.feedUniversalCell.rawValue, bundle: nil), forCellReuseIdentifier: CellIdentifiers.feedUniversalCell.rawValue)
        //end
        
        
        //api instance
        api = ApiService()
        self.api.feedDelegate = self
        self.api.churchGroupsDelegate = self
        self.api.churchMembersDelegate = self
        self.api.aboutDelegate = self
        
        //making api call
        self.getGroups(url: "page=1",churchId: churchItem.id)
        self.getMembers(url: "page=1",churchId: churchItem.id)
        self.getContent(url:"page=1",churchId: churchItem.id)
        api.getChurchInfo(branchId: churchItem.id)
                
        //set menu items
        self.setMenuItems()
        
        //tracking analytics
    }
    
    //setup the menu items for current user
    func setMenuItems() {
        let image = UIImage(named: "info")!.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        let infoBarItem = UIBarButtonItem.init(image: image, style: .plain, target: self, action: #selector(handleClick))
        self.navigationItem.rightBarButtonItem = infoBarItem
    }
    
    func handleClick()  {
        self.startAbout()
    }
    
    //MARK: - segue section
    func startAbout() {
        self.performSegue(withIdentifier: AllKeys.aboutChurchSegue, sender: nil)
    }
    
    
    //set image
    func tappedUserImage()
    {
        ControllerHelpers.presentSingleImage(targetVC: self, url: self.userData.churchLogo)
    }
    
    
    func getGroups(url:String,churchId:String) {
        api.getChurchGroups(churchId: churchId,next:url)
        dataTableView.reloadData()
    }
    
    func getMembers(url:String,churchId:String) {
        api.getChurchMembers(branchId: churchId,next:url)
        dataTableView.reloadData()
    }
    
    func getContent(url:String,churchId:String) {
        api.getChurchContent(branchId: churchId, page: url)
        dataTableView.reloadData()
    }
    
    
    //more delegates
    func didReceiveError(results: String) {
        //show errrors
    }
    
    
    func didReceiveGroupsError(results: String) {
        //groups error
    }
    
    func didReceiveChurchMembersError(results: String) {
        //members error
    }
    
    
    func didReceiveDetail(results: String) {
        //detail on feed
    }
    
    func didReceiveGroupsDetail(results: String) {
        //groups detail
    }
    
    func didReceiveChurchMembersDetail(results: String) {
        //member detail
    }
    
    
    func didReceiveNextUrl(results: String) {
        //next Url
    }
    
    func didReceiveChurchMemberNextUrl(results: String) {
        //member next Url
    }
    
    func didReceiveChurchGroupsNextUrl(results: String) {
        //groups next Url
    }
    
    
    
    func didReceiveSuccess(results: [Feed]) {
        feedsData.append(contentsOf: results)
        self.dataTableView.reloadData()
    }
    
    
    func didReceiveGroupsSuccess(results: [Group]) {
        groupsData.append(contentsOf: results)
        self.dataTableView.reloadData()
    }
    
    
    
    func didReceiveChurchMembersSuccess(results: [Member]) {
        membersData.append(contentsOf: results)
        self.dataTableView.reloadData()
    }
    
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == AllKeys.aboutChurchSegue){
            let aboutController = segue.destination as! AboutChurchController
            aboutController.churchId = self.churchItem.id
            aboutController.churchName = self.churchItem.name
        }
        
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if(positionSelected == 0){
            let feed = membersData[indexPath.row]
            let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.memberCell.rawValue, for: indexPath) as! MemberCell
            cell.feed = feed
            
            //gesture section image
            cell.contentView.isUserInteractionEnabled = true
            cell.contentView.tag = indexPath.row
            let tapped = UITapGestureRecognizer(target: self, action: #selector(self.startDetail(_:)))
            tapped.cancelsTouchesInView = false
            tapped.numberOfTapsRequired = 1
            cell.contentView.addGestureRecognizer(tapped)
            //end of gesture
            
            cell.followActionButton.addTarget(self, action: #selector(ChurchProfileController.followUser(_:)), for: .touchUpInside)
            return cell
        }else if (positionSelected == 1){
            let feed = groupsData[indexPath.row]
            let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.groupCell.rawValue, for: indexPath) as! GroupTableCell
            cell.groupJoinAction.addTarget(self, action: #selector(ChurchProfileController.followGroup(_:)), for: .touchUpInside)
            cell.feed = feed
            return cell
        }else{
            let feed = feedsData[indexPath.row]
            let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.feedUniversalCell.rawValue, for: indexPath) as! FeedUniversalCell
            cell.feed = feed
            
            //gesture section image
            cell.contentView.isUserInteractionEnabled = true
            cell.contentView.tag = indexPath.row
            let tapped = UITapGestureRecognizer(target: self, action: #selector(self.startFeedDetail(_:)))
            tapped.cancelsTouchesInView = false
            tapped.numberOfTapsRequired = 1
            cell.contentView.addGestureRecognizer(tapped)
            //end of gesture
            
            return cell
        }
    }
    
    
    func startDetail(_ sender: UITapGestureRecognizer)  {
        let profileController = UIStoryboard(name:"Main", bundle:nil).instantiateViewController(withIdentifier: AllKeys.userProfileController) as! ProfileViewController
        let member = membersData[(sender.view?.tag)!]
        profileController.user = member
        self.navigationController?.pushViewController(profileController, animated: true)
    }
    
    func startFeedDetail(_ sender: UITapGestureRecognizer)  {
        let feedDetailController = UIStoryboard(name:"Main", bundle:nil).instantiateViewController(withIdentifier: AllKeys.feedDetailController) as! FeedDetailController
        let feed = feedsData[(sender.view?.tag)!]
        feedDetailController.feed = feed
        print("Data to pass \(feed.id)")
        self.navigationController?.pushViewController(feedDetailController, animated: true)
    }
    
    
    //MARK: - Joining the group action
    @IBAction func followGroup(_ sender: UIButton) {
        sender.isEnabled = false
        sender.setTitle("Joined", for: .normal)
        let feed = self.groupsData[sender.tag]
        api.joingChurchGroup(groupId: feed.id)
    }
    
    //MARK: - Following user
    @IBAction func followUser(_ sender: UIButton) {
        sender.isEnabled = false
        sender.setTitle("Following", for: .normal)
        let feed = self.membersData[sender.tag]
        api.followMeber(userId: feed.id)
    }
    
    
    
    //calculating the item height
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        var number = 200;
        switch positionSelected {
        case 0:
            number = 80
        case 1:
            number = 100
        case 2:
            number = 130
        default:
            break
        }
        return CGFloat(number)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var number = 0;
        switch positionSelected{
        case 0:
            number = membersData.count
        case 1:
            number = groupsData.count
        case 2:
            number = feedsData.count
        default:
            break
        }
        return number
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    @IBAction func clickChurchSegment(_ sender: Any) {
        dataTableView.reloadData()
    }
    
    @IBAction func joinChurchButtonClicked(_ sender: Any) {
    }
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return CGFloat(310)
    }
    
    //MARK: - table header
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = self.dataTableView.dequeueReusableHeaderFooterView(withIdentifier: CellIdentifiers.myChurchHeader.rawValue)
        self.headerView = cell as! MyChurchTableHeader
        
        headerView.churchItem = self.churchItem
        headerView.joinButton.addTarget(self, action: #selector(self.startChosing(_:)), for: .touchUpInside)
        headerView.myChurchSegment.addTarget(self, action: #selector(ChurchProfileController.churchSegmentButtonClick(_:)), for: .valueChanged)
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.tappedUserImage))
        headerView.churchLogoImageView.addGestureRecognizer(tap)
        return cell
    }
    
    
    //start editing
    func startChosing(_ sender: UIButton) {
        sender.setTitle("Following", for: .normal)
        sender.isEnabled = false
        api.followChurch(church: self.churchItem,isJoining: false)
    }
    
    
    @IBAction func churchSegmentButtonClick(_ sender: UISegmentedControl) {
        positionSelected = sender.selectedSegmentIndex
        self.dataTableView.reloadData()
    }
    
    func didReceiveAboutError(result: String) {
        
    }
    
    func didReceiveAboutInfo(result: Church) {
        self.churchItem = result
        self.headerView.churchItem = self.churchItem
        self.dataTableView.reloadData()
    }
    
    func didReceiveAbouts(results: [About]) {
        
    }
    
}

