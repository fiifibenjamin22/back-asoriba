//
//  GivingController.swift
//  asoriba
//
//  Created by Bright Ahedor on 15/12/2016.
//  Copyright © 2016 Asoriba Inc. All rights reserved.
//

import UIKit
import Crashlytics
import LiquidFloatingActionButton


class GivingController: UIViewController,UITableViewDelegate, UITableViewDataSource,DonationServiceProtocol {

    //, LiquidFloatingActionButtonDelegate, LiquidFloatingActionButtonDataSource
    
    @IBOutlet weak var givingTableView: UITableView!
    
    @IBOutlet weak var emptyLabel: UILabel!
    var api:ApiService!
    let screenWidth = UIScreen.main.bounds.width
    var activityViewIndicator:UIActivityIndicatorView = UIActivityIndicatorView()
    var refreshControl: UIRefreshControl = UIRefreshControl();
    var feedArray = [Giving]()
    var currentUser = User()
    var nextUrl = ""
    var gateWays = DataOperation.getAllGateWays()
    
    var floatingCells = [LiquidFloatingCell]()
    var floatingActionButton : LiquidFloatingActionButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        givingTableView.backgroundColor = UIColor.groupTableViewBackground

        let tag = "Giving"
        self.navigationItem.title = tag
        
        //get user here
        currentUser = DataOperation.getUser()
        
        givingTableView.delegate = self
        givingTableView.dataSource = self
        givingTableView.alwaysBounceVertical = true
        
        givingTableView.estimatedRowHeight = givingTableView.rowHeight
        givingTableView.rowHeight = UITableViewAutomaticDimension
        
        //register cell
          self.givingTableView.register(UINib(nibName: CellIdentifiers.givingTableCell.rawValue, bundle: nil), forCellReuseIdentifier: CellIdentifiers.givingTableCell.rawValue)
        
        //refreshing
        refreshControl.addTarget(self, action: #selector(GivingController.refresh), for: .valueChanged)
        givingTableView.addSubview(refreshControl)
        
        //api instance
        api = ApiService()
        self.api.donationDelegate = self
        
        //present choosing school here
        if (!currentUser.churchId.isEmpty){
            startProgressBar()
            self.getData()
        }
        
        
        if (currentUser.churchId.isEmpty){
            self.showDialog(message: "You do not have a church to donate to, choose one to start")
        }
        
        self.emptyLabel.isHidden = true
    
        self.setMenuItems()

    }

    override func viewDidAppear(_ animated: Bool) {
        self.gateWays = DataOperation.getAllGateWays();
    }
    
    func startProgressBar()  {
        activityViewIndicator.center = self.view.center
        activityViewIndicator.hidesWhenStopped = true
        activityViewIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray
        self.view.addSubview(activityViewIndicator)
        activityViewIndicator.startAnimating()
    }
    
    
    func getData()  {
        api.getDonations(url: "users/payments/categories/")
    }

    
    //setup the menu items for current user
    func setMenuItems() {
        let search = UIImage(named: "history")!.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        let searchBarItem = UIBarButtonItem.init(image: search, style: .plain, target: self, action: #selector(startUserHistory))
        self.navigationItem.rightBarButtonItem = searchBarItem
        
//        let account = UIImage(named: "user")!.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
//        let userBarItem = UIBarButtonItem.init(image: account, style: .plain, target: self, action: #selector(startUserProfile))
//        self.navigationItem.leftBarButtonItem = userBarItem
    }
    
    func startUserHistory()  {
        let viewController = UIStoryboard(name:AllKeys.mainStoryBoard, bundle:nil).instantiateViewController(withIdentifier: AllKeys.paymentHistoryController) as! PaymentHistoryController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    func startUserProfile() {
         let viewController = UIStoryboard(name:AllKeys.mainStoryBoard, bundle:nil).instantiateViewController(withIdentifier: AllKeys.userProfileController) as! ProfileViewController
 
            let member = Member()
            member.avatar = currentUser.avatar
            member.firstName = currentUser.firstName
            member.lastName = currentUser.lastName
            member.id = currentUser.id
            member.isFollowing = true
            member.numberOfFollowing = currentUser.numberOfFollowers
            member.numberOfGroups = currentUser.numberOfGroups
            viewController.user = member
        
          self.navigationController?.pushViewController(viewController, animated: true)
    }
    
 
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return feedArray.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let feed = feedArray[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.givingTableCell.rawValue, for: indexPath) as! GivingTableCell
        cell.feed = feed
        cell.donationButton.tag = indexPath.row
        cell.donationButton.addTarget(self, action: #selector(GivingController.makeDonationButton(_:)), for: .touchUpInside)
        
        return cell
       
    }
  
    func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
        return false
    }
    
    //calculating the item height
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let height =  CGFloat(100);
        return height
    }
    
    //calling the giving action from the cell
    @IBAction func makeDonationButton(_ sender: UIButton) {
        self.showAction(giving: feedArray[sender.tag],sender: sender)
    }
    
    //showing the giving action
    func showAction(giving:Giving,sender:UIView) {
        if gateWays.isEmpty {
          ControllerHelpers.showDialog(targetVC: self, title: "Asoriba", message: "Sorry, No available payment options")
          return
        }
        
        let alertActionControl = UIAlertController.init(title: nil, message: "Pay with", preferredStyle: .actionSheet)
        for gateWay in gateWays {
            let name = gateWay.name
            if (name != "Absa"){
                let alertActionExpress = UIAlertAction.init(title: "Card", style: .default) {(Alert:UIAlertAction!) -> Void in
                    self.startPaymentDialog(giving: giving,gateWay: gateWay)
                }
                
                let alertActionExpress2 = UIAlertAction.init(title: "Mobile Money", style: .default) {(Alert:UIAlertAction!) -> Void in
                    self.startPaymentDialog(giving: giving,gateWay: gateWay)
                }
                alertActionControl.addAction(alertActionExpress)
                alertActionControl.addAction(alertActionExpress2)

            }
            
            print("print gateway: ", gateWay)
        }
      
        let alertActionCancel = UIAlertAction.init(title: "Cancel", style: .cancel) {(Alert:UIAlertAction!) -> Void in
        }
        
        alertActionControl.addAction(alertActionCancel)
        
        if let presenter = alertActionControl.popoverPresentationController {
            presenter.sourceView = sender
            presenter.sourceRect = sender.bounds
        }
        self.present(alertActionControl,animated:true,completion:nil)
        
    }
    
    
    func showDialog(message:String) {
        let  alert = UIAlertController(title: nil, message: message,preferredStyle: .alert)
        
        let action = UIAlertAction(title: "Okay",style: UIAlertActionStyle.default,
                                        handler: {(alert: UIAlertAction!) in
              self.startChosingChurch()
            })
            alert.addAction(action)
        self.present(alert,animated: true,completion: nil)
    }
    

    func startChosingChurch() {
        guard let vc = UIStoryboard(name:"Main", bundle:nil).instantiateViewController(withIdentifier: AllKeys.chooseChurchController) as? ChooseChurchController else {
            return
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
  
    func didReceiveDonationError(results: String) {
        self.activityViewIndicator.stopAnimating()
        //
    }
    
    func didReceiveDonationDetail(results: String) {
        self.activityViewIndicator.stopAnimating()
        //
    }
    
    func didReceiveDonationSuccess(results: [Giving]) {
        self.refreshControl.endRefreshing()
        self.activityViewIndicator.stopAnimating()
        feedArray.removeAll()
        feedArray.append(contentsOf: results)
        givingTableView.reloadData()
        
        if (feedArray.isEmpty){
            self.emptyLabel.isHidden = false
        }
    }
    
    func didReceiveDonationNextUrl(results: String) {
        self.nextUrl = results
    }
    
    func startPaymentDialog(giving: Giving, gateWay:PaymentGateWays)  {
        let popOverVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: AllKeys.paymentPopOverController) as! PaymentPopOverController
        popOverVC.giving = giving
        popOverVC.gateWay = gateWay
        self.addChildViewController(popOverVC)
        popOverVC.view.frame = self.view.frame
        self.view.addSubview(popOverVC.view)
        popOverVC.didMove(toParentViewController: self)
        
        print("print giving details: ", giving)
        print("print gateway details: ", gateWay)
    }
    
    //calling the resfreshing endpoint
    func refresh()  {
        self.getData()
    }
}
