//
//  SearchFilterViewController.swift
//  asoriba
//
//  Created by Bright Ahedor on 10/05/2017.
//  Copyright © 2017 Asoriba Inc. All rights reserved.
//

import UIKit
import LBTAComponents

class SearchFilterViewController: UIViewController, UISearchBarDelegate {
    var type = "all"
    var category = "all"
    var multimedia = "all"
    var page = "?page=1"
    
    //@IBOutlet weak var filterBtnHeight: NSLayoutConstraint!
    //@IBOutlet weak var resetBtnHeight: NSLayoutConstraint!
    
    @IBOutlet weak var advancedFilter: UISwitch!
    @IBOutlet weak var mediaTypeLabel: UILabel!
    @IBOutlet weak var containerView: UIView!
    

    @IBAction func fromAsoribaUISwitchClick(_ sender: UISwitch) {
       
    }
    
    @IBAction func followingUISwitchClick(_ sender: UISwitch) {
       
    }
    
    @IBAction func mediaTypeAllUISwitchClick(_ sender: UISwitch) {
        if sender.isOn {
            self.mediaTextTypeUISwitch.isOn = false
            self.mediaTypeVideoUISwitch.isOn = false
            self.mediaTypeMusicUISwitch.isOn = false
            self.mediaTypeGalleryUISwitch.isOn = false
        }
    }
    
    @IBAction func textMediaTypeUISwitch(_ sender: UISwitch) {
        if (sender.isOn){
            self.mediaTypeAllUISwitch.isOn = false
        }
    }
    
    @IBAction func musicMediaTypeUISwitch(_ sender: UISwitch) {
        if (sender.isOn){
            self.mediaTypeAllUISwitch.isOn = false
        }
    }
    
    @IBAction func videoMediaTypeUISwitch(_ sender: UISwitch) {
        if (sender.isOn){
            self.mediaTypeAllUISwitch.isOn = false
        }
    }
    
    @IBAction func galleryMediaTypeUISwitch(_ sender: UISwitch) {
        if (sender.isOn){
            self.mediaTypeAllUISwitch.isOn = false
        }
    }
    
    @IBAction func contentTypeAllUISwitch(_ sender: UISwitch) {
        if sender.isOn {
            self.contentTypeDevotionUISwitch.isOn = false
            self.contentTypeEventUISwitch.isOn = false
            self.contentTypeNewsUISwitch.isOn = false
        }
    }
    
    @IBAction func devotionContentTypeUISwitch(_ sender: UISwitch) {
        if (sender.isOn){
            self.contentTyeAllUISwitch.isOn = false
        }
    }
    
    @IBAction func eventContentTypeUISwitch(_ sender: UISwitch) {
        if (sender.isOn){
            self.contentTyeAllUISwitch.isOn = false
        }
    }
    
    @IBAction func newContentTypeUISwitch(_ sender: UISwitch) {
        if (sender.isOn){
            self.contentTyeAllUISwitch.isOn = false
        }
    }
    
  
    
    @IBAction func resetButtonClick(_ sender: UIButton) {
       self.fromAsoribaUISwitch.isOn = true
       self.followingUISwitch.isOn = true
       self.mediaTypeAllUISwitch.isOn = true
       self.mediaTextTypeUISwitch.isOn = false
       self.mediaTypeMusicUISwitch.isOn = false
       self.mediaTypeVideoUISwitch.isOn = false
       self.mediaTypeGalleryUISwitch.isOn = false
       self.contentTyeAllUISwitch.isOn = true
       self.contentTypeDevotionUISwitch.isOn = false
       self.contentTypeEventUISwitch.isOn = false
       self.contentTypeNewsUISwitch.isOn = false
        
        UserDefaults.standard.setValue(true, forKey: "MEDIA_ALL")
        UserDefaults.standard.setValue(false, forKey: "MEDIA_TEXT")
        UserDefaults.standard.setValue(false, forKey: "MEDIA_MUSIC")
        UserDefaults.standard.setValue(false, forKey: "MEDIA_VIDEO")
        UserDefaults.standard.setValue(false, forKey: "MEDIA_GALLERY")
        
        UserDefaults.standard.setValue(true, forKey: "TYPE_ALL")
        UserDefaults.standard.setValue(false, forKey: "TYPE_DEVOTION")
        UserDefaults.standard.setValue(false, forKey: "TYPE_EVENT")
        UserDefaults.standard.setValue(false, forKey: "TYPE_NEWS")
        
        
        UserDefaults.standard.setValue(true, forKey: "FROM_NEWS")
        UserDefaults.standard.setValue(true, forKey: "FROM_CHURCH")
  
    }
    
    @IBAction func applyFilterButtonClick(_ sender: UIButton) {
        self.setTypeParam()
        self.setContentTypeParam()
        self.setMediaTypeParam()
        let url = "users/get_content/?page=1&page_size=10&feed_type=\(self.type)&multimedia_type=\(self.multimedia)&content_category=\(self.category)&keyword="
        NotificationCenter.default.post(name: Notification.Name(AllKeys.filterNotification), object: url)
        self.cancelAction()
    }
    
    @IBOutlet weak var filterElementView: UIView!
    @IBOutlet weak var filterFromAsoriba: UILabel!
    
    @IBOutlet weak var resetBtn: UIButton!
    @IBOutlet weak var filterBtn: UIButton!
    
    @IBOutlet weak var fromAsoribaUISwitch: UISwitch!
    @IBOutlet weak var followingUISwitch: UISwitch!
    @IBOutlet weak var mediaTypeAllUISwitch: UISwitch!
    @IBOutlet weak var mediaTextTypeUISwitch: UISwitch!
    @IBOutlet weak var mediaTypeMusicUISwitch: UISwitch!
    @IBOutlet weak var mediaTypeVideoUISwitch: UISwitch!
    @IBOutlet weak var mediaTypeGalleryUISwitch: UISwitch!
    @IBOutlet weak var contentTyeAllUISwitch: UISwitch!
    @IBOutlet weak var contentTypeDevotionUISwitch: UISwitch!
    @IBOutlet weak var contentTypeEventUISwitch: UISwitch!
    @IBOutlet weak var contentTypeNewsUISwitch: UISwitch!
    
    @IBOutlet weak var mediaTypeLabelAll: UILabel!
    @IBOutlet weak var mediaTypeLabelText: UILabel!
    @IBOutlet weak var mediaTypeLabelMusic: UILabel!
    @IBOutlet weak var mediaTypeLabelGallaries: UILabel!
    @IBOutlet weak var mediaTypeLabelVideos: UILabel!
    
    @IBOutlet weak var contentTypeLabel: UILabel!
    @IBOutlet weak var contentTypeLabelAll: UILabel!
    @IBOutlet weak var contentTypeLabelEvent: UILabel!
    @IBOutlet weak var contentTypeLabelDevotion: UILabel!
    @IBOutlet weak var contentTypeLabelNews: UILabel!
    @IBOutlet weak var advancedLabel: UILabel!
    
    @IBOutlet weak var firstline: UIView!
    @IBOutlet weak var secondLine: UIView!
    
    @IBAction func advancedSwitch(_ sender: UISwitch) {
        switching()
    }
    
    func setContentTypeParam() {
        
        UserDefaults.standard.setValue(self.contentTyeAllUISwitch.isOn, forKey: "TYPE_ALL")
        UserDefaults.standard.setValue(self.contentTypeDevotionUISwitch.isOn, forKey: "TYPE_DEVOTION")
        UserDefaults.standard.setValue(self.contentTypeEventUISwitch.isOn, forKey: "TYPE_EVENT")
        UserDefaults.standard.setValue(self.contentTypeNewsUISwitch.isOn, forKey: "TYPE_NEWS")
        
    
        if self.contentTyeAllUISwitch.isOn {
            self.category = "all"
        } else {
            self.category = ""
            if self.contentTypeDevotionUISwitch.isOn {
              self.category.append("devotion")
            }
            if self.contentTypeEventUISwitch.isOn {
                if (self.category.isEmpty){
                  self.category.append("event")
                } else{
                  self.category.append(",event")
                }
            }
            if self.contentTypeNewsUISwitch.isOn {
                if (self.category.isEmpty){
                    self.category.append("news")
                } else{
                    self.category.append(",news")
                }
            }
        }
    }
    
    func setTypeParam() {
        
        UserDefaults.standard.setValue(self.fromAsoribaUISwitch.isOn, forKey: "FROM_NEWS")
        UserDefaults.standard.setValue(self.followingUISwitch.isOn, forKey: "FROM_CHURCH")
      
        
        if self.fromAsoribaUISwitch.isOn {
            self.type = "news"
        } else if self.followingUISwitch.isOn {
           self.type = "church"
        }else{
           self.type = "all"
        }
    }
    
    func setMediaTypeParam() {
        
        UserDefaults.standard.setValue(self.mediaTypeAllUISwitch.isOn, forKey: "MEDIA_ALL")
        UserDefaults.standard.setValue(self.mediaTextTypeUISwitch.isOn, forKey: "MEDIA_TEXT")
        UserDefaults.standard.setValue(self.mediaTypeMusicUISwitch.isOn, forKey: "MEDIA_MUSIC")
        UserDefaults.standard.setValue(self.mediaTypeVideoUISwitch.isOn, forKey: "MEDIA_VIDEO")
        UserDefaults.standard.setValue(self.mediaTypeGalleryUISwitch.isOn, forKey: "MEDIA_GALLERY")
        
        if self.mediaTypeAllUISwitch.isOn {
            self.multimedia = "all"
        } else {
            self.multimedia = ""
            if self.mediaTextTypeUISwitch.isOn {
                self.multimedia.append("text")
            }
            if self.mediaTypeMusicUISwitch.isOn {
                if (self.multimedia.isEmpty){
                    self.multimedia.append("music")
                } else{
                    self.multimedia.append(",music")
                }
            }
            if self.mediaTypeVideoUISwitch.isOn {
                if (self.multimedia.isEmpty){
                    self.multimedia.append("video")
                } else{
                    self.multimedia.append(",video")
                }
            }
            if self.mediaTypeGalleryUISwitch.isOn {
                if (self.multimedia.isEmpty){
                    self.multimedia.append("gallery")
                } else{
                    self.multimedia.append(",gallery")
                }
            }
        }
    }
    
    //search Bar
    lazy var searchBar : UISearchBar = {
        let sb = UISearchBar()
        sb.placeholder = "Search Posts"
        sb.barTintColor = UIColor.white//(r: 28, g: 90, b: 65)
        sb.translatesAutoresizingMaskIntoConstraints = false
        sb.delegate = self
        UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).backgroundColor = UIColor(r: 28, g: 90, b: 65)
        return sb
    }()
    
    let greenView : UIView = {
        let searchView = UIView()
        searchView.backgroundColor = .green
        searchView.translatesAutoresizingMaskIntoConstraints = false
        return searchView
    }()
    
    let greenViewLayer : UIView = {
        let searchView = UIView()
        searchView.backgroundColor = .green
        searchView.translatesAutoresizingMaskIntoConstraints = false
        return searchView
    }()
    
    let closeBtn : UIButton = {
        let btn = UIButton(type: .system)
        btn.setBackgroundImage(UIImage(named: "close_sign"), for: UIControlState.normal)
        btn.translatesAutoresizingMaskIntoConstraints = true
        return btn
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.filterElementView.layer.cornerRadius = 0
        self.filterElementView.layer.masksToBounds = true
        
        let path = Bundle.main.path(forResource: "configuration", ofType: "plist")
        let root = NSDictionary(contentsOfFile: path!)
        
        //let viewBackground = (root?["headerYellowColor"] as! String)
        let onTintColor = (root?["headerBackgroundColor"] as! String)
        let fromAppName = (root?["fromAppName"] as! String)
        let whiteColor = (root?["tint_white"] as! String)
        
        self.filterElementView.backgroundColor = UIColor().HexToColor(hexString: whiteColor, alpha: 0.9)
        self.fromAsoribaUISwitch.onTintColor = UIColor().HexToColor(hexString: onTintColor)
        self.followingUISwitch.onTintColor = UIColor().HexToColor(hexString: onTintColor)
        self.mediaTypeAllUISwitch.onTintColor = UIColor().HexToColor(hexString: onTintColor)
        self.mediaTextTypeUISwitch.onTintColor = UIColor().HexToColor(hexString: onTintColor)
        self.mediaTypeMusicUISwitch.onTintColor = UIColor().HexToColor(hexString: onTintColor)
        self.mediaTypeVideoUISwitch.onTintColor = UIColor().HexToColor(hexString: onTintColor)
        self.mediaTypeGalleryUISwitch.onTintColor = UIColor().HexToColor(hexString: onTintColor)
        self.contentTyeAllUISwitch.onTintColor = UIColor().HexToColor(hexString: onTintColor)
        self.contentTypeEventUISwitch.onTintColor = UIColor().HexToColor(hexString: onTintColor)
        self.contentTypeNewsUISwitch.onTintColor = UIColor().HexToColor(hexString: onTintColor)
        self.contentTypeDevotionUISwitch.onTintColor = UIColor().HexToColor(hexString: onTintColor)
        
        self.fromAsoribaUISwitch.tintColor = UIColor().HexToColor(hexString: onTintColor)
        self.followingUISwitch.tintColor = UIColor().HexToColor(hexString: onTintColor)
        self.mediaTypeAllUISwitch.tintColor = UIColor().HexToColor(hexString: onTintColor)
        self.mediaTextTypeUISwitch.tintColor = UIColor().HexToColor(hexString: onTintColor)
        self.mediaTypeMusicUISwitch.tintColor = UIColor().HexToColor(hexString: onTintColor)
        self.mediaTypeVideoUISwitch.tintColor = UIColor().HexToColor(hexString: onTintColor)
        self.mediaTypeGalleryUISwitch.tintColor = UIColor().HexToColor(hexString: onTintColor)
        self.contentTyeAllUISwitch.tintColor = UIColor().HexToColor(hexString: onTintColor)
        self.contentTypeEventUISwitch.tintColor = UIColor().HexToColor(hexString: onTintColor)
        self.contentTypeNewsUISwitch.tintColor = UIColor().HexToColor(hexString: onTintColor)
        self.contentTypeDevotionUISwitch.tintColor = UIColor().HexToColor(hexString: onTintColor)
        
        self.resetBtn.setTitleColor(UIColor().HexToColor(hexString: whiteColor), for: .normal)
        self.filterBtn.backgroundColor = UIColor().HexToColor(hexString: onTintColor)
        self.filterBtn.setTitleColor(UIColor().HexToColor(hexString: whiteColor), for: .normal)
        self.filterBtn.layer.cornerRadius = 0
        self.filterBtn.layer.borderWidth = 1
        self.filterBtn.layer.borderColor = UIColor().HexToColor(hexString: whiteColor).cgColor

        self.filterBtn.layer.masksToBounds = true
        self.filterFromAsoriba.text = fromAppName
        
        self.resetBtn.layer.borderWidth = 1
        self.resetBtn.layer.borderColor = UIColor().HexToColor(hexString: whiteColor).cgColor
        self.resetBtn.backgroundColor = UIColor().HexToColor(hexString: onTintColor)
        self.resetBtn.layer.cornerRadius = 0
        self.resetBtn.layer.masksToBounds = true
        
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.8)
        self.showAnimate()
        //let tap = UITapGestureRecognizer(target: self, action: #selector(self.closePopUp(_:)))
        //self.view.addGestureRecognizer(tap)
        self.view.isUserInteractionEnabled = true
        
        
        
        let MEDIA_ALL = UserDefaults.standard.bool(forKey: "MEDIA_ALL")
        let MEDIA_TEXT = UserDefaults.standard.bool(forKey: "MEDIA_TEXT")
        let MEDIA_MUSIC = UserDefaults.standard.bool(forKey: "MEDIA_MUSIC")
        let MEDIA_VIDEO = UserDefaults.standard.bool(forKey: "MEDIA_VIDEO")
        let MEDIA_GALLERY = UserDefaults.standard.bool(forKey: "MEDIA_GALLERY")
        
        self.mediaTypeAllUISwitch.isOn = MEDIA_ALL
        self.mediaTextTypeUISwitch.isOn = MEDIA_TEXT
        self.mediaTypeMusicUISwitch.isOn = MEDIA_MUSIC
        self.mediaTypeVideoUISwitch.isOn = MEDIA_VIDEO
        self.mediaTypeGalleryUISwitch.isOn = MEDIA_GALLERY
        
        if (!MEDIA_TEXT && !MEDIA_MUSIC && !MEDIA_VIDEO && !MEDIA_GALLERY){
           self.mediaTypeAllUISwitch.isOn = true
        }
        
        
        let FROM_NEWS = UserDefaults.standard.bool(forKey: "FROM_NEWS")
        let FROM_CHURCH = UserDefaults.standard.bool(forKey: "FROM_CHURCH")
        
        
        self.fromAsoribaUISwitch.isOn = FROM_NEWS
        self.followingUISwitch.isOn = FROM_CHURCH
        
        
        
        let TYPE_ALL = UserDefaults.standard.bool(forKey: "TYPE_ALL")
        let TYPE_DEVOTION = UserDefaults.standard.bool(forKey: "TYPE_DEVOTION")
        let TYPE_EVENT = UserDefaults.standard.bool(forKey: "TYPE_EVENT")
        let TYPE_NEWS = UserDefaults.standard.bool(forKey: "TYPE_NEWS")
        
        
        self.contentTyeAllUISwitch.isOn = TYPE_ALL
        self.contentTypeDevotionUISwitch.isOn = TYPE_DEVOTION
        self.contentTypeEventUISwitch.isOn = TYPE_EVENT
        self.contentTypeNewsUISwitch.isOn = TYPE_NEWS
        
        if (!TYPE_DEVOTION && !TYPE_EVENT && !TYPE_NEWS){
            self.contentTyeAllUISwitch.isOn = true
        }
        
        switching()
        setUpViews()
        
        self.filterBtn.translatesAutoresizingMaskIntoConstraints = false
        self.resetBtn.translatesAutoresizingMaskIntoConstraints = false
        
        self.filterElementView.isUserInteractionEnabled = true
        self.filterElementView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleKeyboard)))
        
    }
    
    func handleKeyboard(){
        endEditing()

    }
    
    func setUpViews(){
        self.searchBar.delegate = self

        self.filterElementView.addSubview(searchBar)
        self.filterElementView.addSubview(closeBtn)
        
        searchBar.anchor(filterElementView.topAnchor, left: filterElementView.leftAnchor, bottom: nil, right: filterElementView.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 50)
        
        closeBtn.anchor(searchBar.bottomAnchor, left: nil, bottom: nil, right: searchBar.rightAnchor, topConstant: 8, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 40, heightConstant: 30)

        
        self.closeBtn.addTarget(self, action: #selector(handleClose), for: UIControlEvents.touchUpInside)
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        
        
    }
    
    func endEditing(){
        self.filterElementView.endEditing(true)
        searchBar.resignFirstResponder()
    }
    
    var text = ""
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        print("print text typed: \(searchText)")
        text = searchText
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.setTypeParam()
        self.setContentTypeParam()
        self.setMediaTypeParam()
        let url = "users/get_content/\(page)&page_size=10&feed_type=all&multimedia_type=all&content_category=all&keyword=\(text)"
        NotificationCenter.default.post(name: Notification.Name(AllKeys.filterNotification), object: url)
        self.cancelAction()
    }

    
    func switching(){
        
        if advancedFilter.isOn {
            
            self.mediaTypeLabel.isHidden = false
            self.mediaTypeAllUISwitch.isHidden  = false
            self.mediaTextTypeUISwitch.isHidden = false
            self.mediaTypeMusicUISwitch.isHidden = false
            self.mediaTypeVideoUISwitch.isHidden = false
            self.mediaTypeGalleryUISwitch.isHidden = false
            self.contentTyeAllUISwitch.isHidden = false
            self.contentTypeEventUISwitch.isHidden = false
            self.contentTypeDevotionUISwitch.isHidden = false
            self.contentTypeNewsUISwitch.isHidden = false
            
            self.mediaTypeLabelAll.isHidden = false
            self.mediaTypeLabelText.isHidden = false
            self.mediaTypeLabelMusic.isHidden = false
            self.mediaTypeLabelGallaries.isHidden = false
            self.mediaTypeLabelVideos.isHidden = false
            
            self.contentTypeLabelAll.isHidden = false
            self.contentTypeLabelEvent.isHidden = false
            self.contentTypeLabelDevotion.isHidden = false
            self.contentTypeLabelNews.isHidden = false
            self.contentTypeLabel.isHidden = false
            
            self.firstline.isHidden = false
            self.secondLine.isHidden = false
            
        }else{
            
            self.mediaTypeLabel.isHidden = true
            self.mediaTypeAllUISwitch.isHidden = true
            self.mediaTextTypeUISwitch.isHidden = true
            self.mediaTypeMusicUISwitch.isHidden = true
            self.mediaTypeVideoUISwitch.isHidden = true
            self.mediaTypeGalleryUISwitch.isHidden = true
            self.contentTyeAllUISwitch.isHidden = true
            self.contentTypeEventUISwitch.isHidden = true
            self.contentTypeDevotionUISwitch.isHidden = true
            self.contentTypeNewsUISwitch.isHidden = true
            
            self.mediaTypeLabelAll.isHidden = true
            self.mediaTypeLabelText.isHidden = true
            self.mediaTypeLabelMusic.isHidden = true
            self.mediaTypeLabelGallaries.isHidden = true
            self.mediaTypeLabelVideos.isHidden = true

            self.contentTypeLabelAll.isHidden = true
            self.contentTypeLabelEvent.isHidden = true
            self.contentTypeLabelDevotion.isHidden = true
            self.contentTypeLabelNews.isHidden = true
            self.contentTypeLabel.isHidden = true
            
            //self.firstline.isHidden = true
            self.secondLine.isHidden = true

        }
    }
    
    func handleClose(){
        closePopUp()
    }

    //close action on the parent view
    func closePopUp(_ sender: UITapGestureRecognizer) {
        self.view.endEditing(true)
        self.removeAnimate()
        self.view.removeFromSuperview()
    }
    
    //close action on the parent view
    func closePopUp() {
        self.view.endEditing(true)
        //self.view.removeFromSuperview
        self.removeAnimate()
    }
    
    //close action on the button
    func cancelAction() {
        self.removeAnimate()
        self.view.removeFromSuperview()
    }
    
    
    func showAnimate()
    {
        self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view.alpha = 0.0;
        UIView.animate(withDuration: 0.25, animations: {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        });
    }
    
    func removeAnimate()
    {
        UIView.animate(withDuration: 0.25, animations: {
            self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view.alpha = 0.0;
        }, completion:{(finished : Bool)  in
            if (finished)
            {
                self.view.removeFromSuperview()
            }
        });
    }

}
