//
//  HistoryTableCell.swift
//  asoriba
//
//  Created by Bright Ahedor on 23/12/2016.
//  Copyright © 2016 Asoriba Inc. All rights reserved.
//

import UIKit

class HistoryTableCell: UITableViewCell {
    
    //@IBOutlet weak var contentSubLabel: UILabel!
    //@IBOutlet weak var contentLabel: UILabel!
    //@IBOutlet weak var contentMiniSubLabel: UILabel!
    
    let backView : UIView = {
        let v = UIView()
        v.translatesAutoresizingMaskIntoConstraints = false
        v.layer.shadowColor = UIColor.lightGray.cgColor
        v.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        v.layer.shadowRadius = 2.0
        v.layer.shadowOpacity = 1.0
        v.layer.masksToBounds = false
        return v
    }()
    
    let contentLabel : UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.font = UIFont.boldSystemFont(ofSize: 17)
        return lbl
    }()
    
    let remarks : UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.numberOfLines = 8
        lbl.font = UIFont.systemFont(ofSize: 15)
        return lbl
    }()

    let amountAndCurrency : UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.font = UIFont.boldSystemFont(ofSize: 14)
        lbl.textColor = UIColor.black
        return lbl
    }()

    let dateAndtime : UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.font = UIFont.boldSystemFont(ofSize: 13)
        lbl.textColor = UIColor.lightGray
        return lbl
    }()

    let checkImg : UIImageView = {
        let img = UIImageView()
        img.image = UIImage(named: "credit")?.withRenderingMode(.alwaysTemplate)
        img.tintColor = UIColor().HexToColor(hexString: "#004F2F")
        img.translatesAutoresizingMaskIntoConstraints = false
        img.contentMode = .scaleAspectFill
        return img
    }()
    
    var feed: PaymentHistory!{
        didSet {
          updateUI()
        }
    }

    
    func updateUI() {
        
        self.layer.shadowColor = UIColor.lightGray.cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        self.layer.shadowRadius = 2.0
        self.layer.shadowOpacity = 1.0
        self.layer.masksToBounds = false
        self.layer.shadowPath = UIBezierPath(roundedRect: self.bounds, cornerRadius: self.contentView.layer.cornerRadius).cgPath

        self.backView.layer.shadowColor = UIColor.lightGray.cgColor
        self.backView.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        self.backView.layer.shadowRadius = 2.0
        self.backView.layer.shadowOpacity = 1.0
        self.backView.layer.masksToBounds = false
        self.backView.layer.cornerRadius = 5

        addSubview(backView)
        backView.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 5).isActive = true
        backView.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -5).isActive = true
        backView.topAnchor.constraint(equalTo: self.topAnchor, constant: 5).isActive = true
        backView.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -5).isActive = true
        
        backView.addSubview(contentLabel)
        contentLabel.topAnchor.constraint(equalTo: backView.topAnchor, constant: 5).isActive = true
        contentLabel.leftAnchor.constraint(equalTo: backView.leftAnchor, constant: 5).isActive = true
        contentLabel.rightAnchor.constraint(equalTo: backView.rightAnchor, constant: -100).isActive = true
        contentLabel.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        backView.addSubview(dateAndtime)
        dateAndtime.topAnchor.constraint(equalTo: contentLabel.topAnchor).isActive = true
        dateAndtime.bottomAnchor.constraint(equalTo: contentLabel.bottomAnchor).isActive = true
        dateAndtime.leftAnchor.constraint(equalTo: contentLabel.rightAnchor).isActive = true
        dateAndtime.rightAnchor.constraint(equalTo: backView.rightAnchor).isActive = true

        backView.addSubview(amountAndCurrency)
        amountAndCurrency.bottomAnchor.constraint(equalTo: backView.bottomAnchor, constant: 0).isActive = true
        amountAndCurrency.leftAnchor.constraint(equalTo: backView.leftAnchor, constant: 5).isActive = true
        amountAndCurrency.heightAnchor.constraint(equalToConstant: 30).isActive = true
        amountAndCurrency.rightAnchor.constraint(equalTo: backView.rightAnchor, constant: -50).isActive = true
        
        backView.addSubview(remarks)
        remarks.topAnchor.constraint(equalTo: contentLabel.bottomAnchor, constant: 0).isActive = true
        remarks.leftAnchor.constraint(equalTo: contentLabel.leftAnchor).isActive = true
        remarks.rightAnchor.constraint(equalTo: backView.rightAnchor).isActive = true
        remarks.bottomAnchor.constraint(equalTo: amountAndCurrency.topAnchor, constant: 0).isActive = true
        
        backView.addSubview(checkImg)
        checkImg.leftAnchor.constraint(equalTo: amountAndCurrency.rightAnchor).isActive = true
        checkImg.rightAnchor.constraint(equalTo: backView.rightAnchor, constant: -5).isActive = true
        checkImg.topAnchor.constraint(equalTo: remarks.bottomAnchor).isActive = true
        checkImg.bottomAnchor.constraint(equalTo: backView.bottomAnchor, constant: -10).isActive = true
        
        self.contentLabel.text = feed.typeOfTransactions
        self.remarks.text  = feed.remarks
        self.amountAndCurrency.text = feed.currency + " " + feed.amount
        self.dateAndtime.text = feed.recordDate
    }
    
}
