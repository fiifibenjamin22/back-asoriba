//
//  forumLog.swift
//  asoriba
//
//  Created by Benjamin Acquah on 06/10/2017.
//  Copyright © 2017 Asoriba Inc. All rights reserved.
//

import UIKit

class forumLog : NSObject {
    
    var id : String?
    var username : String?
    var message : String?
    var created : Double? = 0.0
}

class snapShotForumLog : NSObject {
    
    var snapshotValueObject : String?
    
}
