//
//  ChurchTableCell.swift
//  asoriba
//
//  Created by Bright Ahedor on 19/12/2016.
//  Copyright © 2016 Asoriba Inc. All rights reserved.
//

import UIKit
import AlamofireImage

class ChurchTableCell: UITableViewCell {
  
    @IBOutlet weak var churchImageView: UIImageView!
    @IBOutlet weak var churchLogoImageView: UIImageView!
    @IBOutlet weak var churchNameLabel: UILabel!
    @IBOutlet weak var churchBranchLabel: UILabel!
    @IBOutlet weak var followChurchButton: UIButton!
    
    @IBOutlet weak var joinChurchButton: UIButton!
    var feed: Church!{
        didSet {
         updateUI()
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func updateUI() {
     
        if var _ = self.churchNameLabel {
            self.churchNameLabel.text = feed.name
        }
        
        if var _ = self.churchBranchLabel {
             self.churchBranchLabel.text = feed.branchName
        }
        
        
        //content image
        if  feed.image != "" {
            self.churchImageView.af_setImage(
                withURL: URL(string: (feed.image))!,
                placeholderImage: UIImage(named: "default"),
                imageTransition: .crossDissolve(0.2)
            )
        }else
        {
            self.churchImageView.image = UIImage(named: "default")
        }
        
        //content image
        if  !feed.churchLogo.isEmpty {
            self.churchLogoImageView.af_setImage(
                withURL: URL(string: (feed.churchLogo))!,
                placeholderImage: UIImage(named: "default"),
                imageTransition: .crossDissolve(0.2)
            )
        }else
        {
            self.churchLogoImageView.image = UIImage(named: "default")
        }
        
        
        //following login
        if (feed.isFollowing){
             self.followChurchButton.setTitle("Following", for: .normal)
        } else {
            self.followChurchButton.setTitle("Follow", for: .normal)
        }
        
        self.followChurchButton.backgroundColor = .clear
        self.followChurchButton.layer.cornerRadius = 5
        self.followChurchButton.layer.borderWidth = 1
        self.followChurchButton.layer.borderColor = UIColor.gray.cgColor
        
        self.followChurchButton.translatesAutoresizingMaskIntoConstraints = false
        self.followChurchButton.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        self.joinChurchButton.backgroundColor = .clear
        self.joinChurchButton.layer.cornerRadius = 5
        self.joinChurchButton.layer.borderWidth = 1
        self.joinChurchButton.layer.borderColor = UIColor.gray.cgColor
        
        self.joinChurchButton.translatesAutoresizingMaskIntoConstraints = false
        self.joinChurchButton.heightAnchor.constraint(equalToConstant: 40).isActive = true
    }
    
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        self.churchImageView.af_cancelImageRequest()
        self.churchLogoImageView.af_cancelImageRequest()

        self.churchImageView.layer.removeAllAnimations()
        self.churchImageView.image = nil
        self.churchLogoImageView.layer.removeAllAnimations()
        self.churchLogoImageView.image = nil
    }
    
}
