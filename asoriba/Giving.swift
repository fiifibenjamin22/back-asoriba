//
//  Giving.swift
//  asoriba
//
//  Created by Bright Ahedor on 15/12/2016.
//  Copyright © 2016 Asoriba Inc. All rights reserved.
//

import Foundation
class Giving {
    var id = ""
    var title = ""
    var description = ""
    var pledgeId = ""
    var pledgeAmount = ""
}
