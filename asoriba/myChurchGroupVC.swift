//
//  myChurchMembersVC.swift
//  asoriba
//
//  Created by Benjamin Acquah on 03/11/2017.
//  Copyright © 2017 Asoriba Inc. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage
import Photos
import SKPhotoBrowser
import Toaster
import Crashlytics

class myChurchGroupVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource ,UICollectionViewDelegateFlowLayout ,GroupsServiceProtocol {
    
    var titleText = ""
    
    var api:ApiService!
    
    var positionSelected = 0
    var filteredUsers = [Member]()
    var membersData = [Member]()
    var groupsData = [Group]()
    var feedsData = [Feed]()
    var userData = DataOperation.getUser()
    var churchItem: Church!
    var feedNextUrl = ""
    var membersNextUrl = ""
    var groupsNextUrl = ""
    var loadedPages = [String]()
    var text = ""
//    var activityViewIndicator:UIActivityIndicatorView = UIActivityIndicatorView()
//    var refreshControl: UIRefreshControl = UIRefreshControl();
    var isRefreshing = false
    
    
    lazy var collectionView : UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.delegate = self
        cv.dataSource = self
        cv.alwaysBounceVertical = true
        cv.showsVerticalScrollIndicator = false
        
        return cv
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationItem.backBarButtonItem?.title = ""
        
        //api instance
        api = ApiService()
        self.api.churchGroupsDelegate = self
        
        collectionView.register(theNewGroupcell.self, forCellWithReuseIdentifier: cellIdentify)
        collectionView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0)
        collectionView.backgroundColor = UIColor.groupTableViewBackground
        
        setUpViews()
        
        self.navigationItem.title = "Church Groups"
        
        //present data
        if !userData.churchId.isEmpty {
            self.getGroups(url: "page=1",churchId: userData.churchId)
        }

    }
    
    func getGroups(url:String,churchId:String) {
        api.getChurchGroups(churchId: churchId,next:url)
    }
    
    func setUpViews(){
        
        view.addSubview(collectionView)

        view.addConstraintsWithFormat("H:|[v0]|", views: collectionView)
        view.addConstraintsWithFormat("V:|[v0]|", views: collectionView)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return groupsData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let feed = groupsData[indexPath.row]
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentify, for: indexPath) as! theNewGroupcell
        cell.feed = feed
        
        //gesture section image
        cell.followBtn.tag = indexPath.row
        cell.followBtn.addTarget(self, action: #selector(self.startGroupDetail(_:)), for: UIControlEvents.touchUpInside)
        //end of gesture
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.view.frame.size.width, height: 150)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    
    //MARK: -- group detail
    func startGroupDetail(_ sender: UIButton)  {
        let group = groupsData[(sender.tag)]
        
        let myAlert = UIAlertController(title: "About \(group.name)", message: "", preferredStyle: UIAlertControllerStyle.alert)
        let joinRequest = UIAlertAction(title: "Send request To Join Group", style: UIAlertActionStyle.default, handler: nil)
        let aboutGroup = UIAlertAction(title: "About Group", style: UIAlertActionStyle.default) { (alert) in
            
            if group.isJoined{
                ControllerHelpers.showMessage(targetVC: self, title: "About: \(group.name)", message: "\(group.description)")
                
            }else{
                
                ControllerHelpers.showMessage(targetVC: self, title: "About: \(group.name)", message: "\(group.description)")
            }
            
        }
//        let yes = UIAlertAction(title: "Yes", style: UIAlertActionStyle.default) { (alert) in
//
//            sender.isEnabled = false
//            sender.setTitle("Joined", for: .normal)
//            let feed = self.groupsData[sender.tag]
//            self.api.joingChurchGroup(groupId: feed.id)
//        }
        let no = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil)
        
        myAlert.addAction(joinRequest)
        myAlert.addAction(aboutGroup)
        myAlert.addAction(no)
//        myAlert.addAction(yes)
        self.present(myAlert, animated: true, completion: nil)
        
    }
    
    func didReceiveGroupsSuccess(results: [Group]) {
        self.groupsData.append(contentsOf: results)
        self.collectionView.reloadData()
    }
    
    func didReceiveGroupsError(results: String) {
        
    }
    
    func didReceiveGroupsDetail(results: String) {
        
    }
    
    func didReceiveChurchGroupsNextUrl(results: String) {
        self.groupsNextUrl = results
    }

}

