//
//  usersCell.swift
//  asoriba
//
//  Created by Benjamin Acquah on 11/09/2017.
//  Copyright © 2017 Asoriba Inc. All rights reserved.
//

import UIKit
import LBTAComponents
import AlamofireImage
import Alamofire
import Firebase

class usersCell: UICollectionViewCell {
    
    let userProfileImg : UIImageView = {
        let img = UIImageView()
        img.image = UIImage(named: "default")
        img.contentMode = .scaleAspectFill
        img.translatesAutoresizingMaskIntoConstraints = false
        img.layer.cornerRadius = 22
        img.layer.masksToBounds = true
        return img
    }()
    
    let userName : UILabel = {
        let lbl = UILabel()
        lbl.text = "User Name"
        lbl.font = UIFont.systemFont(ofSize: 16)
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()

    let profileStatus : UILabel = {
        let lbl = UILabel()
        lbl.text = "Oh! Lord you i will praise..."
        lbl.font = UIFont.systemFont(ofSize: 14)
        lbl.textColor = UIColor.gray
        lbl.numberOfLines = 2
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let seperator : UILabel = {
        let lbl = UILabel()
        lbl.backgroundColor = UIColor.gray
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    private var placeHolderImage = UIImage(named: "default")
    
    let user = DataOperation.getUser()
    var noAction : Bool!{
        didSet {

        }
    }
    
    
    var users: Member!{
        didSet{
            updateUI()
        }
    }
        
    func updateUI()   {
        
        //setting counts
        self.profileStatus.text = "@\(users.otherName)"
        self.userName.text = users.lastName + " " + users.firstName
        
        //content image
        if  users.avatar != "" {
            self.userProfileImg.af_setImage(
                withURL: URL(string: users.avatar)!,
                placeholderImage: placeHolderImage,
                imageTransition: .crossDissolve(0.2)
            )
        }else {
            self.userProfileImg.image = placeHolderImage
        }
        
        
        if self.users.id == self.user.id {
            self.removeFromSuperview()
        } else{
            
        }
        
    }
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setUpViews()
    }
    
    func setUpViews(){
        
        addSubview(userProfileImg)
        addSubview(userName)
        addSubview(profileStatus)
        addSubview(seperator)
        
        userProfileImg.anchor(topAnchor, left: leftAnchor, bottom: nil, right: nil, topConstant: 8, leftConstant: 8, bottomConstant: 0, rightConstant: 0, widthConstant: 44, heightConstant: 44)
        
        userName.anchor(userProfileImg.topAnchor, left: userProfileImg.rightAnchor, bottom: nil, right: rightAnchor, topConstant: 0, leftConstant: 8, bottomConstant: 0, rightConstant: 8, widthConstant: 0, heightConstant: 20)
        
        profileStatus.anchor(userName.bottomAnchor, left: userName.leftAnchor, bottom: userProfileImg.bottomAnchor, right: userName.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        seperator.anchor(profileStatus.bottomAnchor, left: userProfileImg.leftAnchor, bottom: nil, right: profileStatus.rightAnchor, topConstant: 5, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0.5)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
