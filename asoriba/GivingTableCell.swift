//
//  GivingTableCell.swift
//  asoriba
//
//  Created by Bright Ahedor on 18/12/2016.
//  Copyright © 2016 Asoriba Inc. All rights reserved.
//

import UIKit
import LBTAComponents

class GivingTableCell: UITableViewCell {
//    @IBOutlet weak var titleLabel: UILabel!
//    @IBOutlet weak var descriptionLabel: UILabel!
//    @IBOutlet weak var donationButton: UIButton!
    
    let backView : UIView = {
        let v = UIView()
        v.translatesAutoresizingMaskIntoConstraints = false
        v.layer.shadowColor = UIColor.lightGray.cgColor
        v.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        v.layer.shadowRadius = 2.0
        v.layer.shadowOpacity = 1.0
        v.layer.masksToBounds = false
        v.layer.cornerRadius = 5
        v.backgroundColor = .white
        return v
    }()

    let titleLabel : UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.font = UIFont.boldSystemFont(ofSize: 17)
        lbl.backgroundColor = .white
        return lbl
    }()
    
    let descriptionLabel : UITextView = {
        let lbl = UITextView()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.font = UIFont.systemFont(ofSize: 14)
        lbl.textColor = UIColor.darkGray
        lbl.isEditable = false
        lbl.isScrollEnabled = false
        lbl.isScrollEnabled = false
        lbl.isSelectable = false
        return lbl
    }()

    let donationButton : UIButton = {
        let btn = UIButton(type: UIButtonType.system)
        btn.setTitle("Make Donation", for: UIControlState.normal)
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.backgroundColor = .red
        btn.layer.borderWidth = 2
        btn.layer.borderColor = UIColor.white.cgColor
        btn.layer.cornerRadius = 5
        btn.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        btn.setTitleColor(UIColor.white, for: .normal)
        return btn
    }()
    
    
    var parentController: UIViewController!
    
    var feed: Giving!{
        didSet{
            updateUI()
        }
    }
    
    
    
    func updateUI() {
        
        backgroundColor = UIColor.groupTableViewBackground
                
        let path = Bundle.main.path(forResource: "configuration", ofType: "plist")
        let root = NSDictionary(contentsOfFile: path!)
        
        let buttonBackgroundColor = (root?["headerBackgroundColor"] as! String)
        
        self.donationButton.backgroundColor = UIColor().HexToColor(hexString: buttonBackgroundColor)
        
        addSubview(backView)
        backView.addSubview(titleLabel)
        backView.addSubview(descriptionLabel)
        backView.addSubview(donationButton)
        
        backView.anchor(self.topAnchor, left: self.leftAnchor, bottom: self.bottomAnchor, right: self.rightAnchor, topConstant: 5, leftConstant: 5, bottomConstant: 5, rightConstant: 5, widthConstant: 0, heightConstant: 0)
        
        titleLabel.anchor(backView.topAnchor, left: backView.leftAnchor, bottom: nil, right: backView.rightAnchor, topConstant: 5, leftConstant: 5, bottomConstant: 0, rightConstant: 5, widthConstant: 0, heightConstant: 20)
        
        descriptionLabel.anchor(titleLabel.bottomAnchor, left: titleLabel.leftAnchor, bottom: donationButton.topAnchor, right: titleLabel.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        donationButton.anchor(descriptionLabel.bottomAnchor, left: nil, bottom: backView.bottomAnchor, right: descriptionLabel.rightAnchor, topConstant: 1, leftConstant: 0, bottomConstant: 5, rightConstant: 5, widthConstant: 120, heightConstant: 0)
        
        self.titleLabel.text = feed.title
        self.descriptionLabel.text = feed.description
    
        
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
