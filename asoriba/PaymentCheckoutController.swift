//
//  PaymentCheckoutController.swift
//  asoriba
//
//  Created by Bright Ahedor on 21/12/2016.
//  Copyright © 2016 Asoriba Inc. All rights reserved.
//

import UIKit
import Crashlytics
import WebKit

class PaymentCheckoutController: UIViewController,UIWebViewDelegate {
    
    @IBOutlet weak var progressBarView: UIProgressView!
    var checkOutUrl: String!
    @IBOutlet weak var webView: UIWebView!
    private var hasFinishedLoading = false
    var timer: Timer!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let tag = "Check Out"
        self.navigationItem.title = tag
        
        self.webView.delegate = self
        
        let pageUrl = NSURL(string: checkOutUrl)
        if (pageUrl != nil){
            print("print payment url from checkout page: \(String(describing: pageUrl))")
            let request = NSURLRequest(url: pageUrl! as URL)
            self.webView.loadRequest(request as URLRequest)
        }else{
          print("Nil URL")
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        hasFinishedLoading = false
        self.funcToCallWhenStartLoadingYourWebview()
    }

    func webViewDidFinishLoad(_ webView: UIWebView) {
        if let link = webView.request?.url! {
            let check:String = link.relativeString
            print("print payment Return URL \(check)")
            if (check.contains(AllKeys.checkOutRedirect)){
                let vc = self.navigationController?.popViewController(animated: true)
                vc?.dismiss(animated: true, completion: nil)
            }
        }
        self.funcToCallCalledWhenUIWebViewFinishesLoading()
    }
    
    
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        
        return true
    }
    
    func funcToCallWhenStartLoadingYourWebview() {
        self.progressBarView.progress = 0.0
        self.hasFinishedLoading = false
        self.timer = Timer.scheduledTimer(timeInterval: 0.01667, target: self, selector: #selector(PaymentCheckoutController.timerCallback), userInfo: nil, repeats: true)
    }
    
    func funcToCallCalledWhenUIWebViewFinishesLoading() {
        //tracking analytics
        self.hasFinishedLoading = true
    }
    
    func timerCallback() {
        if self.hasFinishedLoading {
            if self.progressBarView.progress >= 1 {
                self.progressBarView.isHidden = true
                self.timer.invalidate()
            } else {
                self.progressBarView.progress += 0.1
            }
        } else {
            self.progressBarView.progress += 0.05
            if self.progressBarView.progress >= 0.95 {
                self.progressBarView.progress = 0.95
            }
        }
    }
    
}

//extension for paymentController
extension PaymentCheckoutController: WKNavigationDelegate {

    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {

        guard let url = navigationAction.request.url else {
            decisionHandler(.allow)
            return
        }

        if url.absoluteString.contains("api/checkout"){
            //for successful payment
            decisionHandler(.cancel)
            
            //_ = self.navigationController?.popViewController(animated: false)
        }else{
            decisionHandler(.allow)
        }
    }
}

