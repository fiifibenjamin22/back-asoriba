//
//  TabBarMoreVC.swift
//  asoriba
//
//  Created by Benjamin Acquah on 26/09/2017.
//  Copyright © 2017 Asoriba Inc. All rights reserved.
//

import UIKit
import AlamofireImage

class TabBarMoreVC: UITableViewController, UICollectionViewDelegateFlowLayout {

    //outlets from the more menu
    @IBOutlet weak var paymentsHistory: UILabel!
    @IBOutlet weak var searchChurch: UILabel!
    @IBOutlet weak var checkIns: UILabel!
    @IBOutlet weak var shareApp: UILabel!
    @IBOutlet weak var savedPost: UILabel!
    @IBOutlet weak var followers: UILabel!
    @IBOutlet weak var followings: UILabel!
    @IBOutlet weak var helpAndSupport: UILabel!
    @IBOutlet weak var forum: UILabel!
    @IBOutlet weak var chats: UILabel!
    @IBOutlet weak var viewProfile: UIView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var statistics: UILabel!
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var myfollowersImg: UIImageView!
    
    var settings : Mics!
    var api:ApiService!
    var userData = DataOperation.getUser()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = false

        self.userImage.contentMode = .scaleAspectFill
        self.userImage.layer.cornerRadius = 25
        self.userImage.layer.masksToBounds = true
        
        self.myfollowersImg.layer.cornerRadius = 5
        self.myfollowersImg.layer.masksToBounds = true
        self.myfollowersImg.contentMode = .scaleAspectFill
        
        self.userName.text = "\(userData.firstName) \(userData.lastName)"
        self.statistics.text = userData.branchName

        //content image
        let avatar = self.userData.avatar
        
        if  !(avatar.isEmpty) {
            self.userImage.af_setImage(
                withURL: URL(string: (avatar))!,
                placeholderImage: UIImage(named: "default"),
                imageTransition: .crossDissolve(0.2)
            )
        }else
        {
            self.userImage.image = UIImage(named: "default")
        }
        
        self.forum.isUserInteractionEnabled = true
        self.forum.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleGotoForums)))
        
        self.chats.isUserInteractionEnabled = true
        self.chats.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleGotoChat)))
        
        self.paymentsHistory.isUserInteractionEnabled = true
        self.paymentsHistory.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleGotoHistory)))
        
        self.searchChurch.isUserInteractionEnabled = true
        //self.searchChurch.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleGotoSearchChurch)))
        
        self.checkIns.isUserInteractionEnabled = true
        self.checkIns.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleGotoCheckIns)))
        
        self.viewProfile.isUserInteractionEnabled = true
        self.viewProfile.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleGotoProfile)))
        
        self.followers.isUserInteractionEnabled = true
        self.followers.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleGotoFollowers)))
        
        self.followings.isUserInteractionEnabled = true
        self.followings.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleGotoFollowings)))
        
        //self.helpAndSupport.isUserInteractionEnabled = true
        //self.helpAndSupport.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleGotoHelper)))
    }
    
    func handleGotoHistory(){
        let viewController = UIStoryboard(name:AllKeys.mainStoryBoard, bundle:nil).instantiateViewController(withIdentifier: AllKeys.paymentHistoryController) as! PaymentHistoryController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
//    func handleGotoSearchChurch(){
//        let vc = ChooseChurchController()
//        self.navigationController?.pushViewController(vc, animated: true)
//    }
    
    func handleGotoCheckIns(){
        let vc = UIStoryboard(name:AllKeys.mainStoryBoard, bundle:nil).instantiateViewController(withIdentifier: AllKeys.scannerViewController) as! ScannerViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func handleGotoProfile(){
        
        let layout = UICollectionViewFlowLayout()
        let vc = profileVC(collectionViewLayout: layout)
        //let profileViewController = UIStoryboard(name:AllKeys.mainStoryBoard, bundle:nil).instantiateViewController(withIdentifier: AllKeys.userProfileController) as! ProfileViewController

        let member = Member()
        member.avatar = userData.avatar
        member.firstName = userData.firstName
        member.lastName = userData.lastName
        member.id = userData.id
        member.isFollowing = true
        member.numberOfFollowing = userData.numberOfFollowers
        member.numberOfGroups = userData.numberOfGroups
        //profileViewController.user = member
        vc.user = member
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func handleGotoChat(){
        let vc = chatVC()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func handleGotoForums(){
        let vc = forumsVC(collectionViewLayout: UICollectionViewFlowLayout())
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func handleGotoFollowers(){
        let vc = myFollowersVC(collectionViewLayout: UICollectionViewFlowLayout())
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func handleGotoFollowings(){
        let vc = followingMeVC(collectionViewLayout: UICollectionViewFlowLayout())
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func handleGotoHelper(){
        let vc = helperVC()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func handleGotoHelpDesk(){
        let vc = helpDeskVC()
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
