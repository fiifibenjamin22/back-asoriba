//
//  CellIdentifiers.swift
//  asoriba
//
//  Created by Bright Ahedor on 15/12/2016.
//  Copyright © 2016 Asoriba Inc. All rights reserved.
//

enum CellIdentifiers: String {
    case givingCell = "GivingCollectionCell"
    case groupCell = "GroupTableCell"
    case feedWithImageTableCell = "FeedImageTableCell"
    case feedNoImageTableCell = "FeedNoImageTableCell"
    case feedVideoTableCell = "FeedVideoTableCell"
    case memberCell = "MemberCell"
    case feedGalleryTwoTablecell = "FeedGalleryTwoTableCell"
    case feedAudioTableCell  = "FeedAudioTableCell"
    case churchTableCell = "ChurchTableCell"
    case givingTableCell = "GivingTableCell"
    case commentTableCell = "CommentTableCell"
    case myChurchHeader = "MyChurchTableHeader"
    case feedHeader = "FeedCollectionHeader"
    case accountHeader = "AccountTableHeader"
    case aboutTableCell = "AboutChurchTableCell"
    case commentSection = "FeedCommentSection"
    case feedUniversalCell = "FeedUniversalCell"
    case historyTableCell = "HistoryTableCell"
    case feedCollectionViewCell = "FeedCollectionViewCell"
    case commentCollectionViewCell = "CommentCollectionViewCell"
    case imageCollectionViewCell = "ImageCollectionViewCell"
    case checkInCollectionViewCell = "CheckInCollectionViewCell"
    case feedUniversalNoImageTableViewCell = "FeedUniversalNoImageTableViewCell"
}

