//
//  YoutubePlayer.swift
//  asoriba
//
//  Created by Bright Ahedor on 28/12/2016.
//  Copyright © 2016 Asoriba Inc. All rights reserved.
//

import UIKit
import YouTubePlayer
import Crashlytics

class YoutubeVideoPlayer: UIViewController {
    
    @IBOutlet var playerView: YouTubePlayerView!
    var videoId = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let tag = "ICGC - media"
        self.navigationItem.title = tag
        
        self.loadVideo()
        
    }
    
 
    
    func loadVideo() {
        playerView.playerVars = [
            "playsinline": "1" as AnyObject,
            "controls": "1" as AnyObject,
            "showinfo": "0" as AnyObject
        ]
        playerView.loadVideoID(videoId)
        
        Answers.logCustomEvent(withName: "play_youtube", customAttributes: ["Youtube_Video" : videoId])
    }
    
  
    func showAlert(message: String) {
        self.present(alertWithMessage(message: message), animated: true, completion: nil)
    }
    
    func alertWithMessage(message: String) -> UIAlertController {
        let alertController =  UIAlertController(title: "", message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        
        return alertController
    }

}
