//
//  WelcomeViewController.swift
//  asoriba
//
//  Created by Bright Ahedor on 14/12/2016.
//  Copyright © 2016 Asoriba Inc. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import SwiftSpinner
import AccountKit

import Fabric
import Crashlytics
import FirebaseAnalytics
import SConnection

class WelcomeController: UIViewController,RegisterServiceProtocol,AKFViewControllerDelegate,RegisterWithNumberServiceProtocol {
    
    var accountKit: AKFAccountKit!
    var dict : [String : AnyObject]!
    var api:ApiService!
    
    @IBOutlet weak var Logos: UIImageView!
    @IBOutlet weak var welcomeLogo: UIImageView!
    @IBOutlet weak var optionsLabel: UILabel!
    
    var blurEffectView:UIVisualEffectView?
    
    @IBOutlet weak var BackGraphics : UIImageView!
    @IBOutlet weak var backgroundScrollView : UIScrollView!
    @IBOutlet weak var buttonsView : UIView!
    
    @IBOutlet weak var phoneNumberButton: UIButton!
    @IBOutlet weak var faceBookButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //api instance
        api = ApiService()
        self.api.accountDelegate = self
        self.api.numberAccountdelegate = self
        
       
        if accountKit == nil {
            self.accountKit = AKFAccountKit(responseType: AKFResponseType.accessToken)
            self.accountKit = AKFAccountKit(responseType: AKFResponseType.authorizationCode)
        }
        
        let path = Bundle.main.path(forResource: "configuration", ofType: "plist")
        let root = NSDictionary(contentsOfFile: path!)
        
        let headerBackground = (root?["headerBackgroundColor"] as! String)
        let clearColor = (root?["clearColor"] as! String)
        let welcomeImage = (root?["Graphic"] as! String)
        let welcomeLogo = (root?["welcomeLogo"] as! String)
        
        view.backgroundColor = UIColor().HexToColor(hexString: headerBackground)
        backgroundScrollView.backgroundColor = UIColor().HexToColor(hexString: clearColor, alpha: 0.0)
        phoneNumberButton.backgroundColor = UIColor().HexToColor(hexString: headerBackground)
        phoneNumberButton.setTitle("Enter Phone Number", for: .normal)
        phoneNumberButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 17)
        
        
        Logos.image = UIImage(named: welcomeLogo)
        
        BackGraphics.image = UIImage(named: welcomeImage)
        self.BackGraphics.contentMode = .scaleAspectFill
        
        if BackGraphics.image != nil{
            
            let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
            blurEffectView = UIVisualEffectView(effect: blurEffect)
            blurEffectView?.frame = view.bounds
            BackGraphics.addSubview(blurEffectView!)
            
        }else{

            self.blurEffectView?.removeFromSuperview()
        }

        //check for availiability
        if (Variables.icgcAppId() == "ICGC-1221") {
            self.faceBookButton.isHidden = true
            self.faceBookButton.removeFromSuperview()
            buttonsView.removeFromSuperview()

            backgroundScrollView.addSubview(phoneNumberButton)
            backgroundScrollView.translatesAutoresizingMaskIntoConstraints = false
            phoneNumberButton.translatesAutoresizingMaskIntoConstraints = false
            backgroundScrollView.isScrollEnabled = false
            
            optionsLabel.text = ""
            
            phoneNumberButton.bottomAnchor.constraint(equalTo: backgroundScrollView.bottomAnchor, constant: -100).isActive = true
            phoneNumberButton.leftAnchor.constraint(equalTo: backgroundScrollView.leftAnchor, constant: 12).isActive = true
            phoneNumberButton.rightAnchor.constraint(equalTo: backgroundScrollView.rightAnchor, constant: -12).isActive = true
            phoneNumberButton.heightAnchor.constraint(equalToConstant: 50).isActive = true

        }else{
            self.faceBookButton.isHidden = false
            self.faceBookButton.setTitle("Login with Facebook", for: UIControlState.normal)
            self.faceBookButton.setTitleColor(UIColor.white, for: UIControlState.normal)
            self.faceBookButton.backgroundColor = .blue
            self.faceBookButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 17)

        }
    }
 
    
    func viewController(_ viewController: UIViewController!, didCompleteLoginWith accessToken: AKFAccessToken!, state: String!) {
      
    }
    
    func viewController(_ viewController: UIViewController!, didCompleteLoginWithAuthorizationCode code: String!, state: String!) {
        SwiftSpinner.show("Connecting You...", animated: true)
        api.verifyAccount(authCode: code)
    }
    
    func viewController(_ viewController: UIViewController!, didFailWithError error: Error!) {
        print("We have an error \(error)")
    }
    
    func viewControllerDidCancel(_ viewController: UIViewController!) {
        print("The user cancel the login")
    }
    
    //register or log-in
    func loginORregister(){
        let inputState: String = NSUUID().uuidString
        let viewController:AKFViewController = accountKit.viewControllerForPhoneLogin(with: nil, state: inputState)  as! AKFViewController
        viewController.enableSendToFacebook = true
        self.prepareLoginViewController(loginViewController: viewController)
        self.present(viewController as! UIViewController, animated: true, completion: nil)
    }

    @IBAction func phoneNumberConnect(_ sender: Any) {
        //login with Phone
        loginORregister()
    }


    //register with phone number
    @IBAction func btnFBLoginPressed(_ sender: AnyObject) {
        //loginORregister()
        faceBookLogin()
    }

    //this is for facebook login button
    func faceBookLogin(){
        let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
        fbLoginManager.logIn(withReadPermissions: ["email"], from: self) { (result, error) in
            if(error != nil){
                ControllerHelpers.showDialog(targetVC: self, title: "Asoriba", message: "Error, connecting you, try again later")
                print("print error: ", error?.localizedDescription ?? "")
            }
            if (error == nil){
                let fbloginresult : FBSDKLoginManagerLoginResult = result!
                if fbloginresult.grantedPermissions != nil {
                    if(fbloginresult.grantedPermissions.contains("email"))
                    {
                        self.getFBUserData()
                        fbLoginManager.logOut()
                        
                    }
                }
            }
        }

    }
    
    //get facebook login credentials
    func getFBUserData(){
        if((FBSDKAccessToken.current()) != nil){
            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email"]).start(completionHandler: { (connection, result, error) -> Void in
                if (error == nil){
                    SwiftSpinner.show("Signing you in...", animated: true)
                    self.dict = result as! [String : AnyObject]!
                    let profile = self.dict["picture"] as! [String : AnyObject]
                    let profileData = profile["data"] as! [String : AnyObject]
                    let profileUrl = profileData["url"] as! String
                    self.api.createAccount(email: self.dict["email"] as! String, firstName: self.dict["first_name"] as! String, lastName: self.dict["last_name"] as! String,socialAvatar: profileUrl)
                    
                    print("print user information \(profileData)")
                    
                }
            })
        }
    }
    
    
    func showDialog(message:String) {
        let  alert = UIAlertController(title: nil, message: message,preferredStyle: .alert)
        let cancel = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil)
        alert.addAction(cancel)
        self.present(alert,animated: true,completion: nil)
    }
    
    
    func didReceiveAccountError(results: String) {
        SwiftSpinner.hide()
        self.showDialog(message: results)
    }
    
    func didReceiveAccountSuccess(result: Bool) {
        api = ApiService()
        api.getPaymentGetways()
        SwiftSpinner.hide()
        self.startMain()
    }
    
    
    func didReceiveAccountDetail(results: String) {
         SwiftSpinner.hide()
         self.showDialog(message: results)
    }
    
    func startMain() {
        guard let vc = UIStoryboard(name:"Main", bundle:nil).instantiateViewController(withIdentifier: AllKeys.mainController) as? MainTabController else {
            return
        }
        self.present(vc, animated: true, completion: nil)
    }
    
    func prepareLoginViewController(loginViewController: AKFViewController) {
        
        loginViewController.delegate = self
        loginViewController.advancedUIManager = nil
        
        //Costumize the theme
        let theme:AKFTheme = AKFTheme.default()
        
        let path = Bundle.main.path(forResource: "configuration", ofType: "plist")
        let root = NSDictionary(contentsOfFile: path!)
        
        let headerBackground = (root?["headerBackgroundColor"] as! String)
        
        theme.headerBackgroundColor = UIColor().HexToColor(hexString: headerBackground)
        theme.iconColor = UIColor().HexToColor(hexString: headerBackground)
        theme.buttonBackgroundColor = UIColor().HexToColor(hexString: headerBackground)
        theme.buttonDisabledBackgroundColor = UIColor().HexToColor(hexString: headerBackground, alpha: 0.5)
        theme.buttonBorderColor = UIColor().HexToColor(hexString: headerBackground)
        theme.inputBackgroundColor = UIColor().HexToColor(hexString: headerBackground, alpha: 0.1)
        theme.titleColor = UIColor().HexToColor(hexString: headerBackground)
        
        loginViewController.theme = theme
        
    }
    
    
    func startNameUpdate() {
        guard let vc = UIStoryboard(name:"Main", bundle:nil).instantiateViewController(withIdentifier: "UpdateNameController") as? UpdateNameController else {
            return
        }
        self.present(vc, animated: true, completion: nil)
    }
    
    //when number registration is successful
    func didReceiveNumberSuccess(result: Bool) {
        
        if (SConnection.isConnectedToNetwork()){
            
            SwiftSpinner.hide()
            //**update gateways
            api = ApiService()
            api.getPaymentGetways()
            //end the call
            let currentUser = DataOperation.getUser()
            if (currentUser.firstName.isEmpty){
                self.startNameUpdate()
                return
            }
            
            if !currentUser.firstName.isEmpty {
                self.startMain()
                return
            }

        }else{
            
            ControllerHelpers.showDialog(targetVC: self, title: "Ooops😭", message: "Your Internet connection is down")
        }
        
    }
    
    func didReceiveNumberError(result: String) {
        SwiftSpinner.hide()
        self.showDialog(message: result)
    }
    

}


extension UIColor{
    func HexToColor(hexString: String, alpha:CGFloat? = 1.0) -> UIColor {
        // Convert hex string to an integer
        let hexint = Int(self.intFromHexString(hexStr: hexString))
        let red = CGFloat((hexint & 0xff0000) >> 16) / 255.0
        let green = CGFloat((hexint & 0xff00) >> 8) / 255.0
        let blue = CGFloat((hexint & 0xff) >> 0) / 255.0
        let alpha = alpha!
        // Create color object, specifying alpha as well
        let color = UIColor(red: red, green: green, blue: blue, alpha: alpha)
        return color
    }
    
        func intFromHexString(hexStr: String) -> UInt32 {
            var hexInt: UInt32 = 0
            // Create scanner
            let scanner: Scanner = Scanner(string: hexStr)
            // Tell scanner to skip the # character
            scanner.charactersToBeSkipped = NSCharacterSet(charactersIn: "#") as CharacterSet //(charactersInString: "#")
            // Scan hex value
            scanner.scanHexInt32(&hexInt)
            return hexInt
        }
}

