//
//  Helpers.swift
//  asoriba
//
//  Created by Bright Ahedor on 15/12/2016.
//  Copyright © 2016 Asoriba Inc. All rights reserved.
//

import UIKit

class Helpers: UIView {
    
    static func getHeightOfLabel(text: String, fontSize:CGFloat, width: CGFloat,numberOfLines:Int) -> CGFloat{
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude))
        label.font = UIFont.systemFont(ofSize: fontSize, weight: UIFontWeightRegular)
        label.text = text
        label.numberOfLines = numberOfLines
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.sizeToFit()
        return label.frame.height
    }
    
    static func heightOfLabel(text: NSAttributedString, fontSize:CGFloat, width: CGFloat,numberOfLines:Int) -> CGFloat{
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude))
        label.font = UIFont.systemFont(ofSize: fontSize, weight: UIFontWeightRegular)
        label.attributedText = text
        label.numberOfLines = numberOfLines
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        let size = label.sizeThatFits(CGSize(width: CGFloat(width), height: CGFloat(FLT_MAX)))
        return size.height
    }
    
    static func roundButton(cornerRadius:CGFloat,button:UIButton, borderColor:UIColor){
        button.setTitleColor(borderColor, for: UIControlState.normal)
        button.backgroundColor = UIColor.clear
        button.layer.borderWidth = 1.0
        button.layer.borderColor = borderColor.cgColor
        button.layer.cornerRadius = cornerRadius
    }
    
    
}
