//
//  FeedUniversalCell.swift
//  asoriba
//
//  Created by Bright Ahedor on 23/12/2016.
//  Copyright © 2016 Asoriba Inc. All rights reserved.
//

import UIKit
import AlamofireImage

class FeedUniversalCell: UITableViewCell {

    @IBOutlet weak var contentImageView: UIImageView!
    @IBOutlet weak var galleryOneImageView: UIImageView!
    @IBOutlet weak var galleryTwoImageView: UIImageView!
    @IBOutlet weak var imageVisualEffect: UIVisualEffectView!
    @IBOutlet weak var contentLabel: UILabel!
    
    @IBOutlet weak var starsLabel: UILabel!
    @IBOutlet weak var playImageView: UIImageView!
   
    var feed: Feed! {
        didSet{
          self.updateUI()
        }
    }
    
    
    func updateUI() {
        
        self.layer.shadowColor = UIColor.lightGray.cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        self.layer.shadowRadius = 2.0
        self.layer.shadowOpacity = 1.0
        self.layer.masksToBounds = false
        self.layer.shadowPath = UIBezierPath(roundedRect: self.bounds, cornerRadius: self.contentView.layer.cornerRadius).cgPath

        //setting counts
        let amen = feed.numberOfAmens
        let amenShowable = amen == 1 ? "\(amen) amen " : "\(amen) amens "
        
        let shares = feed.numberOfShares
        let sharesShowable = shares == 1 ? "\(shares) share " : "\(shares) shares "
       
        
        let comment = feed.numberOfComments
        let commentsShowable = comment == 1 ? "\(comment) comment " : "\(comment) comments "
        
        
        let views = feed.numberOfViews
        let viewsShowable = views == 1 ? "\(views) view" : "\(views) views"
        //end of counts
        
        self.starsLabel.text = amenShowable + sharesShowable + commentsShowable + viewsShowable
        
        var message = ""
        if (feed.title != "" && feed.content == ""){
            message = feed.title
        }else if (feed.title != "" && feed.content != ""){
            message = feed.title + "\n" + feed.content
        }else{
            message = feed.content
        }
   
        self.contentLabel.text = message
        
        if (!feed.mediaFile.isEmpty && feed.mediaType == "audio"){
            //audio passed here
            self.galleryOneImageView.isHidden = true
            self.galleryTwoImageView.isHidden = true
            self.playImageView.image = UIImage(named: "music")
            
            
        }else if (!feed.mediaFile.isEmpty && feed.mediaType == "video"){
            //passed video here
            self.galleryOneImageView.isHidden = true
            self.galleryTwoImageView.isHidden = true
            self.playImageView.image = UIImage(named: "play")
            
            
        }else if (!feed.image.isEmpty){
            //passed with image
            self.galleryOneImageView.isHidden = true
            self.galleryTwoImageView.isHidden = true
            self.playImageView.isHidden = true
            self.imageVisualEffect.isHidden = true
            
            //first image
            let image = feed.image
            if  !image.isEmpty {
                self.contentImageView.af_setImage(
                    withURL: URL(string: (image))!,
                    placeholderImage: UIImage(named: "default"),
                    imageTransition: .crossDissolve(0.2)
                )
            }else
            {
                self.contentImageView.image = UIImage(named: "default")
            }
            
            
        }else if (feed.gallery.count > 0){
            //passing the gallery here
            
        
            self.galleryOneImageView.isHidden = false
            self.galleryTwoImageView.isHidden = false
            self.imageVisualEffect.isHidden = true
            self.playImageView.isHidden = true
            
            //first image
            let imageOne = feed.gallery[0].photoUrl
            if  !imageOne.isEmpty {
                self.galleryOneImageView.af_setImage(
                    withURL: URL(string: (imageOne))!,
                    placeholderImage: UIImage(named: "default"),
                    imageTransition: .crossDissolve(0.2)
                )
            }else
            {
                self.galleryOneImageView.image = UIImage(named: "default")
            }
            
            //secong image
            let imageTwo = feed.gallery[1].photoUrl
            if  !imageTwo.isEmpty {
                self.galleryTwoImageView.af_setImage(
                    withURL: URL(string: (imageTwo))!,
                    placeholderImage: UIImage(named: "default"),
                    imageTransition: .crossDissolve(0.2)
                )
            }else
            {
                self.galleryTwoImageView.image = UIImage(named: "default")
            }
            
            
            
        }else{
            //passed no image here
            self.galleryOneImageView.isHidden = true
            self.galleryTwoImageView.isHidden = true
            self.imageVisualEffect.isHidden = true
            self.playImageView.isHidden = true
            
            self.contentImageView.isHidden = true
            
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.galleryOneImageView.af_cancelImageRequest()
        self.galleryOneImageView.layer.removeAllAnimations()
        self.galleryOneImageView.image = nil
        
        self.galleryTwoImageView.af_cancelImageRequest()
        self.galleryTwoImageView.layer.removeAllAnimations()
        self.galleryTwoImageView.image = nil
        
        self.contentImageView.af_cancelImageRequest()
        self.contentImageView.layer.removeAllAnimations()
        self.contentImageView.image = nil
        
    }
}
