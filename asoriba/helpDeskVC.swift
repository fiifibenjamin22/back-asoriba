//
//  helpDeskVC.swift
//  asoriba
//
//  Created by Benjamin Acquah on 28/09/2017.
//  Copyright © 2017 Asoriba Inc. All rights reserved.
//

import UIKit
import MessageUI
import Foundation

class helpDeskVC: UIViewController, MFMailComposeViewControllerDelegate {
    
    var currentUserData = DataOperation.getUser()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
        
        sendEmail()
    }

    
    func sendEmail() {
        let composeVC = configureMailComposeViewController()

        if MFMailComposeViewController.canSendMail(){

            self.navigationController?.present(composeVC, animated: true, completion: nil)
            
        }else{
            self.showMailErrorAlert()
        }
        
        
    }
    
    func configureMailComposeViewController() -> MFMailComposeViewController {
        let mailComposeVC = MFMailComposeViewController()
        mailComposeVC.mailComposeDelegate = self
        
        // Configure the fields of the interface.
        mailComposeVC.setToRecipients(["appsupport@centralgospel.com"])
        mailComposeVC.setSubject("Help & Support")
        mailComposeVC.setMessageBody("Compose... \r\n\n\n\n\n\n\n\n\n\n ", isHTML: false)
        
        return mailComposeVC
    }
    
    func showMailErrorAlert(){
        
        let sendMailErrorAlert = UIAlertController(title: "Could not send email", message: "Your device must have an active mail account", preferredStyle: .alert)
        let Ok = UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel) { (UIAlertAction) in
            
            self.performSegue(withIdentifier: "gotoHelpDesk", sender: self)
        }
        
        sendMailErrorAlert.addAction(Ok)
//        self.present(sendMailErrorAlert, animated: true, completion: nil)
    }
    
    func dismiss(){
        self.dismiss(animated: true, completion: nil)
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        self.dismiss(animated: true, completion: nil)
    }
}
