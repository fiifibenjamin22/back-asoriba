//
//  PaymentPopOverController.swift
//  asoriba
//
//  Created by Bright Ahedor on 20/12/2016.
//  Copyright © 2016 Asoriba Inc. All rights reserved.
//

import UIKit
import SwiftSpinner
import Crashlytics
import PopupDialog
import SafariServices


class PaymentPopOverController: UIViewController,UIPickerViewDataSource,UIPickerViewDelegate,PaymentServiceProtocol,UITextFieldDelegate,SFSafariViewControllerDelegate{
    
    @IBAction func cancelActionButton(_ sender: Any) {
        self.cancelAction()
    }
    
    @IBAction func makeDonationButtonClick(_ sender: UIButton) {
        self.amount = self.amountTextField.text!
        self.amountTextField.resignFirstResponder()
        self.currency =  self.pickerData[self.currencyPicker.selectedRow(inComponent: 0)]
        if(amount.isEmpty){
            ControllerHelpers.showDialog(targetVC: self, title: "Asoriba", message: "Enter amout to donate")
        } else if (currency.isEmpty){
            ControllerHelpers.showDialog(targetVC: self, title: "Asoriba", message: "Please choose a currency")
            
        } else  {
            //tracking analytics
            api.initPayment(giving: giving, amount: amount, gateWay: gateWay.name, currency: currency, isAnonymous: isAnonymous)
            SwiftSpinner.show("Processing payment", animated: true)
        }
        
       
    }
    
    @IBOutlet weak var makeDonation: UIButton!
    @IBOutlet weak var paymentStatus: UISwitch!
    @IBAction func paymentStatusSwitchClick(_ sender: UISwitch) {
       isAnonymous = sender.isOn
    }
    
    @IBOutlet weak var currencyPicker: UIPickerView!
    @IBOutlet weak var amountTextField: UITextField!
    @IBOutlet weak var paymentTitleLabel: UILabel!
    @IBOutlet var parentView: UIView!
    var giving: Giving!
    var gateWay: PaymentGateWays!
    var api:ApiService!
    var isAnonymous = false
    var amount = ""
    var pickerData = [String]()
    var currency = ""
    var currentUser = DataOperation.getUser()
 
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if Variables.icgcAppId() == "ICGC-1221"{
            pickerData = ["GHS"]
        }else{
            pickerData = ["GHS","USD","NGN","ZAR"]
        }

        let path = Bundle.main.path(forResource: "configuration", ofType: "plist")
        let root = NSDictionary(contentsOfFile: path!)
        
        let paymentStatKnobTint = (root?["headerBackgroundColor"] as! String)
        let statKnobWhite = (root?["headerYellowColor"] as! String)
        
        self.paymentStatus.onTintColor = UIColor().HexToColor(hexString: statKnobWhite)
        self.paymentStatus.thumbTintColor = UIColor().HexToColor(hexString: paymentStatKnobTint)
        self.paymentStatus.tintColor = .brown
        
        self.makeDonation.backgroundColor = UIColor().HexToColor(hexString: paymentStatKnobTint)
        
        
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.8)
        self.showAnimate()
        
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.closePopUp(_:)))
        self.view.addGestureRecognizer(tap)
        self.view.isUserInteractionEnabled = true
        
        self.paymentTitleLabel.text = "Make donations towards \(giving.title)"
        
        //picker delegate
        self.currencyPicker.dataSource = self
        self.currencyPicker.delegate = self
        
        //api instance
        api = ApiService()
        self.api.paymentDelagate = self
        
        amountTextField.delegate = self
        
        isAnonymous = false
        paymentStatus.isOn = false
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(endEditing)))
        
        currency = pickerData[0]
        
    }
    
    func endEditing() {
        view.endEditing(true)
    }
    
    //MARK: Data Sources
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerData.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        print("print selected currency: ",pickerData[row])
        return pickerData[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        currency = pickerData[row]
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
       return 1
    }
 
    //end of delegate

    //close action on the parent view
    func closePopUp(_ sender: UITapGestureRecognizer) {
        self.view.endEditing(true)
        //self.removeAnimate()
        //self.view.removeFromSuperview()
    }
    
    //close action on the button
    func cancelAction() {
        self.removeAnimate()
        self.view.removeFromSuperview()
    }
    
    
    func showAnimate()
    {
        self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view.alpha = 0.0;
        UIView.animate(withDuration: 0.25, animations: {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        });
    }
    
    func removeAnimate()
    {
        UIView.animate(withDuration: 0.25, animations: {
            self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view.alpha = 0.0;
        }, completion:{(finished : Bool)  in
            if (finished)
            {
                self.view.removeFromSuperview()
            }
        });
    }

    //payment issues checked from this point
    
    func didReceivePaymentResult(result: CheckoutResponse) {
        SwiftSpinner.hide()
        let code = result.statusCode
        switch (code) {
        case 2000...3000:
            self.cancelAction()
            
            self.showMessageDialog(title:"Ready To Checkout",message: "Please proceed to checkout", action: result.checkoutUrl)
            print("print payment gatway status code: \(code)")
            
            
            //check if payment gateway is open
            if code >= 2000 && code <= 3000{
                print("code is \(code) then payment gatway is opened")
            }else{
                print("\(code) then payment gatway is close")
            }

            break
        default:
            break
        }
        
    }
    
    var emailUpdate = ""
    
    func didReceivePaymentError(results: String) {
       SwiftSpinner.hide()
        

        let thisCurrentUser = DataOperation.getUser()
        
        if thisCurrentUser.email.isEmpty {

            let alert = UIAlertController(title: "required", message: "Please requires your email address for receipt delivery", preferredStyle: .alert)
            alert.addTextField(configurationHandler: {(_ UpdateEmailtextField: UITextField) -> Void in
                UpdateEmailtextField.placeholder = "Your Email Address "
                UpdateEmailtextField.textColor = UIColor.black
                UpdateEmailtextField.clearButtonMode = .whileEditing
                UpdateEmailtextField.borderStyle = .roundedRect
                UpdateEmailtextField.isSecureTextEntry = false
                UpdateEmailtextField.keyboardType = .emailAddress
                UpdateEmailtextField.keyboardAppearance = .dark
                UpdateEmailtextField.addTarget(self, action: #selector(self.handleTextFieldEdit(sender:)), for: UIControlEvents.touchUpInside)

                self.emailUpdate = UpdateEmailtextField.text!
                
            })
            let yesButton = UIAlertAction(title: "Yes", style: .default, handler: {(_ action: UIAlertAction) -> Void in
                //Handle the ENTER button
                let textField : NSArray = alert.textFields! as NSArray
                let emailTextField: UITextField = textField.object(at: 0) as! UITextField
                
                if (emailTextField.text?.isEmpty)! {
                    return
                }else{
                 
                    self.emailUpdate = emailTextField.text!
                    print("print email from here: ", self.emailUpdate)
                    SwiftSpinner.show("Updating Email...")
                    self.api.updateEmail(emailAddress: self.emailUpdate)
                    SwiftSpinner.hide()

                }
                
            })
            let noButton = UIAlertAction(title: "No", style: .default, handler: {(_ action: UIAlertAction) -> Void in
                //Handle no button.
                return
            })
            // adding the object to the box
            alert.addAction(noButton)
            alert.addAction(yesButton)
            // present the box
            present(alert, animated: true) { _ in }
        }
        
    }
    
    //popup text field
    func handleTextFieldEdit(sender: UITextField){
        
    }
    
    //MARK: - dialog action
    func showMessageDialog(title:String, message:String,action:String) {
        let popup = PopupDialog(title: title, message: message, image: nil)
        // Create buttons
        var text = ""
        if (!action.isEmpty){
           text = "OKAY"
        } else {
           text = "CANCEL"
        }
        let buttonOne = CancelButton(title: text) {
            if (!action.isEmpty){
                self.startCheckOut(url: action)
            }
        }
        popup.addButtons([buttonOne])
        self.present(popup, animated: true, completion: nil)
    }
    
    func showDialog(message:String,url:String) {
        let  alert = UIAlertController(title: nil, message: message,preferredStyle: .alert)
        
        let action = UIAlertAction(title: "Okay",style: UIAlertActionStyle.default,
                                   handler: {(alert: UIAlertAction!) in
                                    self.startCheckOut(url: url)
        })
        alert.addAction(action)
        self.present(alert,animated: true,completion: nil)
    }
    
    func startCheckOut(url:String) {
        //let vc = UIStoryboard(name:AllKeys.mainStoryBoard, bundle:nil).instantiateViewController(withIdentifier: AllKeys.paymentCheckOutController) as! PaymentCheckoutController
        //vc.checkOutUrl = url
        //self.navigationController?.pushViewController(vc, animated: true)

        print("print payment URL IN \(url)")
        
        let webStringProtocol = ""
        let input = url
        let detector = try! NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue)
        let matches = detector.matches(in: input, options: [], range: NSRange(location: 0, length: (input.utf16.count)))
        
        for match in matches{
            
            let webUrl = (input as NSString).substring(with: match.range)
            print("link inside post string: \(webStringProtocol)\(webUrl)")
            let combined = URL(string:"\(webStringProtocol)\(webUrl)")
            
            let svc = SFSafariViewController(url: combined!)
            self.present(svc, animated: true, completion: nil)

        }

    }
    
    // MARK: - Textfield delegates
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true
    }
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        return true
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        return true
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder();
        return true;
    }
    //end of delegate
 

}
