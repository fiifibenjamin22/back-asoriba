//
//  History.swift
//  asoriba
//
//  Created by Bright Ahedor on 23/12/2016.
//  Copyright © 2016 Asoriba Inc. All rights reserved.
//

import Foundation

class PaymentHistory {
    var id = ""
    var payementOptions = ""
    var recordDate = ""
    var source = ""
    var description = ""
    var typeOfTransactions = ""
    var currency = ""
    var amount = ""
    var remarks = ""
    var isAnonymous = false
    
    
}
