//
//  EditProfileController.swift
//  asoriba
//
//  Created by Bright Ahedor on 17/12/2016.
//  Copyright © 2016 Asoriba Inc. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage
import Crashlytics
import PopupDialog
import SwiftSpinner

class EditProfileController: UIViewController,UINavigationControllerDelegate,UIImagePickerControllerDelegate,UITextFieldDelegate,UpdateUserServiceProtocol{
    
    @IBOutlet weak var progressActivityIndicator: UIActivityIndicatorView!
    var api:ApiService!
    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var otherNameTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var emailTextField: UITextField!
    
    @IBOutlet weak var buttonBirthDate: UIButton!
    @IBOutlet weak var buttonGender: UIButton!
    
    @IBOutlet weak var changeBtn: UIButton!
    
    var user: User!
    var imagePicker: UIImagePickerController!
    var gender = "Not Set"
    var date = ""
    var firstName: String!
    var lastName: String!
    var otherName: String!
    var email : String!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let path = Bundle.main.path(forResource: "configuration", ofType: "plist")
        let root = NSDictionary(contentsOfFile: path!)

        let btnBacgroundColor = (root?["headerBackgroundColor"] as! String)

        self.changeBtn.backgroundColor = UIColor().HexToColor(hexString: btnBacgroundColor)
        self.updateRecordsClick.backgroundColor = UIColor().HexToColor(hexString: btnBacgroundColor)
        
        self.imagePicker = UIImagePickerController()
        self.imagePicker.delegate = self
        self.user = DataOperation.getUser()
        
        
        self.otherNameTextField?.delegate = self
        self.lastNameTextField.delegate = self
        self.firstNameTextField.delegate = self
        self.emailTextField.delegate = self
        
       
        self.user = DataOperation.getUser()
        self.firstName = self.user.firstName
        self.lastName = self.user.lastName
        self.otherName = self.user.otherName
        self.email = self.user.email
        
        print("print user from realm: \(self.email)")
        
        self.firstNameTextField.text = self.firstName
        self.lastNameTextField.text = self.lastName
        self.otherNameTextField.text = self.otherName
        self.emailTextField?.text = self.email
        self.date = self.user.dateOfBirth
        self.gender = self.user.gender
        
        print("print username from the othernames: \(self.otherName!)")
        
        if (!self.date.isEmpty){
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            let _ = dateFormatter.date(from: self.date)
            self.buttonBirthDate.setTitle("\(self.date)", for: .normal)
        }
        
        if !self.gender.isEmpty {
           self.buttonGender.setTitle(gender, for: .normal)
        }
        
        Alamofire.request((self.user.avatar)).responseImage { response in
            if let image = response.result.value {
                self.userImageView.image = image
            } else {
                self.userImageView.image = UIImage(named: "default")
            }
        }
        
        
        //api instance
        api = ApiService()
        api.updateUserDelagate = self

        self.setProgressBar()
        // Do any additional setup after loading the view.
        
    }
    
    func setProgressBar()  {
        self.progressActivityIndicator.hidesWhenStopped = true
        self.progressActivityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray
    }
    

    //take photo only if the cammera is available
    func takePhoto()  {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera){
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.allowsEditing = true
            present(imagePicker, animated: true, completion: nil)
        }else{
         self.showCameraNotAvailabeAction()
        }
    }
    
    func pickPhoto()  {
        imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        imagePicker.allowsEditing = true
        present(imagePicker, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {

        let image = info[UIImagePickerControllerOriginalImage] as? UIImage
        userImageView.image = image
        imagePicker.dismiss(animated: true, completion: nil)
        
        self.saveImage(image: image!)
    }
    
    func saveImage(image:UIImage) {
        self.progressActivityIndicator.startAnimating()
        api.updateRecords(image: image)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
       imagePicker.dismiss(animated: true, completion: nil)
    }
    
    func showAction() {
        
        let alertActionControl = UIAlertController.init(title: nil, message: "Pick from", preferredStyle: .actionSheet)
        
        let alertActionCamera = UIAlertAction.init(title: "Camera", style: .default) {(Alert:UIAlertAction!) -> Void in
           self.takePhoto()
        }
        let alertActionGallery = UIAlertAction.init(title: "Gallery", style: .default) {(Alert:UIAlertAction!) -> Void in
            self.pickPhoto()
        }
        
        let alertActionCancel = UIAlertAction.init(title: "Cancel", style: .cancel) {(Alert:UIAlertAction!) -> Void in
            
        }
        
        alertActionControl.addAction(alertActionCamera)
        alertActionControl.addAction(alertActionGallery)
        alertActionControl.addAction(alertActionCancel)
        
        self.present(alertActionControl,animated:true,completion:nil)
    }
    
    func showCameraNotAvailabeAction() {
        let alertActionControl = UIAlertController.init(title: "Sorry", message: "Your device has no camera", preferredStyle: .alert)
        let alertActionCancel = UIAlertAction.init(title: "Cancel", style: .cancel) {(Alert:UIAlertAction!) -> Void in
            
        }
        alertActionControl.addAction(alertActionCancel)
        self.present(alertActionControl,animated:true,completion:nil)
    }

    @IBAction func changeImageButtonClicked(_ sender: Any) {
        self.showAction()
    }
    
    
    // MARK: - Textfield delegates
    func textFieldDidBeginEditing(_ textField: UITextField) {
    }
    
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if (textField == self.firstNameTextField){
           firstName = textField.text!
        }
        if (textField == self.lastNameTextField){
           lastName = textField.text!
        }
        if (textField == self.otherNameTextField){
           otherName = textField.text!
        }
        
        if (textField == self.emailTextField){
            email = textField.text!
        }
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true;
    }
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        return true;
    }
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        return true;
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return true;
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder();
        return true;
    }
    //end of delegate
    
    @IBAction func clickDateOfBirthButton(_ sender: UIButton) {
        self.startDateSheet()
    }
    
    @IBAction func clickGenderButton(_ sender: UIButton) {
        self.showGenderActionSheet()
    }
    
    
    func startDateSheet()  {
        guard let vc = UIStoryboard(name:AllKeys.micsStoryBoard, bundle:nil).instantiateViewController(withIdentifier: AllKeys.birthDateViewController) as? BirthDateViewController else {
            return
        }
        let popup = PopupDialog(viewController: vc, buttonAlignment: .horizontal, transitionStyle: .bounceDown, gestureDismissal: true)
        popup.keyboardShiftsView = true
        let vcInner = popup.viewController as! BirthDateViewController
    
        let buttonDone = DefaultButton(title: "DONE"){
            self.date = vcInner.dateOfBirth
            self.buttonBirthDate.setTitle(self.date, for: .normal)
        }
        let buttonCancel = CancelButton(title: "CANCEL") {
            
        }
        popup.addButtons([buttonCancel,buttonDone])
        self.present(popup, animated: true, completion: nil)
        
    }
    
    
    func showGenderActionSheet() {
        let popup = PopupDialog(title: "Gender", message: "Tell us your gender", image: nil)
        var sex = ""
        
        let buttonOne = DefaultButton(title: "Not Set"){
            sex = "Not Set"
            self.buttonGender.setTitle(sex, for: .normal)
            self.gender = sex
        }
        
        let buttonTwo = DefaultButton(title: "Male"){
            sex = "Male"
            self.buttonGender.setTitle(sex, for: .normal)
            self.gender = sex
        }
        let buttonThree = DefaultButton(title: "Female"){
            sex = "Female"
            self.buttonGender.setTitle(sex, for: .normal)
            self.gender = sex
        }
        let buttonCancel = CancelButton(title: "CANCEL") {
        }
        popup.addButtons([buttonOne,buttonTwo,buttonThree,buttonCancel])
        self.present(popup, animated: true, completion: nil)
    }
    
    
    func didReceiveUpdateError(results: String) {
        //error process update
    }
    
    func didReceiveUpdateStringResult(result: Bool) {
        //refresh UI
        NotificationCenter.default.post(name: Notification.Name(AllKeys.updatedUser), object: nil)
        self.restartApp()
    }
    
    func didReceiveUpdateFileResult(result: Bool) {
        //refresh UI
        NotificationCenter.default.post(name: Notification.Name(AllKeys.updatedUser), object: nil)
        self.restartApp()
    }
    
    func restartApp(){
        //reload application data (renew root view )
        UIApplication.shared.keyWindow?.rootViewController = storyboard!.instantiateViewController(withIdentifier: AllKeys.mainController)
        SwiftSpinner.hide()
    }
    
    @IBOutlet weak var updateRecordsClick: UIButton!
    
    @IBAction func saveAccountChanges(_ sender: UIButton) {       
        
        if (self.firstNameTextField.text?.isEmpty)! {
            ControllerHelpers.showDialog(targetVC: self, title: "Notice", message: "First Name Field Should not be empty")
        }else if (self.lastNameTextField.text?.isEmpty)!{
            ControllerHelpers.showDialog(targetVC: self, title: "Notice", message: "Last Name field Should not be empty")
        }else if (self.gender == "Not Set"){
            ControllerHelpers.showDialog(targetVC: self, title: "Notice", message: "Gender Field must Not be Empty")
        }else if (self.date.isEmpty){
            ControllerHelpers.showDialog(targetVC: self, title: "Notice", message: "Enter Your Date Of Birth")
        }else{
            
            SwiftSpinner.show("Saving your changes...", animated: true)
            self.updateRecordsClick.setTitle("Saving Changes", for: .normal)
            api.updateRecords(firstName: self.firstName, lastName: self.lastName, otherName: self.otherName, sex: self.gender, dateOfBirth: self.date, emailAccount: self.email)
            
            self.updateRecordsClick.isEnabled = true
            
            SwiftSpinner.hide()
            print("print username from the othernames: \(self.otherName!)")
            print("print email account \(email)")
            

        }
    }

}
