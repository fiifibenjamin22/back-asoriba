//
//  ImageCollectionViewCell.swift
//  asoriba
//
//  Created by Bright Ahedor on 25/02/2017.
//  Copyright © 2017 Asoriba Inc. All rights reserved.
//

import UIKit

class ImageCollectionViewCell: UICollectionViewCell {
  
    @IBOutlet weak var feedImageView: UIImageView!
    
    var feed:UIImage!{
        didSet{
          self.updateUI()
        }
    }
    
    func updateUI()  {
       self.feedImageView.image = feed
        print("anything from selected images")
    }
}
