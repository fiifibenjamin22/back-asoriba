//
//  PaymentGateWays.swift
//  asoriba
//
//  Created by Bright Ahedor on 20/12/2016.
//  Copyright © 2016 Asoriba Inc. All rights reserved.
//

import RealmSwift

class PaymentGateWays: Object {
    dynamic var  id = ""
    dynamic var  name = ""
    dynamic var itemDescription = ""
    
    override class func primaryKey() -> String? {
        return "id"
    }
}
