//
//  chatCell.swift
//  asoriba
//
//  Created by Benjamin Acquah on 23/08/2017.
//  Copyright © 2017 Asoriba Inc. All rights reserved.
//

import UIKit
import LBTAComponents
import Firebase

class chatCell: UICollectionViewCell {
    
    let userAva : UIImageView = {
        let img = UIImageView()
        img.image = UIImage(named: "default")?.withRenderingMode(.alwaysOriginal)
        img.translatesAutoresizingMaskIntoConstraints = false
        img.layer.cornerRadius = 32
        img.layer.masksToBounds = true
        return img
    }()
    
    let userName : UILabel = {
        let lbl = UILabel()
        lbl.text = "User Name"
        lbl.font = UIFont.systemFont(ofSize: 16)
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
        
    let timeAgo : UILabel = {
        let lbl = UILabel()
        lbl.text = "1 minuite ago"
        lbl.font = UIFont.boldSystemFont(ofSize: 12)
        lbl.textColor = UIColor.gray
        lbl.textAlignment = .right
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let isOnline : UILabel = {
        let lbl = UILabel()
        lbl.text = "online"
        lbl.font = UIFont.boldSystemFont(ofSize: 12)
        lbl.textColor = UIColor.gray
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let isOnlineIndicator : UILabel = {
        let lbl = UILabel()
        lbl.text = "•"
        lbl.font = UIFont.boldSystemFont(ofSize: 14)
        lbl.textColor = UIColor(r: 28, g: 90, b: 65)
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let finalStatement : UILabel = {
        let lbl = UILabel()
        lbl.text = "this was what you said last..."
        lbl.font = UIFont.systemFont(ofSize: 14)
        lbl.textColor = UIColor.gray
        lbl.numberOfLines = 2
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()

    let seperator : UILabel = {
        let lbl = UILabel()
        lbl.backgroundColor = UIColor.gray
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    var placeHolderImage = UIImage(named: "default")
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = .white
        
        setUpViews()
    }
    
    func setUpViews(){
        
        addSubview(userAva)
        addSubview(userName)
        addSubview(timeAgo)
        addSubview(isOnlineIndicator)
        addSubview(isOnline)
        addSubview(finalStatement)
        addSubview(seperator)
        
        addConstraintsWithFormat("H:|-8-[v0(64)]", views: userAva)
        addConstraintsWithFormat("V:|-8-[v0(64)]", views: userAva)
        
        userName.anchor(topAnchor, left: userAva.rightAnchor, bottom: nil, right: timeAgo.leftAnchor, topConstant: 12, leftConstant: 8, bottomConstant: 0, rightConstant: 8, widthConstant: 0, heightConstant: 20)
        
        timeAgo.anchor(userName.topAnchor, left: nil, bottom: userName.bottomAnchor, right: rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 2, widthConstant: 100, heightConstant: 0)
        
        isOnlineIndicator.anchor(userName.bottomAnchor, left: userName.leftAnchor, bottom: nil, right: nil, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 10, heightConstant: 20)
        
        isOnline.anchor(isOnlineIndicator.topAnchor, left: isOnlineIndicator.rightAnchor, bottom: isOnlineIndicator.bottomAnchor, right: userName.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        finalStatement.anchor(isOnline.bottomAnchor, left: isOnlineIndicator.rightAnchor, bottom: seperator.topAnchor, right: timeAgo.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        seperator.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
        seperator.leftAnchor.constraint(equalTo: userName.leftAnchor).isActive = true
        seperator.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        seperator.heightAnchor.constraint(equalToConstant: 0.5).isActive = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
