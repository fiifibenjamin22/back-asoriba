//
//  findUsVC.swift
//  asoriba
//
//  Created by Benjamin Acquah on 03/11/2017.
//  Copyright © 2017 Asoriba Inc. All rights reserved.
//

import UIKit
import MapKit
import Firebase
import SwiftyJSON

class findUsVC: UIViewController {
    
    @IBOutlet weak var theMapView: MKMapView!
    
    var theLatitude : Double = 5.5544511
    var theLongitude : Double? = -0.23019920000001548
    var currentUser = DataOperation.getUser()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let initialLocation = CLLocation(latitude: theLatitude, longitude: theLongitude!)

        centerMapOnLocation(location: initialLocation)
        
        // show artwork on map
        let artwork = Artwork(title: "\(currentUser.churchName)\n \(currentUser.branchName)",
                              locationName: "Accra, Greater Accra Region, Ghana",
                              discipline: "\(currentUser.churchLogo)",
                              coordinate: CLLocationCoordinate2D(latitude: theLatitude, longitude: theLongitude!))
        theMapView.addAnnotation(artwork)
        
        fetchChurchInformationData()

    }
    
    func fetchChurchInformationData(){
        
        let ref = Database.database().reference().child("churchInformation")
        ref.observe(.childAdded, with: { (snapshot) in
            
            let information = churchInfo()
            let snapshotValue = snapshot.value!
            let snapShotFromChurchInfoObject = snapShotFromChurchInfo()
            snapShotFromChurchInfoObject.snapshotValueObject = snapshotValue as? String
            
            let Value = snapShotFromChurchInfoObject.snapshotValueObject!

            
            let data: NSData = Value.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))! as NSData

            let json = JSON(data: data as Data)
            //get messages from JSON
            if let locName = json["locationName"].string {
                //Now you got your value
                information.locationNames = locName
                let storedNames = information.locationNames
                print("print text messages from user data: ", storedNames!)
                
            }
            
            //get messages from JSON
//            if let locNumber = json["phoneNumbers"].int {
//                //Now you got your value
//                information.branchPhoneNumber = locNumber
//                let storedNumbers = information.branchPhoneNumber
//                print("print text messages from user data: ", storedNumbers)
//
//            }
            
            //get messages from JSON
//            if let locCordinates = json["cordinates"].double {
//                //Now you got your value
//                information.locationCordinates = [locCordinates]
//                let storedCordinates = information.locationCordinates
//                print("print text messages from user data: ", storedCordinates)
//                
//            }

        }, withCancel: nil)

    }
    
    let regionRadius: CLLocationDistance = 1000
    
    func centerMapOnLocation(location: CLLocation) {
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(location.coordinate, regionRadius, regionRadius)
        theMapView.setRegion(coordinateRegion, animated: true)
    }
    
    
    //"{\"phoneNumbers\":\"[+233302688001, +233302688002, +233302688003, +233302688004, +233302688000, +233243392422, +233243392411]\",\"locationName\":\"Christ Temple,International Central Gospel Church\",\"cordinates\":\"[5.603716800000001, -0.18696439999996528]\"}"
    
}




//@available(iOS 11.0, *)
//extension findUsVC: MKMapViewDelegate {
//    // 1
//    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
//        // 2
//        guard let annotation = annotation as? Artwork else { return nil }
//        // 3
//        let identifier = "marker"
//        var view: MKMarkerAnnotationView
//        // 4
//        if let dequeuedView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier)
//            as? MKMarkerAnnotationView {
//            dequeuedView.annotation = annotation
//            view = dequeuedView
//        } else {
//            // 5
//            view = MKMarkerAnnotationView(annotation: annotation, reuseIdentifier: identifier)
//            view.canShowCallout = true
//            view.calloutOffset = CGPoint(x: -5, y: 5)
//            view.rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
//        }
//        return view
//    }
//}


class Artwork: NSObject, MKAnnotation {
    let title: String?
    let locationName: String
    let discipline: String
    let coordinate: CLLocationCoordinate2D
    
    init(title: String, locationName: String, discipline: String, coordinate: CLLocationCoordinate2D) {
        self.title = title
        self.locationName = locationName
        self.discipline = discipline
        self.coordinate = coordinate
        
        super.init()
    }
    
    var subtitle: String? {
        return locationName
    }
}
