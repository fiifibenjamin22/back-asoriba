//
//  profileCell.swift
//  asoriba
//
//  Created by Benjamin Acquah on 26/10/2017.
//  Copyright © 2017 Asoriba Inc. All rights reserved.
//

import UIKit
import LBTAComponents
import Alamofire
import AlamofireImage

class profileHeaderCell: UICollectionViewCell{
    
    let userImageView: UIImageView = {
        let img = UIImageView()
        img.translatesAutoresizingMaskIntoConstraints = false
        img.layer.cornerRadius = 60
        img.layer.masksToBounds = true
        img.contentMode = .scaleAspectFill
        img.image = #imageLiteral(resourceName: "default").withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        return img
    }()
    
    let userBackProfileImageView: UIImageView = {
        let img = UIImageView()
        img.translatesAutoresizingMaskIntoConstraints = false
        img.contentMode = .scaleAspectFill
        img.layer.masksToBounds = true
        img.image = #imageLiteral(resourceName: "default").withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        return img
    }()
    
    let userNameLabel : UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.textColor = .white
        lbl.textAlignment = .center
        lbl.font = UIFont.boldSystemFont(ofSize: 15)
        return lbl
    }()
    
    let userName : UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.textColor = .white
        lbl.textAlignment = .center
        lbl.font = UIFont.boldSystemFont(ofSize: 12)
        return lbl
    }()

    let churchName : UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.textColor = .white
        lbl.textAlignment = .center
        lbl.font = UIFont.boldSystemFont(ofSize: 12)
        return lbl
    }()

    let view : UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor.groupTableViewBackground
        return view
    }()

    let chatActionBtn : UIButton = {
        let btn = UIButton(type: UIButtonType.roundedRect)
        btn.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        btn.backgroundColor = .white
        btn.layer.borderWidth = 2
        btn.layer.cornerRadius = 5
        btn.layer.borderColor = UIColor(r: 255, g: 0, b: 0).cgColor
        
        btn.layer.shadowColor = UIColor.lightGray.cgColor
        btn.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        //v.layer.shadowRadius = 2.0
        btn.layer.shadowOpacity = 1.0
        btn.layer.masksToBounds = false
        return btn
    }()
    
    lazy var settingsActionBtn : UIButton = {
        let btn = UIButton(type: UIButtonType.roundedRect)
        btn.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        btn.backgroundColor = .white
        btn.layer.borderWidth = 2
        btn.layer.cornerRadius = 5
        btn.layer.borderColor = UIColor(r: 255, g: 0, b: 0).cgColor
        btn.layer.shadowColor = UIColor.lightGray.cgColor
        btn.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        //v.layer.shadowRadius = 2.0
        btn.layer.shadowOpacity = 1.0
        btn.layer.masksToBounds = false
        return btn
    }()
    
    //stat view
    let statView : UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor.white
        return view
    }()
    
    //stats label
    let followingCountLbl : UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.text = "29"
        lbl.textColor = UIColor(r: 0, g: 165, b: 240)
        lbl.textAlignment = .center
        return lbl
    }()
    
    let followingLbl : UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.text = "Following"
        lbl.textColor = UIColor(r: 0, g: 165, b: 240)
        lbl.font = UIFont.boldSystemFont(ofSize: 12)
        lbl.textColor = UIColor.lightGray
        lbl.textAlignment = .center
        return lbl
    }()
    
    let followersCountLbl : UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.text = "0"
        lbl.textColor = UIColor(r: 0, g: 165, b: 240)
        lbl.textAlignment = .center
        return lbl
    }()
    
    let followersLbl : UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.text = "Followers"
        lbl.textColor = UIColor(r: 0, g: 165, b: 240)
        lbl.font = UIFont.boldSystemFont(ofSize: 12)
        lbl.textColor = UIColor.lightGray
        lbl.textAlignment = .center
        return lbl
    }()
    
    let churchCountLbl : UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.text = "1"
        lbl.textColor = UIColor(r: 0, g: 165, b: 240)
        lbl.textAlignment = .center
        return lbl
    }()
    
    let churchLbl : UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.text = "Churches"
        lbl.textColor = UIColor(r: 0, g: 165, b: 240)
        lbl.font = UIFont.boldSystemFont(ofSize: 12)
        lbl.textColor = UIColor.lightGray
        lbl.textAlignment = .center
        return lbl
    }()
    
    //status View
    let statusView : UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor.white
        return view
    }()
    
    //status label
    let statusLabel : UILabel = {
        let lbl = UILabel()
        lbl.text = "Hello! I'm new on my ICGC"
        lbl.font = UIFont.systemFont(ofSize: 14)
        lbl.textColor = UIColor.lightGray
        lbl.textAlignment = .left
        return lbl
    }()
    
    let penImage : UIImageView = {
        let img = UIImageView()
        img.image = #imageLiteral(resourceName: "pencil").withRenderingMode(UIImageRenderingMode.alwaysTemplate)
        img.tintColor = UIColor(r: 0, g: 165, b: 240)
        return img
    }()
    
    //divider line
    let dividerLine : UIView = {
        let divider = UIView()
        divider.backgroundColor = UIColor.lightGray
        divider.translatesAutoresizingMaskIntoConstraints = false
        return divider
    }()

    
    var currentUser = DataOperation.getUser()
    
    var userData: Member! {
        didSet {
            updateUI()
        }
    }
    

    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setUpView()
    }
    
    func setUpView(){
        
        addSubview(userBackProfileImageView)
        
        userBackProfileImageView.anchor(self.topAnchor, left: self.leftAnchor, bottom: self.bottomAnchor, right: self.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: self.frame.width, heightConstant: self.frame.height)
        
        addSubview(userImageView)
        userBackProfileImageView.addSubview(userNameLabel)
        userBackProfileImageView.addSubview(userName)
        userBackProfileImageView.addSubview(churchName)
        userBackProfileImageView.addSubview(view)
        addSubview(chatActionBtn)
        addSubview(settingsActionBtn)
        userBackProfileImageView.addSubview(statView)
        userBackProfileImageView.addSubview(statusView)
        
        addSubview(followingCountLbl)
        addSubview(followingLbl)
        
        addSubview(followersCountLbl)
        addSubview(followersLbl)
        
        addSubview(churchCountLbl)
        addSubview(churchLbl)
        
        addSubview(statusLabel)
        addSubview(penImage)

        self.statusLabel.isHidden = true
        self.penImage.isHidden = true
        
        userImageView.centerXAnchor.constraint(equalTo: userBackProfileImageView.centerXAnchor).isActive = true
        userImageView.topAnchor.constraint(equalTo: userBackProfileImageView.topAnchor, constant: 20).isActive = true
        userImageView.widthAnchor.constraint(equalToConstant: 120).isActive = true
        userImageView.heightAnchor.constraint(equalToConstant: 120).isActive = true
        
        userNameLabel.anchor(userImageView.bottomAnchor, left: userBackProfileImageView.leftAnchor, bottom: nil, right: userBackProfileImageView.rightAnchor, topConstant: 12, leftConstant: 8, bottomConstant: 0, rightConstant: 8, widthConstant: 0, heightConstant: 20)
        
        userName.anchor(userNameLabel.bottomAnchor, left: userNameLabel.leftAnchor, bottom: nil, right: userNameLabel.rightAnchor, topConstant: 8, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 20)
        
        churchName.anchor(userName.bottomAnchor, left: userName.leftAnchor, bottom: view.topAnchor, right: userName.rightAnchor, topConstant: 8, leftConstant: 0, bottomConstant: 8, rightConstant: 0, widthConstant: 0, heightConstant: 20)

        view.anchor(nil, left: userBackProfileImageView.leftAnchor, bottom: userBackProfileImageView.bottomAnchor, right: userBackProfileImageView.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 150)
        
        settingsActionBtn.anchor(view.topAnchor, left: nil, bottom: nil, right: view.rightAnchor, topConstant: 8, leftConstant: 0, bottomConstant: 0, rightConstant: 5, widthConstant: 100, heightConstant: 40)
        
        chatActionBtn.anchor(settingsActionBtn.topAnchor, left: nil, bottom: settingsActionBtn.bottomAnchor, right: settingsActionBtn.leftAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 8, widthConstant: 120, heightConstant: 40)
        
        statView.anchor(settingsActionBtn.bottomAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, topConstant: 8, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 50)
        
        let width : CGFloat = self.frame.width / 3
        
        print("print equated width: ",width)
        followingCountLbl.anchor(statView.topAnchor, left: statView.leftAnchor, bottom: statView.bottomAnchor, right: nil, topConstant: 0, leftConstant: 5, bottomConstant: 15, rightConstant: 0, widthConstant: width, heightConstant: 0)
        
        followingLbl.anchor(followingCountLbl.bottomAnchor, left: followingCountLbl.leftAnchor, bottom: statView.bottomAnchor, right: followingCountLbl.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        followersCountLbl.anchor(followingCountLbl.topAnchor, left: followingCountLbl.rightAnchor, bottom: followingCountLbl.bottomAnchor, right: nil, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: width, heightConstant: 0)
        
        followersLbl.anchor(followersCountLbl.bottomAnchor, left: followersCountLbl.leftAnchor, bottom: statView.bottomAnchor, right: followersCountLbl.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        churchCountLbl.anchor(followersCountLbl.topAnchor, left: followersCountLbl.rightAnchor, bottom: followersCountLbl.bottomAnchor, right: statView.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: width, heightConstant: 0)
        
        churchLbl.anchor(churchCountLbl.bottomAnchor, left: churchCountLbl.leftAnchor, bottom: statView.bottomAnchor, right: churchCountLbl.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        statusView.anchor(statView.bottomAnchor, left: statView.leftAnchor, bottom: view.bottomAnchor, right: statView.rightAnchor, topConstant: 5, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        statusLabel.anchor(statusView.topAnchor, left: statusView.leftAnchor, bottom: statusView.bottomAnchor, right: penImage.leftAnchor, topConstant: 0, leftConstant: 5, bottomConstant: 0, rightConstant: 25, widthConstant: 0, heightConstant: 0)
        
        penImage.anchor(statusView.topAnchor, left: nil, bottom: statusView.bottomAnchor, right: statusView.rightAnchor, topConstant: 8, leftConstant: 0, bottomConstant: 8, rightConstant: 5, widthConstant: 20, heightConstant: 20)
    }
    
    
    func updateUI(){
        
        backgroundColor = .blue
        
        //content image
        Alamofire.request((currentUser.churchBackDrop)).responseImage { response in
            if let image = response.result.value {
                self.userBackProfileImageView.image = image
            }else{
                self.userBackProfileImageView.image = UIImage(named: "default")
            }
        }
        
        //user image
        Alamofire.request((userData.avatar)).responseImage { response in
            if let image = response.result.value {
                self.userImageView.image = image
            }else{
                self.userImageView.image = UIImage(named: "default")
            }
        }
        
        if (currentUser.id == userData.id){
            //self.followButton.setTitle("Edit", for: UIControlState.normal)
        }
        self.userImageView.isUserInteractionEnabled = true

        self.userNameLabel.text = userData.firstName + " " + userData.lastName
        self.userName.text = "@\(userData.otherName)"
        
        let path = Bundle.main.path(forResource: "configuration", ofType: "plist")
        let root = NSDictionary(contentsOfFile: path!)
        
        let headerBackground = (root?["headerBackgroundColor"] as! String)
        let tint_white = (root?["tint_white"] as! String)
        
        if userData.isFollowing {
            
            self.settingsActionBtn.setTitle("Unfollow", for: UIControlState.normal)
            self.settingsActionBtn.layer.cornerRadius = 3
            self.settingsActionBtn.layer.borderWidth = 2
            self.settingsActionBtn.backgroundColor = UIColor().HexToColor(hexString: headerBackground)
            self.settingsActionBtn.layer.borderColor = UIColor().HexToColor(hexString: tint_white).cgColor
            
        }else {
            self.settingsActionBtn.setTitle("Follow", for: UIControlState.normal)
            self.settingsActionBtn.layer.cornerRadius = 3
            self.settingsActionBtn.layer.borderWidth = 2
            self.settingsActionBtn.backgroundColor = UIColor().HexToColor(hexString: tint_white)
            self.settingsActionBtn.layer.borderColor = UIColor().HexToColor(hexString: headerBackground).cgColor
            self.settingsActionBtn.setTitleColor(UIColor().HexToColor(hexString: headerBackground), for: UIControlState.normal)
        }
        
        self.chatActionBtn.layer.cornerRadius = 3
        self.chatActionBtn.layer.borderWidth = 2
        self.chatActionBtn.backgroundColor = UIColor().HexToColor(hexString: headerBackground)
        self.chatActionBtn.layer.borderColor = UIColor().HexToColor(hexString: tint_white).cgColor
        
        
        if currentUser.id == userData.id {
            print("print 2nd user's id: \(userData.id)")
            self.chatActionBtn.setTitle("My messages", for: UIControlState.normal)
            self.settingsActionBtn.setTitle("settings", for: UIControlState.normal)
            self.settingsActionBtn.tintColor = UIColor().HexToColor(hexString: tint_white)
            self.chatActionBtn.tintColor = UIColor().HexToColor(hexString: tint_white)
        }else{
            self.chatActionBtn.setTitle("message", for: UIControlState.normal)
            self.settingsActionBtn.setTitle("Follow", for: UIControlState.normal)
            self.settingsActionBtn.tintColor = UIColor().HexToColor(hexString: tint_white)
            self.chatActionBtn.tintColor = UIColor().HexToColor(hexString: tint_white)
            
            //            if self.settingsActionBtn.currentTitle == "Follow"{
            //                self.chatActionBtn.isEnabled = false
            //            }else{
            //                self.chatActionBtn.isEnabled = true
            //            }
        }
        
        self.followersCountLbl.text = "\(userData.numberOfFollowers)"
        self.followingCountLbl.text = "\(userData.numberOfFollowing)"
        //self.churchCountLbl.text = "\(userData.numberOfChurches)"
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

