//
//  chooseChurchCell.swift
//  asoriba
//
//  Created by Benjamin Acquah on 22/08/2017.
//  Copyright © 2017 Asoriba Inc. All rights reserved.
//

import UIKit

class chooseChurchCell: UICollectionViewCell {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = .white
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
