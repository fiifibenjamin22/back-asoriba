//
//  MemberCell.swift
//  asoriba
//
//  Created by Bright Ahedor on 15/12/2016.
//  Copyright © 2016 Asoriba Inc. All rights reserved.
//

import UIKit
import AlamofireImage

class MemberCell: UITableViewCell {
    
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var userFollowersCountLabel: UILabel!
    @IBOutlet weak var followActionButton: UIButton!
    private var placeHolderImage = UIImage(named: "default")
    
    let user = DataOperation.getUser()
    
    var noAction : Bool!{
        didSet {
          self.followActionButton.isHidden = true
        }
    }
    

    var feed: Member!{
        didSet{
          updateUI()
        }
    }
    
    
    func updateUI()   {
        
        let path = Bundle.main.path(forResource: "configuration", ofType: "plist")
        let root = NSDictionary(contentsOfFile: path!)
        
        let headerBackground = (root?["headerBackgroundColor"] as! String)
        let headerYellowBackground = (root?["tint_white"] as! String)
        
        self.followActionButton.layer.borderWidth = 2
        self.followActionButton.layer.borderColor = UIColor().HexToColor(hexString: headerBackground).cgColor
        
        
        
        
        //setting counts
        let followers = feed.numberOfFollowing
        let followingShowable = followers == 1 ? "\(followers) Follower" : "\(followers) Followers"
        self.userFollowersCountLabel.text = followingShowable
        self.userNameLabel.text = feed.lastName + " " + feed.firstName
        
        if (feed.isFollowing){
            self.followActionButton.setTitle("Following", for: .normal)
            self.followActionButton.backgroundColor = UIColor().HexToColor(hexString: headerBackground)
            
        }else {
           self.followActionButton.setTitle("Follow", for: .normal)
            self.followActionButton.backgroundColor = UIColor().HexToColor(hexString: headerYellowBackground)
            self.followActionButton.setTitleColor(UIColor().HexToColor(hexString: headerBackground), for: UIControlState.normal)
        }
        //content image
        if  feed.avatar != "" {
            self.userImageView.af_setImage(
                withURL: URL(string: feed.avatar)!,
                placeholderImage: placeHolderImage,
                imageTransition: .crossDissolve(0.2)
            )
        }else {
           self.userImageView.image = placeHolderImage
        }
        
        
        if self.feed.id == self.user.id {
           self.followActionButton.isHidden = true
        } else{
            self.followActionButton.isHidden = false
        }

    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.userImageView.af_cancelImageRequest()
        self.userImageView.layer.removeAllAnimations()
        self.userImageView.image = nil
    }
    
}
