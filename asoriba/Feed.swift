//
//  Feed.swift
//  asoriba
//
//  Created by Bright Ahedor on 15/12/2016.
//  Copyright © 2016 Asoriba Inc. All rights reserved.
//

import RealmSwift

class Feed: Object{
    dynamic var id = ""
    dynamic var title = ""
    dynamic var content = ""
    dynamic var date = ""
    dynamic var image = ""
    dynamic var location = ""
    dynamic var numberOfComments = 0
    dynamic var numberOfAmens = 0
    dynamic var numberOfShares = 0
    dynamic var numberOfViews = 0
    dynamic var mediaType = ""
    dynamic var mediaFile = ""
    dynamic var medialUrl = ""
    dynamic var mediaThumbnail = ""
    dynamic var type = ""
    var gallery = List<Gallery>()
    dynamic var hasAmened = false
    dynamic var member: Member?
    dynamic var quotation: Quotation?
    
    override class func primaryKey() -> String? {
        return "id"
    }
    

}
