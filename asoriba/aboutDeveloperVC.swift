//
//  aboutDeveloperVC.swift
//  asoriba
//
//  Created by Benjamin Acquah on 01/11/2017.
//  Copyright © 2017 Asoriba Inc. All rights reserved.
//

import UIKit
import LBTAComponents
import WebKit
import SConnection

class aboutDeveloperVC: UIViewController,WKUIDelegate {
    
    private var hasFinishedLoading = false
    var timer: Timer!
    @IBOutlet weak var progressBar: UIProgressView!
    var webView: WKWebView!
    
    override func loadView() {
        let webConfiguration = WKWebViewConfiguration()
        webView = WKWebView(frame: .zero, configuration: webConfiguration)
        webView.uiDelegate = self
        view = webView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "About Developer"
        
        //chech for network availiability
        if (SConnection.isConnectedToNetwork()){
            
            //call method
            let myURL = URL(string: "https://www.asoriba.com")
            let myRequest = URLRequest(url: myURL!)
            webView.load(myRequest)

            
        }else{
            ControllerHelpers.showDialog(targetVC: self, title: "Ooops😭", message: "Your Internet connection is down")
        }
        
    }
}


class aboutChurchVC: UIViewController,WKUIDelegate {
    
    private var hasFinishedLoading = false
    var timer: Timer!
    var webView: WKWebView!
    
    override func loadView() {
        let webConfiguration = WKWebViewConfiguration()
        webView = WKWebView(frame: .zero, configuration: webConfiguration)
        webView.uiDelegate = self
        view = webView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "About Church"
        
        //chech for network availiability
        if (SConnection.isConnectedToNetwork()){
            
            //call method
            let myURL = URL(string: "https://www.centralgospel.com")
            let myRequest = URLRequest(url: myURL!)
            webView.load(myRequest)
            
            
        }else{
            ControllerHelpers.showDialog(targetVC: self, title: "Ooops😭", message: "Your Internet connection is down")
        }
        
    }
}

class aboutAppVC: UIViewController,WKUIDelegate {
    
    private var hasFinishedLoading = false
    var timer: Timer!
    var webView: WKWebView!
    
    override func loadView() {
        let webConfiguration = WKWebViewConfiguration()
        webView = WKWebView(frame: .zero, configuration: webConfiguration)
        webView.uiDelegate = self
        view = webView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "About App"
        
        //chech for network availiability
        if (SConnection.isConnectedToNetwork()){
            
            //call method
            let myURL = URL(string: "https://itunes.apple.com/gh/app/asoriba/id1198146836?mt=8")
            let myRequest = URLRequest(url: myURL!)
            webView.load(myRequest)
            
            
        }else{
            ControllerHelpers.showDialog(targetVC: self, title: "Ooops😭", message: "Your Internet connection is down")
        }
        
    }
}
