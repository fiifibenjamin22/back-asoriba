//
//  ViewController.swift
//  asoriba
//
//  Created by Bright Ahedor on 14/12/2016.
//  Copyright © 2016 Asoriba Inc. All rights reserved.
//

import UIKit

class OnboardController:UIPageViewController,UIPageViewControllerDataSource,UIPageViewControllerDelegate {
    lazy var viewControllerArray:[UIViewController] = {
        return [self.VCInstance(name: "walk1"),
                self.VCInstance(name: "walk2"),
                self.VCInstance(name: "walk3")]
    }()
    
    
    private func VCInstance(name: String) -> UIViewController{
        return UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: name)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.dataSource = self
        self.delegate = self
        
        if let fisrtViewController = viewControllerArray.first{
            setViewControllers([fisrtViewController], direction: .forward, animated: true, completion: nil)
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        for view in self.view.subviews{
            if view is UIScrollView{
                view.frame = UIScreen.main.bounds
            }else if view is UIPageControl{
                view.backgroundColor = UIColor.clear
            }
        }
    }
    public func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController?{
        guard let viewControllerIndex = viewControllerArray.index(of: viewController) else {
            return nil
        }
        let previousIndex = viewControllerIndex - 1
        
        guard previousIndex >= 0 else {
            return viewControllerArray.last
        }
        
        guard viewControllerArray.count > previousIndex else {
            return nil
        }
        
        return viewControllerArray[previousIndex]
    }
    
    
    public func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController?{
        guard let viewControllerIndex = viewControllerArray.index(of: viewController) else {
            return nil
        }
        let nextIndex = viewControllerIndex + 1
        
        guard nextIndex < viewControllerArray.count else {
            return viewControllerArray.first
        }
        
        guard viewControllerArray.count > nextIndex else {
            return nil
        }
        
        return viewControllerArray[nextIndex]
    }
    
    public func presentationCount(for pageViewController: UIPageViewController) -> Int {
        return viewControllerArray.count
    }
    
    public func presentationIndex(for pageViewController: UIPageViewController) -> Int {
        guard let firstViewController = viewControllers?.first, let firstViewControllerIndex
            = viewControllerArray.index(of: firstViewController) else {
                return 0
        }
        
        return firstViewControllerIndex
    }
    
}

