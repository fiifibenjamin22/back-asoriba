//
//  chatLogCell.swift
//  asoriba
//
//  Created by Benjamin Acquah on 19/09/2017.
//  Copyright © 2017 Asoriba Inc. All rights reserved.
//

import UIKit
import LBTAComponents

class chatLogCell: UICollectionViewCell {
    
    //chat log UIComponents
    let userName : UILabel = {
        let lbl = UILabel()
        lbl.text = ""
        lbl.textAlignment = .justified
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let chatter1_ProfileImage : UIImageView = {
        let img = UIImageView()
        img.image = UIImage(named: "try")
        img.contentMode = .scaleAspectFill
        img.layer.cornerRadius = 16
        img.layer.masksToBounds = true
        img.translatesAutoresizingMaskIntoConstraints = false
        return img
    }()
    
    let messageTextView : UITextView = {
        let txt = UITextView()
        txt.text = ""
        txt.font = UIFont.systemFont(ofSize: 16)
        txt.backgroundColor = .clear
        txt.isEditable = false
        txt.isSelectable = false
        txt.isScrollEnabled = false
        txt.translatesAutoresizingMaskIntoConstraints = false
        return txt
    }()
    
    let chatBubbleView : UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.gray
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.cornerRadius = 16
        view.layer.masksToBounds = true
        return view
    }()
    
    let timeCreated : UILabel = {
        let lbl = UILabel()
        lbl.text = ""
        lbl.font = UIFont.boldSystemFont(ofSize: 9)
        lbl.adjustsFontSizeToFitWidth = true
        lbl.textColor = UIColor.white
        lbl.textAlignment = .right
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let checkMark : UIButton = {
        let btn = UIButton()
        btn.setBackgroundImage(#imageLiteral(resourceName: "delivered"), for: UIControlState.normal)
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    var placeHolderImage = UIImage(named: "default")
    
    var bubbleWidthAnchor : NSLayoutConstraint?
    var bubbleRightAnchor : NSLayoutConstraint?
    var bubbleLeftAnchor : NSLayoutConstraint?
    var timeRightAnchor : NSLayoutConstraint?
    var timeTopAnchor : NSLayoutConstraint?

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setUpViews()
    }
    
    func setUpViews(){
        
        let path = Bundle.main.path(forResource: "configuration", ofType: "plist")
        let root = NSDictionary(contentsOfFile: path!)
        
        let dynamicBackgroundColor = (root?["headerBackgroundColor"] as! String)
        let textColoring = (root?["tint_white"] as! String)
        
        self.chatBubbleView.backgroundColor = UIColor().HexToColor(hexString: dynamicBackgroundColor)
        self.messageTextView.textColor = UIColor().HexToColor(hexString: textColoring)
        
        //addSubview(userName)
        addSubview(chatter1_ProfileImage)
        addSubview(chatBubbleView)
        addSubview(messageTextView)
        chatBubbleView.addSubview(timeCreated)
        chatBubbleView.addSubview(checkMark)
        
        let width : CGFloat
        width = 200
        
        bubbleRightAnchor = chatBubbleView.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -8)
        bubbleRightAnchor?.isActive = true
        bubbleWidthAnchor = chatBubbleView.widthAnchor.constraint(equalToConstant: width)
        bubbleWidthAnchor?.isActive = true
        bubbleLeftAnchor = chatBubbleView.leftAnchor.constraint(equalTo: chatter1_ProfileImage.rightAnchor, constant: 8)
        
        chatBubbleView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        chatBubbleView.heightAnchor.constraint(equalTo: self.heightAnchor).isActive = true
        
        messageTextView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        messageTextView.leftAnchor.constraint(equalTo: chatBubbleView.leftAnchor, constant: 8).isActive = true
        messageTextView.rightAnchor.constraint(equalTo: chatBubbleView.rightAnchor).isActive = true
        messageTextView.heightAnchor.constraint(equalTo: self.heightAnchor).isActive = true

        chatter1_ProfileImage.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 8).isActive = true
        chatter1_ProfileImage.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        chatter1_ProfileImage.widthAnchor.constraint(equalToConstant: 32).isActive = true
        chatter1_ProfileImage.heightAnchor.constraint(equalToConstant: 32).isActive = true
        
        checkMark.rightAnchor.constraint(equalTo: messageTextView.rightAnchor, constant: -8).isActive = true
        checkMark.bottomAnchor.constraint(equalTo: messageTextView.bottomAnchor, constant: -2).isActive = true
        checkMark.widthAnchor.constraint(equalToConstant: 15).isActive = true
        checkMark.heightAnchor.constraint(equalToConstant: 15).isActive = true
        
        
        timeRightAnchor = timeCreated.rightAnchor.constraint(equalTo: checkMark.leftAnchor, constant: -5)
        timeRightAnchor?.isActive = true
        timeCreated.bottomAnchor.constraint(equalTo: checkMark.bottomAnchor).isActive = true
        
        timeTopAnchor = timeCreated.topAnchor.constraint(equalTo: checkMark.topAnchor, constant: 0)
        timeTopAnchor?.isActive = true
        timeCreated.leftAnchor.constraint(equalTo: messageTextView.leftAnchor).isActive = true
        
    }
    
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
