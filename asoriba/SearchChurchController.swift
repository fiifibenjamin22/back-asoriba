//
//  SearchChurchController.swift
//  asoriba
//
//  Created by Bright Ahedor on 24/01/2017.
//  Copyright © 2017 Asoriba Inc. All rights reserved.
//

import UIKit
import Crashlytics
import SwiftSpinner
import Toaster

class SearchChurchController: UIViewController,UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate,ChurchServiceProtocol,JoinChurchServiceProtocol {

    @IBOutlet weak var searchBar: UISearchBar!
    var loadedPages = [String]()
    @IBOutlet weak var churchTableView: UITableView!
    var searchActive : Bool = false
    var feedsData = [Church]()
    let screenWidth = UIScreen.main.bounds.width
    var api:ApiService!
    var nextUrl = ""
    var text = ""
    var refreshControl = UIRefreshControl();
    var activityViewIndicator:UIActivityIndicatorView = UIActivityIndicatorView()
    
    @IBOutlet weak var noContentLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.churchTableView.delegate = self
        self.churchTableView.dataSource = self
        self.searchBar.delegate = self
        
        let path = Bundle.main.path(forResource: "configuration", ofType: "plist")
        let root = NSDictionary(contentsOfFile: path!)
        
        let greenBackGroundColor = (root?["headerBackgroundColor"] as! String)
        searchBar.barTintColor = UIColor().HexToColor(hexString: greenBackGroundColor)

        for subView in searchBar.subviews {
            for subView1 in subView.subviews{
                if let textField = subView1 as? UITextField{
                    subView1.backgroundColor = UIColor.black.withAlphaComponent(0.3)
                    //use the code below if you want to change the color of placeholder
                    let textFieldInsideUISearchBarLabel = textField.value(forKey: "placeholderLabel") as? UILabel
                    textFieldInsideUISearchBarLabel?.textColor = .white
                    textField.textColor = .white
                    textField.font = UIFont.boldSystemFont(ofSize: 14)
                }
            }
        }
        self.churchTableView.alwaysBounceVertical = true
        
        
        self.noContentLabel.isHidden = true
        //register cells
        self.churchTableView.register(UINib(nibName: CellIdentifiers.churchTableCell.rawValue, bundle: nil), forCellReuseIdentifier: CellIdentifiers.churchTableCell.rawValue)
        
        //api instance
        api = ApiService()
        self.api.churchDelegate = self
        self.api.joinChurchDelegate = self
        // Do any additional setup after loading the view.
        
        //tracking analytics
    }
    

    func startProgressBar()  {
        activityViewIndicator.center = self.view.center
        activityViewIndicator.hidesWhenStopped = true
        activityViewIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray
        self.view.addSubview(activityViewIndicator)
        activityViewIndicator.startAnimating()
    }
    
    func getData(page:String)  {
        self.api.searchChurches(url: "connections/search/?type=church&\(page)&page_size=10&keyword=\(text)")
    }

    
    func didReceiveChurchError(results: String) {
        activityViewIndicator.stopAnimating()
    }
    
    
    func didReceiveChurchDetail(results: String) {
        activityViewIndicator.stopAnimating()
        
    }
    
    func didReceiveChurchNextUrl(results: String) {
        self.nextUrl = results
    }
    
    func didReceiveChurchSuccess(results: [Church]) {
        activityViewIndicator.stopAnimating()
        self.feedsData = results
        self.churchTableView.reloadData()
        if results.isEmpty {
           self.noContentLabel.isHidden = false
           Toast(text: "No result found").show()
        }  else{
           self.noContentLabel.isHidden = true
        }
    }
    
    func didReceiveJoinChurchError(results: String) {
        SwiftSpinner.hide()
        self.showDialog(message: results, isError: true)
    }
    
    
    func didReceiveJoinChurchDetail(results: String) {
        SwiftSpinner.hide()
        self.showDialog(message: results, isError: true)
    }
    
    func didReceiveJoinChurchSuccess(results: Church) {
        SwiftSpinner.hide()
        self.showDialog(message: "Successfully joined \(results.name)", isError: false)
    }
    
    func showDialog(message:String,isError: Bool) {
        let  alert = UIAlertController(title: nil, message: message,preferredStyle: .alert)
        if (isError == true) {
            let cancel = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil)
            alert.addAction(cancel)
        }else{
            
            let action = UIAlertAction(title: "Okay",
                                       style: UIAlertActionStyle.default,
                                       handler: {(alert: UIAlertAction!) in
                                        
                                        self.restartApp()
            })
            alert.addAction(action)
        }
        self.present(alert,animated: true,completion: nil)
    }
    
    func restartApp(){
        //reload application data (renew root view )
        let vc = UIStoryboard(name:AllKeys.mainStoryBoard, bundle:nil).instantiateViewController(withIdentifier: AllKeys.mainController) as! MainTabController
        //let churchItem = feedsData[(sender.view?.tag)!]
        //vc.churchItem = churchItem
        self.navigationController?.navigationBar.isHidden = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let feed = feedsData[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.churchTableCell.rawValue, for: indexPath) as! ChurchTableCell
        
        cell.feed = feed
        
        cell.joinChurchButton.tag = indexPath.row
        cell.joinChurchButton.addTarget(self, action: #selector(self.joinChurch(_:)), for: .touchUpInside)
        
        cell.followChurchButton.tag = indexPath.row
        cell.followChurchButton.addTarget(self, action: #selector(self.followChurch(_:)), for: .touchUpInside)
        
        
        //gesture section content
        cell.contentView.isUserInteractionEnabled = true
        cell.contentView.tag = indexPath.row
        let tappedContent = UITapGestureRecognizer(target: self, action: #selector(self.tappedContent(_:)))
        tappedContent.cancelsTouchesInView = false
        tappedContent.numberOfTapsRequired = 2
        cell.contentView.addGestureRecognizer(tappedContent)
        //end of gesture
        
        return cell
    }
    
    
    func tappedContent(_ sender: UITapGestureRecognizer)
    {
        let vc = UIStoryboard(name:AllKeys.mainStoryBoard, bundle:nil).instantiateViewController(withIdentifier: AllKeys.churchProfileController) as! ChurchProfileController
        let churchItem = feedsData[(sender.view?.tag)!]
        vc.churchItem = churchItem
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let number = 250;
        return CGFloat(number)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return feedsData.count
    }
    
    
    //start comment here
    @IBAction func joinChurch(_ sender: UIButton) {
        let feed = self.feedsData[sender.tag]
        api.followChurch(church: feed,isJoining: true)
        SwiftSpinner.show("Getting church ready...", animated: true)
    }
    
    //play audio here
    @IBAction func followChurch(_ sender: UIButton) {
        let feed = self.feedsData[sender.tag]
        let title = sender.currentTitle
        if (title == "Follow") {
            sender.setTitle("Following", for: .normal)
            api.followChurch(church: feed,isJoining: true)
        } else {
            sender.setTitle("Follow", for: .normal)
            api.unFollowChurch(church: feed,isJoining: false)
        }
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchActive = true;
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        searchActive = false;
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        searchActive = false;
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        print("Text \(searchText)")
        self.text = searchText
        if (searchText.characters.count > 3) {
            self.getData(page: "page=1")
            //calling the protocall function
            self.startProgressBar()
        }
        
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let currentOffset = scrollView.contentOffset.y
        let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height
        
        if (maximumOffset - currentOffset) <= 400 {
            if(!self.nextUrl.isEmpty && !loadedPages.contains(self.nextUrl)){
                loadedPages.append(self.nextUrl)
                getData(page: self.nextUrl)
                print("Next Url \(self.nextUrl)")
            }
        }
    }


}
