//
//  AudioControllerViewController.swift
//  asoriba
//
//  Created by Bright Ahedor on 10/02/2017.
//  Copyright © 2017 Asoriba Inc. All rights reserved.
//

import Foundation
import UIKit
import MediaPlayer
import Jukebox
import AlamofireImage


class AudioControllerViewController: UIViewController,JukeboxDelegate {
    
    var feed: Feed!
    var jukebox : Jukebox!
    
    @IBAction func closePopUpButton(_ sender: UIButton) {
        self.closePopUp()
    }
    
    @IBAction func clickedAudioAction(_ sender: UIButton) {
        let string = self.audioActionButton.currentTitle
        if (string == "Pause") {
            self.audioActionButton.setTitle("Play", for: .normal)
            jukebox.pause()
        }else if string == "Play" {
            self.audioActionButton.setTitle("Stop", for: .normal)
            jukebox.play()
        }else {
            self.audioActionButton.setTitle("Play", for: .normal)
            jukebox.stop()
        }
       
    }

    @IBOutlet weak var audioCoverImageView: UIImageView!
    @IBOutlet weak var audioBgImageView: UIImageView!
    @IBOutlet weak var audioNameLabel: UILabel!
    @IBOutlet weak var audioUIprogressView: UIProgressView!
    @IBOutlet weak var audioActionButton: UIButton!
    
    @IBOutlet weak var audioTimeLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
        
        // begin receiving remote events
        UIApplication.shared.beginReceivingRemoteControlEvents()

        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.8)
        self.showAnimate()
        
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.closePopUp(_:)))
        self.view.addGestureRecognizer(tap)
        self.view.isUserInteractionEnabled = true
        
        jukebox = Jukebox(delegate: self, items: [
            JukeboxItem(URL: URL(string: feed.mediaFile)!)])!
        jukebox.play()
    }
    
    //end of delegate
    
    //close action on the parent view
    func closePopUp(_ sender: UITapGestureRecognizer) {
        self.view.endEditing(true)
        self.removeAnimate()
        self.view.removeFromSuperview()
        jukebox.stop()
    }
    
    //close action on the parent view
    func closePopUp() {
        self.view.endEditing(true)
        self.removeAnimate()
        self.view.removeFromSuperview()
        jukebox.stop()
    }
    
    //close action on the button
    func cancelAction() {
        self.removeAnimate()
        self.view.removeFromSuperview()
    }
    
    
    func showAnimate()
    {
        self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view.alpha = 0.0;
        UIView.animate(withDuration: 0.25, animations: {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        });
    }
    
    func removeAnimate()
    {
        UIView.animate(withDuration: 0.25, animations: {
            self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view.alpha = 0.0;
        }, completion:{(finished : Bool)  in
            if (finished)
            {
                self.view.removeFromSuperview()
            }
        });
    }
    
    // MARK:- Helpers -
    func populateLabelWithTime(_ label : UILabel, time: Double) {
        let minutes = Int(time / 60)
        let seconds = Int(time) - minutes * 60
        
        label.text = String(format: "%02d", minutes) + ":" + String(format: "%02d", seconds)
    }
    
    
    func resetUI()
    {
        self.audioNameLabel.text = ""
        self.audioTimeLabel.text = "00:00"
        self.audioUIprogressView.setProgress(0, animated: true)
    }
    
    override func remoteControlReceived(with event: UIEvent?) {
        if event?.type == .remoteControl {
            switch event!.subtype {
            case .remoteControlPlay :
                jukebox.play()
            case .remoteControlPause :
                jukebox.pause()
            case .remoteControlNextTrack :
                jukebox.playNext()
            case .remoteControlPreviousTrack:
                jukebox.playPrevious()
            case .remoteControlTogglePlayPause:
                if jukebox.state == .playing {
                    jukebox.pause()
                } else {
                    jukebox.play()
                }
            default:
                break
            }
        }
    }
    
    override var prefersStatusBarHidden : Bool {
        return true
    }
    
    func configureUI ()
    {
        resetUI()
        
        var message = feed.title
        if message.isEmpty {
            message = feed.content
        }
        self.audioNameLabel.text = message
        self.audioActionButton.layer.cornerRadius = 12
        view.backgroundColor = UIColor.black
        
        if  !(feed.member?.avatar.isEmpty)! {
            self.audioCoverImageView.af_setImage(
                withURL: URL(string: (feed.member?.avatar)!)!,
                placeholderImage: UIImage(named: "default"),
                imageTransition: .crossDissolve(0.2)
            )
        }else
        {
            self.audioCoverImageView.image = UIImage(named: "default")
        }
        
        if  !(feed.member?.avatar.isEmpty)! {
            self.audioBgImageView.af_setImage(
                withURL: URL(string: (feed.member?.avatar)!)!,
                placeholderImage: UIImage(named: "default"),
                imageTransition: .crossDissolve(0.2)
            )
        }else
        {
            self.audioBgImageView.image = UIImage(named: "default")
        }
    }
    
    // MARK:- JukeboxDelegate -
    
    func jukeboxDidLoadItem(_ jukebox: Jukebox, item: JukeboxItem) {
        print("Jukebox did load: \(item.URL.lastPathComponent)")
    }
    
    func jukeboxPlaybackProgressDidChange(_ jukebox: Jukebox) {
        
        if let currentTime = jukebox.currentItem?.currentTime, let duration = jukebox.currentItem?.meta.duration {
            let value = Float(currentTime / duration)
            self.audioUIprogressView.setProgress(value, animated: true)
            self.populateLabelWithTime(audioTimeLabel, time: currentTime)
            
        } else {
            resetUI()
        }
    }
    
    func jukeboxStateDidChange(_ jukebox: Jukebox) {
        
        UIView.animate(withDuration: 0.3, animations: { () -> Void in
            self.audioActionButton.alpha = jukebox.state == .loading ? 0 : 1
            self.audioActionButton.isEnabled = jukebox.state == .loading ? false : true
        })
        
        if jukebox.state == .ready {
            self.audioActionButton.setTitle("Play", for: .normal)
        } else if jukebox.state == .loading  {
            self.audioActionButton.setTitle("Pause", for: .normal)
        } else {
            var text = "Play"
            switch jukebox.state {
            case .playing, .loading:
                text = "Pause"
            case .paused, .failed, .ready:
                text = "Play"
            }
            self.audioActionButton.setTitle(text, for: .normal)
        }
        
        print("Jukebox state changed to \(jukebox.state)")
    }
    
    func jukeboxDidUpdateMetadata(_ jukebox: Jukebox, forItem: JukeboxItem) {
        print("Item updated:\n\(forItem)")
    }


}
