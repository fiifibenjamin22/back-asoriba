//
//  chatVC.swift
//  asoriba
//
//  Created by Benjamin Acquah on 23/08/2017.
//  Copyright © 2017 Asoriba Inc. All rights reserved.
//

import UIKit
import LBTAComponents
import FirebaseAuth
import Firebase
import FirebaseDatabase
import Alamofire
import AlamofireImage


class chatVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    lazy var collectionView : UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.delegate = self
        cv.dataSource = self
        cv.alwaysBounceVertical = true
        cv.showsVerticalScrollIndicator = false
        cv.backgroundColor = .white
        return cv
    }()
    
    var requestId = ""
    let currentUser = DataOperation.getUser()
    var chatusersHistory = [ChatUser]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView.register(chatCell.self, forCellWithReuseIdentifier: cellIdentify)
        self.collectionView.contentInset = UIEdgeInsetsMake(0, 0, 50, 0)
        view.backgroundColor = .white
        
        self.navigationItem.title = "Chats"
        let chatHistory = DataOperation.getChatUserData()
        chatusersHistory.append(contentsOf: chatHistory)
        
        self.collectionView.reloadData()

        setUpView()
        setUpnavItem()
    }
        
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
        
        chatusersHistory.removeAll()
        let chatHistory = DataOperation.getChatUserData()
        chatusersHistory.append(contentsOf: chatHistory)
        self.collectionView.reloadData()
    }
    
    func setUpView(){
        
        view.addSubview(collectionView)
        
        view.addConstraintsWithFormat("H:|[v0]|", views: collectionView)
        view.addConstraintsWithFormat("V:|[v0]|", views: collectionView)

    }
    
    func setUpnavItem(){
        
        let newChatImage = UIImage(named: "new_chat")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        let newChatBarButtonItem = UIBarButtonItem(image: newChatImage, style: .plain, target: self, action: #selector(handleNewChat))
        navigationItem.rightBarButtonItems = [newChatBarButtonItem]
        
//        let myProfileImage = UIImage(named: "user")?.withRenderingMode(.alwaysTemplate)
//        let myProfileBarButtonItem = UIBarButtonItem(image: myProfileImage, style: .plain, target: self, action: #selector(startUserProfile))
//        navigationItem.leftBarButtonItems = [myProfileBarButtonItem]
    }
    
    func handleNewChat(){
        
        let vc = chatUsersVC()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    // MARK: - Navigation
    func startUserProfile()  {
        let profileViewController = UIStoryboard(name:AllKeys.mainStoryBoard, bundle:nil).instantiateViewController(withIdentifier: AllKeys.userProfileController) as! ProfileViewController
        
        let member = Member()
        member.avatar = currentUser.avatar
        member.firstName = currentUser.firstName
        member.lastName = currentUser.lastName
        member.id = currentUser.id
        member.isFollowing = true
        member.numberOfFollowing = currentUser.numberOfFollowers
        member.numberOfGroups = currentUser.numberOfGroups
        profileViewController.user = member
        
        self.navigationController?.pushViewController(profileViewController, animated: true)
        
    }

    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return chatusersHistory.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentify, for: indexPath) as! chatCell
        let chatHistory = chatusersHistory[indexPath.row]
        cell.finalStatement.text = chatHistory.lastmessage
        cell.userName.text = "\(chatHistory.user2_firstname) \(chatHistory.user2_lastname)"
        cell.isOnline.text = chatHistory.userName
        
        let timeStamp : NSDate = NSDate(timeIntervalSince1970: TimeInterval(chatHistory.dateCreated / 1000))
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US")
        dateFormatter.dateFormat = "h:mm a"
        dateFormatter.timeZone = NSTimeZone.local
        dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
        cell.timeAgo.text = dateFormatter.string(from: timeStamp as Date)
        
        let chatuser_2avatar = chatHistory.profileImage
        
        if chatuser_2avatar != "" {
            cell.userAva.af_setImage(
                withURL: URL(string:  chatuser_2avatar)!,
                placeholderImage: cell.placeHolderImage,
                imageTransition: .crossDissolve(0.2)
            )
        }else{
            cell.userAva.image = cell.placeHolderImage
        }
        
        //MARK: sorting maximum and minimum time time created
        
//        self.chatusersHistory = Array(self.chatusersHistory)
//
//        self.chatusersHistory.sort { (message1, message2) -> Bool in
//
//            return message1.dateCreated > message2.dateCreated
//        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.view.frame.width, height: 80)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    //select a user to chat with
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let user2_lastname = chatusersHistory[indexPath.row].user2_lastname
        let user2_firstname = chatusersHistory[indexPath.row].user2_firstname
        let chat2_userName = chatusersHistory[indexPath.row].userName
        let user2_id = chatusersHistory[indexPath.row].user2_id
        let user2_profileImage = chatusersHistory[indexPath.row].profileImage
        
        let chat2_numberOfFollowers : Int = chatusersHistory[indexPath.row].numberOfFollowers
        let chat2_numberOfGroups : Int = chatusersHistory[indexPath.row].numberOfGroups
        
        let toChatLogCV = chatLogCV(collectionViewLayout: UICollectionViewFlowLayout())
        toChatLogCV.chatUser_2Id = user2_id
        toChatLogCV.chatUser_2firstName = user2_firstname
        toChatLogCV.chatUser_2lastname = user2_lastname
        toChatLogCV.chatuser_2avatar = user2_profileImage
        toChatLogCV.chatUserName = chat2_userName
        
        toChatLogCV.chat2_numberOfFollowers = chat2_numberOfFollowers
        toChatLogCV.chat2_numberOfGroups = chat2_numberOfGroups
        
        self.navigationController?.pushViewController(toChatLogCV, animated: true)
        
    }
}
