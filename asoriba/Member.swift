//
//  Member.swift
//  asoriba
//
//  Created by Bright Ahedor on 15/12/2016.
//  Copyright © 2016 Asoriba Inc. All rights reserved.
//

import RealmSwift

class Member: Object{
    var id = ""
    var firstName = ""
    var lastName = ""
    var otherName = ""
    var avatar = ""
    var numberOfFollowing = 0
    var numberOfGroups = 0
    var numberOfFollowers = 0
    var isMobile = true
    var isFollowing = false
    
    override class func primaryKey() -> String? {
        return "id"
    }
    
    
}
