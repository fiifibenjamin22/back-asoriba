//
//  AboutChurchController.swift
//  asoriba
//
//  Created by Bright Ahedor on 21/12/2016.
//  Copyright © 2016 Asoriba Inc. All rights reserved.
//

import UIKit
import Crashlytics
import PopupDialog

class AboutChurchController: UIViewController,UITableViewDelegate,UITableViewDataSource,AboutServiceProtocol{
    
    @IBOutlet weak var emptyAboutLabel: UILabel!
    @IBOutlet weak var aboutChurchTable: UITableView!
    var activityViewIndicator:UIActivityIndicatorView = UIActivityIndicatorView()
    var churchId: String!
    var churchName: String!
    var aboutArray = [About]()
    var api:ApiService!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        aboutChurchTable.delegate = self
        aboutChurchTable.dataSource = self
        aboutChurchTable.alwaysBounceVertical = true
        
        //register cells
        self.aboutChurchTable.register(UINib(nibName: CellIdentifiers.aboutTableCell.rawValue, bundle: nil), forCellReuseIdentifier: CellIdentifiers.aboutTableCell.rawValue)
        
        
        //api instance
        api = ApiService()
        self.api.aboutDelegate = self
        
        if (!churchId.isEmpty){
            api.getChurchInfo(branchId: churchId)
            //calling the protocall function
            self.startProgressBar()
            
        }
        if (churchId.isEmpty){
            self.showDialog(message: "You do not have a church, choose one to start")
        }
        
        
        self.emptyAboutLabel.isHidden = true
        
    }
    
    
    func startProgressBar()  {
        activityViewIndicator.center = self.view.center
        activityViewIndicator.hidesWhenStopped = true
        activityViewIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray
        self.view.addSubview(activityViewIndicator)
        activityViewIndicator.startAnimating()
    }
    
    
    func showDialog(message:String) {
        let  alert = UIAlertController(title: nil, message: message,preferredStyle: .alert)
        
        let action = UIAlertAction(title: "Okay",style: UIAlertActionStyle.default,
                                   handler: {(alert: UIAlertAction!) in
                                    self.startChosingChurch()
        })
        alert.addAction(action)
        self.present(alert,animated: true,completion: nil)
    }
    
    
    func startChosingChurch() {
        guard let vc = UIStoryboard(name:"Main", bundle:nil).instantiateViewController(withIdentifier: AllKeys.chooseChurchController) as? ChooseChurchController else {
            return
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK: - Table view delegate
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let feed = aboutArray[indexPath.row]
            let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.aboutTableCell.rawValue, for: indexPath) as! AboutChurchTableCell
            cell.feed = feed
        
        //gesture account
        let tapped = UITapGestureRecognizer(target: self, action: #selector(self.tappedContent(_:)))
        cell.contentView.isUserInteractionEnabled = true
        cell.contentView.tag = indexPath.row
        tapped.cancelsTouchesInView = false
        tapped.numberOfTapsRequired = 1
        cell.contentView.addGestureRecognizer(tapped)
        //end user gesture
        
        
        return cell
    }
    
    //MARK: now to user
    func tappedContent(_ sender: UITapGestureRecognizer)
    {
        let feed = self.aboutArray[(sender.view?.tag)!]
        showDialog(about: feed)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
         let header = CGFloat(100);
         let feed = aboutArray[indexPath.row]
         let contentHeight = Helpers.getHeightOfLabel(text: feed.subTitle, fontSize: CGFloat(17), width: UIScreen.main.bounds.width - CGFloat(16),numberOfLines: 3)
        return header + contentHeight
    }
    
    
    
    
    func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
        return false
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return aboutArray.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
         let item = aboutArray[indexPath.row]
         self.showDialog(about: item)
    }
    
    //MARK: - dialog action
    func showDialog(about:About) {
        
        let popOverVC = UIStoryboard(name: AllKeys.mainStoryBoard, bundle: nil).instantiateViewController(withIdentifier: AllKeys.aboutDetailViewController) as! AboutDetailViewController
        popOverVC.about = about
        self.addChildViewController(popOverVC)
        popOverVC.view.frame = self.view.frame
        self.view.addSubview(popOverVC.view)
        popOverVC.didMove(toParentViewController: self)
     
    }
    
    
    // MARK: - Navigation

    //MARK: - About protocol implementation
    func didReceiveAboutError(result: String) {
        self.activityViewIndicator.stopAnimating()
    }
    
    func didReceiveAboutInfo(result: Church) {
        self.activityViewIndicator.stopAnimating()
        
    }
    
    func didReceiveAbouts(results: [About]) {
        self.activityViewIndicator.stopAnimating()
        aboutArray.append(contentsOf: results)
        
        self.aboutChurchTable.reloadData()
        if(aboutArray.isEmpty){
            self.emptyAboutLabel.isHidden = false
        }
    }

}
