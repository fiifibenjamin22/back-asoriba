//
//  chatLogCV.swift
//  asoriba
//
//  Created by Benjamin Acquah on 17/09/2017.
//  Copyright © 2017 Asoriba Inc. All rights reserved.
//

import UIKit
import Firebase
import RealmSwift
import SwiftyJSON
import SwiftDate
import AlamofireImage
import Alamofire

var smallest : Int = 0
var largest : Int = 0
var isDelivered : Bool? = true

let realm = try! Realm()
class chatLogCV: UICollectionViewController, UITextFieldDelegate, UICollectionViewDelegateFlowLayout {
    
    var chatUser_2Id = ""
    var chatUser_2firstName = ""
    var chatUser_2lastname = ""
    var chatUserName = ""
    var chatuser_2avatar = ""
    
    var chat2_numberOfFollowers : Int = 0
    var chat2_numberOfGroups : Int = 0
    
    let chatBackView : UIImageView = {
        let img = UIImageView()
        img.image = #imageLiteral(resourceName: "chatandforumBack").withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        img.contentMode = .scaleAspectFill
        img.layer.masksToBounds = true
        img.translatesAutoresizingMaskIntoConstraints = false
        return img
    }()

    
    lazy var inputTextField : UITextField = {
        let TextField = UITextField()
        TextField.placeholder = "Enter Message..."
        TextField.keyboardAppearance = .dark
        TextField.delegate = self
        TextField.backgroundColor = .white
        TextField.translatesAutoresizingMaskIntoConstraints = false
        return TextField
    }()
 
    var userData = DataOperation.getUser()
    var chatMessagesArray = [Message]()
    var membersData = [Member]()
    var messageText = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.collectionView?.backgroundColor = .white
        self.collectionView?.alwaysBounceVertical = true
        self.collectionView?.contentInset = UIEdgeInsetsMake(8, 0, 8, 0)
        self.tabBarController?.tabBar.isHidden = true
        
        print(Realm.Configuration.defaultConfiguration.fileURL!)
        
        setupKeyboardObservers()
        
        self.collectionView?.register(chatLogCell.self, forCellWithReuseIdentifier: cellIdentify)
        collectionView?.keyboardDismissMode = .interactive
        
        
        returnTheLargestNumber()
        returnTheSmallestNumber()
        
        observeMessages()
        setUpNavBarWithUser()
        
        //setupBacgroundImage()
    }
    
    func setupBacgroundImage(){
        view.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        view.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        view.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        view.heightAnchor.constraint(equalTo: view.heightAnchor).isActive = true

    }
    
    func setUpNavBarWithUser(){
        
        let placeHolderImage = UIImage(named: "default")
        
        let titleView = UIView()
        titleView.frame = CGRect(x: 0, y: 0, width: 100, height: 40)
        titleView.backgroundColor = .clear
        
        let profileImageView = UIImageView()
        profileImageView.translatesAutoresizingMaskIntoConstraints = false
        profileImageView.layer.cornerRadius = 20
        profileImageView.layer.masksToBounds = true
        profileImageView.contentMode = .scaleAspectFill
        
        if chatuser_2avatar != "" {
            profileImageView.af_setImage(
                withURL: URL(string:  chatuser_2avatar)!,
                placeholderImage: placeHolderImage,
                imageTransition: .crossDissolve(0.2)
            )
        }else{
            profileImageView.image = placeHolderImage
        }

        titleView.addSubview(profileImageView)
        profileImageView.leftAnchor.constraint(equalTo: titleView.leftAnchor).isActive = true
        profileImageView.centerYAnchor.constraint(equalTo: titleView.centerYAnchor).isActive = true
        profileImageView.widthAnchor.constraint(equalToConstant: 40).isActive = true
        profileImageView.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        let nameLabel = UILabel()
        nameLabel.translatesAutoresizingMaskIntoConstraints = false
        nameLabel.text = "\(chatUser_2firstName) \(chatUser_2lastname)"
        nameLabel.textColor = .white
        nameLabel.font = UIFont.boldSystemFont(ofSize: 15)
        
        titleView.addSubview(nameLabel)
        nameLabel.anchor(titleView.topAnchor, left: profileImageView.rightAnchor, bottom: titleView.bottomAnchor, right: titleView.rightAnchor, topConstant: 0, leftConstant: 5, bottomConstant: 0, rightConstant: 5, widthConstant: 0, heightConstant: 0)
        
        nameLabel.isUserInteractionEnabled = true
        nameLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleGotoProfile)))
        
        self.navigationItem.titleView = titleView
    }
    
    //MARK: -- member profile
    func handleGotoProfile()  {
        let profileViewController = UIStoryboard(name:AllKeys.mainStoryBoard, bundle:nil).instantiateViewController(withIdentifier: AllKeys.userProfileController) as! ProfileViewController
        
        let member = Member()
        member.avatar = chatuser_2avatar
        member.firstName = chatUser_2firstName
        member.lastName = chatUser_2lastname
        member.id = chatUser_2Id
        member.isFollowing = true
        member.numberOfFollowing = chat2_numberOfFollowers
        member.numberOfGroups = chat2_numberOfGroups
        profileViewController.user = member
        
        self.navigationController?.pushViewController(profileViewController, animated: true)
        
    }
    
    lazy var inputContainerView : UIView = {
        
        let containerView = UIView()
        containerView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 50)
        containerView.backgroundColor = .white
        
        let sendButton = UIButton(type: .system)
        sendButton.setTitle("Send", for: .normal)
        sendButton.addTarget(self, action: #selector(sendMessage), for: UIControlEvents.touchUpInside)
        sendButton.translatesAutoresizingMaskIntoConstraints = false
        containerView.addSubview(sendButton)
        
        sendButton.rightAnchor.constraint(equalTo: containerView.rightAnchor).isActive = true
        sendButton.centerYAnchor.constraint(equalTo: containerView.centerYAnchor).isActive = true
        sendButton.widthAnchor.constraint(equalToConstant: 80).isActive = true
        sendButton.heightAnchor.constraint(equalTo: containerView.heightAnchor).isActive = true
        
        containerView.addSubview(self.inputTextField)
        self.inputTextField.leftAnchor.constraint(equalTo: containerView.leftAnchor, constant: 8).isActive = true
        self.inputTextField.centerYAnchor.constraint(equalTo: containerView.centerYAnchor).isActive = true
        self.inputTextField.rightAnchor.constraint(equalTo: sendButton.leftAnchor).isActive = true
        self.inputTextField.heightAnchor.constraint(equalTo: containerView.heightAnchor).isActive = true
        
        let seperatorLine = UIView()
        seperatorLine.backgroundColor = UIColor.lightGray
        seperatorLine.translatesAutoresizingMaskIntoConstraints = false
        containerView.addSubview(seperatorLine)
        
        seperatorLine.leftAnchor.constraint(equalTo: containerView.leftAnchor).isActive = true
        seperatorLine.topAnchor.constraint(equalTo: containerView.topAnchor).isActive = true
        seperatorLine.widthAnchor.constraint(equalTo: containerView.widthAnchor).isActive = true
        seperatorLine.heightAnchor.constraint(equalToConstant: 1).isActive = true
        
        return containerView
    }()
    
    func backgroundView(){
        
        
    }
    
    override var inputAccessoryView: UIView? {
        get {
            
            return inputContainerView
        }
    }
    
    
    override var canBecomeFirstResponder: Bool {
        return true
    }
    
    func setupKeyboardObservers(){
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardDidShow), name: NSNotification.Name.UIKeyboardDidShow, object: nil)
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
    func handleKeyboardDidShow(){
        //scroll to the last index
        if chatMessagesArray.count > 0 {
            let indexPath = NSIndexPath(item: self.chatMessagesArray.count - 1, section: 0)
            self.collectionView?.scrollToItem(at: indexPath as IndexPath, at: .bottom, animated: true)
            
        }
    }

    
    //sort for the largest number and return it
    func returnTheLargestNumber() -> Int {
        
        let first = Int(userData.id)
        let second = Int(chatUser_2Id)
        largest = max(first!, second!)
        print("print the largest: ", largest)
        
        return largest
    }
    
    //sort for the smallest number and return it
    func returnTheSmallestNumber() -> Int {
        
        let first = Int(userData.id)
        let second = Int(chatUser_2Id)
        smallest = min(first!, second!)
        print("print the smallest: ", smallest)
        
        return smallest
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {

        return chatMessagesArray.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentify, for: indexPath) as! chatLogCell
        cell.backgroundColor = .white
        let messages = chatMessagesArray[indexPath.row]
        cell.messageTextView.text = messages.message
        
        let timeStamp : NSDate = NSDate(timeIntervalSince1970: messages.created! / 1000)
        print("print raw date: \(timeStamp)")
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US")
        dateFormatter.dateFormat = "h:mm a"
        dateFormatter.timeZone = NSTimeZone.local
        dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
        cell.timeCreated.text = dateFormatter.string(from: timeStamp as Date)
        
        setUpCell(cell: cell, message: messages)

        //modify bubble view width
        cell.bubbleWidthAnchor?.constant = estimatedFrameForText(text: messages.message!).width + 32
        
        return cell
    }
    
    private func setUpCell(cell: chatLogCell, message: Message) {
        
        //chech for incoming and outgoing messages
        if message.id == userData.id {
            //outgoing messages
            let path = Bundle.main.path(forResource: "configuration", ofType: "plist")
            let root = NSDictionary(contentsOfFile: path!)
            
            let dynamicBackgroundColor = (root?["headerBackgroundColor"] as! String)
            cell.chatBubbleView.backgroundColor = UIColor().HexToColor(hexString: dynamicBackgroundColor)
            cell.chatter1_ProfileImage.isHidden = true
            
            cell.bubbleRightAnchor?.isActive = true
            cell.bubbleLeftAnchor?.isActive = false
            cell.checkMark.isHidden = false
            cell.timeRightAnchor?.constant = -5
            cell.timeTopAnchor?.constant = 0
        }else{
            //incomming messages
            cell.chatBubbleView.backgroundColor = .gray
            cell.chatter1_ProfileImage.isHidden = false

            cell.bubbleRightAnchor?.isActive = false
            cell.bubbleLeftAnchor?.isActive = true
            cell.checkMark.isHidden = true
            cell.timeRightAnchor?.constant = 10
            cell.timeTopAnchor?.constant = 5
            
            if chatuser_2avatar != "" {
                cell.chatter1_ProfileImage.af_setImage(
                    withURL: URL(string:  chatuser_2avatar)!,
                    placeholderImage: cell.placeHolderImage,
                    imageTransition: .crossDissolve(0.2)
                )
            }else{
                cell.chatter1_ProfileImage.image = cell.placeHolderImage
            }
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var height : CGFloat = 80
        
        //get the dynamic height
        if let text = chatMessagesArray[indexPath.row].message {
            height = estimatedFrameForText(text: text).height + 30
        }
        
        return CGSize(width: view.frame.width, height: height)
    }
    
    private func estimatedFrameForText(text: String) -> CGRect {
        let size = CGSize(width: 200, height: 1000)
        let options = NSStringDrawingOptions.usesFontLeading.union(NSStringDrawingOptions.usesLineFragmentOrigin)
        return NSString(string: text).boundingRect(with: size, options: options, attributes: [NSFontAttributeName : UIFont.systemFont(ofSize: 16)], context: nil)
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        self.collectionView?.collectionViewLayout.invalidateLayout()
    }
    
    var containerViewBottomAnchor : NSLayoutConstraint?
    
    func sendMessage(){
        self.messageText = self.inputTextField.text!
        
        if !self.messageText.isEmpty {
         
            //get user2_id snapshot from firebase and save it into the database
            let timeStamp : Double = Date().timeIntervalSince1970 * 1000
            
            let myuser_id = userData.id
            let conc_id = "\(smallest)-\(largest)"
            let userName = "\(userData.firstName) \(userData.lastName)"
            
            //save into firebase database
            //send as a JSON format to firebase
            let json = "{\"message\":\"\(inputTextField.text!)\",\"username\":\"\(userName)\",\"id\":\"\(myuser_id)\",\"created\":\(timeStamp)}"
            let ref = Database.database().reference().child("ICGC").child("messages")
            let childRef = ref.child(conc_id).childByAutoId()
            let values = json
            childRef.setValue(values)
            
            //update realm with second users recent chat data
            DataOperation.updateChatUsers(user2_id: chatUser_2Id, firstName: chatUser_2firstName, lastName: chatUser_2lastname, dateCreated: timeStamp , profileImage: chatuser_2avatar, lastMessage: inputTextField.text!, userName: chatUserName, numberOfFollowers: chat2_numberOfFollowers, numberOfGroups: chat2_numberOfGroups)

            self.inputTextField.text = ""
        }
    }
    

    //observe chat data from the firebase database
    func observeMessages(){

        let conc_id = "\(smallest)-\(largest)"
        let ref = Database.database().reference().child("ICGC").child("messages")
        ref.child(conc_id).observe(.childAdded, with: { (snapshot) in
            
            let message = Message()
            let snapShotMessagesLogObject = snapShotMessagesLog()
            
            let snapshotValue = snapshot.value!
            snapShotMessagesLogObject.snapshotValueObject = snapshotValue as? String

            let theMessageValue = snapShotMessagesLogObject.snapshotValueObject!
            
            let data: NSData = theMessageValue.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))! as NSData
            
            let json = JSON(data: data as Data)
            //get messages from JSON
            if let textMessage = json["message"].string {
                //Now you got your value
                message.message = textMessage
                let storedtextMessage = message.message!
                print("print text messages from user data: ", storedtextMessage)

            }
            //get username from JSON
            if let username = json["username"].string {
                message.username = username
                print("print text username from user data: ", username)

            }
            //get id from JSON
            if let my_id_in_firebase = json["id"].string {
                message.id = my_id_in_firebase
                print("print text id from user data: ", my_id_in_firebase)

            }
            //get timecreated from JSON
            if let timeCreated = json["created"].double {
                message.created = timeCreated
                print("print text time from user data: ", timeCreated)

            }
            
            self.chatMessagesArray.append(message)
                        
            DispatchQueue.main.async {
                self.collectionView?.reloadData()
                
                //scroll to the last index
                let indexPath = NSIndexPath(item: self.chatMessagesArray.count - 1, section: 0)
                self.collectionView?.scrollToItem(at: indexPath as IndexPath, at: .bottom, animated: true)
            }
            
        }, withCancel: nil)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        sendMessage()
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {

    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {

    }
}
