//
//  forumVC.swift
//  asoriba
//
//  Created by Benjamin Acquah on 06/10/2017.
//  Copyright © 2017 Asoriba Inc. All rights reserved.
//

import UIKit
import LBTAComponents
import Firebase
import SwiftyJSON

class forumsVC: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    
    var forumTopicsArray = [forumTopics]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.collectionView?.backgroundColor = .white
        self.navigationItem.title = "Forums"
        
        self.collectionView?.register(forumsCell.self, forCellWithReuseIdentifier: cellIdentify)
        
        fetchForumTopics()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        UIView.animate(withDuration: 0.5) {
            self.tabBarController?.tabBar.isHidden = false
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return forumTopicsArray.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentify, for: indexPath) as! forumsCell
        let forumTopic = forumTopicsArray[indexPath.row]
        cell.titleLabel.text = forumTopic.topicTitle
        cell.topicDescription.text = forumTopic.topicDescription
        cell.notificationIcon.isUserInteractionEnabled = true
        cell.notificationIcon.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleSetNotification)))
        
        return cell
    }
    
    func handleSetNotification(){
        print("12345")
    }
    
    //fetch topics
    func fetchForumTopics(){
        
        let ref = Database.database().reference().child("ICGC").child("forumtopics")
        ref.observe(.childAdded, with: { (snapshot) in
            
            let forumTopic = forumTopics()
            
            if let value = snapshot.value as? NSDictionary as? [String : AnyObject] {
                
                forumTopic.topicTitle = value["title"] as? String
                forumTopic.id = value["id"] as? String
                forumTopic.topicDescription = value["description"] as? String
                self.forumTopicsArray.append(forumTopic)
                
                DispatchQueue.main.async {
                    self.collectionView?.reloadData()
                }
                
            }
            
        }, withCancel: nil)
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let cv = forumLogCV(collectionViewLayout: UICollectionViewFlowLayout())
        cv.id = forumTopicsArray[indexPath.row].id!
        cv.topicTitle = forumTopicsArray[indexPath.row].topicTitle!
        
        self.navigationController?.pushViewController(cv, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.view.frame.size.width, height: 120)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
}
