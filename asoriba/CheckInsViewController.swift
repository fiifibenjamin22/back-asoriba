//
//  CheckInsViewController.swift
//  asoriba
//
//  Created by Bright Ahedor on 16/03/2017.
//  Copyright © 2017 Asoriba Inc. All rights reserved.
//

import UIKit

class CheckInsViewController: UIViewController,CheckInServiceProtocol ,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{

    @IBOutlet weak var noCheckLabel: UILabel!
    let apiService = ApiService()
    var feedArray = [CheckIn]()
    @IBOutlet weak var feedCollectionView: UICollectionView!
    let user = DataOperation.getUser()
    let numberOfItemsPerRow : CGFloat = 1.0
    var activityViewIndicator:UIActivityIndicatorView = UIActivityIndicatorView()
    var refreshControl: UIRefreshControl = UIRefreshControl();
    var loadedPages = [String]()
    var nextUrl = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        
        let tag = "Check History"
        self.navigationItem.title = tag
        
        
        self.feedCollectionView.delegate = self
        self.feedCollectionView.dataSource = self
        self.apiService.checkInServiceProtocol = self
        
        //register cell
        self.feedCollectionView.register(UINib(nibName: CellIdentifiers.checkInCollectionViewCell.rawValue, bundle: nil), forCellWithReuseIdentifier: CellIdentifiers.checkInCollectionViewCell.rawValue)
        //end section here
        
        //refreshing
        refreshControl.addTarget(self, action: #selector(GivingController.refresh), for: .valueChanged)
        feedCollectionView.addSubview(refreshControl)
        
        startProgressBar()
        self.getData(page: "?page=1")
        
        self.noCheckLabel.isHidden = true
        
        
    }
    
    func startProgressBar()  {
        activityViewIndicator.center = self.view.center
        activityViewIndicator.hidesWhenStopped = true
        activityViewIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray
        self.view.addSubview(activityViewIndicator)
        activityViewIndicator.startAnimating()
    }
    


    func getData(page: String)  {
        self.loadedPages.append(page)
        self.apiService.checkIns(url: "attendance/check_ins/\(page)&page_size=10")
    }
    
    //MARK:- collection view delegate
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return feedArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let feed = self.feedArray[indexPath.row]
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CellIdentifiers.checkInCollectionViewCell.rawValue, for: indexPath) as! CheckInCollectionViewCell
        cell.backgroundColor = UIColor.white
        
        cell.feed = feed
                
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, shouldHighlightItemAt indexPath: IndexPath) -> Bool {
        return false
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return CGFloat(AllKeys.cellSpacing)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return CGFloat(AllKeys.cellSpacing)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: CGFloat(AllKeys.cellSpacing), left: 0.0, bottom: 0.0, right: 0.0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
        let contentWidth = collectionView.bounds.width
        let totalSpace = flowLayout.sectionInset.left
            + flowLayout.sectionInset.right
            + (flowLayout.minimumInteritemSpacing * CGFloat(numberOfItemsPerRow - 1))
        let size = Int((contentWidth - totalSpace) / CGFloat(numberOfItemsPerRow))
        return CGSize(width: size, height: Int(100))
    }
    

    func didReceiveCheckError(result: String) {
      self.activityViewIndicator.stopAnimating()
      if self.feedArray.isEmpty {
         self.noCheckLabel.text = "Unable to get your recorded checkins"
      }
    }
    
    func didReceiveCheckResult(result: CheckIn) {
        
    }
    
    func didReceiveChecksResult(results: [CheckIn]) {
       self.activityViewIndicator.stopAnimating()
       self.feedArray.append(contentsOf: results)
       self.feedCollectionView.reloadData()
        if self.feedArray.isEmpty {
           self.noCheckLabel.isHidden = false
        }
    }
    
    func didReceiveCheckNextUrl(result: String) {
        self.nextUrl = result
    }
    
    
    //MARK: - scroll handling section
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let currentOffset = scrollView.contentOffset.y
        let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height
        
        if (maximumOffset - currentOffset) <= 400 {
            if !(self.nextUrl.isEmpty) && !self.loadedPages.contains(self.nextUrl) {
                self.getData(page: self.nextUrl)
                
            }
        }
    }

    //calling the resfreshing endpoint
    func refresh()  {
        self.getData(page: "?page=1")
    }

}
