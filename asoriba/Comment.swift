//
//  Comment.swift
//  asoriba
//
//  Created by Bright Ahedor on 19/12/2016.
//  Copyright © 2016 Asoriba Inc. All rights reserved.
//

import RealmSwift

class Comment: Object {
    var id = ""
    var content = ""
    var date = ""
    dynamic var member: Member?
    
    override class func primaryKey() -> String? {
        return "id"
    }
   
}
