//
//  ActionPerformersController.swift
//  asoriba
//
//  Created by Bright Ahedor on 24/01/2017.
//  Copyright © 2017 Asoriba Inc. All rights reserved.
//

import UIKit
import Crashlytics

class ActionPerformersController: UIViewController,ChurchMembersServiceProtocol,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var membersTableView: UITableView!
    var isAmeners = false
    var api:ApiService!
    var membersData = [Member]()
    var loadedPages = [String]()
    var feedType = ""
    var contentId = ""
    var nextUrl = "page=1"
    var activityViewIndicator: UIActivityIndicatorView = UIActivityIndicatorView()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        var tag = ""
        if isAmeners == true {
            tag = "Those Who Amened"
            feedType = "amen"
        } else {
            tag = "Those Who Shared"
            feedType = "share"
        }
        
        self.navigationItem.title = tag

        self.membersTableView.delegate = self
        self.membersTableView.dataSource = self
        self.membersTableView.alwaysBounceVertical = true
        
     
        //member cells
        self.membersTableView.register(UINib(nibName: CellIdentifiers.memberCell.rawValue, bundle: nil), forCellReuseIdentifier: CellIdentifiers.memberCell.rawValue)
    
        
        //api instance
        api = ApiService()
        api.churchMembersDelegate = self
    
       
       
        self.startProgressBar()
        getData()
        
    }

    func startProgressBar()  {
        activityViewIndicator.center = self.view.center
        activityViewIndicator.hidesWhenStopped = true
        activityViewIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray
        self.view.addSubview(activityViewIndicator)
        activityViewIndicator.startAnimating()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
            let feed = membersData[indexPath.row]
            let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.memberCell.rawValue, for: indexPath) as! MemberCell
            cell.feed = feed
        
            cell.noAction = true
            //gesture section image
            cell.contentView.isUserInteractionEnabled = true
            cell.contentView.tag = indexPath.row
            let tapped = UITapGestureRecognizer(target: self, action: #selector(self.startDetail(_:)))
            tapped.cancelsTouchesInView = false
            tapped.numberOfTapsRequired = 1
            cell.contentView.addGestureRecognizer(tapped)
            //end of gesture
            
            return cell
        
    }
    
    
    //MARK: -- member profile
    func startDetail(_ sender: UITapGestureRecognizer)  {
        let profileController = UIStoryboard(name:"Main", bundle:nil).instantiateViewController(withIdentifier: AllKeys.userProfileController) as! ProfileViewController
        let member = membersData[(sender.view?.tag)!]
        profileController.user = member
        self.navigationController?.pushViewController(profileController, animated: true)
    }

    //calculating the item height
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CGFloat(80)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return membersData.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
        return false
    }
    
    func didReceiveChurchMembersError(results: String) {
        self.activityViewIndicator.stopAnimating()
    }
    
    func didReceiveChurchMemberNextUrl(results: String) {
        self.nextUrl = results
    }
    
    func didReceiveChurchMembersDetail(results: String) {
        self.activityViewIndicator.stopAnimating()
    }
    
    func didReceiveChurchMembersSuccess(results: [Member]) {
        self.membersData.append(contentsOf: results)
        self.membersTableView.reloadData()
        self.activityViewIndicator.stopAnimating()
    }
    
    func getData() {
         self.loadedPages.append(nextUrl)
         api.getActionPerfomers(contentId:contentId,type:feedType,next:nextUrl)
    }
    
    //MARK: - scroll handling section
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let currentOffset = scrollView.contentOffset.y
        let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height
        
        if (maximumOffset - currentOffset) <= 100 {
            if(!self.nextUrl.isEmpty && !loadedPages.contains(self.nextUrl)){
                getData()
            }
        }
    }
    

}
