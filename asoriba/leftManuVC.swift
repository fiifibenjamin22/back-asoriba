//
//  leftManuVC.swift
//  asoriba
//
//  Created by Benjamin Acquah on 11/09/2017.
//  Copyright © 2017 Asoriba Inc. All rights reserved.
//

import UIKit
import LBTAComponents

let headerID = "headerID"
class leftManuVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    lazy var collectionView : UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.delegate = self
        cv.dataSource = self
        cv.alwaysBounceVertical = true
        cv.showsVerticalScrollIndicator = false
        cv.backgroundColor = .white
        return cv
    }()
    
    let statusBarColor = UIView()
    
    let manuItemNames = ["Search","Forum","Payment history","Profile","Invite Friends","Settings","Help"]
    let manuItemIcons = ["search","forum","payment_history","profile","app_invite","setting","help"]

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        self.navigationController?.navigationBar.isHidden = true
        
        if let path = Bundle.main.path(forResource: "configuration", ofType: "plist"){
            let root = NSDictionary(contentsOfFile: path)
            
            let manuIconsColor = (root?["headerBackgroundColor"] as! String)
            
            statusBarColor.backgroundColor = UIColor().HexToColor(hexString: manuIconsColor)
        }
        
        collectionView.register(sideManuCell.self, forCellWithReuseIdentifier: cellIdentify)
        collectionView.register(sideManuHeaderCell.self, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: headerID)
        
        setUpViews()
        
    }
    
    func setUpViews(){
        
        view.addSubview(collectionView)
        
        view.addConstraintsWithFormat("H:|[v0]|", views: collectionView)
        view.addConstraintsWithFormat("V:|[v0]|", views: collectionView)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 7
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentify, for: indexPath) as! sideManuCell
        cell.itemName.text = manuItemNames[indexPath.item]
        cell.itemIcon.image = UIImage(named: manuItemIcons[indexPath.item])?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
        
        if let path = Bundle.main.path(forResource: "configuration", ofType: "plist"){
            let root = NSDictionary(contentsOfFile: path)
            
            let manuIconsColor = (root?["headerBackgroundColor"] as! String)
            
            cell.itemIcon.tintColor = UIColor().HexToColor(hexString: manuIconsColor)
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.view.frame.size.width, height: 50)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: headerID, for: indexPath) as! sideManuHeaderCell
        return header
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: self.view.frame.size.width, height: self.view.frame.size.height / 3.0 - 10)
    }
    
}
