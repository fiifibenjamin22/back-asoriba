//
//  churchInformation.swift
//  asoriba
//
//  Created by Benjamin Acquah on 03/11/2017.
//  Copyright © 2017 Asoriba Inc. All rights reserved.
//

import Foundation

class churchInfo: NSObject {
    
    var locationNames : String?
    var locationCordinates = [Double]()
    var branchPhoneNumber = [Int]()
}

class snapShotFromChurchInfo: NSObject {
    
    var snapshotValueObject : String?

}
