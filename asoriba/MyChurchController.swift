//
//  MyChurchController.swift
//  asoriba
//
//  Created by Bright Ahedor on 15/12/2016.
//  Copyright © 2016 Asoriba Inc. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage
import Photos
import SKPhotoBrowser
import Crashlytics
import Fabric


class MyChurchController: UIViewController,UITableViewDelegate,UITableViewDataSource,GroupsServiceProtocol,ChurchMembersServiceProtocol,FeedServiceProtocol,AboutServiceProtocol{
    
    @IBOutlet weak var dataTableView: UITableView!
    
    var api:ApiService!
    
    var positionSelected = 0
    var membersData = [Member]()
    var groupsData = [Group]()
    var feedsData = [Feed]()
    var userData = DataOperation.getUser()
    var churchItem: Church!
    var feedNextUrl = ""
    var membersNextUrl = ""
    var groupsNextUrl = ""
    var loadedPages = [String]()
    var myChurch = [Church]()
    var header = MyChurchTableHeader()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let navTitleLabel = UILabel()
        navTitleLabel.text = userData.branchName
        navTitleLabel.textAlignment = .center
        navTitleLabel.font = UIFont.boldSystemFont(ofSize: 15)
        navTitleLabel.textColor = .white
        navTitleLabel.adjustsFontSizeToFitWidth = true
        self.navigationItem.titleView = navTitleLabel
        
        dataTableView.delegate = self
        dataTableView.dataSource = self
        dataTableView.alwaysBounceVertical = true
        
        
        //regiser header
        let nib = UINib(nibName: CellIdentifiers.myChurchHeader.rawValue, bundle: nil)
        self.dataTableView.register(nib, forHeaderFooterViewReuseIdentifier: CellIdentifiers.myChurchHeader.rawValue)
        
        
        //register cells
        self.dataTableView.register(UINib(nibName: CellIdentifiers.groupCell.rawValue, bundle: nil), forCellReuseIdentifier: CellIdentifiers.groupCell.rawValue)
        self.dataTableView.register(UINib(nibName: CellIdentifiers.memberCell.rawValue, bundle: nil), forCellReuseIdentifier: CellIdentifiers.memberCell.rawValue)
        self.dataTableView.register(UINib(nibName: CellIdentifiers.feedWithImageTableCell.rawValue, bundle: nil), forCellReuseIdentifier: CellIdentifiers.feedWithImageTableCell.rawValue)
        
        self.dataTableView.register(UINib(nibName: CellIdentifiers.feedUniversalNoImageTableViewCell.rawValue, bundle: nil), forCellReuseIdentifier: CellIdentifiers.feedUniversalNoImageTableViewCell.rawValue)
        
        
         self.dataTableView.register(UINib(nibName: CellIdentifiers.feedUniversalCell.rawValue, bundle: nil), forCellReuseIdentifier: CellIdentifiers.feedUniversalCell.rawValue)
        //end
        
        //api instance
        api = ApiService()
        self.api.feedDelegate = self
        self.api.churchGroupsDelegate = self
        self.api.churchMembersDelegate = self
        self.api.aboutDelegate = self
        
       //setting the table data
        
        if !userData.churchId.isEmpty {
            self.getGroups(url: "page=1",churchId: userData.churchId)
            self.getMembers(url: "page=1",churchId: userData.churchId)
            self.getContent(url:"?page=1",churchId: userData.churchId)
            api.getChurchInfo(branchId: userData.churchId)
        }
        
    }

    func setUpNavBarItems(){
        
        let moreImage = UIImage(named: "searching")?.withRenderingMode(.alwaysTemplate)
        let moreBarButtonItem = UIBarButtonItem(image: moreImage, style: .plain, target: self, action: #selector(handleMore))
        moreBarButtonItem.width = 50
//        moreBarButtonItem.imageInsets = UIEdgeInsetsMake(0, 0, 0, 5)
        
        navigationItem.rightBarButtonItems = [moreBarButtonItem]
    }

    @objc func handleMore(){
        
        let searchPageVC = ChooseChurchController()
        self.navigationController?.pushViewController(searchPageVC, animated: true)
    }
    
    //MARK: - About protocol implementation
    func didReceiveAboutError(result: String) {
       
    }
    
    func didReceiveAboutInfo(result: Church) {
        //update church info
        //DataOperation.updateUserChurchNumbers(id: self.userData.id, numberOfFollowers: result.numberOfFollowers, numberOfGroups: result.numberOfGroups)
        //update the header stuffs
        header.churchItem = result
    }
    
    func didReceiveAbouts(results: [About]) {
        
    }

    //set image
    func tappedUserImage()
    {
        ControllerHelpers.presentSingleImage(targetVC: self, url: self.userData.churchLogo)
    }
    
  
    func getGroups(url:String,churchId:String) {
        api.getChurchGroups(churchId: churchId,next:url)
    }
    
    func getMembers(url:String,churchId:String) {
        api.getChurchMembers(branchId: churchId,next:url)
    }
    
    func getContent(url:String,churchId:String) {
        api.getChurchContent(branchId: churchId, page: url)
    }
    
    
    //
    func didReceiveError(results: String) {
        //show errrors
    }
    
    
    func didReceiveGroupsError(results: String) {
        //groups error
    }
    
    func didReceiveChurchMembersError(results: String) {
        //members error
    }
    
    
    func didReceiveDetail(results: String) {
        //detail on feed
    }
    
    func didReceiveGroupsDetail(results: String) {
        //groups detail
    }
    
    func didReceiveChurchMembersDetail(results: String) {
        //member detail
    }
    
    
    func didReceiveNextUrl(results: String) {
        //next Url
        self.feedNextUrl = results
    }
    
    func didReceiveChurchMemberNextUrl(results: String) {
        //member next Url
        self.membersNextUrl = results
    }
    
    func didReceiveChurchGroupsNextUrl(results: String) {
        //groups next Url
        self.groupsNextUrl = results
    }
    

    func didReceiveSuccess(results: [Feed]) {
        self.feedsData.append(contentsOf: results)
        self.dataTableView.reloadData()
    }
    
    
    func didReceiveGroupsSuccess(results: [Group]) {
         self.groupsData.append(contentsOf: results)
         self.dataTableView.reloadData()
    }
    

    func didReceiveChurchMembersSuccess(results: [Member]) {
         self.membersData.append(contentsOf: results)
         self.dataTableView.reloadData()
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == AllKeys.aboutChurchSegue){
            let aboutController = segue.destination as! AboutChurchController
            aboutController.churchId = self.userData.churchId
            aboutController.churchName = self.userData.churchName
        }
        
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
        if(positionSelected == 0){
              let feed = membersData[indexPath.row]
              let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.memberCell.rawValue, for: indexPath) as! MemberCell
            
              cell.feed = feed
            
              if self.userData.id == feed.id {
                cell.followActionButton.isHidden = true
              }else {
                cell.followActionButton.isHidden = false
                cell.followActionButton.tag = indexPath.row
                cell.followActionButton.addTarget(self, action: #selector(MyChurchController.followUser(_:)), for: .touchUpInside)
               }
            
              //gesture section image
              cell.contentView.isUserInteractionEnabled = true
              cell.contentView.tag = indexPath.row
              let tapped = UITapGestureRecognizer(target: self, action: #selector(self.startDetail(_:)))
              cell.contentView.addGestureRecognizer(tapped)
              //end of gesture
            
            
              return cell
        }else if (positionSelected == 1){
            let feed = groupsData[indexPath.row]
            let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.groupCell.rawValue, for: indexPath) as! GroupTableCell
            cell.groupJoinAction.addTarget(self, action: #selector(MyChurchController.followGroup(_:)), for: .touchUpInside)
            cell.feed = feed
            
            //gesture section image
            cell.aboutImg.isUserInteractionEnabled = true
            cell.aboutImg.tag = indexPath.row
            let tapped = UITapGestureRecognizer(target: self, action: #selector(self.startGroupDetail(_:)))
            tapped.cancelsTouchesInView = false
            tapped.numberOfTapsRequired = 1
            cell.aboutImg.addGestureRecognizer(tapped)
            //end of gesture
            
            return cell
        }else{
            let feed = feedsData[indexPath.row]
            
            if (feed.gallery.count == 0 && feed.image.isEmpty && feed.mediaFile.isEmpty){
                let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.feedUniversalNoImageTableViewCell.rawValue, for: indexPath) as! FeedUniversalNoImageTableViewCell
                
                cell.feed = feed
                
                //gesture section image
                cell.contentView.isUserInteractionEnabled = true
                cell.contentView.tag = indexPath.row
                let tapped = UITapGestureRecognizer(target: self, action: #selector(self.startFeedDetail(_:)))
                tapped.cancelsTouchesInView = false
                tapped.numberOfTapsRequired = 1
                cell.contentView.addGestureRecognizer(tapped)
                //end of gesture
                
                return cell
                
            }else {
                let  cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.feedUniversalCell.rawValue, for: indexPath) as! FeedUniversalCell
                
                cell.feed = feed
                
                //gesture section image
                cell.contentView.isUserInteractionEnabled = true
                cell.contentView.tag = indexPath.row
                let tapped = UITapGestureRecognizer(target: self, action: #selector(self.startFeedDetail(_:)))
                tapped.cancelsTouchesInView = false
                tapped.numberOfTapsRequired = 1
                cell.contentView.addGestureRecognizer(tapped)
                //end of gesture
                
                return cell
                
            }
        }
    }
    
    //MARK: -- group detail
    func startGroupDetail(_ sender: UITapGestureRecognizer)  {
        let group = groupsData[(sender.view?.tag)!]
        
        if group.isJoined{
            let member = "Your Are Already A Member"
            ControllerHelpers.showMessage(targetVC: self, title: "About: \(group.name)", message: "\(group.description) \n\n \(member)")
            
        }else{
            let notMember = "Your are not a member click JOIN to be a member"
            ControllerHelpers.showMessage(targetVC: self, title: "About: \(group.name)", message: "\(group.description) \n\n \(notMember)")
        }

        print("print group info as,\n Group Name: \(group.name) \n Group Description: \(group.description) \n Member: \(group.isJoined)")
    }
    
    
    
    //MARK: -- member profile
    func startDetail(_ sender: UITapGestureRecognizer)  {
        let profileController = UIStoryboard(name:AllKeys.mainStoryBoard, bundle:nil).instantiateViewController(withIdentifier: AllKeys.userProfileController) as! ProfileViewController
        let member = membersData[(sender.view?.tag)!]
        profileController.user = member
      
        self.navigationController?.pushViewController(profileController, animated: true)
    }
    
    func startFeedDetail(_ sender: UITapGestureRecognizer)  {
        let feedDetailController = UIStoryboard(name:AllKeys.mainStoryBoard, bundle:nil).instantiateViewController(withIdentifier: AllKeys.feedDetailController) as! FeedDetailController
        let feed = feedsData[(sender.view?.tag)!]
        feedDetailController.feed = feed
        self.navigationController?.pushViewController(feedDetailController, animated: true)
    }
    
    //MARK: - Joining the group action
    @IBAction func followGroup(_ sender: UIButton) {
        sender.isEnabled = false
        sender.setTitle("Joined", for: .normal)
        let feed = self.groupsData[sender.tag]
        api.joingChurchGroup(groupId: feed.id)
        
        let printDataToBeSent = api.joinChurchDelegate
        print("print group data here: \(String(describing: printDataToBeSent))")
        
        ControllerHelpers.showMessage(targetVC: self, title: "Welcome", message: "You just joined Our \(feed.name.localizedCapitalized)")
    }
    
    //MARK: - Following user
    @IBAction func followUser(_ sender: UIButton) {
        let titleText = sender.currentTitle
        let feed = self.membersData[sender.tag]
        if (titleText == "Follow") {
            sender.setTitle("Following", for: .normal)
            feed.isFollowing = true
            api.followMeber(userId: feed.id)
        } else {
            feed.isFollowing = false
            sender.setTitle("Follow", for: .normal)
            api.unFollowMeber(userId: feed.id)
        }
    }
    
    
    
    //calculating the item height
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        var number = 200;
        switch positionSelected {
        case 0:
            number = 80
        case 1:
            number = 100
        case 2:
            number = 130
        default:
            break
        }
        return CGFloat(number)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var number = 0;
        switch positionSelected{
        case 0:
            number = membersData.count
        case 1:
            number = groupsData.count
        case 2:
            number = feedsData.count
        default:
            break
        }
        return number
        
    }

    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return CGFloat(310)
    }
    
    //implement the header
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = self.dataTableView.dequeueReusableHeaderFooterView(withIdentifier: CellIdentifiers.myChurchHeader.rawValue)
        header = cell as! MyChurchTableHeader
        header.userData = self.userData
        header.joinButton.addTarget(self, action: #selector(self.startCheckIn(_:)), for: .touchUpInside)
        header.myChurchSegment.addTarget(self, action: #selector(self.churchSegmentButtonClick(_:)), for: .valueChanged)
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.tappedUserImage))
        header.churchLogoImageView.addGestureRecognizer(tap)
        return cell
    }
    
    func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
        return false
    }
    
    
    //start editing
    func startCheckIn(_ sender: UIButton) {
        let vc = UIStoryboard(name:AllKeys.mainStoryBoard, bundle:nil).instantiateViewController(withIdentifier: AllKeys.scannerViewController) as! ScannerViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    @IBAction func churchSegmentButtonClick(_ sender: UISegmentedControl) {
       positionSelected = sender.selectedSegmentIndex
       self.dataTableView.reloadData()
    }
    
    //MARK: - scroll handling section
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let currentOffset = scrollView.contentOffset.y
        let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height
        
        if (maximumOffset - currentOffset) <= 400 {
            if !(self.feedNextUrl.isEmpty) && !self.loadedPages.contains(self.feedNextUrl) {
                let churchId = self.userData.churchId
                self.loadedPages.append(self.feedNextUrl)
                getContent(url: self.feedNextUrl,churchId: churchId)
            }
            
            if !(self.groupsNextUrl.isEmpty) && !self.loadedPages.contains(self.groupsNextUrl) {
                let churchId = self.userData.churchId
                self.loadedPages.append(self.groupsNextUrl)
                getGroups(url: self.groupsNextUrl,churchId: churchId)
            }
            
            if !(self.membersNextUrl.isEmpty) && !self.loadedPages.contains(self.membersNextUrl) {
                let churchId = self.userData.churchId
                self.loadedPages.append(self.membersNextUrl)
                getMembers(url: self.membersNextUrl,churchId: churchId)
            }
            
        }
    }
    
    
    //check config file to navigate
    
    @IBOutlet weak var searchBtn: UIBarButtonItem!
    
    @IBAction func usersAndChurchSearchSwitcher(_ sender: Any) {
        
    }
    
}
