//
//  BirthDateViewController.swift
//  asoriba
//
//  Created by Bright Ahedor on 10/04/2017.
//  Copyright © 2017 Asoriba Inc. All rights reserved.
//

import UIKit

class BirthDateViewController: UIViewController {

    var dateOfBirth =  ""
    let dateFormatter = DateFormatter()
    @IBOutlet weak var dateOfBirthPicker: UIDatePicker!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        dateFormatter.dateFormat = "yyyy-MM-dd"
        self.dateOfBirth =  dateFormatter.string(from: dateOfBirthPicker.date)
    }
    
    
    @IBAction func datePickerChanged(_ sender: UIDatePicker) {
        self.dateOfBirth = dateFormatter.string(from: sender.date)
    }
}
