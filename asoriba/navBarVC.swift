//
//  navBarVC.swift
//  asoriba
//
//  Created by Benjamin Acquah on 17/08/2017.
//  Copyright © 2017 Asoriba Inc. All rights reserved.
//

import UIKit

class navBarVC: UINavigationController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let path = Bundle.main.path(forResource: "configuration", ofType: "plist")
        let root = NSDictionary(contentsOfFile: path!)
        
        let navBarColor = (root?["headerBackgroundColor"] as! String)
        
        navigationBar.barTintColor = UIColor().HexToColor(hexString: navBarColor)
    }
}
