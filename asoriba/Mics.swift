//
//  Mics.swift
//  asoriba
//
//  Created by Bright Ahedor on 14/12/2016.
//  Copyright © 2016 Asoriba Inc. All rights reserved.
//

import Foundation
import UIKit
import SwiftDate


class Mics {

    static func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.characters.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    static func getCountryCode() -> String{
        var code = "Gh";
        if let countryCode = (Locale.current as NSLocale).object(forKey: .countryCode) as? String{
            code = countryCode
        }
      return code
    }
    
    static func getAppVersion() -> String{
        var versionCode = "1.0"
        if let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
            versionCode = version
        }
        
        return versionCode
    }
    
    static func setDate(dateString: String) -> String  {
        // MARK: Date from string with custom format
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US")
        dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
        dateFormatter.dateStyle = .medium
        dateFormatter.timeStyle = .medium
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZZZZZ"
        if let date = dateFormatter.date(from: dateString){
            
            
            //local readable time
            dateFormatter.dateFormat = "EEE • MMM d, yyyy • h:mm a"
            dateFormatter.timeZone = NSTimeZone.local
            let timeStamp = dateFormatter.string(from: date)
            
            return timeStamp

        }else{
            let nothing = ""
            return nothing
        }
        
    }
    
    static func setDateElapsed(dateString: String) -> String  {
        // MARK: Date from string with custom format
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US")
        dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
        dateFormatter.dateStyle = .medium
        dateFormatter.timeStyle = .medium
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZZZZZ"
        let date = dateFormatter.date(from: dateString)!
        let (colloquial,_) = try! date.colloquialSinceNow()
        return "\(colloquial)"
        
    }
    
    static func formatNumber(number: Int,text:String) -> String  {
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .decimal
        var textShowable = ""
        if let formattedNumber = numberFormatter.number(from: String(number)){
           textShowable = Int(formattedNumber) == 1 ? "\(formattedNumber) \(text)" : "\(formattedNumber) \(text)s"
        } else{
           textShowable  = number == 1 ? "\(number) \(text)" : "\(number) \(text)s"
        }
        return textShowable
        
    }
    
    static func getAttributeText(feed:Feed) -> NSAttributedString {
        
        var message = ""
        
        if (feed.title != "" && feed.content == ""){
            message = feed.title
        }else if (feed.title != "" && feed.content != ""){
            message = feed.title + "\n\n" + feed.content
        }else{
            message = feed.content
        }
        
        var messageAttribute = NSMutableAttributedString(string: "")
        
        var title = feed.title
        
        if (!title.isEmpty){
            title = title   + "\n\n"
            let titleMutableString = NSMutableAttributedString(string: title, attributes: [NSFontAttributeName:UIFont(name: "fontawesome",size: 17.0)!])
            titleMutableString.addAttribute(NSForegroundColorAttributeName,value: UIColor.black,range: (titleMutableString.string as NSString).range(of: title))
            messageAttribute = titleMutableString
        }
        
        let content = feed.content
        if (!content.isEmpty){
            let messageMutableString = NSMutableAttributedString(string: content, attributes: [NSFontAttributeName:UIFont(name: "Arial Hebrew",size: 15.0)!])
            messageMutableString.addAttribute(NSForegroundColorAttributeName,value: UIColor.black,range: (messageMutableString.string as NSString).range(of: content))
            messageAttribute.append(messageMutableString)
        }
        
        return messageAttribute
        
    }
    
    static func getAttributeContent(text:String) -> NSAttributedString {
            let titleMutableString = NSMutableAttributedString(string: text, attributes: [NSFontAttributeName:UIFont(name: "fontawesome",size: 17.0)!])
        
        return titleMutableString
        
    }

}
