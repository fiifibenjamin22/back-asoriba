//
//  helperVC.swift
//  asoriba
//
//  Created by Benjamin Acquah on 31/10/2017.
//  Copyright © 2017 Asoriba Inc. All rights reserved.
//

import UIKit
import LBTAComponents
import SafariServices
import ActiveLabel
import MessageUI

class helperVC: UIViewController,MFMailComposeViewControllerDelegate {
    
    @IBOutlet weak var getAppVersion: UILabel!
    
    @IBOutlet weak var aboutDeveloper: UILabel!
    @IBOutlet weak var aboutDevTitle: UILabel!
    
    @IBOutlet weak var aboutChurch: UILabel!
    @IBOutlet weak var aboutChurchTitle: UILabel!
    
    @IBOutlet weak var callFunction: UILabel!
    @IBOutlet weak var callImage: UIImageView!
    @IBOutlet weak var emailCenter: UILabel!
    @IBOutlet weak var emailImage: UIImageView!
    
    var strPhoneNumber = "+233302688001"
    var emailTo = "appsupport@centralgospel.com"
    var version = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.white
        
        //self.tabBarController?.tabBar.isHidden = true
        self.navigationItem.title = "Settings"
        
        setups()
    }
    
    func setups(){
        self.getAppVersion?.text = "verson: " + version
        
        self.aboutDeveloper?.isUserInteractionEnabled = true
        self.aboutDeveloper?.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(visitAsoriba)))
        self.aboutDevTitle?.isUserInteractionEnabled = true
        self.aboutDevTitle?.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(visitAsoriba)))
        
        self.getAppVersion?.isUserInteractionEnabled = true
        self.getAppVersion?.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(visitAppStore)))
        
        self.aboutChurch?.isUserInteractionEnabled = true
        self.aboutChurch?.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(visitChurch)))
        self.aboutChurchTitle?.isUserInteractionEnabled = true
        self.aboutChurchTitle?.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(visitChurch)))
        
        self.callFunction?.isUserInteractionEnabled = true
        self.callFunction?.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(makePhoneCall)))
        self.callImage?.isUserInteractionEnabled = true
        self.callImage?.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(makePhoneCall)))

        self.emailCenter?.isUserInteractionEnabled = true
        self.emailCenter?.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(sendEmail)))
        self.emailImage?.isUserInteractionEnabled = true
        self.emailImage?.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(sendEmail)))
    }
    
    func visitAsoriba(){
        
        let vc = aboutDeveloperVC()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func visitChurch(){
        
        let vc = aboutChurchVC()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func visitAppStore(){
        
        let vc = aboutAppVC()
        self.navigationController?.pushViewController(vc, animated: true)
    }

    func makePhoneCall(){
        
        if let phoneCallURL:URL = URL(string: "tel:\(strPhoneNumber)") {
            let application:UIApplication = UIApplication.shared
            if (application.canOpenURL(phoneCallURL)) {
                let alertController = UIAlertController(title: "My ICGC", message: "Do you want to call \n\(self.strPhoneNumber)?", preferredStyle: .alert)
                let yesPressed = UIAlertAction(title: "Yes", style: .default, handler: { (action) in
                    //application.openURL(phoneCallURL)
                    
                    if #available(iOS 10.0, *) {
                        application.open(phoneCallURL, options: [:], completionHandler: nil)
                    } else {
                        UIApplication.shared.openURL(phoneCallURL)
                    }

                })
                let noPressed = UIAlertAction(title: "No", style: .default, handler: { (action) in
                    
                })
                alertController.addAction(yesPressed)
                alertController.addAction(noPressed)
                present(alertController, animated: true, completion: nil)
            }
        }

    }
    
    func sendEmail(){
        
        let myAlert = UIAlertController(title: "Select Options", message: "Select which mailing system to use.", preferredStyle: UIAlertControllerStyle.alert)
        let appleMail = UIAlertAction(title: "Mail App From your Iphone", style: UIAlertActionStyle.default) { (alert) in
            
            if !MFMailComposeViewController.canSendMail()
            {
                let alertMsg = UIAlertController(title: "Could not send email", message: "Your device must have an active mail account.", preferredStyle: UIAlertControllerStyle.alert)
                alertMsg.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                self.present(alertMsg, animated: true, completion: nil)
            }
            else
            {
                let composeVC = MFMailComposeViewController()
                composeVC.mailComposeDelegate = self
                
                // Configure the fields of the interface.
                composeVC.setToRecipients([self.emailTo])
                composeVC.setSubject("Subject")
                composeVC.setMessageBody("", isHTML: false)
                
                // Present the view controller modally.
                self.present(composeVC, animated: true, completion: nil)
            }

        }
        
        let copyAlert = UIAlertController(title: "Copied", message: "This reciepient email \(emailTo) has been copied so PASTE it into the RECEIPIENT field.", preferredStyle: UIAlertControllerStyle.alert)
        let ok = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (alert) in
            
            let gmailSelection = UIAlertAction(title: "Gmail App From your Iphone", style: UIAlertActionStyle.default) { (alert) in
                
                UIPasteboard.general.string = "\(self.emailTo)"
                
                let vc = UIActivityViewController(activityItems: [self.emailTo], applicationActivities: [])
                vc.excludedActivityTypes = [ UIActivityType.postToFacebook, UIActivityType.postToTwitter, UIActivityType.postToWeibo, UIActivityType.message, UIActivityType.print, UIActivityType.copyToPasteboard,UIActivityType.assignToContact,UIActivityType.saveToCameraRoll,UIActivityType.addToReadingList, UIActivityType.postToFlickr, UIActivityType.postToVimeo,UIActivityType.postToTencentWeibo,UIActivityType.airDrop]
                
                vc.setValue("Help & Support" , forKey: "subject")
                //vc.setValue(emailTo, forKey: "Recipients")
                
                
                if let popOverController = vc.popoverPresentationController{
                    popOverController.sourceView = self.view
                    popOverController.sourceRect = self.view.bounds
                }
                
                self.present(vc, animated: true, completion: nil)
                
            }
            
            let cancel = UIAlertAction(title: "Ok", style: UIAlertActionStyle.cancel, handler: nil)
            
            myAlert.addAction(appleMail)
            myAlert.addAction(gmailSelection)
            myAlert.addAction(cancel)
            self.present(myAlert, animated: true, completion: nil)
            
        }
        
        copyAlert.addAction(ok)
        self.present(copyAlert, animated: true, completion: nil)
    }
}
