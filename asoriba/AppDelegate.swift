//
//  AppDelegate.swift
//  asoriba
//
//  Created by Bright Ahedor on 14/12/2016.
//  Copyright © 2016 Asoriba Inc. All rights reserved.
//

import UIKit

import Fabric
import Crashlytics

import FBSDKLoginKit
import RealmSwift
import UserNotifications
import Whisper

import Firebase
import FirebaseInstanceID
import FirebaseMessaging
import IQKeyboardManagerSwift

import MMDrawerController


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate, MessagingDelegate {
    
    var window: UIWindow?
    var drawerContainer : MMDrawerController?
    
    var gcmMessageIDKey = "AAAA1AxS3OA:APA91bH1c9RxbxYtID31Zdela0Dm0Z5ESptX9_wqr1FSrxzDH160dViAXgAbchGYAbgzVZ7nW5Ob5yPqJgjpCrdN4pXNBtwguPjk3VScamKJAiODG1z4DZLx6HYy4axv0D8aNm4jyCWJ8gfMS_hev_-zPHQf_JP4Xg"
    
    let unscribeText = "Could Not subscribe to set topic"
    let scribeText = "subscribed to set topic"
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey : Any]? = nil) -> Bool {
        
        FirebaseApp.configure()
        
        //setUp Crashlitics
       Fabric.with([Crashlytics.self])
        
        //set status bar background color manually
        //let statusBarBackgroundColor = UIView()
        
        //add it to the main window as sub view
        //window?.addSubview(statusBarBackgroundColor)
        //add some constraint
        //left to right or with or x-axis
        //window?.addConstraintsWithFormat(format: "H:|[v0]|", views: statusBarBackgroundColor)
        //top to bottom or height or y-axis
        //window?.addConstraintsWithFormat(format: "V:|[v0(20)]", views: statusBarBackgroundColor)

        //let path = Bundle.main.path(forResource: "configuration", ofType: "plist")
        //let root = NSDictionary(contentsOfFile: path!)

        //let statusBarColor = (root?["headerBackgroundColor"] as! String)
        //statusBarBackgroundColor.backgroundColor = UIColor().HexToColor(hexString: statusBarColor)

        //set status bar background color manually
        //let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
        //if statusBar.responds(to:#selector(setter: UIView.backgroundColor)) {
        //    statusBar.backgroundColor = UIColor().HexToColor(hexString: statusBarColor)
        //}

        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in
            
                    
                    DispatchQueue.main.async(execute: {
                        application.registerForRemoteNotifications()
                    })
                    
            })
            // For iOS 10 data message (sent via FCM
            Messaging.messaging().delegate = self
            
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        //realm migration
        let config = Realm.Configuration(
            schemaVersion: 2,
            migrationBlock: { migration, oldSchemaVersion in
                if (oldSchemaVersion < 1) {
                    
                }
        })
        Realm.Configuration.defaultConfiguration = config
        //ending the migration
        
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        
        UINavigationBar.appearance().tintColor = UIColor.white
        
        let color = Mics.hexStringToUIColor(hex: AllKeys.primaryHexCode)
        UINavigationBar.appearance().barTintColor = color
        application.statusBarStyle = .lightContent
        
        
        UINavigationBar.appearance().tintColor = UIColor.white
        UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName : UIColor.white]
        
        
        UITabBar.appearance().barTintColor = UIColor.white
        UITabBar.appearance().tintColor = color
                
        //keyboard library
        IQKeyboardManager.sharedManager().enable = true
        
        //is from notification
        let notification: [AnyHashable: Any]? = (launchOptions?[UIApplicationLaunchOptionsKey.remoteNotification] as? [AnyHashable: Any])
        if notification != nil {
            self.startController(userInfo: notification!)
            self.application(application, didReceiveRemoteNotification: notification!)
        } else {
            /**************** app controler checks and login *******/
            var nextViewController: UIViewController
            let storyboard = UIStoryboard(name: AllKeys.mainStoryBoard, bundle: nil)
            if DataOperation.userIsEmpty() {
                nextViewController = storyboard.instantiateViewController(withIdentifier: AllKeys.onboardViewController)
            } else if (DataOperation.getUser().firstName.isEmpty)  {
                nextViewController = storyboard.instantiateViewController(withIdentifier: AllKeys.updateNameController)
                //update with all gateways
                let api = ApiService()
                api.getPaymentGetways()
                //
            }else {
                nextViewController = storyboard.instantiateViewController(withIdentifier: AllKeys.mainController)
                //update with all gateways
                let api = ApiService()
                api.getPaymentGetways()
            }
            
            /**************end of checks *************/
            self.window?.rootViewController = nextViewController
            self.window?.makeKeyAndVisible()
            
        }
        
        //check current application state
        if application.applicationState == .active{
            application.applicationIconBadgeNumber = 0
        }else{
            let notificationCount = UserDefaults.standard.integer(forKey: AllKeys.notificationCount)
            UIApplication.shared.applicationIconBadgeNumber = notificationCount - 1
        }
        
        //buildNavigationDrawerInterface()

        return true
    }

    //facebook integration
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        return FBSDKApplicationDelegate.sharedInstance().application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        FBSDKAppEvents.activateApp()
    }
    
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        Messaging.messaging().shouldEstablishDirectChannel = false
        
        let notificationCount = UserDefaults.standard.integer(forKey: AllKeys.notificationCount)
        UIApplication.shared.applicationIconBadgeNumber = notificationCount - 1

    }
    
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        Answers.logCustomEvent(withName: "cold_open", customAttributes: ["app_version" : Mics.getAppVersion()])
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        FBSDKAppEvents.activateApp()

    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    
    // [start receiving messages from firebase] //
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        
        print("print sent information sent: \(userInfo)")
        
        let messageID = userInfo[gcmMessageIDKey]
            print("print Message ID: ", messageID ?? "no firebase message Id")

    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        
        
        let messageID = userInfo[gcmMessageIDKey]
            print("print Message ID: ", messageID ?? "no firebase message Id")
        
        completionHandler(UIBackgroundFetchResult.noData)
        
    }
    // [finish receiving messages from firebase]  //
    
    
    //register for remote notification and send FCM TOKEN
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        var tokenString = ""
        
        Messaging.messaging().setAPNSToken(deviceToken, type: MessagingAPNSTokenType.sandbox)
        Messaging.messaging().setAPNSToken(deviceToken, type: MessagingAPNSTokenType.prod)
        
        for i in 0..<deviceToken.count {
            tokenString += String(format: "%02.2hhx", arguments: [deviceToken[i]])
        }
        
        print("Device Token:", tokenString)
        
        if let refreshedToken = InstanceID.instanceID().token(){
            
            print("print Device registration ID From Firebase: \(String(describing: refreshedToken))")

        }
        
    }
    
    
    //When failed to Register and send fcm token
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        
        print("print Registration failed! registration error Is: \(error.localizedDescription)")
    }
    // [End Registration for notification] //
    
    
    //Start Refresh token
    func messaging(_ messaging: Messaging, didRefreshRegistrationToken fcmToken: String) {
        
        print("print fcmToken: \(fcmToken)")
        
        if !DataOperation.userIsEmpty(){

            let apiService = ApiService()
            //apiService.subscribeToTopics()
            Messaging.messaging().subscribe(toTopic: "\(apiService.subscribeToTopics())")
            print("print: ", scribeText)

        }else{

            print("print ", unscribeText)
        }

    }
    
    //recieve firebase message
    func application(received remoteMessage: MessagingRemoteMessage) {
        print("print remote message: \(remoteMessage.appData)")
    }
    
    
    //Action handlers
    func startController(userInfo:[AnyHashable:Any]) {
        
        if let action = userInfo["click_action"] as? String {
            print("Action \(action)")
            let homeStoryboard = UIStoryboard(name: AllKeys.mainStoryBoard, bundle: nil)
            let vc  = homeStoryboard.instantiateViewController(withIdentifier: AllKeys.mainController) as! MainTabController
            let feedDetail = homeStoryboard.instantiateViewController(withIdentifier: AllKeys.feedDetailController) as! FeedDetailController
            
            
            switch action {
            case "COMMENT":
                let id = userInfo["content_id"] as! String
                
                let feed = Feed()
                feed.id = id
                feedDetail.feed = feed
                
                self.window?.rootViewController = vc
                self.window?.makeKeyAndVisible()
                
                let controller = vc.viewControllers?.first as! UINavigationController
                controller.pushViewController(feedDetail, animated: true)
                
                Answers.logCustomEvent(withName: "hot_open", customAttributes: ["app_version" : Mics.getAppVersion(), "feed_id": feed.id])
                
                break
            case "USER_HAS_FOLLOWED":
                let id = userInfo["user_id"] as! String
                
                let profileDetail = homeStoryboard.instantiateViewController(withIdentifier: AllKeys.userProfileController) as! ProfileViewController
                
                let member = Member()
                member.id = id
                
                profileDetail.user = member
                self.window?.rootViewController = vc
                self.window?.makeKeyAndVisible()
                
                
                let controller = vc.viewControllers?.first as! UINavigationController
                controller.pushViewController(profileDetail, animated: true)
                
                Answers.logCustomEvent(withName: "hot_open", customAttributes: ["app_version" : Mics.getAppVersion(), "member_id": member.id])

                break
            case "TYPE_PAYMENT_COMPLETED":
                let paymentInfo = userInfo["record_id"] as! String
                
                print("print payment info \(paymentInfo)")
                
//                if let aps = userInfo["record_id"] as? NSDictionary {
//                    if let alert = aps["alert"] as? NSDictionary{
//                        if let message = alert["message"] as? NSString {
//                            
//                        }else{
//                            if let alert = userInfo["record_id"] as? String {
//                                
//                            }
//                        }
//                    }
//                }

                
                self.window?.rootViewController = vc
                self.window?.makeKeyAndVisible()
                
                vc.selectedIndex = 2
                
                let paymentHistory = homeStoryboard.instantiateViewController(withIdentifier: AllKeys.paymentHistoryController) as! PaymentHistoryController
                let controller = vc.viewControllers?[2] as! UINavigationController
                controller.pushViewController(paymentHistory, animated: true)
                
                break
            case "JOIN_CHURCH_APPROVED":
                break
            case "PERSONAL_MESSAGE":
                
                let id = userInfo["content_id"] as! String
                
                let feed = Feed()
                feed.id = id
                feedDetail.feed = feed
                
                self.window?.rootViewController = vc
                self.window?.makeKeyAndVisible()
                
                let controller = vc.viewControllers?.first as! UINavigationController
                controller.pushViewController(feedDetail, animated: true)
                
                Answers.logCustomEvent(withName: "hot_open", customAttributes: ["app_version" : Mics.getAppVersion(), "feed_id": feed.id])
                
                break
            case "APP_UPDATE":
                
                self.window?.rootViewController = vc
                self.window?.makeKeyAndVisible()
                
                vc.selectedIndex = 0
                
                break
            case "USER_POST_PROMPT":
                break
            case "CHURCH_CONTENT":
                let id = userInfo["content_id"] as! String
                
                let feed = Feed()
                feed.id = id
                feedDetail.feed = feed
                
                self.window?.rootViewController = vc
                self.window?.makeKeyAndVisible()
                
                let controller = vc.viewControllers?.first as! UINavigationController
                controller.pushViewController(feedDetail, animated: true)
                
                Answers.logCustomEvent(withName: "hot_open", customAttributes: ["app_version" : Mics.getAppVersion(), "feed_id": feed.id, "country": Mics.getCountryCode()])

                
                break
                
                //in case of chat message received
            case "USER_CHAT_MESSAGE":
                
                
                
                break
 
            default:
                self.window?.rootViewController = vc
                self.window?.makeKeyAndVisible()
                
                break
            }
            
            print("PASS \(action)")
            
        }
        
        
    }
    


    //start message handling
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        //present notification locally
        let userInfo = notification.request.content.userInfo as NSDictionary
        
        
        //prsent payment notification
//        if let aps = userInfo["record_id"] as? NSDictionary {
//            if let alert = aps["alert"] as? NSDictionary{
//                if let message = alert["message"] as? NSString {
//
//                    //let myUserInfo =
//
//                }else{
//                    if let alert = userInfo["record_id"] as? String {
//
//                    }
//                }
//            }
//        }

        
        //with swizzling disabled
        Messaging.messaging().appDidReceiveMessage(userInfo as! [AnyHashable : Any])
        
        //print Message ID
        if let messageID = userInfo[gcmMessageIDKey]{
            print("print MessagID recieved In presentation: ", messageID)
        }
        
        print("print Message Received:: \(userInfo)")

        // Print full message.
        print("print Arrived: \(userInfo as! [String:AnyObject])")
        
        let notificationCount = UserDefaults.standard.integer(forKey: AllKeys.notificationCount)
        UIApplication.shared.applicationIconBadgeNumber = notificationCount + 1

        completionHandler([.badge, .alert, .sound])
    }
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        
        let userInfo = response.notification.request.content.userInfo as NSDictionary
        
        let notificationCount = UserDefaults.standard.integer(forKey: AllKeys.notificationCount)
        UIApplication.shared.applicationIconBadgeNumber = notificationCount - 1
        
        startController(userInfo: userInfo as! [AnyHashable : Any])
        let action = response.actionIdentifier
        print("Action taken \(String(describing: action.capitalized))")
        
        //print Message ID
        if let messageID = userInfo[gcmMessageIDKey]{
            print("print MessagID recieved In didReceive: ", messageID)
        }
        
        print("print: \(userInfo)")
        
        completionHandler()
    }
    
    //End message Handling
}
