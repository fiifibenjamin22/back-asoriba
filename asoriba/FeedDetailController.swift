//
//  FeedDetailController.swift
//  asoriba
//
//  Created by Bright Ahedor on 16/12/2016.
//  Copyright © 2016 Asoriba Inc. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage
import Crashlytics
import Photos
import SKPhotoBrowser
import MediaPlayer
import Jukebox
import MobilePlayer
import SafariServices
import ActiveLabel


class FeedDetailController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,ContentServiceProtocol,CommentServiceProtocol,UITextFieldDelegate {

    @IBOutlet weak var commentButton: UIButton!
    @IBOutlet weak var commentTextField: UITextField!
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
 
    var api:ApiService!
    let screenWidth = UIScreen.main.bounds.width
    var activityViewIndicator:UIActivityIndicatorView = UIActivityIndicatorView()
    var feedArray = [AnyObject]()
    var currentUser = User()
    var nextUrl = ""
    var feed: Feed!
    var comentText = ""
    let numberOfItemsPerRow : CGFloat = 1.0
    var membersData = [Member]()
    
    // variable to hold keybarod frame
    var keyboard = CGRect()
    
    @IBOutlet weak var feedCollectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
                
        //notification to show keyboard
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        
        let tag = "More"
        self.navigationItem.title = tag
        
        //get user
        currentUser = DataOperation.getUser()
        
        
        self.feedCollectionView.delegate = self
        self.feedCollectionView.dataSource = self
        
    
        //register cell
        self.feedCollectionView.register(UINib(nibName: CellIdentifiers.feedCollectionViewCell.rawValue, bundle: nil), forCellWithReuseIdentifier: CellIdentifiers.feedCollectionViewCell.rawValue)
       
        
        self.feedCollectionView.register(UINib(nibName: CellIdentifiers.commentCollectionViewCell.rawValue, bundle: nil), forCellWithReuseIdentifier: CellIdentifiers.commentCollectionViewCell.rawValue)
        //end section here
        
        
        //calling the protocall function
        self.startProgressBar()
        
        
        //api instance
        api = ApiService()
        api.contentDelegate = self
        api.commentsDelegate = self
        api.getSingleContent(url: "users/get_content/?content_id=\(feed.id)")
        api.addImpression(item:feed,type:"click")
        
        self.commentTextField.delegate = self
        
        if (!currentUser.avatar.isEmpty) {
            self.setUserImage()
        }
              
        self.commentButton.layer.cornerRadius = 5
        self.commentButton.layer.masksToBounds = true
        self.commentTextField.textColor = .black
    }
    
    // func loading when keyboard is shown
    func keyboardWillShow(notification : NSNotification) {
        
        // defnine keyboard frame size
        keyboard = ((notification.userInfo?[UIKeyboardFrameEndUserInfoKey]! as AnyObject).cgRectValue)!
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.feedCollectionView.reloadData()
    }
    
    func setUserImage()  {
        let placeHolderImage = UIImage(named: "default")
        let image = self.currentUser.avatar
        if  !image.isEmpty {
            self.userImageView.af_setImage(
                withURL: URL(string: image)!,
                placeholderImage: placeHolderImage,
                imageTransition: .crossDissolve(0.2)
            )}else {
            self.userImageView.image = placeHolderImage
        }
    }

    func startProgressBar()  {
        activityViewIndicator.center = self.view.center
        activityViewIndicator.hidesWhenStopped = true
        activityViewIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray
        self.view.addSubview(activityViewIndicator)
        activityViewIndicator.startAnimating()
    }
    
    
    func didReceiveContent(result: Feed) {
        self.activityViewIndicator.stopAnimating()
        self.feedArray.append(result)
        self.feed = result
        self.feedCollectionView.reloadData()
        self.fetchComments(contentId: result.id,page: "?page=1")
        api.addImpression(item:feed,type:"impressions")
    }
    
    func didReceiveContentError(results: String) {
          self.activityViewIndicator.stopAnimating()
        //error
    }
    
    func fetchComments(contentId:String,page:String) {
       api.getContentComments(contentId: contentId, page: page)
    }
    
    //get comment
    func didReceiveComment(result: Comment) {
       
    }
    
    func didReceiveComments(results: [Comment]) {
        for comment in results {
           feedArray.append(comment as AnyObject)
        }
        self.feedCollectionView.reloadData()
    }
    
    func didReceiveComentError(result: String) {
        //
    }
    
    func didReceiveCommentsError(result: String) {
        //
    }
    
    func didReceiveCommentsNextUrl(result: String) {
        self.nextUrl = result
    }


    
    //MARK:- collection view delegate
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return feedArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        let feedObject = self.feedArray[indexPath.row]
        if feedObject is Feed {
            let feed = feedObject as! Feed
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CellIdentifiers.feedCollectionViewCell.rawValue, for: indexPath) as! FeedCollectionViewCell
            cell.backgroundColor = UIColor.white
            cell.feed = feed
            cell.isDetail = true
        
            cell.amenButton.tag = indexPath.row
            cell.amenButton.addTarget(self, action: #selector(self.amenButton(_:)), for: .touchUpInside)
            cell.shareButton.tag = indexPath.row
            cell.shareButton.addTarget(self, action: #selector(self.shareButton(_:)), for: .touchUpInside)
            
            
            cell.audioPlayButton.tag = indexPath.row
            cell.audioPlayButton.addTarget(self, action: #selector(self.playAudioCommand(_:)), for: .touchUpInside)
            
            
            //gesture account
            let tappedUser = UITapGestureRecognizer(target: self, action: #selector(self.tappedUserLabel(_:)))
            cell.userNameLabel!.isUserInteractionEnabled = true
            cell.userNameLabel!.tag = indexPath.row
            tappedUser.cancelsTouchesInView = false
            tappedUser.numberOfTapsRequired = 1
            cell.userNameLabel!.addGestureRecognizer(tappedUser)
            //end user gesture
            
            //gesture account
            let tappedUserImage = UITapGestureRecognizer(target: self, action: #selector(self.tappedUserLabel(_:)))
            cell.userImageView!.isUserInteractionEnabled = true
            cell.userImageView!.tag = indexPath.row
            tappedUserImage.cancelsTouchesInView = false
            tappedUserImage.numberOfTapsRequired = 1
            cell.userImageView!.addGestureRecognizer(tappedUserImage)
            //end user gesture
            
            //gesture section ameners
            cell.amenCountLabel!.isUserInteractionEnabled = true
            cell.amenCountLabel!.tag = indexPath.row
            let tappedAmenCount = UITapGestureRecognizer(target: self, action: #selector(self.tapPerfomers(_:)))
            tappedAmenCount.cancelsTouchesInView = false
            tappedAmenCount.numberOfTapsRequired = 1
            cell.amenCountLabel!.addGestureRecognizer(tappedAmenCount)
            //end of gesture
            
            //gesture section sharers
            cell.shareCountLabel!.isUserInteractionEnabled = true
            cell.shareCountLabel!.tag = indexPath.row
            let tappedShareCount = UITapGestureRecognizer(target: self, action: #selector(self.tapSharePerfomers(_:)))
            tappedShareCount.cancelsTouchesInView = false
            tappedShareCount.numberOfTapsRequired = 1
            cell.shareCountLabel!.addGestureRecognizer(tappedShareCount)
            //end of ameners gesture
            
            
            //gesture section sharers
            cell.mediaSectionUIView!.isUserInteractionEnabled = true
            cell.mediaSectionUIView!.tag = indexPath.row
            let tappedMedia = UITapGestureRecognizer(target: self, action: #selector(self.tappedMediaSection(_:)))
            tappedMedia.cancelsTouchesInView = false
            tappedMedia.numberOfTapsRequired = 1
            cell.mediaSectionUIView!.addGestureRecognizer(tappedMedia)
            //end of ameners gesture
            
            //feed content gesture            
            cell.feedContentLabel.enabledTypes = [.url]
            cell.feedContentLabel.URLColor = UIColor(red: 18 / 255, green: 165 / 255, blue: 244 / 255, alpha: 1.0)
            cell.feedContentLabel.URLSelectedColor = UIColor.darkText
            cell.feedContentLabel.font = UIFont.systemFont(ofSize: 13)

            let webStringProtocol = ""
            let input2 = cell.feedContentLabel.text
            let detector2 = try! NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue)
            let matches2 = detector2.matches(in: input2!, options: [], range: NSRange(location: 0, length: (input2?.utf16.count)!))
            
            for match in matches2{
                
                let webUrl = (input2! as NSString).substring(with: match.range)
                print("link inside post string: \(webStringProtocol)\(webUrl)")
                let combined = URL(string:"\(webStringProtocol)\(webUrl)")
                
                cell.feedContentLabel.handleURLTap({ (webUrl) in
                    
                    let svc = SFSafariViewController(url: combined!)
                    self.present(svc, animated: true, completion: nil)
                    
                })
                
            }
            
            //gesture for quotation
            cell.quotationLabel!.isUserInteractionEnabled = true
            cell.quotationLabel!.tag = indexPath.row
            let tappedQuote = UITapGestureRecognizer(target: self, action: #selector(self.tapQuote(_:)))
            tappedQuote.cancelsTouchesInView = false
            tappedQuote.numberOfTapsRequired = 1
            cell.quotationLabel!.addGestureRecognizer(tappedQuote)
            //end of gesture
            
            let input = cell.feedContentLabel.text
            let detector = try! NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue)
            let matches = detector.matches(in: input!, options: [], range: NSRange(location: 0, length: (input?.utf16.count)!))
            
            for match in matches{
                
                let url = (input! as NSString).substring(with: match.range)
                print("link inside post string: \(url)")
            }
            
            return cell
        
        }else {
        
            let feed = feedObject as! Comment
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CellIdentifiers.commentCollectionViewCell.rawValue, for: indexPath) as! CommentCollectionViewCell
            cell.backgroundColor = UIColor.white
            cell.comment = feed
            
            //username tap gesture
            cell.userNameLabel!.isUserInteractionEnabled = true
            cell.userNameLabel!.tag = indexPath.row
            let tappedCommentator = UITapGestureRecognizer(target: self, action: #selector(self.tapCommentor(_:)))
            tappedCommentator.cancelsTouchesInView = false
            tappedCommentator.numberOfTapsRequired = 1
            cell.userNameLabel!.addGestureRecognizer(tappedCommentator)

            
//            cell.deletedCommentButton.tag = indexPath.row
//            cell.deletedCommentButton.addTarget(self, action: #selector(self.removeComment(_:)), for: .touchUpInside)
            
            return cell
        
        }
        
    }
    
    
    //MARK: -- Handling gestures
    func tappedMediaSection(_ sender: UITapGestureRecognizer)
    {
        let currentFeed = feedArray[(sender.view?.tag)!] as! Feed
        if (currentFeed.mediaType == "video") {
            ControllerHelpers.processVideoPlayer(targetVC: self, feed: currentFeed)
        }else  {
            ControllerHelpers.presentImage(targetVC: self, feed: currentFeed)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, shouldHighlightItemAt indexPath: IndexPath) -> Bool {
        return false
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return CGFloat(AllKeys.cellSpacing)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return CGFloat(AllKeys.cellSpacing)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: CGFloat(AllKeys.cellSpacing), left: 0.0, bottom: 0.0, right: 0.0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
        let contentWidth = collectionView.bounds.width
        let totalSpace = flowLayout.sectionInset.left
            + flowLayout.sectionInset.right
            + (flowLayout.minimumInteritemSpacing * CGFloat(numberOfItemsPerRow - 1))
        let size = Int((contentWidth - totalSpace) / CGFloat(numberOfItemsPerRow))
        let feed = self.feedArray[indexPath.row]
        return ConfigureCell.configureFeedCell(feed:feed,size:size,contentWidth:contentWidth,lines:0)
        
    }

    
    //MARK: tap a quotation
    func tapQuote(_ sender: UITapGestureRecognizer)
    {
        let feed = feedArray[(sender.view?.tag)!] as! Feed
        ControllerHelpers.startQuote(targetVC: self, quote: feed.quotation!)
    }
    
    //MARK: set performers gesture
    func tapPerfomers(_ sender: UITapGestureRecognizer)
    {
        let feed = feedArray[(sender.view?.tag)!]
        ControllerHelpers.startPerformers(targetVC: self, feedId: feed.id,isAmeners: true)
    }
    
    //MARK: set performers gesture
    func tapSharePerfomers(_ sender: UITapGestureRecognizer)
    {
        let feed = feedArray[(sender.view?.tag)!]
        ControllerHelpers.startPerformers(targetVC: self, feedId: feed.id,isAmeners: false)
    }
    
    //send the amen signal
    @IBAction func amenButton(_ sender: UIButton) {
        let feed = self.feedArray[sender.tag] as! Feed
        feed.hasAmened = true
        feed.numberOfAmens = feed.numberOfAmens + 1
        self.feedCollectionView.reloadData()
        self.api.sayAmen(item: feed)
    }
    
    //send the share signal
    @IBAction func shareButton(_ sender: UIButton) {
        let feed = self.feedArray[sender.tag] as! Feed
        ControllerHelpers.presentSharer(targetVC: self, feed: feed)
    }
    
    //remove comment here
    @IBAction func removeComment(_ sender: UIButton) {
        
//        let feed = self.feedArray[sender.tag] as! Comment
//        self.feedArray.remove(at: sender.tag)
//        feed.numberOfComments = feed.numberOfComments - 1
//        print("number of comments: \(feed.numberOfComments)")
//        print("number of comments: \(feed.numberOfComments - 1)")
//        self.feedCollectionView.reloadData()
//        self.api.deleteComment(commentId: feed.id, comment: feed.content)
    }
        
    
    
    //MARK: set image gesture
    func tappedImage(_ sender: UITapGestureRecognizer)
    {
        let feed = feedArray[(sender.view?.tag)!]
        self.presentImage(feed: feed as! Feed)
        
    }
    
    
    
    //MARK: now to user
    func tappedUserLabel(_ sender: UITapGestureRecognizer)
    {
        
        let feed = feedArray[(sender.view?.tag)!] as! Feed

        //is user
        if (feed.member?.isMobile)!{
            let layout = UICollectionViewFlowLayout()
            let vc = profileVC(collectionViewLayout: layout)

            let member = feed.member
            vc.user = member!
            self.navigationController?.pushViewController(vc, animated: true)
            
            //is church not user
        } else {
            
            let member = feed.member
            let church = Church()
            church.id = (member?.id)!
            church.name = (member?.firstName)!
            church.branchName = (member?.lastName)!
            church.churchLogo = (member?.avatar)!

            let layout = UICollectionViewFlowLayout()
            let churchController = myChurchVC(collectionViewLayout: layout)
            churchController.isMyChurch = false
            churchController.churchItem = church

            self.navigationController?.pushViewController(churchController, animated: true)

        }
        
    }
    
    func tapCommentor(_ sender: UITapGestureRecognizer){
        
        let layout = UICollectionViewFlowLayout()
        let vc = profileVC(collectionViewLayout: layout)
        let member = feed.member
        vc.user = member!
        print("\(String(describing: member))")
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    //MARK: set image gesture
    func startVideo(_ sender: UITapGestureRecognizer)
    {
        let feed = feedArray[(sender.view?.tag)!] as! Feed
        ControllerHelpers.processVideoPlayer(targetVC: self, feed: feed)
    }
    
    
    func presentImage(feed:Feed)  {
        ControllerHelpers.presentImage(targetVC: self, feed: feed)
    }
    
    
    // MARK: - Textfield delegates
    func textFieldDidBeginEditing(_ textField: UITextField) {
    
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        
    }
    
//    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
//        return true;
//    }
//    func textFieldShouldClear(_ textField: UITextField) -> Bool {
//        self.commentTextField.clearsOnBeginEditing = true
//        return true;
//    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        return true;
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return true;
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder();
        return true;
    }
    
    //end of delegate
    
  
    //MARK: - add comment here
    @IBAction func startCommentButton(_ sender: UIButton) {
        self.comentText = self.commentTextField.text!
        if !self.comentText.isEmpty {
            
            let item = Comment()
            item.content = self.comentText
            item.date = "Now"
            
            let user = Member()
            user.avatar = currentUser.avatar
            user.firstName = currentUser.firstName
            user.lastName = currentUser.lastName
            user.otherName = currentUser.otherName
            user.id = currentUser.id

            item.member = user
            feedArray.insert(item, at: 1)
            self.feedCollectionView.reloadData()
            
            api.sendComment(contentId: self.feed.id,comment: comentText)
            self.commentTextField.text = ""
            
            let feed = feedArray[0] as! Feed
            feed.numberOfComments = feed.numberOfComments + 1
            self.feedCollectionView.reloadData()
            
            self.textFieldDidEndEditing(self.commentTextField)
            //view.endEditing(true)
            commentButton.endEditing(true)
            self.commentTextField.resignFirstResponder()
        }
    }
    
    
    //play audio here
    @IBAction func playAudioCommand(_ sender: UIButton) {
        self.startAudioDialog()
    }
    
    func startAudioDialog()  {
        let popOverVC = UIStoryboard(name: AllKeys.mainStoryBoard, bundle: nil).instantiateViewController(withIdentifier: AllKeys.audioControllerViewController) as! AudioControllerViewController
        popOverVC.feed = self.feed
        self.addChildViewController(popOverVC)
        popOverVC.view.frame = self.view.frame
        self.view.addSubview(popOverVC.view)
        popOverVC.didMove(toParentViewController: self)
    }
    

}
