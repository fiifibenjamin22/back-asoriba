//
//  Church.swift
//  asoriba
//
//  Created by Bright Ahedor on 16/12/2016.
//  Copyright © 2016 Asoriba Inc. All rights reserved.
//

import Foundation

class Church {
    var id = ""
    var name = ""
    var branchName = ""
    var image = ""
    var churchLogo = ""
    var isFollowing = false
    var isMainBranch = false
    var numberOfFollowers = 0
    var numberOfGroups = 0
}
