//
//  BibleVerseViewController.swift
//  asoriba
//
//  Created by Bright Ahedor on 16/03/2017.
//  Copyright © 2017 Asoriba Inc. All rights reserved.
//

import UIKit
import Crashlytics

class BibleVerseViewController: UIViewController,UIWebViewDelegate  {

    @IBOutlet weak var progressBarView: UIProgressView!
    var checkOutVerse: Quotation!
    @IBOutlet weak var webView: UIWebView!
    private var hasFinishedLoading = false
    var timer: Timer!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tag = "Bible verse"
        self.navigationItem.title = tag
        
        self.webView.delegate = self
        let verse = checkOutVerse.book + " " + checkOutVerse.chapter + ":" + checkOutVerse.verse
        let pageUrl = NSURL(string: "\(AllKeys.bibleVerse)\(verse.replacingOccurrences(of: " ", with: "+"))")
        if (pageUrl != nil){
            let request = NSURLRequest(url: pageUrl! as URL)
            self.webView.loadRequest(request as URLRequest)
        }
        
        //tracking analytics
    }
    
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        hasFinishedLoading = false
        self.funcToCallWhenStartLoadingYourWebview()
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        self.funcToCallCalledWhenUIWebViewFinishesLoading()
    }
    
    
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        return true
    }
    
    func funcToCallWhenStartLoadingYourWebview() {
        self.progressBarView.progress = 0.0
        self.hasFinishedLoading = false
        self.timer = Timer.scheduledTimer(timeInterval: 0.01667, target: self, selector: #selector(PaymentCheckoutController.timerCallback), userInfo: nil, repeats: true)
    }
    
    func funcToCallCalledWhenUIWebViewFinishesLoading() {
        self.hasFinishedLoading = true
    }
    
    func timerCallback() {
        if self.hasFinishedLoading {
            if self.progressBarView.progress >= 1 {
                self.progressBarView.isHidden = true
                self.timer.invalidate()
            } else {
                self.progressBarView.progress += 0.1
            }
        } else {
            self.progressBarView.progress += 0.05
            if self.progressBarView.progress >= 0.95 {
                self.progressBarView.progress = 0.95
            }
        }
    }

    

}
