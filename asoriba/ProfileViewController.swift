//
//  ProfileViewController.swift
//  asoriba
//
//  Created by Bright Ahedor on 17/12/2016.
//  Copyright © 2016 Asoriba Inc. All rights reserved.
//

import UIKit
import Alamofire
import Photos
import SKPhotoBrowser
import Crashlytics



class ProfileViewController: UIViewController ,UITableViewDelegate,UITableViewDataSource,ChurchMembersServiceProtocol,FeedServiceProtocol,FollowingServiceProtocol{
    
    var api:ApiService!
    
    @IBOutlet weak var dataTableView: UITableView!
    var membersData = [Member]()
    var memberFollowingData = [Member]()
    var feedsData = [Feed]()
    var loadedPages = [String]()
    var feedNextUrl = ""
    var followersNextUrl = ""
    var followingNextUrl = ""
    var requestId = ""
    let currentUser = DataOperation.getUser()
    
    var positionSelected = 0 {
        didSet {
         print("Called \(positionSelected)")
        }
    }
    
    var user:Member!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("print feed data: ", feedsData)
        
        dataTableView.delegate = self
        dataTableView.dataSource = self
        dataTableView.alwaysBounceVertical = true
        
        //regiser header
        let nib = UINib(nibName: CellIdentifiers.accountHeader.rawValue, bundle: nil)
        self.dataTableView.register(nib, forHeaderFooterViewReuseIdentifier: CellIdentifiers.accountHeader.rawValue)
        
        
        //register cells
        self.dataTableView.register(UINib(nibName: CellIdentifiers.groupCell.rawValue, bundle: nil), forCellReuseIdentifier: CellIdentifiers.groupCell.rawValue)
        self.dataTableView.register(UINib(nibName: CellIdentifiers.memberCell.rawValue, bundle: nil), forCellReuseIdentifier: CellIdentifiers.memberCell.rawValue)
        self.dataTableView.register(UINib(nibName: CellIdentifiers.feedWithImageTableCell.rawValue, bundle: nil), forCellReuseIdentifier: CellIdentifiers.feedWithImageTableCell.rawValue)
        
        
        //api instance
        api = ApiService()
        api.churchMembersDelegate = self
        api.followingServiceDelegate = self
        api.feedDelegate = self
        
        requestId  = ""
        var tag = ""
        if self.user.id == self.currentUser.id {
            requestId = "me"
            tag = self.currentUser.firstName
            setMenuItems()
        }else{
           tag = self.user.firstName
           requestId = self.user.id
        }
        
        self.navigationItem.title = tag
        self.getDataFollowers(page: "page=1")
        self.getDataFollowings(page: "page=1")
        self.getData(page: "page=1",userId: self.currentUser.id)

    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        DispatchQueue.main.async {
            self.dataTableView.reloadData()
        }

    }

    func getDataFollowers(page: String)  {
        loadedPages.append(page)
        api.getMemberFollowers(userId: requestId, next: page)
        print("print requestID :", requestId)

    }
    
    func getDataFollowings(page: String)  {
        loadedPages.append(page)
        api.getMemberFollowings(userId: requestId, next: page)
        
    }
    
    func getData(page: String,userId:String)  {
        loadedPages.append(page)
        self.api.getFeed(url: "users/get_content/\(page)&page_size=10&feed_type=all&member_id=\(userId)&multimedia_type=all&content_category=all&keyword=")
    }
    
    //setup the menu items for current user
    func setMenuItems() {
        let searchBarItem = UIBarButtonItem.init(title: "Edit", style: .plain, target: self, action: #selector(handleEditClick))
        self.navigationItem.rightBarButtonItem = searchBarItem
    }
    
   
    //MARK: - segue section
    func startEditing() {
        self.performSegue(withIdentifier: AllKeys.editProfileSegue, sender: self.user)
    }
    
    func handleEditClick()  {
        self.startEditing()
    }
    
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == AllKeys.editProfileSegue {
          let editingController = segue.destination as! EditProfileController
          editingController.user = DataOperation.getUser()
        }
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if(positionSelected == 0){
            let feed = membersData[indexPath.row]
            let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.memberCell.rawValue, for: indexPath) as! MemberCell
            cell.feed = feed
            
            cell.followActionButton.tag = indexPath.row
            cell.followActionButton.addTarget(self, action: #selector(self.followUser(_:)), for: .touchUpInside)
            
            //gesture section image
            cell.contentView.isUserInteractionEnabled = true
            cell.contentView.tag = indexPath.row
            let tapped = UITapGestureRecognizer(target: self, action: #selector(self.startDetail(_:)))
            cell.contentView.addGestureRecognizer(tapped)
            //end of gesture
            
            return cell
            
        }else if (positionSelected == 1){
            
            let feed = memberFollowingData[indexPath.row]
            let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.memberCell.rawValue, for: indexPath) as! MemberCell
            cell.feed = feed
            cell.followActionButton.addTarget(self, action: #selector(self.followUser(_:)), for: .touchUpInside)
            
            //gesture section image
            cell.contentView.isUserInteractionEnabled = true
            cell.contentView.tag = indexPath.row
            let tapped = UITapGestureRecognizer(target: self, action: #selector(self.startFollowingDetail(_:)))
            tapped.cancelsTouchesInView = false
            tapped.numberOfTapsRequired = 1
            cell.contentView.addGestureRecognizer(tapped)
            //end of gesture
            
            return cell
            
        }else{
            let feed = feedsData[indexPath.row]
            
            if (feed.gallery.count == 0 && feed.image.isEmpty && feed.mediaFile.isEmpty){
                let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.feedUniversalNoImageTableViewCell.rawValue, for: indexPath) as! FeedUniversalNoImageTableViewCell
                
                cell.feed = feed
                
                //gesture section image
                cell.contentView.isUserInteractionEnabled = true
                cell.contentView.tag = indexPath.row
                let tapped = UITapGestureRecognizer(target: self, action: #selector(self.startFeedDetail(_:)))
                tapped.cancelsTouchesInView = false
                tapped.numberOfTapsRequired = 1
                cell.contentView.addGestureRecognizer(tapped)
                //end of gesture
                
                return cell
                
            }else {
                let  cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.feedUniversalCell.rawValue, for: indexPath) as! FeedUniversalCell
                
                cell.feed = feed
                
                //gesture section image
                cell.contentView.isUserInteractionEnabled = true
                cell.contentView.tag = indexPath.row
                let tapped = UITapGestureRecognizer(target: self, action: #selector(self.startFeedDetail(_:)))
                tapped.cancelsTouchesInView = false
                tapped.numberOfTapsRequired = 1
                cell.contentView.addGestureRecognizer(tapped)
                //end of gesture
                
                return cell
                
            }
        }
            
//        }else{
//
//            let feed = feedsData[indexPath.row]
//            let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.feedUniversalCell.rawValue, for: indexPath) as! FeedUniversalCell
//            cell.feed = feed
//            print("print users feed: ", cell.feed)
//
//            //gesture section image
//            cell.contentView.isUserInteractionEnabled = true
//            cell.contentView.tag = indexPath.row
//            let tapped = UITapGestureRecognizer(target: self, action: #selector(self.startFeedDetail(_:)))
//            tapped.cancelsTouchesInView = false
//            tapped.numberOfTapsRequired = 1
//            cell.contentView.addGestureRecognizer(tapped)
//            //end of gesture
//
//            return cell
//        }
    }
    
    
    
    //MARK: -- member profile
    func startDetail(_ sender: UITapGestureRecognizer)  {
        let profileController = UIStoryboard(name:AllKeys.mainStoryBoard, bundle:nil).instantiateViewController(withIdentifier: AllKeys.userProfileController) as! ProfileViewController
        let member = membersData[(sender.view?.tag)!]
        print("member \(member)")
        profileController.user = member
        
        self.navigationController?.pushViewController(profileController, animated: true)
    }
    
    //MARK: -- members followings profile
    func startFollowingDetail(_ sender: UITapGestureRecognizer)  {
        let profileController = UIStoryboard(name:AllKeys.mainStoryBoard, bundle:nil).instantiateViewController(withIdentifier: AllKeys.userProfileController) as! ProfileViewController
        let member = memberFollowingData[(sender.view?.tag)!]
        print("member \(member)")
        profileController.user = member
        
        self.navigationController?.pushViewController(profileController, animated: true)
    }
    
    func startFeedDetail(_ sender: UITapGestureRecognizer)  {
        let feedDetailController = UIStoryboard(name:"Main", bundle:nil).instantiateViewController(withIdentifier: AllKeys.feedDetailController) as! FeedDetailController
        let feed = feedsData[(sender.view?.tag)!]
        feedDetailController.feed = feed
        print("Data to pass \(feed.id)")
        self.navigationController?.pushViewController(feedDetailController, animated: true)
    }
    
   
    //MARK: - Following user
    @IBAction func followUser(_ sender: UIButton) {
        let titleText = sender.currentTitle!
        let feed = self.membersData[sender.tag]
        if (titleText == "Follow") {
            api.followMeber(userId: feed.id)
            sender.setTitle("Unfollow", for: .normal)
            feed.isFollowing = true
        } else {
            api.unFollowMeber(userId: feed.id)
            sender.setTitle("Follow", for: .normal)
            feed.isFollowing = false
        }
    }
    
    
    //calculating the item height
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        var number = 200;
        switch positionSelected {
        case 0:
            number = 80
        case 1:
            number = 80
        case 2:
            number = 210
        default:
            break
        }
        return CGFloat(number)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var number = 0;
        switch positionSelected{
        case 0:
            number = membersData.count
        case 1:
            number = memberFollowingData.count
        case 2:
            number = feedsData.count
        default:
            break
        }
        return number
        
    }
    
    
    func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
        return false
    }
    
    @IBAction func clickChurchSegment(_ sender: Any) {
        dataTableView.reloadData()
    }
    
    @IBAction func joinChurchButtonClicked(_ sender: Any) {
        
    }
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return CGFloat(270)
    }
    
    //implement the header
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = self.dataTableView.dequeueReusableHeaderFooterView(withIdentifier: CellIdentifiers.accountHeader.rawValue)
        let header = cell as! AccountTableHeader
        header.userData = self.user
        header.userSegmentControl.addTarget(self, action: #selector(ProfileViewController.churchSegmentButtonClick(_:)), for: .valueChanged)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(ProfileViewController.tappedUserImage))
        header.userImageView.addGestureRecognizer(tap)
        header.followButton.addTarget(self, action: #selector(ProfileViewController.followButtonClick(_:)), for: .touchUpInside)
        return cell
    }
    
    
    
    @IBAction func churchSegmentButtonClick(_ sender: UISegmentedControl) {
        positionSelected = sender.selectedSegmentIndex
        self.dataTableView.reloadData()
    }
    
    
    @IBAction func followButtonClick(_ sender: UIButton) {
        
        let text = sender.currentTitle!
        if (text == "Edit"){
           self.startEditing()
        }else{

            if text == "Follow" {

                //sender.isEnabled = false
                sender.setTitle("Unfollow", for: .normal)
                api.followMeber(userId: self.user.id)
                self.user.isFollowing = true
                //set follow button title to Following

            }else{
                //sender.isEnabled = false
                sender.setTitle("Follow", for: .normal)
                api.followMeber(userId: self.user.id)
                self.user.isFollowing = false
                //set follow button title to Following

            }


        }
       
    }
    
    //start editing
    func startEditting(url:String) {
        let vc = UIStoryboard(name:"Main", bundle:nil).instantiateViewController(withIdentifier: AllKeys.editProfileSegue) as! EditProfileController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK: - image preview
    func tappedUserImage()
    {
        SKPhotoBrowserOptions.backgroundColor = Mics.hexStringToUIColor(hex: AllKeys.primaryHexCode)
        SKPhotoBrowserOptions.textAndIconColor = UIColor.white
        SKPhotoBrowserOptions.toolbarTextShadowColor = UIColor.clear
        SKPhotoBrowserOptions.toolbarFont = UIFont(name: "Futura", size: 16.0)
        SKPhotoBrowserOptions.captionFont = UIFont(name: "Helvetica", size: 18.0)!
        
        // 1. create URL Array
        var images = [SKPhoto]()
        let photo = SKPhoto.photoWithImageURL(self.user.avatar)
        photo.shouldCachePhotoURLImage = false // you can use image cache by true(NSCache)
        images.append(photo)
        
        // 2. create PhotoBrowser Instance, and present.
        let browser = SKPhotoBrowser(photos: images)
        browser.initializePageIndex(0)
        present(browser, animated: true, completion: {})

    }
    
    
    //MARK: - feed section
    func didReceiveError(results: String) {
        //
    }
    
    func didReceiveNextUrl(results: String) {
        //
    }
    
    func didReceiveSuccess(results: [Feed]) {
        feedsData.append(contentsOf: results)
        dataTableView.reloadData()
    }
    
    func didReceiveDetail(results: String) {
        //
    }
    //end
    
    
    func didReceiveChurchMembersError(results: String) {
        //error
    }
    
    func didReceiveChurchMemberNextUrl(results: String) {
        self.followersNextUrl = results
    }
    
    func didReceiveChurchMembersDetail(results: String) {
        //something happened
    }
    
    func didReceiveChurchMembersSuccess(results: [Member]) {
        self.membersData.append(contentsOf: results)
        self.dataTableView.reloadData()
    }
    //
    
    func didReceiveFollowingError(results: String) {
        
    }
    
    func didReceiveFollowingDetail(results: String) {
        
    }
    
    func didReceiveFollowingNextUrl(results: String) {
        self.followingNextUrl = results
    }
    
    func didReceiveFollowingSuccess(results: [Member]) {
        self.memberFollowingData.append(contentsOf: results)
        self.dataTableView.reloadData()
    }
    
    //MARK: - scroll handling section
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let currentOffset = scrollView.contentOffset.y
        let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height
        
        if (maximumOffset - currentOffset) <= 100 {
            if(!self.feedNextUrl.isEmpty && !loadedPages.contains(self.feedNextUrl)){
                getData(page: self.feedNextUrl,userId: self.user.id)
            }
            if(!self.followersNextUrl.isEmpty && !loadedPages.contains(self.followersNextUrl)){
                self.getDataFollowers(page: self.followersNextUrl)
            }
            if(!self.followingNextUrl.isEmpty && !loadedPages.contains(self.followingNextUrl)){
                self.getDataFollowings(page: self.followingNextUrl)
            }
        }
    }
    
}
