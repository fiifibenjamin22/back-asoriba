//
//  RecordAudio.swift
//  asoriba
//
//  Created by Bright Ahedor on 09/03/2017.
//  Copyright © 2017 Asoriba Inc. All rights reserved.
//

import UIKit
import AVFoundation
import Alamofire

class RecordAudioController: UIViewController,AVAudioRecorderDelegate,AVAudioPlayerDelegate{
    
    var recordingSession: AVAudioSession!
    var audioRecorder: AVAudioRecorder!
    var player:AVAudioPlayer!
  
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var recordTimerLabel: UILabel!
    @IBOutlet weak var recordButton: UIButton!
    @IBOutlet weak var stopButton: UIButton!
    @IBOutlet weak var endRecordingButton: UIButton!
    @IBOutlet weak var userImageView: UIImageView!
    
    var audioFilename : URL!
    var meterTimer:Timer!
    let user = DataOperation.getUser()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.8)
        self.showAnimate()
        
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.closePopUp(_:)))
        self.view.addGestureRecognizer(tap)
        self.view.isUserInteractionEnabled = true
        
        recordingSession = AVAudioSession.sharedInstance()
        
        do {
            try recordingSession.setCategory(AVAudioSessionCategoryPlayAndRecord)
            try recordingSession.setActive(true)
            recordingSession.requestRecordPermission() { [unowned self] allowed in
                DispatchQueue.main.async {
                    if allowed {
                       self.loadRecordingUI()
                    } else {
                        // failed to record!
                    }
                }
            }
        } catch {
            // failed to record!
        }
        
    
        self.loadRecordingUI()
        
        if self.user.avatar.isEmpty {
         self.userImageView.isHidden = true
            
    
        }else {
            Alamofire.request((self.user.avatar)).responseImage { response in
                if let image = response.result.value {
                    self.userImageView.image = image
                }else{
                    self.userImageView.isHidden = true
                }
            }
        }
        
       
        
        
    }
    
    
    func endEditing() {
        view.endEditing(true)
    }
    
    
    func startPlaying()  {
        
            var url:URL?
            if self.audioRecorder != nil {
                url = self.audioRecorder.url
            } else {
                url = self.audioFilename!
            }

            do {
                self.player = try AVAudioPlayer(contentsOf: url!)
                player.delegate = self
                player.prepareToPlay()
                player.volume = 1.0
                player.play()
                self.playButton.isEnabled = false
                self.stopButton.isEnabled = true
                
            } catch let error as NSError {
                self.player = nil
                print(error.localizedDescription)
                self.playButton.isEnabled = true
                
            }
        
        
        
    }
  
    func updateAudioMeter(_ timer:Timer) {
        
        if audioRecorder.isRecording {
            let min = Int(audioRecorder.currentTime / 60)
            let sec = Int(audioRecorder.currentTime.truncatingRemainder(dividingBy: 60))
            let s = String(format: "%02d:%02d", min, sec)
            self.recordTimerLabel.text = s
            audioRecorder.updateMeters()
        }
    }
    
    //close action on the parent view
    func closePopUp(_ sender: UITapGestureRecognizer) {
        self.view.endEditing(true)
        self.removeAnimate()
        self.view.removeFromSuperview()
    }
    
    //close action on the button
    func cancelAction() {
        self.removeAnimate()
        self.view.removeFromSuperview()
    }
    
    
    func showAnimate()
    {
        self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view.alpha = 0.0;
        UIView.animate(withDuration: 0.25, animations: {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        });
    }
    
    func removeAnimate()
    {
        UIView.animate(withDuration: 0.25, animations: {
            self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view.alpha = 0.0;
        }, completion:{(finished : Bool)  in
            if (finished)
            {
                self.view.removeFromSuperview()
            }
        });
    }
    
    @IBAction func playButtonClick(_ sender: UIButton) {
        self.playButton.isEnabled = false
        self.recordButton.isEnabled = false
        startPlaying()
    }
    
    @IBAction func recordButtonClicked(_ sender: UIButton) {
        if audioRecorder == nil {
            startRecording()
        } else {
            finishRecording(success: true)
        }
    }
   
    @IBAction func stopButtonClick(_ sender: UIButton) {
        if player != nil && player.isPlaying {
           player.stop()
           self.playButton.isEnabled = true
           self.recordButton.isEnabled = true
        }
        if audioRecorder != nil && audioRecorder.isRecording {
            self.finishRecording(success: true)
            self.playButton.isEnabled = true
        }
        
    }
    
    @IBAction func endRecordingClick(_ sender: UIButton) {
        NotificationCenter.default.post(name: Notification.Name(AllKeys.audioNotification), object: self.audioFilename )
        self.cancelAction()
    }
    
    func loadRecordingUI() {
        self.recordButton.isEnabled = true
        self.playButton.isEnabled = false
        Helpers.roundButton(cornerRadius: CGFloat(2.0), button: self.endRecordingButton, borderColor: UIColor.white)
    }
    
    func startRecording() {
        let format = DateFormatter()
        format.dateFormat="yyyy-MM-dd-HH-mm-ss"
        let currentFileName = "recording-\(format.string(from: Date())).m4a"
        audioFilename = getDocumentsDirectory().appendingPathComponent(currentFileName)
        let settings = [
                AVFormatIDKey: Int(kAudioFormatMPEG4AAC),
                AVSampleRateKey: 12000,
                AVNumberOfChannelsKey: 1,
                AVEncoderAudioQualityKey: AVAudioQuality.high.rawValue
        ]
            
        do {
            audioRecorder = try AVAudioRecorder(url: audioFilename, settings: settings)
            audioRecorder.delegate = self
            audioRecorder.record()
            self.recordButton.isEnabled = false
            self.playButton.isEnabled = false
            self.meterTimer = Timer.scheduledTimer(timeInterval: 0.1,target:self,
                                                   selector:#selector(RecordAudioController.updateAudioMeter(_:)),
                                                   userInfo:nil,
                                                   repeats:true)
        } catch {
            finishRecording(success: false)
        }
        
    }
    
    func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let documentsDirectory = paths[0]
        return documentsDirectory
    }
    
    func finishRecording(success: Bool) {
        audioRecorder.stop()
        audioRecorder = nil
        self.meterTimer.invalidate()
        if success {
            self.recordButton.isEnabled = true
        } else {
            self.recordButton.isEnabled = true
        }
    }
    
    func audioRecorderDidFinishRecording(_ recorder: AVAudioRecorder, successfully flag: Bool) {
        if !flag {
            finishRecording(success: false)
        }
    }
    
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        self.recordButton.isEnabled = true
    }
    
    func audioPlayerDecodeErrorDidOccur(_ player: AVAudioPlayer, error: Error?) {
        print("Error while playing audio \(String(describing: error?.localizedDescription))")
    }
}
