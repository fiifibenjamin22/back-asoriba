//
//  customBackendUser.swift
//  asoriba
//
//  Created by Benjamin Acquah on 17/09/2017.
//  Copyright © 2017 Asoriba Inc. All rights reserved.
//

import UIKit

class Message: NSObject {
    
    var message : String?
    var username : String?
    var id : String?
    var created : Double? = 0.0

    //to json
}

class snapShotMessagesLog : NSObject {
    
    var snapshotValueObject : String?

}

extension String {
    func toJSON() -> Any? {
        guard let data = self.data(using: .utf8, allowLossyConversion: false) else { return nil }
        return try? JSONSerialization.jsonObject(with: data, options: .mutableContainers)
    }
}
