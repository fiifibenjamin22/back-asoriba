//
//  Protocols.swift
//  asoriba
//
//  Created by Bright Ahedor on 15/12/2016.
//  Copyright © 2016 Asoriba Inc. All rights reserved.
//

import Foundation


//MARK: - feed protocol
protocol FeedServiceProtocol{
    func didReceiveSuccess(results:[Feed])
    func didReceiveError(results:String)
    func didReceiveDetail(results:String)
    func didReceiveNextUrl(results:String)
}

//MARK: - donation protocol
protocol DonationServiceProtocol{
    func didReceiveDonationSuccess(results:[Giving])
    func didReceiveDonationError(results:String)
    func didReceiveDonationDetail(results:String)
    func didReceiveDonationNextUrl(results:String)
}


//MARK: - registration protocol
protocol RegisterServiceProtocol{
    func didReceiveAccountSuccess(result:Bool)
    func didReceiveAccountError(results:String)
    func didReceiveAccountDetail(results:String)
}

//MARK: - number protocol
protocol RegisterWithNumberServiceProtocol{
    func didReceiveNumberSuccess(result:Bool)
    func didReceiveNumberError(result:String)
}

//MARK: - church members protocol
protocol ChurchMembersServiceProtocol{
    func didReceiveChurchMembersSuccess(results:[Member])
    func didReceiveChurchMembersError(results:String)
    func didReceiveChurchMembersDetail(results:String)
    func didReceiveChurchMemberNextUrl(results:String)
}

//MARK: - church members protocol
protocol FollowingServiceProtocol{
    func didReceiveFollowingSuccess(results:[Member])
    func didReceiveFollowingError(results:String)
    func didReceiveFollowingDetail(results:String)
    func didReceiveFollowingNextUrl(results:String)
}


//MARK: - church groups protocol
protocol GroupsServiceProtocol{
    func didReceiveGroupsSuccess(results:[Group])
    func didReceiveGroupsError(results:String)
    func didReceiveGroupsDetail(results:String)
    func didReceiveChurchGroupsNextUrl(results:String)
}

//MARK: - fetch User protocol
//protocol UsersfetchProtocol{
//    func didReceiveUserSuccess(results:[UsersList])
//    func didReceiveUserError(results:String)
//    func didReceiveUserDetail(results:String)
//    func didReceiveUserNextUrl(results:String)
//}

//MARK: - church protocol
protocol ChurchServiceProtocol{
    func didReceiveChurchSuccess(results:[Church])
    func didReceiveChurchError(results:String)
    func didReceiveChurchDetail(results:String)
    func didReceiveChurchNextUrl(results:String)
}

//MARK: - joing church protocol
protocol JoinChurchServiceProtocol{
    func didReceiveJoinChurchSuccess(results:Church)
    func didReceiveJoinChurchError(results:String)
    func didReceiveJoinChurchDetail(results:String)
}

//MARK: - content delegate
protocol ContentServiceProtocol{
    func didReceiveContent(result:Feed)
    func didReceiveContentError(results:String)
}

//MARK: - payment delegate
protocol PaymentServiceProtocol{
    func didReceivePaymentResult(result:CheckoutResponse)
    func didReceivePaymentError(results:String)
}

//MARK: - payment delegate
protocol UpdateUserServiceProtocol{
    func didReceiveUpdateStringResult(result:Bool)
    func didReceiveUpdateFileResult(result:Bool)
    func didReceiveUpdateError(results:String)
}

//MARK: - comments delegate
protocol CommentServiceProtocol{
    func didReceiveComments(results:[Comment])
    func didReceiveComment(result:Comment)
    func didReceiveComentError(result:String)
    func didReceiveCommentsError(result:String)
    func didReceiveCommentsNextUrl(result:String)
}

//MARK: - about delegate
protocol AboutServiceProtocol{
    func didReceiveAboutInfo(result:Church)
    func didReceiveAbouts(results:[About])
    func didReceiveAboutError(result:String)
}

//MARK: - payment history delegate
protocol PaymentHistoryServiceProtocol{
    func didReceiveHistories(results:[PaymentHistory])
    func didReceiveHistoryError(result:String)
    func didReceiveHistoryNextUrl(result:String)
}

//MARK: - make post delagate
protocol PostServiceProtocol{
    func didReceivePostResult(result:Feed)
    func didReceivePostError(result:String)
}

//MARK: - make post delagate
protocol CheckInServiceProtocol{
    func didReceiveChecksResult(results:[CheckIn])
    func didReceiveCheckResult(result:CheckIn)
    func didReceiveCheckError(result:String)
    func didReceiveCheckNextUrl(result:String)
}






