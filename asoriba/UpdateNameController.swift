//
//  UpdateNameController.swift
//  asoriba
//
//  Created by Bright Ahedor on 21/12/2016.
//  Copyright © 2016 Asoriba Inc. All rights reserved.
//

import UIKit
import SwiftSpinner

class UpdateNameController: UIViewController,UpdateUserServiceProtocol,UITextFieldDelegate,UINavigationControllerDelegate,UIImagePickerControllerDelegate{

    @IBOutlet weak var firstNameTextFiled: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    var api:ApiService!
    
    @IBOutlet weak var otherNametextField: UITextField!
    @IBOutlet weak var emailTxt: UITextField!
    
    @IBOutlet weak var DoBLabel: UILabel!
    @IBOutlet weak var continueButton: UIButton!
    @IBOutlet weak var activityFIeldsView: UIView!
    @IBOutlet weak var profileImage: UIImageView!
    
    
    var firstName = ""
    var lastName = ""
    var otherName = ""
    var email = ""
    var dateOfBirth = ""
    var imagePicker: UIImagePickerController!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        //api instance
        api = ApiService()
        self.api.updateUserDelagate = self
        
        imagePicker?.delegate = self
        
        self.firstNameTextFiled.delegate = self
        self.lastNameTextField.delegate = self
        self.otherNametextField.delegate = self
        //self.emailTxt.delegate = self
        
        let path = Bundle.main.path(forResource: "configuration", ofType: "plist")
        let root = NSDictionary(contentsOfFile: path!)
        
        let headerBackgrounds = (root?["headerBackgroundColor"] as! String)
        
        view.backgroundColor = UIColor().HexToColor(hexString: headerBackgrounds)
        continueButton.backgroundColor = UIColor().HexToColor(hexString: headerBackgrounds)
        
        DoBLabel.translatesAutoresizingMaskIntoConstraints = false
        DoBLabel.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        // declare select image tap
        self.profileImage.layer.cornerRadius = 60
        self.profileImage.layer.masksToBounds = true
        let avaTap = UITapGestureRecognizer(target: self, action: #selector(handleSelectImage))
        avaTap.numberOfTapsRequired = 1
        self.profileImage.isUserInteractionEnabled = true
        self.profileImage.addGestureRecognizer(avaTap)
        
    }


    //MARK: - Delegates and data sources
    //MARK: Data Sources
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    

    // MARK: - Textfield delegates
    func textFieldDidBeginEditing(_ textField: UITextField) {
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if (textField == self.firstNameTextFiled){
            firstName = textField.text!
            print(firstName)
        }
        if (textField == self.lastNameTextField){
            lastName = textField.text!
            print(lastName)
        }
        if (textField == self.otherNametextField){
            otherName = textField.text!
            print(otherName)
        }
        
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true;
    }
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        return true;
    }
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        return true;
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return true;
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder();
        return true;
    }
    
    func didReceiveUpdateError(results: String) {
        SwiftSpinner.hide()
        self.showDialog(message: results)
    }
    
    func didReceiveUpdateStringResult(result: Bool) {
        SwiftSpinner.hide()
        self.startMain()
    }
    
    func startMain() {
        guard let vc = UIStoryboard(name:"Main", bundle:nil).instantiateViewController(withIdentifier: AllKeys.mainController) as? MainTabController else {
            return
        }
        self.present(vc, animated: true, completion: nil)
    }
    
    func didReceiveUpdateFileResult(result: Bool) {
        //refresh UI
    }
    
    //MARK: action section
    @IBAction func continueButtonClick(_ sender: Any) {
        
        //validate if not return
        if (firstNameTextFiled.text?.isEmpty)! {
           self.showDialog(message: "Provide first name and continue")
            return
        }
        if (lastNameTextField.text?.isEmpty)! {
            self.showDialog(message: "Provide last name and continue")
            return
        }
        
        if (otherNametextField.text?.isEmpty)! {
            self.showDialog(message: "Provide a username and continue")
            return
        }
        
        if (dateOfBirth.isEmpty) {
            self.showDialog(message: "Specify your date of birth and continue")
            return
        }
        
        
        //validate before moving into the app
        if (!firstName.isEmpty && !lastName.isEmpty && !otherName.isEmpty){
           SwiftSpinner.show("Just a moment...", animated: true)
            api.updateRecord(firstName: firstName, lastName: lastName, otherName: otherName, dateOfBirth: dateOfBirth)
        }
        
    }
    
    //MARK: dialog section
    func showDialog(message:String) {
        let  alert = UIAlertController(title: nil, message: message,preferredStyle: .alert)
        let cancel = UIAlertAction(title: "Okay", style: UIAlertActionStyle.cancel, handler: nil)
        alert.addAction(cancel)
        self.present(alert,animated: true,completion: nil)
    }
    
    @IBAction func dateOfBirthPicker(_ sender: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        dateOfBirth = dateFormatter.string(from: sender.date)
    }
    
    // connect selected image to our ImageView
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let image = info[UIImagePickerControllerOriginalImage] as? UIImage
        self.profileImage.image = image
        self.saveImage(image: profileImage.image!)
        self.dismiss(animated: true, completion: nil)
    }
    
    func saveImage(image:UIImage) {
        api.updateRecords(image: image)
    }
    
    func handleSelectImage(){
        
        let pickers = UIAlertController(title: "Select Option", message: nil, preferredStyle: .actionSheet)
        let camera = UIAlertAction(title: "Take Shot From Camera", style: UIAlertActionStyle.default) { (UIAlertAction) in
            let picker = UIImagePickerController()
            picker.delegate = self
            picker.sourceType = .camera
            picker.allowsEditing = true
            self.present(picker, animated: true, completion: nil)
            
        }
        
        let photoAlbum = UIAlertAction(title: "Saved Photo Album ", style: UIAlertActionStyle.default) { (UIAlertAction) in
            let picker = UIImagePickerController()
            picker.delegate = self
            picker.sourceType = .savedPhotosAlbum
            picker.allowsEditing = true
            self.present(picker, animated: true, completion: nil)
            
        }
        
        let photoLibrary = UIAlertAction(title: "Photo Library", style: UIAlertActionStyle.default) { (UIAlertAction) in
            let picker = UIImagePickerController()
            picker.delegate = self
            picker.sourceType = .photoLibrary
            picker.allowsEditing = true
            self.present(picker, animated: true, completion: nil)
            
        }
        
        let cancel = UIAlertAction(title: "cancel", style: UIAlertActionStyle.cancel, handler: nil)
        
        pickers.addAction(camera)
        pickers.addAction(photoAlbum)
        pickers.addAction(photoLibrary)
        pickers.addAction(cancel)
        
        self.present(pickers, animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func showCameraNotAvailabeAction() {
        let alertActionControl = UIAlertController.init(title: "Sorry", message: "Your device has no camera", preferredStyle: .alert)
        let alertActionCancel = UIAlertAction.init(title: "Cancel", style: .cancel) {(Alert:UIAlertAction!) -> Void in
            
        }
        alertActionControl.addAction(alertActionCancel)
        self.present(alertActionControl,animated:true,completion:nil)
    }
}
