//
//  forumLogCV.swift
//  asoriba
//
//  Created by Benjamin Acquah on 06/10/2017.
//  Copyright © 2017 Asoriba Inc. All rights reserved.
//

import UIKit
import LBTAComponents
import Firebase
import SwiftyJSON
import Alamofire
import AlamofireImage

class forumLogCV: UICollectionViewController, UITextFieldDelegate, UICollectionViewDelegateFlowLayout {
    
    var id = ""
    var topicTitle = ""
    var userData = DataOperation.getUser()
    var forumMessagesArray = [forumLog]()
    var messageText = ""
    
    lazy var inputTextField : UITextField = {
        let TextField = UITextField()
        TextField.placeholder = "Enter Message..."
        TextField.keyboardAppearance = .dark
        TextField.delegate = self
        TextField.backgroundColor = .white
        TextField.translatesAutoresizingMaskIntoConstraints = false
        return TextField
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.collectionView?.backgroundColor = .white
        self.collectionView?.alwaysBounceVertical = true
        self.collectionView?.contentInset = UIEdgeInsetsMake(8, 0, 8, 0)
        self.tabBarController?.tabBar.isHidden = true

        self.collectionView?.register(forumLogCell.self, forCellWithReuseIdentifier: cellIdentify)
        
        self.navigationItem.title = topicTitle
        
        collectionView?.keyboardDismissMode = .interactive
        
        self.inputTextField.becomeFirstResponder()
        if forumMessagesArray.count < 1 {
            self.inputTextField.becomeFirstResponder()
        }

        observeMessages()
        setupKeyboardObservers()
    }
    
    func setupKeyboardObservers(){
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardDidShow), name: NSNotification.Name.UIKeyboardDidShow, object: nil)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
    func handleKeyboardDidShow(){
        //scroll to the last index
        if forumMessagesArray.count > 0 {
            
            UIView.animate(withDuration: 0.5, animations: {
                
                let indexPath = NSIndexPath(item: self.forumMessagesArray.count - 1, section: 0)
                self.collectionView?.scrollToItem(at: indexPath as IndexPath, at: .bottom, animated: true)
                
            }, completion: nil)

        }
    }

    
    lazy var inputContainerView : UIView = {
        
        let containerView = UIView()
        containerView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 50)
        containerView.backgroundColor = .white
        
        let sendButton = UIButton(type: .system)
        sendButton.setTitle("Send", for: .normal)
        sendButton.addTarget(self, action: #selector(sendMesages), for: UIControlEvents.touchUpInside)
        sendButton.translatesAutoresizingMaskIntoConstraints = false
        containerView.addSubview(sendButton)
        
        sendButton.rightAnchor.constraint(equalTo: containerView.rightAnchor).isActive = true
        sendButton.centerYAnchor.constraint(equalTo: containerView.centerYAnchor).isActive = true
        sendButton.widthAnchor.constraint(equalToConstant: 80).isActive = true
        sendButton.heightAnchor.constraint(equalTo: containerView.heightAnchor).isActive = true
        
        containerView.addSubview(self.inputTextField)
        self.inputTextField.leftAnchor.constraint(equalTo: containerView.leftAnchor, constant: 8).isActive = true
        self.inputTextField.centerYAnchor.constraint(equalTo: containerView.centerYAnchor).isActive = true
        self.inputTextField.rightAnchor.constraint(equalTo: sendButton.leftAnchor).isActive = true
        self.inputTextField.heightAnchor.constraint(equalTo: containerView.heightAnchor).isActive = true
        
        let seperatorLine = UIView()
        seperatorLine.backgroundColor = UIColor.lightGray
        seperatorLine.translatesAutoresizingMaskIntoConstraints = false
        containerView.addSubview(seperatorLine)
        
        seperatorLine.leftAnchor.constraint(equalTo: containerView.leftAnchor).isActive = true
        seperatorLine.topAnchor.constraint(equalTo: containerView.topAnchor).isActive = true
        seperatorLine.widthAnchor.constraint(equalTo: containerView.widthAnchor).isActive = true
        seperatorLine.heightAnchor.constraint(equalToConstant: 1).isActive = true
        
        return containerView
    }()
    
    override var inputAccessoryView: UIView? {
        get {
            return inputContainerView
        }
    }
    
    override var canBecomeFirstResponder: Bool {
        return true
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return forumMessagesArray.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentify, for: indexPath) as! forumLogCell
        let messages = forumMessagesArray[indexPath.row]
        cell.messageTextView.text = messages.message
        cell.userName.text = messages.username
        
        let timeStamp : NSDate = NSDate(timeIntervalSince1970: messages.created! / 1000)
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US")
        dateFormatter.dateFormat = "h:mm a"
        dateFormatter.timeZone = NSTimeZone.local
        dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
        cell.timeCreated.text = dateFormatter.string(from: timeStamp as Date)
        
        setUpCell(cell: cell, message: messages)
        
        //modify bubble view width
        cell.bubbleWidthAnchor?.constant = estimatedFrameForText(text: messages.message!).width + 32
        
        return cell
    }
    
    private func setUpCell(cell: forumLogCell, message: forumLog) {
        
        //chech for incoming and outgoing messages
        if message.id == userData.id {
            //outgoing messages
            let path = Bundle.main.path(forResource: "configuration", ofType: "plist")
            let root = NSDictionary(contentsOfFile: path!)
            
            let dynamicBackgroundColor = (root?["headerBackgroundColor"] as! String)
            cell.chatBubbleView.backgroundColor = UIColor().HexToColor(hexString: dynamicBackgroundColor)
            cell.chatter1_ProfileImage.isHidden = true
            
            cell.bubbleRightAnchor?.isActive = true
            cell.bubbleLeftAnchor?.isActive = false
            cell.checkMark.isHidden = false
            cell.timeRightAnchor?.constant = -5
            cell.timeTopAnchor?.constant = 0
            cell.userName.isHidden = true
        }else{
            //incomming messages
            cell.chatBubbleView.backgroundColor = .gray
            cell.chatter1_ProfileImage.isHidden = false
            
            cell.bubbleRightAnchor?.isActive = false
            cell.bubbleLeftAnchor?.isActive = true
            cell.checkMark.isHidden = true
            cell.timeRightAnchor?.constant = 10
            cell.timeTopAnchor?.constant = 5
            cell.userName.isHidden = false
            cell.userName.backgroundColor = .green
            
        }
        
    }
    
    func sendMesages(){

        self.messageText = self.inputTextField.text!
        
        if !messageText.isEmpty {
            
            let timeStamp : Double = NSDate().timeIntervalSince1970 * 1000
            let myuser_id = userData.id
            let userName = "\(userData.firstName) \(userData.lastName)"
            
            let json = "{\"message\":\"\(inputTextField.text!)\",\"username\":\"\(userName)\",\"id\":\"\(myuser_id)\",\"created\":\(timeStamp)}"
            
            let ref = Database.database().reference().child("ICGC").child("messages")
            let childRef = ref.child(id).childByAutoId()
            let values = json
            childRef.setValue(values)
            
            self.inputTextField.text = ""
        }
    }
    
    //observe chat data from the firebase database
    func observeMessages(){
        
        let ref = Database.database().reference().child("ICGC").child("messages")
        ref.child(id).observe(.childAdded, with: { (snapshot) in
            
            let message = forumLog()
            let snapShotForumLogObject = snapShotForumLog()
            
            let snapshotValue = snapshot.value!
            snapShotForumLogObject.snapshotValueObject = snapshotValue as? String
            
            let theMessageValue = snapShotForumLogObject.snapshotValueObject!
            
            let data: NSData = theMessageValue.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))! as NSData
            
            let json = JSON(data: data as Data)
            //get messages from JSON
            if let textMessage = json["message"].string {
                //Now you got your value
                message.message = textMessage
                let storedtextMessage = message.message!
                print("print text messages from user data: ", storedtextMessage)
                
            }
            //get username from JSON
            if let username = json["username"].string {
                message.username = username
                print("print text username from user data: ", username)
                
            }
            //get id from JSON
            if let my_id_in_firebase = json["id"].string {
                message.id = my_id_in_firebase
                print("print text id from user data: ", my_id_in_firebase)
                
            }
            //get timecreated from JSON
            if let timeCreated = json["created"].double {
                message.created = timeCreated
                print("print text time from user data: ", timeCreated)
                
            }
            
            self.forumMessagesArray.append(message)
            
            DispatchQueue.main.async {
                self.collectionView?.reloadData()
                
                //scroll to the last index
                let indexPath = NSIndexPath(item: self.forumMessagesArray.count - 1, section: 0)
                self.collectionView?.scrollToItem(at: indexPath as IndexPath, at: .bottom, animated: true)

            }
            
        }, withCancel: nil)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        sendMesages()
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var height : CGFloat = 80
        
        //get the dynamic height
        if let text = forumMessagesArray[indexPath.row].message {
            height = estimatedFrameForText(text: text).height + 30
        }

        return CGSize(width: view.frame.width, height: height)
    }
    
    private func estimatedFrameForText(text: String) -> CGRect {
        let size = CGSize(width: 200, height: 1000)
        let options = NSStringDrawingOptions.usesFontLeading.union(NSStringDrawingOptions.usesLineFragmentOrigin)
        return NSString(string: text).boundingRect(with: size, options: options, attributes: [NSFontAttributeName : UIFont.systemFont(ofSize: 16)], context: nil)
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        self.collectionView?.collectionViewLayout.invalidateLayout()
    }
    
}
