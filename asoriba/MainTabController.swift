//
//  MainTabController.swift
//  asoriba
//
//  Created by Bright Ahedor on 14/12/2016.
//  Copyright © 2016 Asoriba Inc. All rights reserved.
//

import UIKit
import UserNotifications
import FirebaseInstanceID
import FirebaseAnalytics
import Crashlytics

class MainTabController: UITabBarController {
    
    var api:ApiService!
   
    override func viewDidLoad() {
        super.viewDidLoad()
     
        
        if let path = Bundle.main.path(forResource: "asoriba-switchnavigateConfig", ofType: "plist"){
            let root = NSDictionary(contentsOfFile: path)
            print(root ?? "")
            selectedIndex = 0
        }else{
            selectedIndex = 0
        }
        

        //update with all gateways
        api = ApiService()
        api.getPaymentGetways()
                
        //save and count the launches to update the user profile
        let count = UserDefaults.standard.integer(forKey: "COUNT")
        if count >= 5 {
           UserDefaults.standard.set(0, forKey: "COUNT")
            //send request
        }else {
            UserDefaults.standard.set(count + 1, forKey: "COUNT")
        }
        
        if let refreshedToken = InstanceID.instanceID().token() {
            api.updateUserTokens(instanceId: refreshedToken)
        }
        
        api.subscribeToTopics()
        
        
        let path = Bundle.main.path(forResource: "configuration", ofType: "plist")
        let root = NSDictionary(contentsOfFile: path!)
        
        let tabTintColor = (root?["tabBarColors"] as! String)
        let tabBarTintColor = (root?["hexWhite"] as! String)
        let uselectedTintColor = (root?["headerBackgroundColor"] as! String)
        
        
        tabBar.tintColor = UIColor().HexToColor(hexString: tabBarTintColor)
        tabBar.barTintColor = UIColor().HexToColor(hexString: tabTintColor)
        tabBar.unselectedItemTintColor = UIColor().HexToColor(hexString: uselectedTintColor)
        
        UITabBarItem.appearance().setTitleTextAttributes([NSForegroundColorAttributeName : UIColor().HexToColor(hexString: uselectedTintColor)], for: UIControlState.selected)
        
        UITabBarItem.appearance().setTitleTextAttributes([NSForegroundColorAttributeName : UIColor().HexToColor(hexString: tabBarTintColor)], for: .normal)
        
    }
    
    // UITabBarControllerDelegate
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        NotificationCenter.default.post(name: Notification.Name(AllKeys.selectTabNotificationState), object: nil)
    }
}


