//
//  FeedCollectionViewCell.swift
//  asoriba
//
//  Created by Bright Ahedor on 15/02/2017.
//  Copyright © 2017 Asoriba Inc. All rights reserved.
//

import UIKit
import AlamofireImage
import ActiveLabel
import Firebase
import Crashlytics

@available(iOS 9.0, *)
class FeedCollectionViewCell: UICollectionViewCell {
    
    var isColorInverted = true
    var asoribaColors : UIColor!
    var icgcColors : UIColor!
    var icgcGreenColor = UIColor(red: 0.0 / 255, green: 79 / 255, blue: 75 / 255, alpha: 1.0)//GREEN
    var icgcYellowColor = UIColor(red: 240 / 255, green: 199 / 255, blue: 75 / 255, alpha: 1.0)//YELLOW
    
    @IBOutlet weak var userNameUIView: UIView!
    @IBOutlet weak var userImageView: UIImageView!
    
    
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var feedTimeLabel: UILabel!
    
    @IBOutlet weak var mediaSectionUIView: UIView!
    @IBOutlet weak var mediaSectionUIViewConstraint: NSLayoutConstraint!
    @IBOutlet weak var singleImageView: UIImageView!
    @IBOutlet weak var singleImageUIVisualEffect: UIVisualEffectView!
    @IBOutlet weak var videoPlayImageView: UIImageView!
    @IBOutlet weak var galleryImageStackView: UIStackView!
    @IBOutlet weak var firstImageView: UIImageView!
    @IBOutlet weak var secondImageView: UIImageView!
    @IBOutlet weak var secondImageVisualEffect: UIVisualEffectView!
    @IBOutlet weak var galleryCountLabel: UILabel!
    @IBOutlet weak var audioSectionUIView: UIView!
    @IBOutlet weak var audioIconImageView: UIImageView!
    @IBOutlet weak var audioPlayButton: UIButton!
    @IBOutlet weak var audioSectionUIViewConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var feedContentLabel: ActiveLabel!
    
    @IBOutlet weak var feedContentConstraint: NSLayoutConstraint!
    @IBOutlet weak var amenCountLabel: UILabel!
    @IBOutlet weak var shareCountLabel: UILabel!
    @IBOutlet weak var commentCountLabel: UILabel!
    @IBOutlet weak var viewCountLabel: UILabel!
    @IBOutlet weak var amenButton: UIButton!
    
    @IBOutlet weak var shareButton: UIButton!
    
    @IBOutlet weak var commentButton: UIButton!
    @IBOutlet weak var quotationLabel: UILabel!
    @IBOutlet weak var quotationConstraint: NSLayoutConstraint!
    @IBOutlet weak var mediaLabel: UILabel!
    //@IBOutlet weak var readMore: UILabel!
    
    private var placeHolderImage = UIImage(named: "default")
    private var music = UIImage(named: "musical-note")
    var isDetail = false
    
    var feed: Feed!{
        didSet{
          self.updateUI()
        }
    }
    
    func updateUI()  {
        
        self.feedContentLabel.textColor = UIColor.darkGray
        
        self.singleImageView.contentMode = .scaleAspectFill
        self.singleImageView.clipsToBounds = true
        
        self.userNameLabel.font = UIFont.boldSystemFont(ofSize: 15)
        self.userNameLabel.font = UIFont.systemFont(ofSize: 15, weight: 10)
        self.userNameLabel.textColor = UIColor.black
        
        let path = Bundle.main.path(forResource: "configuration", ofType: "plist")
        let root = NSDictionary(contentsOfFile: path!)
        
        let nameLabelColor = (root?["headerBackgroundColor"] as! String)
        let LabelYellowColor = (root?["headerYellowColor"] as! String)
        let mediaLabelConfig = (root?["mediaLabel"] as! String)
        
        self.userNameLabel.adjustsFontSizeToFitWidth = true
        
        amenCountLabel.textColor = UIColor().HexToColor(hexString: nameLabelColor)
        shareCountLabel.textColor = UIColor().HexToColor(hexString: nameLabelColor)
        commentCountLabel.textColor = UIColor().HexToColor(hexString: nameLabelColor)
        viewCountLabel.textColor = UIColor().HexToColor(hexString: nameLabelColor)
        mediaLabel.text = mediaLabelConfig
        
        self.layer.shadowColor = UIColor.lightGray.cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        self.layer.shadowRadius = 2.0
        self.layer.shadowOpacity = 1.0
        self.layer.masksToBounds = false
        self.layer.shadowPath = UIBezierPath(roundedRect: self.bounds, cornerRadius: self.contentView.layer.cornerRadius).cgPath

        //setting counts
        self.amenCountLabel.text = Mics.formatNumber(number: feed.numberOfAmens, text: "amen")
        self.shareCountLabel.text = Mics.formatNumber(number: feed.numberOfShares, text: "share")
        self.commentCountLabel.text = Mics.formatNumber(number: feed.numberOfComments, text: "comment")
        self.viewCountLabel.text = Mics.formatNumber(number: feed.numberOfViews, text: "view")
        //end of counts
        
        if (feed.hasAmened){
            self.amenButton.setTitle("Amened", for: .normal)
            
        }else {
            self.amenButton.setTitle("Amen", for: .normal)
        }
        
        //user image
        if let member = feed.member{
            
            self.feedTimeLabel.text = Mics.setDate(dateString: feed.date)
            if (member.isMobile) {
                self.userNameLabel.text =  (member.firstName) + " " + (member.lastName)
            }else {
                
                self.userNameLabel.text =  member.firstName
            }
            
            if  !(member.avatar.isEmpty) {
                self.userImageView.af_setImage(
                    withURL: URL(string: (member.avatar))!,
                    placeholderImage: placeHolderImage,
                    imageTransition: .crossDissolve(0.2)
                )}else {
                self.userImageView.image = placeHolderImage
            }
        }
        //end of user
        
        if let quotation = feed.quotation {
            self.quotationLabel.isHidden = false
            self.quotationConstraint.constant = 17
            self.quotationLabel.text = quotation.book + " " + quotation.chapter + ":" + quotation.verse
        } else {
           self.quotationLabel.isHidden = true
           self.quotationConstraint.constant = 0
        }
        
        //content
        
        let messageAttribute = Mics.getAttributeText(feed: feed)
        
        if isDetail == true {
            self.feedContentLabel.numberOfLines = 0
            self.feedContentConstraint.constant = Helpers.heightOfLabel(text: messageAttribute, fontSize: CGFloat(15), width: UIScreen.main.bounds.width-CGFloat(16),numberOfLines: 0)
            
        }else{
            self.feedContentLabel.numberOfLines = 5
            self.feedContentConstraint.constant = Helpers.heightOfLabel(text: messageAttribute, fontSize: CGFloat(16), width: UIScreen.main.bounds.width-CGFloat(16),numberOfLines: 5)
            
            //analytics
            Answers.logContentView(withName: "Detail_View", contentType: feed.content, contentId: feed.id, customAttributes: nil)
        }
        
        self.feedContentLabel.attributedText = messageAttribute
        //end of content
        
        //audio
        if !feed.mediaFile.isEmpty && feed.mediaType == "audio" {
            print("Audio cell--- \(feed.mediaType)")
            self.audioSectionUIView.isHidden = false
            self.audioSectionUIViewConstraint.constant = 40
            self.videoPlayImageView.isHidden = true
            self.mediaSectionUIViewConstraint.constant = 0
            self.mediaSectionUIView.isHidden = true
            self.audioIconImageView.image = self.music
            Helpers.roundButton(cornerRadius: 2, button: self.audioPlayButton,borderColor: Mics.hexStringToUIColor(hex: AllKeys.primaryHexCode))
        }
            //passing video
        else if (feed.mediaType == "video") {
            print("Video cell")
            
            self.videoPlayImageView.isHidden = false
            self.mediaSectionUIViewConstraint.constant = 300
            self.mediaSectionUIView.isHidden = false
            self.firstImageView.isHidden = true
            self.secondImageView.isHidden = true
            self.singleImageView.isHidden = false
            self.secondImageVisualEffect.isHidden = true
            self.galleryCountLabel.isHidden = true
            self.singleImageUIVisualEffect.isHidden = false
            self.galleryImageStackView.isHidden = true
            self.audioSectionUIViewConstraint.constant = 0
            self.audioSectionUIView.isHidden = true
            
            let videoUrl = feed.medialUrl
            let videoThumb = feed.mediaThumbnail
            
            if !videoThumb.isEmpty {
                print("Thumb \(videoThumb)")
                self.singleImageView.af_setImage(
                    withURL: URL(string: (videoThumb))!,
                    placeholderImage: placeHolderImage,
                    imageTransition: .crossDissolve(0.2)
                )
            
            } else if (!videoUrl.isEmpty && (videoUrl.count > AllKeys.youtubeVideoIdLengthPositive) ){
                let videoId = videoUrl.substring(from:videoUrl.index(videoUrl.endIndex, offsetBy: AllKeys.youtubeVideoIdLength))
                let image = "http://img.youtube.com/vi/\(videoId)/mqdefault.jpg"
                self.singleImageView.af_setImage(
                    withURL: URL(string: (image))!,
                    placeholderImage: placeHolderImage,
                    imageTransition: .crossDissolve(0.2)
                )
            }

            
            
        }
        else if !feed.image.isEmpty {
            print("Feed with image")
            
            self.videoPlayImageView.isHidden = true
            self.mediaSectionUIViewConstraint.constant = 300
            self.mediaSectionUIView.isHidden = false
            self.firstImageView.isHidden = true
            self.singleImageView.isHidden = false
            self.singleImageUIVisualEffect.isHidden = true
            self.secondImageView.isHidden = true
            self.secondImageVisualEffect.isHidden = true
            self.galleryCountLabel.isHidden = true
            self.audioSectionUIViewConstraint.constant = 0
            self.audioSectionUIView.isHidden = true
            self.galleryImageStackView.isHidden = true
            
            //content image
            let image = feed.image
            if  !image.isEmpty {
                self.singleImageView.af_setImage(
                    withURL: URL(string: image)!,
                    placeholderImage: placeHolderImage,
                    imageTransition: .crossDissolve(0.2)
                )
            }else
            {
                self.singleImageView.image = placeHolderImage
            }
            
            
            
        }
            //gallery items
        else if (feed.gallery.count > 0){
            print("Gallery cell -- \(feed.gallery.count)")
                self.audioSectionUIViewConstraint.constant = 0
                self.audioSectionUIView.isHidden = true
                self.mediaSectionUIView.isHidden = false
                self.audioSectionUIView.isHidden = true
                self.audioSectionUIViewConstraint.constant = 0
                self.mediaSectionUIViewConstraint.constant = 300
                self.galleryImageStackView.isHidden = false
                //gallery
                let galleryCount = feed.gallery.count
                if galleryCount == 1 {
                    self.secondImageVisualEffect.isHidden = true
                    self.firstImageView.isHidden = true
                    self.galleryCountLabel.isHidden  = true
                    self.secondImageView.isHidden = true
                    let image = feed.gallery[0].photoUrl
                    if  !image.isEmpty {
                        self.singleImageView.af_setImage(
                            withURL: URL(string: image)!,
                            placeholderImage: UIImage(named: "default"),
                            imageTransition: .crossDissolve(0.2)
                        )
                    }else{
                        self.singleImageView.image = UIImage(named: "default")
                    }
                    
                } else if (galleryCount >= 2){
                    self.singleImageView.isHidden = true
                    self.singleImageUIVisualEffect.isHidden = true
                    self.videoPlayImageView.isHidden = true
                    self.firstImageView.isHidden = false
                    self.secondImageView.isHidden = false
                    self.galleryCountLabel.isHidden = false
                    self.secondImageVisualEffect.isHidden = false
                    if galleryCount == 2 {
                      self.galleryCountLabel.isHidden = true
                      self.secondImageVisualEffect.isHidden = true
                    }
                    self.galleryCountLabel.text = "\(galleryCount - 2)  +"
                    //gallery image
                    let imageOne = feed.gallery[0].photoUrl
                    let imageTwo = feed.gallery[1].photoUrl
                    
                    //first image
                    if  !imageOne.isEmpty {
                        self.firstImageView.af_setImage(
                            withURL: URL(string: imageOne)!,
                            placeholderImage: UIImage(named: "default"),
                            imageTransition: .crossDissolve(0.2)
                        )
                    }else
                    {
                        self.firstImageView.image = UIImage(named: "default")
                    }
                    
                    //second image
                    if  !imageTwo.isEmpty {
                        self.secondImageView.af_setImage(
                            withURL: URL(string: imageTwo)!,
                            placeholderImage: UIImage(named: "default"),
                            imageTransition: .crossDissolve(0.2)
                        )
                    }else
                    {
                        self.secondImageView.image = UIImage(named: "default")
                    }
            }
   
        }else
            //no image oooo
        {
            print("No image cell -- \(feed.mediaType) \(feed.type)")
            self.videoPlayImageView.isHidden = true
            self.mediaSectionUIViewConstraint.constant = 0
            self.mediaSectionUIView.isHidden = true
            self.firstImageView.isHidden = true
            self.singleImageView.isHidden = true
            self.singleImageUIVisualEffect.isHidden = true
            self.secondImageView.isHidden = true
            self.secondImageVisualEffect.isHidden = true
            self.galleryCountLabel.isHidden = true
            self.audioSectionUIView.isHidden = true
            self.audioSectionUIViewConstraint.constant = 0
            
        }
        
        if feed.hasAmened {
            //self.amenButton.setTitleColor(Mics.hexStringToUIColor(hex: AllKeys.primaryHexCode), for: .normal)
            self.amenButton.backgroundImage(for: UIControlState.normal)?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
            self.amenButton.tintColor = UIColor().HexToColor(hexString: LabelYellowColor)
            self.amenButton.setTitleColor(UIColor().HexToColor(hexString: nameLabelColor), for: UIControlState.normal)
            //self.amenButton.isEnabled = false
        }
       
    }
    
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        self.userImageView.af_cancelImageRequest()
        self.userImageView.layer.removeAllAnimations()
        self.userImageView.image = nil
        
        self.firstImageView.af_cancelImageRequest()
        self.firstImageView.layer.removeAllAnimations()
        self.firstImageView.image = nil
        
        self.secondImageView.af_cancelImageRequest()
        self.secondImageView.layer.removeAllAnimations()
        self.secondImageView.image = nil
        
        
        self.singleImageView.af_cancelImageRequest()
        self.singleImageView.layer.removeAllAnimations()
        self.singleImageView.image = nil
    }
}

