//
//  suggesterCell.swift
//  asoriba
//
//  Created by Benjamin Acquah on 8/6/17.
//  Copyright © 2017 Asoriba. All rights reserved.
//

import UIKit
import LBTAComponents

class suggesterCell: UICollectionViewCell{
    
    let userImage : UIImageView = {
        let ava = UIImageView()
        ava.image = UIImage(named: "")
        ava.translatesAutoresizingMaskIntoConstraints = false
        ava.backgroundColor = .black
        ava.layer.cornerRadius = 30
        ava.layer.masksToBounds = true
        return ava
    }()
    
    let usernameLbl : UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.text = "User Name"
        lbl.font = UIFont.boldSystemFont(ofSize: 15)
        lbl.textColor = UIColor.darkGray
        return lbl
    }()
    
    let churchLbl : UILabel = {
        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.text = "church"
        lbl.font = UIFont.boldSystemFont(ofSize: 12)
        lbl.textColor = UIColor.darkGray
        return lbl
    }()
    
    let actionBtn : UIButton = {
        let btn = UIButton(type: UIButtonType.roundedRect)
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.setTitle("Follow", for: UIControlState.normal)
        btn.setTitleColor(UIColor.red, for: UIControlState.normal)
        btn.backgroundColor = .white
        btn.layer.borderWidth = 2
        btn.layer.cornerRadius = 2
        btn.layer.borderColor = UIColor.red.cgColor
        return btn
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = UIColor.white
        
        self.layer.shadowColor = UIColor.lightGray.cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        self.layer.shadowRadius = 2.0
        self.layer.shadowOpacity = 0.5
        self.layer.masksToBounds = false
        self.layer.shadowPath = UIBezierPath(roundedRect: self.bounds, cornerRadius: self.contentView.layer.cornerRadius).cgPath
        
        setupViews()
    }
    
    func setupViews(){
        
        addSubview(userImage)
        addSubview(usernameLbl)
        addSubview(churchLbl)
        addSubview(actionBtn)
        
        //x-axis
        addConstraintsWithFormat(format: "H:|-8-[v0(60)]", views: userImage)
        
        //y-axis
        addConstraintsWithFormat(format: "V:|-5-[v0(60)]-5-|", views: userImage)
        
        usernameLbl.leftAnchor.constraint(equalTo: userImage.rightAnchor, constant: 8).isActive = true
        usernameLbl.rightAnchor.constraint(equalTo: actionBtn.leftAnchor, constant: -5).isActive = true
        usernameLbl.topAnchor.constraint(equalTo: userImage.topAnchor).isActive = true
        usernameLbl.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        churchLbl.leftAnchor.constraint(equalTo: usernameLbl.leftAnchor).isActive = true
        churchLbl.rightAnchor.constraint(equalTo: usernameLbl.rightAnchor).isActive = true
        churchLbl.topAnchor.constraint(equalTo: usernameLbl.bottomAnchor).isActive = true
        churchLbl.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        actionBtn.rightAnchor.constraint(equalTo: rightAnchor, constant: -8).isActive = true
        actionBtn.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        actionBtn.heightAnchor.constraint(equalToConstant: 40).isActive = true
        actionBtn.widthAnchor.constraint(equalToConstant: 80).isActive = true
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

