//
//  SearchViewController.swift
//  asoriba
//
//  Created by Bright Ahedor on 21/12/2016.
//  Copyright © 2016 Asoriba Inc. All rights reserved.
//

import UIKit
import MediaPlayer
import Jukebox
import MobilePlayer
import Crashlytics
import Toaster
import LBTAComponents

class SearchViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout, UISearchBarDelegate,FeedServiceProtocol{
    
//    @IBOutlet weak var searchBar: UISearchBar!
    var searchActive : Bool = false
    let screenWidth = UIScreen.main.bounds.width
    var api:ApiService!
    var activityViewIndicator:UIActivityIndicatorView = UIActivityIndicatorView()
    var feedArray = [Feed]()
    var currentUser = User()
    var nextUrl = ""
    var searchUrl = ""
    var loadedPages = [String]()
    var startSearchProgress = false
    let numberOfItemsPerRow : CGFloat = 1.0
//    @IBOutlet weak var feedCollectionView: UICollectionView!
    @IBOutlet weak var noResultLabel: UILabel!
    
    lazy var feedCollectionView : UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.alwaysBounceHorizontal = true
        cv.isScrollEnabled = false
        cv.backgroundColor = UIColor.groupTableViewBackground
        cv.delegate = self
        cv.dataSource = self
        return cv
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        self.searchBar.delegate = self
//        self.noResultLabel.isHidden = true
        
        //api instance
        api = ApiService()
        self.api.feedDelegate = self
        
        
        
        //register cell
//        self.feedCollectionView.register(UINib(nibName: CellIdentifiers.feedCollectionViewCell.rawValue, bundle: nil), forCellWithReuseIdentifier: CellIdentifiers.feedCollectionViewCell.rawValue)
        
        feedCollectionView.register(FeedCollectionViewCell.self, forCellWithReuseIdentifier: CellIdentifiers.feedCollectionViewCell.rawValue)
        
        //end section here
        setUpViews()
        
        //juke declaration
        UIApplication.shared.beginReceivingRemoteControlEvents()
        
        NotificationCenter.default.addObserver(self, selector: #selector(SearchViewController.receivedNotification(notification:)), name: Notification.Name(AllKeys.filterNotification), object: nil)
    }
    
    
    
    func receivedNotification(notification: Notification){
        let url = notification.object as! String
        //Take Action on Notification
        getFilterData(url:url)
    }
    
//    //setup the menu items for current user
//    func setMenuItems() {
//        let searchBarItem = UIBarButtonItem.init(title: "Filter", style: .plain, target: self, action: #selector(tapSearchFilter))
//        self.navigationItem.rightBarButtonItem = searchBarItem
//
//    }
    
    
    //MARK: - verse picker
    func tapSearchFilter()
    {
        let popOverVC = UIStoryboard(name: AllKeys.micsStoryBoard, bundle: nil).instantiateViewController(withIdentifier: AllKeys.searchFilterViewController) as! SearchFilterViewController
        self.addChildViewController(popOverVC)
        popOverVC.view.frame = self.view.frame
        self.view.addSubview(popOverVC.view)
        popOverVC.didMove(toParentViewController: self)
        
    }
    
    
    func startProgressBar()  {
        activityViewIndicator.center = self.view.center
        activityViewIndicator.hidesWhenStopped = true
        activityViewIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray
        self.view.addSubview(activityViewIndicator)
        activityViewIndicator.startAnimating()
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchActive = true
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        searchActive = false
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        searchActive = false
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if (searchText.characters.count > 3) {
            self.getData(page: "?page=1", text: searchText)
            self.startProgressBar()
            self.startSearchProgress = true
        }
    }
    
    func getFilterData(page: String,type:String,category:String,multimedia:String)  {
        feedArray.removeAll()
        loadedPages.append(page)
        self.searchUrl = "users/get_content/\(page)&page_size=10&feed_type=\(type)&multimedia_type=\(multimedia)&content_category=\(category)&keyword="
        self.api.getFeed(url: self.searchUrl)
        self.startProgressBar()
    }
    
    func getFilterData(url:String)  {
        feedArray.removeAll()
        loadedPages.append(url)
        self.searchUrl = url
        self.api.getFeed(url: self.searchUrl)
        self.startProgressBar()
    }
    
    //make call
    func getData(page: String,text:String)  {
        loadedPages.append(page)
        self.api.getFeed(url: "users/get_content/\(page)&page_size=10&feed_type=all&multimedia_type=all&content_category=all&keyword=\(text)")
    }
    
    //make call
    func getMoreData(url: String)  {
        let firstRange = url.characters.index(of: "?")
        let secondRange = url.characters.index(of: "&")
        let newUrl = self.searchUrl.replacingCharacters(in: firstRange!..<secondRange!, with: self.nextUrl)
        loadedPages.append(url)
        self.api.getFeed(url: newUrl)
    }
    
    //MARK:- collection view delegate
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return feedArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let feed = self.feedArray[indexPath.row]
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CellIdentifiers.feedCollectionViewCell.rawValue, for: indexPath) as! FeedCollectionViewCell
        cell.backgroundColor = UIColor.white
        cell.feed = feed
        
        cell.isDetail = false
        
        cell.amenButton.tag = indexPath.row
        cell.amenButton.addTarget(self, action: #selector(self.amenButton(_:)), for: .touchUpInside)
        cell.shareButton.tag = indexPath.row
        cell.shareButton.addTarget(self, action: #selector(self.shareButton(_:)), for: .touchUpInside)
        cell.commentButton.tag = indexPath.row
        cell.commentButton.addTarget(self, action: #selector(self.startComment(_:)), for: .touchUpInside)
        
        cell.audioPlayButton.tag = indexPath.row
        cell.audioPlayButton.addTarget(self, action: #selector(self.playAudioCommand(_:)), for: .touchUpInside)
        
        
        //gesture section
        cell.feedContentLabel.isUserInteractionEnabled = true
        cell.feedContentLabel.tag = indexPath.row
        let tapped = UITapGestureRecognizer(target: self, action: #selector(self.tappedContent(_:)))
        tapped.cancelsTouchesInView = false
        tapped.numberOfTapsRequired = 1
        cell.feedContentLabel.addGestureRecognizer(tapped)
        //end of gesture
        
        //gesture account
        let tappedUser = UITapGestureRecognizer(target: self, action: #selector(self.tappedUserLabel(_:)))
        cell.userNameLabel!.isUserInteractionEnabled = true
        cell.userNameLabel!.tag = indexPath.row
        tappedUser.cancelsTouchesInView = false
        tappedUser.numberOfTapsRequired = 1
        cell.userNameLabel!.addGestureRecognizer(tappedUser)
        //end user gesture
        
        //gesture account
        let tappedUserImage = UITapGestureRecognizer(target: self, action: #selector(self.tappedUserLabel(_:)))
        cell.userImageView!.isUserInteractionEnabled = true
        cell.userImageView!.tag = indexPath.row
        tappedUserImage.cancelsTouchesInView = false
        tappedUserImage.numberOfTapsRequired = 1
        cell.userImageView!.addGestureRecognizer(tappedUserImage)
        //end user gesture
        
        //gesture section ameners
        cell.amenCountLabel!.isUserInteractionEnabled = true
        cell.amenCountLabel!.tag = indexPath.row
        let tappedAmenCount = UITapGestureRecognizer(target: self, action: #selector(self.tapPerfomers(_:)))
        tappedAmenCount.cancelsTouchesInView = false
        tappedAmenCount.numberOfTapsRequired = 1
        cell.amenCountLabel!.addGestureRecognizer(tappedAmenCount)
        //end of gesture
        
        //gesture section sharers
        cell.shareCountLabel!.isUserInteractionEnabled = true
        cell.shareCountLabel!.tag = indexPath.row
        let tappedShareCount = UITapGestureRecognizer(target: self, action: #selector(self.tapSharePerfomers(_:)))
        tappedShareCount.cancelsTouchesInView = false
        tappedShareCount.numberOfTapsRequired = 1
        cell.shareCountLabel!.addGestureRecognizer(tappedShareCount)
        //end of ameners gesture
        
        
        //gesture section sharers
        cell.mediaSectionUIView!.isUserInteractionEnabled = true
        cell.mediaSectionUIView!.tag = indexPath.row
        let tappedMedia = UITapGestureRecognizer(target: self, action: #selector(self.tappedMediaSection(_:)))
        tappedMedia.cancelsTouchesInView = false
        tappedMedia.numberOfTapsRequired = 1
        cell.mediaSectionUIView!.addGestureRecognizer(tappedMedia)
        //end of ameners gesture
        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, shouldHighlightItemAt indexPath: IndexPath) -> Bool {
        return false
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return CGFloat(AllKeys.cellSpacing)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return CGFloat(AllKeys.cellSpacing)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: CGFloat(AllKeys.cellSpacing), left: 0.0, bottom: 0.0, right: 0.0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
        let contentWidth = collectionView.bounds.width
        let totalSpace = flowLayout.sectionInset.left
            + flowLayout.sectionInset.right
            + (flowLayout.minimumInteritemSpacing * CGFloat(numberOfItemsPerRow - 1))
        let size = Int((contentWidth - totalSpace) / CGFloat(numberOfItemsPerRow))
        let feed = self.feedArray[indexPath.row]
        return ConfigureCell.configureFeedCell(feed:feed,size:size,contentWidth:contentWidth,lines:5)
        
    }
    
    
    //MARK: set performers gesture
    func tapPerfomers(_ sender: UITapGestureRecognizer)
    {
        let feed = feedArray[(sender.view?.tag)!]
        ControllerHelpers.startPerformers(targetVC: self, feedId: feed.id,isAmeners: true)
    }
    
    //MARK: set performers gesture
    func tapSharePerfomers(_ sender: UITapGestureRecognizer)
    {
        let feed = feedArray[(sender.view?.tag)!]
        ControllerHelpers.startPerformers(targetVC: self, feedId: feed.id,isAmeners: false)
    }
    
    //send the amen signal
    @IBAction func amenButton(_ sender: UIButton) {
        let feed = self.feedArray[sender.tag]
        feed.hasAmened = true
        feed.numberOfAmens = feed.numberOfAmens + 1
        self.feedCollectionView.reloadData()
        self.api.sayAmen(item: feed)
    }
    
    //send the share signal
    @IBAction func shareButton(_ sender: UIButton) {
        let feed = self.feedArray[sender.tag]
        ControllerHelpers.presentSharer(targetVC: self, feed: feed)
    }
    
    //start comment here
    @IBAction func startComment(_ sender: UIButton) {
        let feed = self.feedArray[sender.tag]
        self.performSegue(withIdentifier: AllKeys.feeddetailSegue, sender: feed )
    }
    
    
    //MARK: -- Handling gestures
    func tappedMediaSection(_ sender: UITapGestureRecognizer)
    {
        let currentFeed = feedArray[(sender.view?.tag)!]
        if (currentFeed.mediaType == "video") {
            ControllerHelpers.processVideoPlayer(targetVC: self, feed: currentFeed)
        }else  {
            ControllerHelpers.presentImage(targetVC: self, feed: currentFeed)
        }
    }
    
  
    
    //MARK: -- Handling gestures
    //MARK: set image gesture
    func tappedImage(_ sender: UITapGestureRecognizer)
    {
        let feed = feedArray[(sender.view?.tag)!]
        self.presentImage(feed: feed)
        
    }
    
    
    //MARK: now to user
    func tappedUserLabel(_ sender: UITapGestureRecognizer)
    {
        let feed = feedArray[(sender.view?.tag)!]
        
        //is user
        if (feed.member?.isMobile)!{
            let vc = UIStoryboard(name:AllKeys.mainStoryBoard, bundle:nil).instantiateViewController(withIdentifier: AllKeys.userProfileController) as! ProfileViewController
            let member = feed.member
            vc.user = member!
            self.navigationController?.pushViewController(vc, animated: true)
            //is church not user
        } else {
            let member = feed.member
            let church = Church()
            church.id = (member?.id)!
            church.name = (member?.firstName)!
            church.branchName = (member?.lastName)!
            church.churchLogo = (member?.avatar)!
            
            let churchController = UIStoryboard(name:AllKeys.mainStoryBoard, bundle:nil).instantiateViewController(withIdentifier: AllKeys.churchProfileController) as! ChurchProfileController
            churchController.isMyChurch = false
            churchController.churchItem = church
            
            self.navigationController?.pushViewController(churchController, animated: true)
            
        }
        
    }
    
    
    //MARK: start detail here
    func tappedContent(_ sender: UITapGestureRecognizer)
    {
        let feed = feedArray[(sender.view?.tag)!]
        ControllerHelpers.startDetail(targetVC: self,feed:feed)
    }
    
    //MARK: set image gesture
    func startVideo(_ sender: UITapGestureRecognizer)
    {
        let feed = feedArray[(sender.view?.tag)!]
        ControllerHelpers.processVideoPlayer(targetVC: self, feed: feed)
    }
    
    
    func presentImage(feed:Feed)  {
         ControllerHelpers.presentImage(targetVC: self, feed: feed)
    }
    
    
    func didReceiveSuccess(results: [Feed]) {
        self.activityViewIndicator.stopAnimating()
        if self.startSearchProgress {
           self.feedArray.removeAll()
        }
        self.feedArray.append(contentsOf: results)
        self.feedCollectionView.reloadData()
        if (self.feedArray.isEmpty){
            self.noResultLabel.isHidden = false
        } else {
            self.noResultLabel.isHidden = true
        }
        self.startSearchProgress = false
    }
    
    func didReceiveNextUrl(results: String) {
        self.nextUrl = results
    }
 
    func didReceiveDetail(results: String) {
        self.activityViewIndicator.stopAnimating()
    }
    
    func didReceiveError(results: String) {
        self.activityViewIndicator.stopAnimating()
    }
    
    //play audio here
    @IBAction func playAudioCommand(_ sender: UIButton) {
        let feed = feedArray[sender.tag]
        self.startAudioDialog(feed: feed)
    }
    
    func startAudioDialog(feed:Feed)  {
        let popOverVC = UIStoryboard(name: AllKeys.mainStoryBoard, bundle: nil).instantiateViewController(withIdentifier: AllKeys.audioControllerViewController) as! AudioControllerViewController
        popOverVC.feed = feed
        self.addChildViewController(popOverVC)
        popOverVC.view.frame = self.view.frame
        self.view.addSubview(popOverVC.view)
        popOverVC.didMove(toParentViewController: self)
    }
    
    
    //MARK: - scroll handling section
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let currentOffset = scrollView.contentOffset.y
        let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height
        
        if (maximumOffset - currentOffset) <= 400 {
            if !(self.nextUrl.isEmpty) && !self.loadedPages.contains(self.nextUrl) {
                self.getMoreData(url: self.searchUrl)
            }
        }
    }
    
    
    func setUpViews(){
        
        print("search view controller is fetched")
        
        view.addSubview(feedCollectionView)
        
        feedCollectionView.anchor(view.topAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
    }

}
