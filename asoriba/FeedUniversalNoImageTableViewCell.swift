//
//  FeedUniversalNoImageTableViewCell.swift
//  asoriba
//
//  Created by Bright Ahedor on 05/06/2017.
//  Copyright © 2017 Asoriba Inc. All rights reserved.
//

import UIKit

class FeedUniversalNoImageTableViewCell: UITableViewCell {
    
    @IBOutlet weak var contentLabel: UILabel!
    @IBOutlet weak var starsLabel: UILabel!

    
    var feed: Feed! {
        didSet{
            self.updateUI()
        }
    }
    
    
    func updateUI() {
        
        self.layer.shadowColor = UIColor.lightGray.cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        self.layer.shadowRadius = 2.0
        self.layer.shadowOpacity = 1.0
        self.layer.masksToBounds = false
        self.layer.shadowPath = UIBezierPath(roundedRect: self.bounds, cornerRadius: self.contentView.layer.cornerRadius).cgPath

        //setting counts
        let amen = feed.numberOfAmens
        let amenShowable = amen == 1 ? "\(amen) amen " : "\(amen) amens "
        
        let shares = feed.numberOfShares
        let sharesShowable = shares == 1 ? "\(shares) share " : "\(shares) shares "
        
        
        let comment = feed.numberOfComments
        let commentsShowable = comment == 1 ? "\(comment) comment " : "\(comment) comments "
        
        
        let views = feed.numberOfViews
        let viewsShowable = views == 1 ? "\(views) view" : "\(views) views"
        //end of counts
        
        self.starsLabel.text = amenShowable + sharesShowable + commentsShowable + viewsShowable
        
        var message = ""
        if (feed.title != "" && feed.content == ""){
            message = feed.title
        }else if (feed.title != "" && feed.content != ""){
            message = feed.title + "\n" + feed.content
        }else{
            message = feed.content
        }
        
        self.contentLabel.text = message
        
    }
}
