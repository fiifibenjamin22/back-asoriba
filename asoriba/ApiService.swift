//
//  ApiService.swift
//  asoriba
//
//  Created by Bright Ahedor on 15/12/2016.
//  Copyright © 2016 Asoriba Inc. All rights reserved.
//


import Alamofire
import SwiftyJSON
import RealmSwift
import FirebaseMessaging
import Firebase
import MRProgress

import Crashlytics
import Fabric
import UIKit

class ApiService : NSObject{
    var feedDelegate:FeedServiceProtocol?
    var accountDelegate:RegisterServiceProtocol?
    var churchMembersDelegate:ChurchMembersServiceProtocol?
    
    //groups
    var churchGroupsDelegate:GroupsServiceProtocol?
    //var fetchUsersDelegate: UsersfetchProtocol?
    
    var churchDelegate:ChurchServiceProtocol?
    var joinChurchDelegate:JoinChurchServiceProtocol?
    var contentDelegate:ContentServiceProtocol?
    var donationDelegate:DonationServiceProtocol?
    var numberAccountdelegate: RegisterWithNumberServiceProtocol?
    var paymentDelagate:PaymentServiceProtocol?
    var updateUserDelagate:UpdateUserServiceProtocol?
    var commentsDelegate:CommentServiceProtocol?
    var aboutDelegate:AboutServiceProtocol?
    var postServiceProtocol:PostServiceProtocol?
    var followingServiceDelegate:FollowingServiceProtocol?
    var paymentHistoryDelegate:PaymentHistoryServiceProtocol?
    var checkInServiceProtocol:CheckInServiceProtocol?
    let headers = ["X-ASORIBA-APP-ID": Variables.appId(),"X-ASORIBA-APP-SECRET-KEY":Variables.secretKey(),"X-ASORIBA-APP-ORIGIN":Mics.getCountryCode(),"X-ASORIBA-APP-VERSION":Mics.getAppVersion(),"X-ASORIBA-CUSTOM-APP-ID": Variables.icgcAppId()]
    
    
    override init(){}
    
    //MARK: - set dynamic headers
    func getHeaders() -> [String: String]  {
        let token = DataOperation.getUser().token
        let headers = ["X-ASORIBA-APP-ID": Variables.appId(),"X-ASORIBA-APP-SECRET-KEY":Variables.secretKey(),"X-ASORIBA-APP-ORIGIN":Mics.getCountryCode(),"X-ASORIBA-APP-VERSION":Mics.getAppVersion(),"Authorization":"Mobile " + token,"X-ASORIBA-CUSTOM-APP-ID": Variables.icgcAppId()]
        print("print users token: \(token)")
        return headers
    }
    
    
    //MARK: - get a single content
    func getSingleContent(url:String)  {
      
        let realUrl = "\(Variables.baseUrl())\(url)"
        
        Alamofire.request(realUrl, method: .get, parameters: nil, encoding: JSONEncoding.default,headers: getHeaders())
            .responseJSON { response in
                if let status = response.response?.statusCode {
                    print("Detail \(status)")
                    switch(status){
                    case 200...201:
                        let rawData = response.data!
                        let item = JSON(data: rawData)
                        let feed = self.getFeedItem(item: item["results"])
                        self.contentDelegate?.didReceiveContent(result: feed)
                        break
                    case 202...401:
                        let items = JSON(data: response.data!)
                        let detail =  items["details"].stringValue
                        self.contentDelegate?.didReceiveContentError(results: detail)
                        break
                    default:
                        self.contentDelegate?.didReceiveContentError(results: AllKeys.failureMessage)
                    }
                }
                
        }
    }
    //MARK: - get createAccount
    func createAccount(email: String, firstName: String, lastName: String,socialAvatar: String){
        
        let realUrl = "\(Variables.baseUrl())users/signup/"
        let params = ["first_name": firstName,"last_name":lastName,"email":email,"social_avatar":socialAvatar]
    
        Alamofire.request(realUrl, method: .post, parameters: params, encoding: JSONEncoding.default,headers: headers)
            .responseJSON { response in
                if let status = response.response?.statusCode {
                    switch(status){
                    case 200...201:
                        self.parseAccount(rawData: response.data!,isPhoneNumberType: false,isUpadeStrings: false,isUpdateImage: false)
                        
                        print("print: ", response.response as Any)
                        break
                    default:
                        self.accountDelegate?.didReceiveAccountError(results: AllKeys.failureMessage)
                    }
                }
                
        }
    }
    
    //MARK: - verify account
    func verifyAccount(authCode: String){
        
        let realUrl = "\(Variables.baseUrl())users/signup/"
        let params = ["first_name": "","last_name":"","email":"","social_avatar":"","auth_code":authCode]
        
        Alamofire.request(realUrl, method: .post, parameters: params, encoding: JSONEncoding.default,headers: headers)
            .responseJSON { response in
                if let status = response.response?.statusCode {
                    print("Status \(status)")
                    switch(status){
                    case 200...201:
                         self.parseAccount(rawData: response.data!, isPhoneNumberType: true,isUpadeStrings: false,isUpdateImage: false)
                        break
                    case 202...401:
                        let items = JSON(data: response.data!)
                        let error =  items["details"].stringValue
                        self.numberAccountdelegate?.didReceiveNumberError(result: error)
                        break
                    default:
                        self.numberAccountdelegate?.didReceiveNumberError(result: AllKeys.failureMessage)
                    }
                }
                
        }
    }
    
    
    
    //MARK: - get churches
    func getChurches(url:String){
        
        let realUrl = "\(Variables.baseUrl())\(url)"
        print("print church: \(realUrl)")
        
        Alamofire.request(realUrl, method: .get, parameters: nil, encoding: JSONEncoding.default,headers: getHeaders())
            .responseJSON { response in
                if let status = response.response?.statusCode {
                    switch(status){
                    case 200...201:
                        self.parseChurch(rawData: response.data!)
                        break
                    default:
                        self.churchDelegate?.didReceiveChurchError(results: AllKeys.failureMessage)
                    }
                }
                
        }
    }
    
    //MARK: - search churches
    func searchChurches(url:String){
        
        let realUrl = "\(Variables.baseUrl())\(url)"
        
        Alamofire.request(realUrl, method: .get, parameters: nil, encoding: JSONEncoding.default,headers: getHeaders())
            .responseJSON { response in
                if let status = response.response?.statusCode {
                    switch(status){
                    case 200...201:
                        self.parseChurch(rawData: response.data!)
                        break
                    default:
                        self.churchDelegate?.didReceiveChurchError(results: AllKeys.failureMessage)
                    }
                }
                
        }
    }
    
    
    
    //MARK: - passing church item here
    func parseChurchItem(item: JSON) -> Church {
        let churchData = Church()
        let id = String(item["id"].intValue)
        let numberOfFollowers = item["followers_count"].intValue
        let numberOfFollowings = item["following_count"].intValue
        let branchName = item["branch_name"].stringValue
        let isMainBranch = item["is_main_church"].boolValue
        _ = item["logo"].stringValue
        let isFollowing = item["is_following"].boolValue
        
        if let church = item["church"].dictionary {
            let backdrop = church["backdrop"]?.stringValue
            let churchName = church["church_name"]?.stringValue
            let logo = church["logo"]?.stringValue
   
            churchData.id = id
            churchData.branchName = branchName
                        
            churchData.churchLogo = logo!
            churchData.isMainBranch = isMainBranch
            churchData.image = backdrop!
            churchData.isFollowing = isFollowing
            churchData.name = churchName!
            churchData.numberOfFollowers = numberOfFollowers
        }
        
        return churchData
        
    }
    
    
    
    //MARK: - passing church here
    func parseChurch(rawData: Data) {
        let items = JSON(data: rawData)
        let results = items["results"].dictionaryValue
        print("print church: \(results)")
        
        if let next = results["next"]?.string {
            self.churchDelegate?.didReceiveChurchNextUrl(results: next)
        }
        if let detail = results["details"]?.string {
            self.churchDelegate?.didReceiveChurchDetail(results: detail)
        }
        var feeds = [Church]()
        if let result = results["results"]?.array {
            for item in result {
                let churchItem = self.parseChurchItem(item: item)
                feeds.append(churchItem)
            }
        }
        self.churchDelegate?.didReceiveChurchSuccess(results: feeds)
     }
    
    
    //MARK: - follow church
    func followChurch(church:Church,isJoining:Bool){
        let realUrl = "\(Variables.baseUrl())users/actions/follow/"
        let params = ["branch_id": church.id,"is_main_church":isJoining] as [String : Any]
        
        
        Alamofire.request(realUrl, method: .post, parameters: params, encoding: JSONEncoding.default,headers: getHeaders())
            .responseJSON { response in
                if let status = response.response?.statusCode {
                    switch(status){
                    case 200...201:
                        if (!isJoining){break}
                         Messaging.messaging().unsubscribe(fromTopic: "branch-\(church)")
                        self.persistChurch(rawData: response.data!,church:church)
                        break
                    default:
                        self.joinChurchDelegate?.didReceiveJoinChurchError(results: AllKeys.failureMessage)
                        break
                    }
                }
        }
    }
    
    //MARK: - unfollow church
    func unFollowChurch(church:Church,isJoining:Bool){
        let realUrl = "\(Variables.baseUrl())users/actions/unfollow/"
        let params = ["branch_id": church.id,"is_main_church":isJoining] as [String : Any]
        
        Alamofire.request(realUrl, method: .post, parameters: params, encoding: JSONEncoding.default,headers: getHeaders())
            .responseJSON { response in
                if let status = response.response?.statusCode {
                    switch(status){
                    case 200...201:
                        if (!isJoining){break}
                        self.persistChurch(rawData: response.data!,church:church)
                        break
                    default:
                        self.joinChurchDelegate?.didReceiveJoinChurchError(results: AllKeys.failureMessage)
                        break
                    }
                }
                
        }
    }
    
    //MARK: - follow user
    func followMeber(userId:String){
        let realUrl = "\(Variables.baseUrl())users/actions/follow/"
        let params = ["user_id": userId ]
        Alamofire.request(realUrl, method: .post, parameters: params, encoding: JSONEncoding.default,headers: getHeaders())
            .responseJSON { response in
                if let status = response.response?.statusCode {
                    switch(status){
                    case 200...201:
                        let items = JSON(data: response.data!)
                        print("Print follow member action: \(items)")
                        break
                    default:
                        print("print follow member action: ", response.result as Any)
                        break
                    }
                }
        }
    }
    
    //MARK: - unfollow user
    func unFollowMeber(userId:String){
        let realUrl = "\(Variables.baseUrl())users/actions/unfollow/"
        let params = ["user_id": userId ]
  
        Alamofire.request(realUrl, method: .put, parameters: params, encoding: JSONEncoding.default,headers: getHeaders())
            .responseJSON { response in
                if let status = response.response?.statusCode {
                    print("Status \(status)")
                    switch(status){
                    case 200...201:
                        let items = JSON(data: response.data!)
                        print("Print unfollow member action: \(items)")
                        break
                    default:
                        print("print follow member action: ", response.result)

                        break
                    }
                }
        }
    }
    
    //MARK: - save and update user church
    func persistChurch(rawData: Data,church:Church) {
        let items = JSON(data: rawData)
        if  let data = items["results"]["status_code"].int {
            if (data == 2101){
                let userId = DataOperation.getUser().id
                DataOperation.updateUserChurch(id:userId,churchId:church.id,churchName:church.branchName,churchLogo:church.churchLogo,backDrop:church.image)
                self.joinChurchDelegate?.didReceiveJoinChurchSuccess(results: church)
            }else {
             self.joinChurchDelegate?.didReceiveJoinChurchError(results: AllKeys.failureMessage)
            }
        }else{
        self.joinChurchDelegate?.didReceiveJoinChurchError(results: AllKeys.failureMessage)
        }
        
    }
    

    //MARK: - get action performers
    func getActionPerfomers(contentId:String,type:String,next:String){
        
        let realUrl = "\(Variables.baseUrl())users/actions/\(contentId)/performers/?engagement_type=\(type)&\(next)&page_size=20"
       
        Alamofire.request(realUrl, method: .get, parameters: nil, encoding: JSONEncoding.default,headers: getHeaders())
            .responseJSON { response in
                if let status = response.response?.statusCode {
                    print("Status \(status)")
                    switch(status){
                    case 200...201:
                        self.parseChurchMembers(rawData: response.data!)
                        break
                    default:
                        self.churchMembersDelegate?.didReceiveChurchMembersError(results: AllKeys.failureMessage)
                    }
                }
                
        }
    }
    
    //MARK: - get members  followings
    func getMemberFollowers(userId:String,next:String){
        let realUrl = "\(Variables.baseUrl())users/actions/\(userId)/followers/?category=users&\(next)&page_size=20"
   
        Alamofire.request(realUrl, method: .get, parameters: nil, encoding: JSONEncoding.default,headers: getHeaders())
            .responseJSON { response in
                if let status = response.response?.statusCode {
                    print("Status \(status)")
                    switch(status){
                    case 200...201:
                        self.parseChurchMembers(rawData: response.data!)
                        break
                    default:
                        self.churchMembersDelegate?.didReceiveChurchMembersError(results: AllKeys.failureMessage)
                    }
                }
                
        }
    }
    
    //MARK: - get members  followings
    func getMemberFollowings(userId:String,next:String){
        
        let realUrl = "\(Variables.baseUrl())users/actions/\(userId)/following/?category=users&\(next)&page_size=20"
        
        Alamofire.request(realUrl, method: .get, parameters: nil, encoding: JSONEncoding.default,headers: getHeaders())
            .responseJSON { response in
                if let status = response.response?.statusCode {
                    
                    print("Status \(status)")
                    print("print \(realUrl)")
                    switch(status){
                    case 200...201:
                        self.parseChurchMembersFollowings(rawData: response.data!)
                        break
                    default:
                        self.followingServiceDelegate?.didReceiveFollowingError(results: AllKeys.failureMessage)
                    }
                }
                
        }
    }
    
    //MARK: - get members groups followings
    func getMemberGroupsFollowing(userId:String,next:String){
        
        let realUrl = "\(Variables.baseUrl())users/actions/\(userId)/following/?category=branches&\(next)&page_size=10"
   
        Alamofire.request(realUrl, method: .get, parameters: nil, encoding: JSONEncoding.default,headers: getHeaders())
            .responseJSON { response in
                if let status = response.response?.statusCode {
                    print("Status \(status)")
                    switch(status){
                    case 200...201:
                        self.parseChurchGroups(rawData: response.data!)
                        break
                    default:
                        self.churchGroupsDelegate?.didReceiveGroupsError(results: AllKeys.failureMessage)
                    }
                }
                
        }
    }
    
    
    
    //MARK: - get church members
    func getChurchMembers(branchId:String,next:String){
        
        let realUrl = "\(Variables.baseUrl())branches/\(branchId)/followers/?\(next)&page_size=25"
        Alamofire.request(realUrl, method: .get, parameters: nil, encoding: JSONEncoding.default,headers: getHeaders())
            .responseJSON { response in
                if let status = response.response?.statusCode {
                    print("Status \(status)")
                    switch(status){
                    case 200...201:
                        self.parseChurchMembers(rawData: response.data!)
                        break
                    default:
                        self.churchMembersDelegate?.didReceiveChurchMembersError(results: AllKeys.failureMessage)
                    }
                }
                
        }
    }
    
    
    //MARK: - parse members following
    func parseChurchMembersFollowings(rawData: Data) {
        let items = JSON(data: rawData)
        if let next = items["results"]["next"].string {
            self.followingServiceDelegate?.didReceiveFollowingNextUrl(results: next)
        }
        if let detail = items["results"]["detail"].string {
            self.followingServiceDelegate?.didReceiveFollowingDetail(results: detail)
        }
        
        var feeds = [Member]()
        if let results = items["results"]["results"].array {
            for item in results {
                feeds.append(self.parseChurchMemberItem(item: item))
            }
            self.followingServiceDelegate?.didReceiveFollowingSuccess(results: feeds)
        }
        
    }
    
    
    //MARK: - search Users
    func searchChurcheMembers(url:String){
        let realUrl = "\(Variables.baseUrl())\(url)"
        
        Alamofire.request(realUrl, method: .get, parameters: nil, encoding: JSONEncoding.default,headers: getHeaders())
            .responseJSON { response in
                if let status = response.response?.statusCode {
                    switch(status){
                    case 200...201:
                        self.parseChurchMembers(rawData: response.data!)
                        break
                    default:
                        self.churchMembersDelegate?.didReceiveChurchMembersError(results: AllKeys.failureMessage)
                    }
                }
                
        }
    }
    
    
    //MARK: - parse Church members
    func parseChurchMembers(rawData: Data) {
        let items = JSON(data: rawData)
        if let next = items["results"]["next"].string {
            self.churchMembersDelegate?.didReceiveChurchMemberNextUrl(results: next)
        }
        if let detail = items["results"]["detail"].string {
            self.churchMembersDelegate?.didReceiveChurchMembersDetail(results: detail)
        }
        
        var feeds = [Member]()
        //print("Print friends \(items)")
        if let results = items["results"]["results"].array {
            for item in results {
                feeds.append(self.parseChurchMemberItem(item: item))
            }
            self.churchMembersDelegate?.didReceiveChurchMembersSuccess(results: feeds)
        }
        
    }
    
    
    //MARK: - pass member item
    func parseChurchMemberItem(item: JSON) -> Member {
        let member = Member()
        let id = String(item["id"].intValue)
        let _ = item["auth_key"].stringValue
        let avatar = item["avatar"].stringValue
        let firstName = item["first_name"].stringValue
        let lastName = item["last_name"].stringValue
        let _ = item["email"].stringValue
        let _ = item["username"].stringValue
        let otherName = item["username"].stringValue
        
       
        
        let followersCount = item["followers_count"].intValue
        let followingCount = item["following_count"].intValue
        let is_following = item["is_following"].boolValue
        
        member.avatar = avatar
        member.firstName = firstName
        member.lastName = lastName
        member.id = id
        member.numberOfFollowing = followersCount
        member.numberOfFollowers = followingCount
        print("print number of followersCount: ", followersCount)
        print("print number of followingCount: ", followingCount)
        member.otherName = otherName
        member.isFollowing = is_following
        print("Is follwing \(is_following)")
        return member
        
    }
    
    
    //MARK: - get church groups
    func getChurchGroups(churchId:String,next:String){
        let realUrl = "\(Variables.baseUrl())branches/\(churchId)/?extra_fields=groups&\(next)&page_size=10"
        
        Alamofire.request(realUrl, method: .get, parameters: nil, encoding: JSONEncoding.default,headers: getHeaders())
            .responseJSON { response in
                if let status = response.response?.statusCode {
                    print("Status \(status)")
                    switch(status){
                    case 200...201:
                        self.parseChurchGroups(rawData: response.data!)
                        break
                    default:
                        self.churchGroupsDelegate?.didReceiveGroupsError(results: AllKeys.failureMessage)
                    }
                }
                
        }
    }
    
    //MARK: - parse Church Groups
    func parseChurchGroups(rawData: Data) {
        let items = JSON(data: rawData)
        if let next = items["results"]["next"].string {
            self.churchGroupsDelegate?.didReceiveChurchGroupsNextUrl(results: next)
        }
        if let detail = items["results"]["detail"].string {
            self.churchGroupsDelegate?.didReceiveGroupsDetail(results: detail)
        }
        
        var feeds = [Group]()
        if let results = items["results"]["groups"].array {
            for item in results {
                feeds.append(self.parseChurchGroupsItem(item: item))
            }
            self.churchGroupsDelegate?.didReceiveGroupsSuccess(results: feeds)
        }
    }
    
    //MARK: - pass group item
    func parseChurchGroupsItem(item: JSON) -> Group {
        let groupItem = Group()
        let id = String(item["id"].intValue)
        let _ = item["created"].stringValue
        let description = item["description"].stringValue
        let logo = item["logo"].stringValue
        let name = item["name"].stringValue
        
        groupItem.description = description
        groupItem.id = id
        groupItem.image = logo
        groupItem.name = name
        
        return groupItem
        
    }
    
    //MARK: - send groups data to server
    
    
    
    //MARK: - get feeds
    func getFeed(url:String){
            let realUrl = Variables.baseUrl()+url
            let headers = getHeaders()
            //print("Real url \(headers)")
            Alamofire.request(realUrl, method: .get, parameters: nil, encoding: JSONEncoding.default,headers: headers)
                .responseJSON { response in
                    if let status = response.response?.statusCode {
                        switch(status){
                        case 200...201:
                            self.parseFeed(rawData: response.data!)
                            break
                        default:
                            self.feedDelegate?.didReceiveError(results: AllKeys.failureMessage)
                        }
                    }
                    
            }
    }
    
    //MARK: - get donations
    func getDonations(url:String){
        
        let realUrl = Variables.baseUrl()+url
        
        
        Alamofire.request(realUrl, method: .get, parameters: nil, encoding: JSONEncoding.default,headers: getHeaders())
            .responseJSON { response in
                if let status = response.response?.statusCode {
                    switch(status){
                    case 200...401:
                        self.parseDonations(rawData: response.data!)
                        break
                    default:
                        self.donationDelegate?.didReceiveDonationError(results: AllKeys.failureMessage)
                    }
                }
                
        }
    }
    
    
    //MARK: - call for sharing
    func addImpression(item:Feed,type:String){
        
        let url = Variables.baseUrl() + "users/actions/\(item.id)/engage/?engagement_type=\(type)";
        
        Answers.logCustomEvent(withName: "content_action", customAttributes: ["app_version": Mics.getAppVersion(),"shared" : item.id])
        
        Alamofire.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default,headers: self.getHeaders())
            .responseJSON { response in
                if let status = response.response?.statusCode {
                    print("Status \(status)")
                    switch(status){
                    case 200...201:
                        let _ = response.data
                        break
                    default:
                        break
                    }
                }
                
        }
    }
    
    //MARK: - call for sharing
    func addShare(item:Feed){
        
        let url = Variables.baseUrl() + "users/actions/\(item.id)/engage/?engagement_type=share";
        
        print("print share url: ", url)
        
        Answers.logCustomEvent(withName: "content_action", customAttributes: ["app_version":Mics.getAppVersion(),"shared" : item.id])
      
        Alamofire.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default,headers: self.getHeaders())
            .responseJSON { response in
                if let status = response.response?.statusCode {
                    print("Status \(status)")
                    switch(status){
                    case 200...201:
                        let _ = response.data
                        break
                    default:
                        break
                    }
                }
                
        }
    }
    
    //MARK: - call to amen
    func sayAmen(item:Feed){
        
        Answers.logCustomEvent(withName: "content_action", customAttributes: ["app_version":Mics.getAppVersion(),"amen" : item.id])
        let url = Variables.baseUrl() + "users/actions/\(item.id)/engage/?engagement_type=amen";
     
        Alamofire.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default,headers: self.getHeaders())
            .responseJSON { response in
                if let status = response.response?.statusCode {
                    print("Status \(status)")
                    switch(status){
                    case 200...201:
                        let _ = response.data

                        break
                    default:
                        break
                    }
                }
                
        }
    }
    

    
    //MARK: - pass feed data here
    func parseFeed(rawData: Data) {
        let items = JSON(data: rawData)
        if let next = items["results"]["content"]["next"].string {
            self.feedDelegate?.didReceiveNextUrl(results: next)
        }
        if let detail = items["results"]["content"]["detail"].string {
            self.feedDelegate?.didReceiveDetail(results: detail)
        }
        var feeds = [Feed]()
        
        if let results = items["results"]["content"]["results"].array {
           // print("Called Content \(results)")
            for item in results {
                feeds.append(self.getFeedItem(item: item))
            }
            
         self.feedDelegate?.didReceiveSuccess(results: feeds)
        }
        
    }
    
    
    //MARK: - pass each feed item
    func getFeedItem(item: JSON) -> Feed {
        let feed = Feed()
        
        let id = String(item["id"].intValue)
        let date = item["modified"].stringValue
        let location = item["location"].stringValue
        var image = ""
        
        let title = item["title"].stringValue
        let content = item["message"].stringValue
        let mediaType = item["media_type"].stringValue
        let mediaUrl = item["media_url"].stringValue
        let mediaFile = item["media_file"].stringValue
        let mediaThumbnail = item["media_thumbnail"].stringValue
    
        //counts
        let numberOfComments = item["no_of_comments"].intValue
        let numberOfShares = item["shares"].intValue
        let numberOfAmens = item["amens"].intValue
        let numberOfView = item["impressions"].intValue
        
        var photoArray = List<Gallery>()
        if let photos = item["gallery"].array {
        for photo in photos {
            let photoItem = photo["photo_url"].stringValue
                let galleryItem = Gallery()
                galleryItem.photoUrl = photoItem
                photoArray.append(galleryItem)
            }
            if (photoArray.count == 1){
                image =  photoArray[0].photoUrl
                photoArray = List<Gallery>()
            }
        }
    
        let hasAmen = item["has_amened"].boolValue
        let type = item["type"].stringValue
    
        let member = Member()
    
        if let user = item["user"].dictionary {
            let userId = String(describing: user["id"]?.intValue)
            let firstName = user["first_name"]?.stringValue
            let lastName = user["last_name"]?.stringValue
            //let number = item["phone_no"].stringValue
            //let email = item["email"].stringValue
            let avatar = user["avatar"]?.stringValue
    
            member.avatar = avatar!
            member.firstName = firstName!
            member.id = userId
            member.lastName = lastName!
            member.isMobile = true
    
        } else {
    
            let branchId =  String(item["branch_id"].intValue)
            
            //for ICGC Only
            let branchName = item["branch_name"].stringValue
            
            //for asoriba only
            //let branchName = item["church_name"].stringValue
            let avatar = item["church_thumbnail_80x80"].stringValue
            
            print("print branch Name: ", branchName)
            
            
            member.avatar = avatar
            member.firstName = branchName
            member.id = branchId
            member.lastName = branchName
            member.isMobile = false
    
            //churchTitle = member.firstName
        }
        
        if let quotations = item["quotations"].array {
            for quotation in quotations {
                let book = quotation["book"].stringValue
                let chapter = quotation["chapter"].intValue
                let verse = quotation["verse"].stringValue
                let quote = Quotation()
                quote.book = book
                quote.chapter = String(chapter)
                quote.verse = verse
                feed.quotation = quote
            }
        }
        
    
   
        feed.id = id
        feed.title = title
        feed.content = content
        feed.date = date
        feed.image = image
        feed.location = location
        feed.numberOfComments = numberOfComments;
        feed.numberOfAmens = numberOfAmens;
        feed.numberOfViews = numberOfView;
        feed.numberOfShares = numberOfShares
        feed.mediaType = mediaType;
        feed.mediaFile = mediaFile;
        feed.medialUrl = mediaUrl
        feed.mediaThumbnail = mediaThumbnail
        feed.type = type;
        feed.gallery = photoArray;
        feed.hasAmened = hasAmen;
        feed.member = member
    
        return feed
    }
    
    //MARK: - passing user
    func parseAccount(rawData: Data,isPhoneNumberType: Bool,isUpadeStrings: Bool,isUpdateImage:Bool) {
        let item = JSON(data: rawData)
        if let result = item["results"].dictionary {
            let firstName = result["first_name"]?.stringValue
            let lastName = result["last_name"]?.stringValue
            let otherName = result["username"]?.stringValue
            let email = result["email"]?.stringValue
            let authkey = result["auth_key"]?.stringValue
            let number = result["phone_no"]?.stringValue
            let userId = result["id"]?.stringValue
            let numberOfFollowing = result["following_count"]?.intValue
            let numberOfFollowers = result["followers_count"]?.intValue
            var realAvatar = ""
            let avatar = result["avatar"]?.stringValue
            if let socialAvatar = result["social_avatar"]?.string {
                if (avatar?.isEmpty)! {
                    realAvatar = socialAvatar
                }else{
                    realAvatar = avatar!
                }
            } else {
              realAvatar = avatar!
            }
            
            var dateOfBirth = ""
            if let date = result["date_of_birth"]?.string {
                dateOfBirth = date
            }
            var gender = ""
            if let sex = result["sex"]?.string {
                gender = sex
            }
            var churchId = ""
            var groupsCount = 0;
            var churchName = ""
            var churchLogo = ""
            var churchBack = ""
            var userChurchBranch = ""
            
            var phoneNumbers = [JSON]()
            var locationName = ""
            var locationsCordinate = [JSON]()

            if let branch = result["branch"]?.dictionary {
                
                print("print all from branch :", branch)
                
               churchId = String(describing: branch["id"]!.intValue)
                userChurchBranch = (branch["branch_name"]?.stringValue)!

                phoneNumbers = (branch["phone_nos"]?.arrayValue)!
                locationName = (branch["location"]?.stringValue)!
                locationsCordinate = (branch["location_coordinates"]?.arrayValue)!
                print("print phone numbers: \(phoneNumbers)\n \(locationName)\n \(locationsCordinate)")
                
                //save to firebase
                let json = "{\"phoneNumbers\":\"\(phoneNumbers)\",\"locationName\":\"\(locationName)\",\"cordinates\":\"\(locationsCordinate)\"}"
                let ref = Database.database().reference().child("churchInformation")
                let childRef = ref.childByAutoId()
                let values = json
                childRef.setValue(values)
                
                if let inGroupsCount = (branch["groups"]?.array){
                   groupsCount = inGroupsCount.count
                }
                if let church = branch["church"]?.dictionary {
                    churchName = (church["church_name"]?.stringValue)!
                    churchLogo = (church["logo"]?.stringValue)!
                    churchBack = (church["backdrop"]?.stringValue)!
                    //userChurchBranch = ""//(church["branch_name"]?.stringValue)!
                }
                
            }
            
            
            
            DataOperation.deleteUser()
            DataOperation.saveNewUser(id: userId!,token: authkey!,email: email!,number:number!,fName:firstName!,lName:lastName!,oName: otherName!,churchId:churchId,
                                      churchName:churchName,branchName:userChurchBranch,numberOfGroups:groupsCount,numberOfFollowers:numberOfFollowing!, numberOfFollowings: numberOfFollowers!,avatar:realAvatar,churchLogo: churchLogo,churchBackDrop: churchBack,date:dateOfBirth,gender:gender)
            
            //phone number account created
            if(isPhoneNumberType){
               self.numberAccountdelegate?.didReceiveNumberSuccess(result: true)
            }
            
            //account create with facebook
            if(!isPhoneNumberType && !isUpdateImage && !isUpadeStrings){
              self.accountDelegate?.didReceiveAccountSuccess(result: true)
            }
            
            //update image
            if(isUpdateImage){
                self.updateUserDelagate?.didReceiveUpdateFileResult(result: true)
            }
            
            //update others
            if(isUpadeStrings){
                self.updateUserDelagate?.didReceiveUpdateStringResult(result: true)
            }
        }
       
    }
    
    //MARK: - passing donation item
    func parseDonations(rawData: Data) {
        let item = JSON(data: rawData)
        if let next = item["next"].string {
            self.feedDelegate?.didReceiveNextUrl(results: next)
        }
        if let detail = item["details"].string {
            self.feedDelegate?.didReceiveDetail(results: detail)
        }
        var feed = [Giving]()
        if let results = item["results"].array {
            for item in results {
              let givingItem = self.parseDonation(item: item)
              feed.append(givingItem)
            }
            self.donationDelegate?.didReceiveDonationSuccess(results: feed)
        }else{
          let detail = item["details"].stringValue
          self.donationDelegate?.didReceiveDonationError(results: detail)
        }
    }

    //MARK: - passing donation item here
    func parseDonation(item: JSON) -> Giving {
            let name = item["name"].stringValue
            let description = item["description"].stringValue
            //let date = item["modified"].stringValue
            //let dateCreated = item["created"].stringValue
            let id = String(describing: item["id"].intValue)
        
            let giving = Giving()
            giving.id = id
            giving.description = description
            giving.title = name
        
            return giving
    }
    
    //MARK: - fetch all the gateways
    func getPaymentGetways() {
        let realUrl = "\(Variables.baseUrl())users/payments/gateways/"
        Alamofire.request(realUrl, method: .get, parameters: nil, encoding: JSONEncoding.default,headers: getHeaders())
            .responseJSON { response in
                if let status = response.response?.statusCode {
                    switch(status){
                    case 200...201:
                        self.parsePaymentGetWays(rawData: response.data!)
                        break
                    default:
                        break
                    }
                }
                
        }
    }
    
    
    //MARK: - parse the gateway data
    func parsePaymentGetWays(rawData: Data)  {
        let item = JSON(data: rawData)
        if let result = item["results"].array {
            for item in result {
                self.parsePaymentGetWay(item: item)
            }
            
        }
    }
    
    //MARK: - persist the gateway items
    func parsePaymentGetWay(item: JSON)  {
        let name = item["name"].stringValue
        let description = item["description"].stringValue
        let id = String(describing: item["id"].intValue)
        
        let gateWay = PaymentGateWays()
        gateWay.id = id
        gateWay.itemDescription = description
        gateWay.name = name
        DataOperation.saveGateWays(data: gateWay)
    }
    
    //MARK: - start payment
    func initPayment(giving: Giving,amount:String,gateWay:String,currency:String,isAnonymous:Bool) {
        
        let realUrl = "\(Variables.baseUrl())users/payments/initialize/"
        
        let params = ["amount": amount,"category_id":giving.id,"gateway":gateWay,"pledge_id":giving.pledgeId,"currency":currency,"is_anonymous":isAnonymous] as [String : Any]
        
        Alamofire.request(realUrl, method: .post, parameters: params, encoding: JSONEncoding.default,headers: getHeaders())
            .responseJSON { response in
                if let status = response.response?.statusCode {
                    switch(status){
                    case 200...201:
                        self.parseCheckOut(rawData: response.data!)
                        break
                        
                    default:
                        self.paymentDelagate?.didReceivePaymentError(results: AllKeys.failureMessage)
                        
                        print("print payment error responds: ", response.error.debugDescription)

                        
                        break
                    }
                }
                
        }
    }
    
    //MARK: - parse the check out data
    func parseCheckOut(rawData: Data)  {
        let item = JSON(data: rawData)
        print("ITEM \(item)")
        if (item.dictionary != nil) {
            let details = item["details"].stringValue
            let checkoutUrl = item["checkout_url"].stringValue
            
            print("print payment checkout Url: ", checkoutUrl)
            
            let statusCode = item["status_code"].intValue
            let response = CheckoutResponse()
            response.checkoutUrl = checkoutUrl
            response.details = details
            response.statusCode = statusCode
            self.paymentDelagate?.didReceivePaymentResult(result: response)
        } else {
          self.paymentDelagate?.didReceivePaymentError(results: AllKeys.failureMessage)
        }
    }
    
    
    //MARK: - update profile string data from edit profile
    func updateRecords(firstName:String,lastName:String,otherName:String,sex:String,dateOfBirth:String, emailAccount: String)  {
        let realUrl = "\(Variables.baseUrl())users/profile/me/"
        let params = ["first_name":firstName,"last_name":lastName,"username":otherName,"sex":sex,"date_of_birth":dateOfBirth, "email": emailAccount] as [String : Any]
        
        print("Status \(realUrl)")

        print("Names \(firstName) \(lastName) \(otherName) \(sex) \(dateOfBirth) \(emailAccount)")
        
        Alamofire.request(realUrl, method: .put, parameters: params, encoding: JSONEncoding.default,headers: self.getHeaders())
            .responseJSON { response in
                if let status = response.response?.statusCode {
                    switch(status){
                    case 200...301:
                        print("Status \(status)")
                        self.parseAccount(rawData: response.data!, isPhoneNumberType: false, isUpadeStrings: true, isUpdateImage: false)
                        break
                    case 302...401:
                        let details = JSON(data: response.data!)
                        self.updateUserDelagate?.didReceiveUpdateError(results: details["details"].stringValue)
                        break
                    default:
                        self.updateUserDelagate?.didReceiveUpdateError(results: AllKeys.failureMessage)
                        break
                    }
                }
        }
    }
    
    //MARK: - update profile string data from login
    func updateRecord(firstName:String,lastName:String,otherName:String,dateOfBirth:String)  {
        let realUrl = "\(Variables.baseUrl())users/profile/me/"
        let params = ["first_name":firstName,"last_name":lastName,"username":otherName,"date_of_birth":dateOfBirth] as [String : Any]
        
        print("Names \(firstName) \(lastName) \(otherName)  \(dateOfBirth)")

        Alamofire.request(realUrl, method: .put, parameters: params, encoding: JSONEncoding.default,headers: self.getHeaders())
            .responseJSON { response in
                if let status = response.response?.statusCode {
                    print("Status \(status)")
                    switch(status){
                    case 200...301:
                        self.parseAccount(rawData: response.data!, isPhoneNumberType: false, isUpadeStrings: true, isUpdateImage: false)
                        break
                    case 302...401:
                        let details = JSON(data: response.data!)
                        self.updateUserDelagate?.didReceiveUpdateError(results: details["details"].stringValue)
                        break
                    default:
                        self.updateUserDelagate?.didReceiveUpdateError(results: AllKeys.failureMessage)
                        break
                    }
                }

        }
    }
    
    //MARK: - update email address for payment on the dialogue box
    func updateEmail(emailAddress: String) {
        let realUrl = "\(Variables.baseUrl())users/profile/me/"
        let params = ["email":emailAddress] as [String : Any]

        print("Names \(emailAddress)")
        
        Alamofire.request(realUrl, method: .put, parameters: params, encoding: JSONEncoding.default,headers: self.getHeaders())
            .responseJSON { response in
                if let status = response.response?.statusCode {
                    print("Status \(status)")
                    switch(status){
                    case 200...301:
                        self.parseAccount(rawData: response.data!, isPhoneNumberType: false, isUpadeStrings: true, isUpdateImage: false)
                        break
                    case 302...401:
                        let details = JSON(data: response.data!)
                        self.updateUserDelagate?.didReceiveUpdateError(results: details["details"].stringValue)
                        break
                    default:
                        self.updateUserDelagate?.didReceiveUpdateError(results: AllKeys.failureMessage)
                        break
                    }
                }
                
        }
    }
    
    //MARK: - update with file
    func updateRecords(image:UIImage)  {
        let realUrl = "\(Variables.baseUrl())users/profile/me/"
       
        print("URL \(realUrl)")
        
        let imageData = UIImageJPEGRepresentation(image,1)
        
        let parameters = ["avatar": "avatar.jpeg"]
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
                    multipartFormData.append(imageData!, withName: "avatar", fileName: "avatar.jpeg", mimeType: "image/jpeg")
                    for (key, value) in parameters {
                        multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
                    }
                }, to:realUrl,method: .put,headers: self.getHeaders())
                { (result) in
                    switch result {
                    case .success(let upload, _, _):
                        
                        upload.uploadProgress(closure: { (progress) in
                            //Print progress
                            print("Loading \(progress)")
                        })
                        
                        upload.responseJSON { response in
                            self.parseAccount(rawData: response.data!, isPhoneNumberType: false, isUpadeStrings: false, isUpdateImage: true)
                        }
                        
                    case .failure(let encodingError):
                        self.updateUserDelagate?.didReceiveUpdateError(results: AllKeys.failureMessage)
                        print(encodingError)
                        break
                        
                    }
                }
        
    }
    
    //MARK: - get comments
    func getContentComments(contentId:String,page:String) {
        
        let realUrl = "\(Variables.baseUrl())posts/\(contentId)/comments/\(page)&page_size=10"
        
        print("URL \(realUrl)")
        
        Alamofire.request(realUrl, method: .get, parameters: nil, encoding: JSONEncoding.default,headers: getHeaders())
            .responseJSON { response in
                if let status = response.response?.statusCode {
                    switch(status){
                    case 200...201:
                        self.parseComments(rawData: response.data!)
                        break
                    default:
                        self.commentsDelegate?.didReceiveCommentsError(result: AllKeys.failureMessage)
                        break
                    }
                }
                
        }
    }
    
    //MARK: - parse the comment data
    func parseComments(rawData: Data)  {
        let item = JSON(data: rawData)
        
        if let next = item["comments"]["next"].string {
            self.commentsDelegate?.didReceiveCommentsNextUrl(result: next)
        }
        var comments = [Comment]()
        if let result = item["comments"]["results"].array {
            for item in result {
                comments.append(self.parseCommentItem(item: item))
            }
            self.commentsDelegate?.didReceiveComments(results: comments)
        }
        if let _ = item["comments"]["detail"].dictionary {
            self.commentsDelegate?.didReceiveCommentsError(result: AllKeys.failureMessage)
        }
    }
    
    //MARK: - parse comment itme
    func parseCommentItem(item: JSON) -> Comment {
        let content = item["message"].stringValue
        let date = item["modified"].stringValue
        let id = String(describing: item["id"].intValue)
        
        let userId = String(describing: item["user"]["id"].intValue)
        let userAvatar = item["user"]["thumbnail_80x80"].stringValue
        let firstName = item["user"]["first_name"].stringValue
        let lastName = item["user"]["last_name"].stringValue
        let otherName = item["user"]["username"].stringValue
        
        let member = Member()
        member.avatar = userAvatar
        member.firstName = firstName
        member.lastName = lastName
        member.id = userId
        member.otherName = otherName
        
        let comment = Comment()
        comment.content = content
        comment.date = date
        comment.id = id
        comment.member = member
        
        return comment
        
    }
    
    //MARK: - get church info
    func getChurchInfo(branchId:String) {
        let realUrl = "\(Variables.baseUrl())branches/\(branchId)/?extra_fields=groups,about,content"
        
        Alamofire.request(realUrl, method: .get, parameters: nil, encoding: JSONEncoding.default,headers: getHeaders())
            .responseJSON { response in
                if let status = response.response?.statusCode {
                    switch(status){
                    case 200...201:
                        self.parseAboutData(rawData: response.data!)
                        break
                    default:
                        self.aboutDelegate?.didReceiveAboutError(result: AllKeys.failureMessage)
                        break
                    }
                }
                
        }
    }
    
    //MARK: - get church content
    func getChurchContent(branchId:String,page:String) {

        let realUrl = "\(Variables.baseUrl())users/get_content/\(page)&page_size=10&feed_type=all&branch_id=\(branchId)&content_category=all&multimedia_type=all&keyword="
        Alamofire.request(realUrl, method: .get, parameters: nil, encoding: JSONEncoding.default,headers: getHeaders())
            .responseJSON { response in
                if let status = response.response?.statusCode {
                    print("Content Status \(status)")
                    switch(status){
                    case 200...201:
                        let rawData = response.data!
                        let items = JSON(data: rawData)
                        if let result = items["results"]["content"]["results"].array {
                            var feeds = [Feed]()
                            for item in result {
                                feeds.append(self.getFeedItem(item: item))
                            }
                            self.feedDelegate?.didReceiveSuccess(results: feeds)
                        }
                        if let next = items["results"]["content"]["next"].string {
                            print("NEXT URL \(next)")
                            self.feedDelegate?.didReceiveNextUrl(results: next)
                        }
                        
                        if let _ = items["detail"].dictionary {
                            self.feedDelegate?.didReceiveDetail(results: AllKeys.failureMessage)
                        }
                        break
                    default:
                        self.feedDelegate?.didReceiveError(results: AllKeys.failureMessage)
                        break
                    }
                }
                
        }
    }
    
    
    
    //MARK:- parse about data
    func parseAboutData(rawData: Data)  {
        let items = JSON(data: rawData)
        if let result = items["results"].dictionary {
            let isMain = result["is_main_church"]?.boolValue
            let isFollowing = result["is_following"]?.boolValue
            let numberOfFollowers = result["followers_count"]?.intValue
            let numberOfFollowings = result["following_count"]?.intValue
            let numberOfGroups = result["groups"]?.arrayValue.count

            
            var id = String(describing: result["id"]?.intValue)
            
            let branchName  =  result["branch_name"]?.stringValue
            
            //print("print branch name from about data: ",branchName!)
            
            var name  =  result["church_name"]?.stringValue
            var logo  =  result["logo"]?.stringValue
            var back  =  ""
            if let church = result["church"]?.dictionary{
                name = church["church_name"]?.stringValue
                id = String(describing: church["id"]?.intValue)
                logo = church["logo"]?.stringValue
                back = (church["backdrop"]?.stringValue)!
            }
            
            let church = Church()
            church.branchName = branchName!
            church.name = name!
            church.id = id
            church.churchLogo = logo!
            church.image = back
            church.isMainBranch = isMain!
            church.isFollowing = isFollowing!
            church.numberOfGroups = numberOfGroups!
            church.numberOfFollowers = numberOfFollowers!
            self.aboutDelegate?.didReceiveAboutInfo(result: church)
            
            if let result = items["results"]["about"].array {
                var feed = [About]()
                for item in result {
                    feed.append(self.parseAboutItem(item: item))
                }
                self.aboutDelegate?.didReceiveAbouts(results: feed)
            }
        }
        if let _ = items["detail"].dictionary {
            self.aboutDelegate?.didReceiveAboutError(result: AllKeys.failureMessage)
        }
    }
    
    //MARK: - parse the about item
    func parseAboutItem(item: JSON) -> About {
        let name = item["name"].stringValue
        let id = String(describing: item["id"].intValue)
        
        let arrayData = item["sub_headers"].array?.count
        var subTitle = ""
        var content = ""
        var photo = ""
        if arrayData! > 0 {
            let subHeader = item["sub_headers"][0].dictionaryValue
            subTitle = (subHeader["title"]?.stringValue)!
            content = (subHeader["content"]?.stringValue)!
            photo = (subHeader["photo"]?.stringValue)!
        }
      
        let about = About()
        about.content = content
        about.id = id
        about.subTitle = subTitle
        about.title = name
        about.image = photo
        
        return about
        
    }
    
    //MARK: - join church group
    func joingChurchGroup(groupId:String) {
        let realUrl = "\(Variables.baseUrl())/users/groups/join/"
        let parameters = ["group_id": groupId]
        
        Alamofire.request(realUrl, method: .post, parameters: parameters, encoding: JSONEncoding.default,headers: getHeaders())
            .responseJSON { response in
                if let status = response.response?.statusCode {
                    switch(status){
                    case 200...201:
                        Messaging.messaging().subscribe(toTopic: "department-\(groupId)")
                        print("Joined Group \(String(describing: response.result.value))")
                        break
                    default:
                        break
                    }
                }

        }
    }
    
    //MARK: - send comment
    func sendComment(contentId:String,comment:String) {
        let realUrl = "\(Variables.baseUrl())/posts/comment/"
        let parameters = ["content_id": contentId,"message":comment]
        
        Alamofire.request(realUrl, method: .post, parameters: parameters, encoding: JSONEncoding.default,headers: getHeaders())
            .responseJSON { response in
                if let status = response.response?.statusCode {
                    switch(status){
                    case 200...201:
                        //subscibe for messages
                        Messaging.messaging().subscribe(toTopic: "commentson-\(contentId)")
                        break
                    default:
                        break
                    }
                }
                
        }
    }
    
    //MARK: - delete comment
    func deleteComment(commentId:String) {
        let realUrl = "\(Variables.baseUrl())/posts/comment/"
        let parameters = ["comment_id": commentId]
        
        Alamofire.request(realUrl, method: .post, parameters: parameters, encoding: JSONEncoding.default,headers: getHeaders())
            .responseJSON { response in
                if let status = response.response?.statusCode {
                    switch(status){
                    case 200...201:
                        break
                    default:
                        break
                    }
                }
                
        }
    }
    
    //0205737981
    
    //MARK: - add post
    func addPost(message:String,quotation:Quotation) {
        let realUrl = "\(Variables.baseUrl())/posts/post/"
        var parameters = ["message": message,"item_type":""]
        if !quotation.book.isEmpty {
           parameters = ["message":message,"book":quotation.book,"chapter":quotation.chapter,"verse":quotation.verse]
        }
        Alamofire.request(realUrl, method: .post, parameters: parameters, encoding: JSONEncoding.default,headers: getHeaders())
            .responseJSON { response in
                if let status = response.response?.statusCode {
                    switch(status){
                    case 200...201:
                        let rawData = response.data!
                        let item = JSON(data: rawData)
                        let feed = self.getFeedItem(item: item)
                        self.postServiceProtocol?.didReceivePostResult(result: feed)
                        NotificationCenter.default.post(name: Notification.Name(AllKeys.newPostNotification), object: feed)
                        break
                    default:
                        self.postServiceProtocol?.didReceivePostError(result: "Unable to post, try later")
                        break
                    }
                }
                
        }
    }
    
    //MARK: - update with file
    func addPostImage(images:[UIImage],content:String,quotation:Quotation)  {
        let realUrl = "\(Variables.baseUrl())/posts/post/"
        var parameters = ["message": content,"media_type":"gallery"]
        if !quotation.book.isEmpty {
            parameters = ["message":content,"book":quotation.book,"chapter":quotation.chapter,"verse":quotation.verse]
        }
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (index, image) in images.enumerated() {
                if let imageData = UIImageJPEGRepresentation(image, 1) {
                    multipartFormData.append(imageData, withName: "media[\(index)]", fileName: "\(NSUUID().uuidString).jpeg", mimeType: "image/jpeg")
                }
            }
            for (key, value) in parameters {
                multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
            }
            
        }, to:realUrl,method: .post,headers: getHeaders())
        { (result) in
            switch result {
            case .success(let upload, _, _):
                upload.uploadProgress(closure: { (progress) in
                    print("Loading \(progress)")
                
                })
                
                upload.responseJSON { response in
                    let rawData = response.data!
                    let item = JSON(data: rawData)
                    let feed = self.getFeedItem(item: item)
                    self.postServiceProtocol?.didReceivePostResult(result: feed)
                    
                    NotificationCenter.default.post(name: Notification.Name(AllKeys.newPostNotification), object: feed)
                }
                
            case .failure(let encodingError):
                print(encodingError)
                break
                
            }
        }
    }
    
    
    //MARK: - post with video
    func addPostAudio(audioPath:URL,content:String,quotation:Quotation)  {
        let realUrl = "\(Variables.baseUrl())/posts/post/"
        
        print("print post audio url: \(realUrl)")
        var parameters = ["message": content,"media_type":"audio"]
        if !quotation.book.isEmpty {
            parameters = ["message":content,"book":quotation.book,"chapter":quotation.chapter,"verse":quotation.verse]
        }
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            multipartFormData.append(audioPath, withName: "media")
            for (key, value) in parameters {
                multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
            }
            
        }, to:realUrl,method: .post,headers: getHeaders())
        { (result) in
            switch result {
            case .success(let upload, _, _):
                upload.uploadProgress(closure: { (progress) in
                    print("Loading \(progress)")
                })
                
                upload.responseJSON { response in
                    let rawData = response.data!
                    let item = JSON(data: rawData)
                    let feed = self.getFeedItem(item: item)
                    self.postServiceProtocol?.didReceivePostResult(result: feed)
                    NotificationCenter.default.post(name: Notification.Name(AllKeys.newPostNotification), object: feed)
                }
                
            case .failure(let encodingError):
                print(encodingError)
                break
                
            }
        }
    }

    
    //MARK: - post with video
    func addPostVideo(image:UIImage,videoPath:URL,content:String,quotation:Quotation)  {
        let realUrl = "\(Variables.baseUrl())/posts/post/"
        var parameters = ["message": content,"media_type":"video"]
        if !quotation.book.isEmpty {
            parameters = ["message":content,"book":quotation.book,"chapter":quotation.chapter,"verse":quotation.verse]
        }

        Alamofire.upload(multipartFormData: { (multipartFormData) in
            if let imageData = UIImageJPEGRepresentation(image,0.5){
                   multipartFormData.append(imageData, withName: "media_thumbnail", fileName: "\(NSUUID().uuidString).jpeg", mimeType: "image/jpeg")
            }
            multipartFormData.append(videoPath, withName: "media")
            for (key, value) in parameters {
                multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
            }
            
        }, to:realUrl,method: .post,headers: getHeaders())
        { (result) in
            switch result {
            case .success(let upload, _, _):
                upload.uploadProgress(closure: { (progress) in
                    print("Loading \(progress)")
                })
                
                upload.responseJSON { response in
                    let rawData = response.data!
                    let item = JSON(data: rawData)
                    let feed = self.getFeedItem(item: item)
                    self.postServiceProtocol?.didReceivePostResult(result: feed)
                    
                    NotificationCenter.default.post(name: Notification.Name(AllKeys.newPostNotification), object: feed)
                }
                
            case .failure(let encodingError):
                print(encodingError)
                break
                
            }
        }
    }
    
    
    //MARK: - payment history
    func getPaymentHistory(url:String) {
        let realUrl = "\(Variables.baseUrl())users/payments/records/\(url)&page_size=10"
        print("Real URL \(realUrl)")
        Alamofire.request(realUrl, method: .get, parameters: nil, encoding: JSONEncoding.default,headers: getHeaders())
            .responseJSON { response in
                if let status = response.response?.statusCode {
                    print("Status \(status)")
                    switch(status){
                    case 200...201:
                        self.parsePaymentHistoryData(rawData: response.data!)
                        break
                    default:
                        self.paymentHistoryDelegate?.didReceiveHistoryError(result: AllKeys.failureMessage)
                        break
                    }
                }
                
        }
    }
    
    
    //MARK:- parse check in data
    func parseCheckInsData(rawData: Data)  {
        let items = JSON(data: rawData)
        var feed = [CheckIn]()
        if let result = items["results"]["records"]["results"].array {
            for item in result {
                feed.append(self.parseCheckInItem(item: item))
            }
            self.checkInServiceProtocol?.didReceiveChecksResult(results: feed)
        }
        if let details = items["details"].string {
            self.checkInServiceProtocol?.didReceiveCheckError(result: details)
        }
    }
    
    //MARK: - parse check item
    func parseCheckInItem(item: JSON) -> CheckIn {

        let source = item["source"].stringValue
        let id = String(describing: item["id"].intValue)
        let date = item["modified"].stringValue
        let time = item["check_in_time"].stringValue
        let attempts = item["attempts"].intValue
        var name = ""
        if let nameInner = item["session"]["name"].string {
            name = nameInner
        }
        
        var serviceName = ""
        if let nameInner = item["session"]["service"]["name"].string {
            serviceName = nameInner
        }
        
        let checkIn = CheckIn()
        checkIn.id = id
        checkIn.attempts = attempts
        checkIn.device = source
        checkIn.program = "\(serviceName)\n\(name)"
        checkIn.date = date
        checkIn.checkInTime = time
    
        return checkIn
        
    }
    
    //MARK:- parse payment history data
    func parsePaymentHistoryData(rawData: Data)  {
        let items = JSON(data: rawData)
        var feed = [PaymentHistory]()
        if let result = items["transactions"]["results"].array {
            for item in result {
                feed.append(self.parsePaymentHistoryItem(item: item))
            }
            self.paymentHistoryDelegate?.didReceiveHistories(results: feed)
        }
        if let _ = items["detail"].dictionary {
            self.paymentHistoryDelegate?.didReceiveHistoryError(result: AllKeys.failureMessage)
        }
    }
    
    //MARK: - parse payment history item
    func parsePaymentHistoryItem(item: JSON) -> PaymentHistory {
        let options = item["payment_option"].stringValue
        let id = String(describing: item["id"].intValue)
        let date = item["record_date"].stringValue
        let currency = item["currency"].stringValue
        let amount = item["amount"].stringValue
        let remarks = item["description"].stringValue
        let status = item["is_anonymous"].boolValue
        let accountType = item["account_category"]["name"].stringValue
        
        let transactions = PaymentHistory()
        transactions.payementOptions = options
        transactions.id = id
        transactions.recordDate = date
        transactions.currency = currency
        transactions.amount = amount
        transactions.typeOfTransactions = accountType
        transactions.remarks = remarks
        transactions.isAnonymous = status
    
        return transactions
        
    }
    
    //MARK: - payment history
    func updateUserTokens(instanceId:String) {
        let realUrl = "\(Variables.baseUrl())users/user_settings/"
        
        let parameters = ["firebase_instance_id": instanceId]
        
        Alamofire.request(realUrl, method: .post, parameters: parameters, encoding: JSONEncoding.default,headers: getHeaders())
            .responseJSON { response in
                if let status = response.response?.statusCode {
                    switch(status){
                    case 200...201:
                        break
                    default:
                        break
                    }
                }
                
        }
    }
    
    //MARK: - record checkin to event
    func checkIn(content:String) {
        let realUrl = "\(Variables.baseUrl())attendance/qr_check_in/"
        let parameters = ["encoded_string": content]
        Alamofire.request(realUrl, method: .post, parameters: parameters, encoding: JSONEncoding.default,headers: getHeaders())
            .responseJSON { response in
                
                if let status = response.response?.statusCode {
                    print("Status of check-in \(status)")
                    switch (status){
                    case 200...301:
                        let rawData = response.data!
                        let item = JSON(data: rawData)
                        let details = item["details"].stringValue
                        let feed = CheckIn()
                        feed.program = details
                        self.checkInServiceProtocol?.didReceiveCheckResult(result: feed)
                        break

                    case 302...401:
                        
                        let details = JSON(data: response.data!)
                        self.checkInServiceProtocol?.didReceiveCheckError(result: details["details"].stringValue)
                        
                        break

                    default:
                        self.checkInServiceProtocol?.didReceiveCheckError(result: AllKeys.failureMessage)

                        print("print error: ", response.error?.localizedDescription as Any)
                        break
                    }
                }
        }
    }
    
    
    //MARK: - get checkin rcords
    func checkIns(url:String) {
        let realUrl = "\(Variables.baseUrl())\(url)"
        print("URL \(realUrl)")
        Alamofire.request(realUrl, method: .get, parameters: nil, encoding: JSONEncoding.default,headers: getHeaders())
            .responseJSON { response in
                if let status = response.response?.statusCode {
                    switch(status){
                    case 200...201:
                        let rawData = response.data!
                        self.parseCheckInsData(rawData: rawData)
                        break
                    default:
                        self.checkInServiceProtocol?.didReceiveCheckError(result: AllKeys.failureMessage)
                        break
                    }
                }
                
        }
    }
    
    
    //MARK: - get request for cloud functions on firebase notification
    //let NEW_FORUM_MESSAGE_CLOUD_MESSAGE_BASE_URL = "https://us-central1-asoriba-android-f7975.cloudfunctions.net/sendForumNotification"

    func sendCloudMessageToForumTopic(topicId: String, forumName: String, userFullName: String, currentUserId: String, gist: String){
        
        let NEW_CHAT_MESSAGE_CLOUD_MESSAGE_BASE_URL = "https://us-central1-asoriba-android-f7975.cloudfunctions.net/sendChatNotification"

        Alamofire.request(NEW_CHAT_MESSAGE_CLOUD_MESSAGE_BASE_URL, method: .get, parameters: nil, encoding: JSONEncoding.default,headers: getHeaders())
            .responseJSON { response in
                if let status = response.response?.statusCode {
                    switch(status){
                    case 200...201:
                        
                        let rawData = response.data!
                        
                        print("print incoming notification: ", rawData)
                        
                        break
                    default:
                        
                        print("forum notifications error")
                        
                        break
                    }
                }
                
        }
    }
    
    //MARK: - subscribe user to topics
    func subscribeToTopics() {
        let user  = DataOperation.getUser()
        Messaging.messaging().subscribe(toTopic: "user-\(user.id)")
        Messaging.messaging().subscribe(toTopic: "branch-\(user.churchId)")
        Messaging.messaging().subscribe(toTopic: "asoriba")
        
    }
    
    
}

