//
//  PaymentHistoryController.swift
//  asoriba
//
//  Created by Bright Ahedor on 23/12/2016.
//  Copyright © 2016 Asoriba Inc. All rights reserved.
//

import UIKit
import Crashlytics

class PaymentHistoryController: UIViewController,UITableViewDelegate,UITableViewDataSource,PaymentHistoryServiceProtocol {
    
    @IBOutlet weak var noPaymentLabel: UILabel!
    @IBOutlet weak var historyTableView: UITableView!
    var api:ApiService!
    let screenWidth = UIScreen.main.bounds.width
    var refreshControl = UIRefreshControl()
    var activityViewIndicator:UIActivityIndicatorView = UIActivityIndicatorView()
    var currentUser: User!
    var feeds = [PaymentHistory]()
    var loadedPages = [String]()
    var nextUrl = ""
    var isRefreshing = false
    
   
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //get user
        currentUser = DataOperation.getUser()
        
        
        self.historyTableView.delegate = self
        self.historyTableView.dataSource = self
        self.historyTableView.alwaysBounceVertical = true
        self.automaticallyAdjustsScrollViewInsets = false
        
        self.historyTableView.estimatedRowHeight = 140
        
        //register cell
        self.historyTableView.register(UINib(nibName: CellIdentifiers.historyTableCell.rawValue, bundle: nil), forCellReuseIdentifier: CellIdentifiers.historyTableCell.rawValue)
        
        //calling the protocall function
        self.startProgressBar()
        
        //refreshing
        self.refreshControl.addTarget(self, action: #selector(PaymentHistoryController.refresh), for: .valueChanged)
        historyTableView.addSubview(refreshControl)
        
        
        api = ApiService()
        self.api.paymentHistoryDelegate = self
        self.noPaymentLabel.isHidden = true
        

        self.getData(page: "?page=1")
        
        //tracking analytics
        Answers.logCustomEvent(withName: "PAYMENT_HISTORY", customAttributes: nil)
        

    }
    
    func getData(page: String)  {
        if(isRefreshing){
            loadedPages.removeAll()
        }
        loadedPages.append(page)
        self.api.getPaymentHistory(url: page)
    }
    
    func refresh()  {
        isRefreshing = true
        self.getData(page: "?page=1")
    }

    func startProgressBar()  {
        activityViewIndicator.center = self.view.center
        activityViewIndicator.hidesWhenStopped = true
        activityViewIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray
        self.view.addSubview(activityViewIndicator)
        activityViewIndicator.startAnimating()
    }
    

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return feeds.count
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let feed = feeds[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: CellIdentifiers.historyTableCell.rawValue, for: indexPath) as! HistoryTableCell
        cell.feed = feed
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //self.showAction(giving: feeds[indexPath.row])
    }
    
    func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
        return false
    }
    

    
    //calculating the item height
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let height =  CGFloat(140);
        return height
    }

    
    //MARK: - payment history implementations
    func didReceiveHistoryError(result: String) {
        self.activityViewIndicator.stopAnimating()
    }
    
    func didReceiveHistories(results: [PaymentHistory]) {
        //checking for refreshing and clearing all content
        self.activityViewIndicator.stopAnimating()
        self.refreshControl.endRefreshing()
        if(!results.isEmpty && isRefreshing){
            feeds.removeAll()
        }
        isRefreshing = false
        self.feeds.append(contentsOf: results)
        self.historyTableView.reloadData()
        if(feeds.isEmpty){
            self.noPaymentLabel.isHidden = false
        } else {
            self.noPaymentLabel.isHidden = true
        }
    }
    
    func didReceiveHistoryNextUrl(result: String)  {
        self.nextUrl = result
    }
    
    //MARK: - scroll handling section
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let currentOffset = scrollView.contentOffset.y
        let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height
        
        if (maximumOffset - currentOffset) <= 100 {
            if(!self.nextUrl.isEmpty && !loadedPages.contains(self.nextUrl)){
                loadedPages.append(self.nextUrl)
                self.getData(page: self.nextUrl)
            }
        }
    }

}
