//
//  ScannerViewController.swift
//  asoriba
//
//  Created by Bright Ahedor on 16/03/2017.
//  Copyright © 2017 Asoriba Inc. All rights reserved.
//

import UIKit
import Crashlytics
import Toaster
import BSQRCodeReader
import SwiftSpinner
import PopupDialog

class ScannerViewController: UIViewController,BSQRCodeReaderDelegate,CheckInServiceProtocol{

    @IBOutlet weak var reader: BSQRCodeReader!
    let apiService = ApiService()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.reader.delegate = self
        
        let tag = "Check In"
        self.navigationItem.title = tag
        self.apiService.checkInServiceProtocol = self
        
        
        self.setMenuItems()
        
        Answers.logCustomEvent(withName: tag.uppercased(), customAttributes: nil)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.reader.startScanning()
    }
    
    
    
    // MARK: BSQRCodeReaderDelegate
    func didFailWithError(_ error: NSError) {
        
    }
    func beforeStartScanning(_ reader: BSQRCodeReader){
        
    }
    
    func afterStopScanning(_ reader: BSQRCodeReader){
        
    }
    
    func didCaptureQRCodeWithContent(_ content: String) -> Bool {
        self.apiService.checkIn(content: content)
        
        SwiftSpinner.show("Recording attendance", animated: true)
        
        return true
    }

    func showDialog(title:String,message:String)  {
        let popup = PopupDialog(title: title, message: message, image: nil)
        let buttonOne = CancelButton(title: "OK") {
            let vc = self.navigationController?.popViewController(animated: true)
            vc?.dismiss(animated: true, completion: nil)
        }
        
        popup.addButtons([buttonOne])
        self.present(popup, animated: true, completion: nil)
    }
    
    func didReceiveCheckError(result: String) {
      SwiftSpinner.hide()
        ControllerHelpers.showDialog(targetVC: self, title: "Notice", message: result)
    }
    
    func didReceiveCheckResult(result: CheckIn) {
        SwiftSpinner.hide()
        self.showDialog(title: "Checked In", message: result.program)
    }
    
    func didReceiveChecksResult(results: [CheckIn]) {
        
    }
    
    func didReceiveCheckNextUrl(result: String) {
        
    }
    
    
    //setup the menu items for current user
    func setMenuItems() {
        let checkInImage = #imageLiteral(resourceName: "history").withRenderingMode(.alwaysOriginal)
        let history = UIBarButtonItem(image: checkInImage, style: UIBarButtonItemStyle.plain,target: self, action: #selector(handleClick))
        //let infoBarItem = UIBarButtonItem.init(title: "Check History", style: .plain, target: self, action: #selector(handleClick))
        self.navigationItem.rightBarButtonItem = history
    }
    
    func handleClick()  {
        guard let vc = UIStoryboard(name:AllKeys.mainStoryBoard, bundle:nil).instantiateViewController(withIdentifier: AllKeys.checkInsViewController) as? CheckInsViewController else {
            return
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }


}
