//
//  User.swift
//  asoriba
//
//  Created by Bright Ahedor on 14/12/2016.
//  Copyright © 2016 Asoriba Inc. All rights reserved.
//

import RealmSwift

// Dog model
class User: Object {
    dynamic var firstName = ""
    dynamic var lastName = ""
    dynamic var otherName = ""
    dynamic var id = ""
    dynamic var token = ""
    dynamic var number = ""
    dynamic var email = ""
    dynamic var churchId = ""
    dynamic var gender = ""
    dynamic var dateOfBirth = ""
    dynamic var churchName = ""
    dynamic var branchName = ""
    dynamic var churchLogo = ""
    dynamic var churchBackDrop = ""
    dynamic var avatar = ""
    dynamic var numberOfFollowers = 0
    dynamic var numberOfFollowings = 0
    dynamic var numberOfGroups = 0
    
    override class func primaryKey() -> String? {
        return "id"
    }
    
}
