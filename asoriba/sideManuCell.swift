//
//  sideManuCell.swift
//  asoriba
//
//  Created by Benjamin Acquah on 12/09/2017.
//  Copyright © 2017 Asoriba Inc. All rights reserved.
//

import UIKit
import LBTAComponents

class sideManuCell: UICollectionViewCell {
    
    let itemIcon: UIImageView = {
        let img = UIImageView()
        img.image = UIImage(named: "try")?.withRenderingMode(UIImageRenderingMode.alwaysOriginal)
        img.contentMode = .scaleAspectFit
        img.layer.cornerRadius = 5
        img.layer.masksToBounds = true
        img.translatesAutoresizingMaskIntoConstraints = false
        return img
    }()
    
    let itemName : UILabel = {
        let lbl = UILabel()
        lbl.text = "Manu Items"
        lbl.font = UIFont.boldSystemFont(ofSize: 14)
        lbl.textColor = UIColor.darkGray
        return lbl
    }()
    
    let line : UILabel = {
        let lbl = UILabel()
        lbl.backgroundColor = UIColor.lightGray
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setUpViews()
    }
    
    func setUpViews(){
        
        addSubview(itemIcon)
        addSubview(itemName)
        addSubview(line)
        
//        itemIcon.anchor(topAnchor, left: leftAnchor, bottom: bottomAnchor, right: nil, topConstant: 5, leftConstant: 8, bottomConstant: 5, rightConstant: 0, widthConstant: self.frame.height, heightConstant: 0)
        
        itemIcon.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        itemIcon.leftAnchor.constraint(equalTo: leftAnchor, constant: 8).isActive = true
        itemIcon.widthAnchor.constraint(equalToConstant: 30).isActive = true
        itemIcon.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        itemName.anchor(nil, left: itemIcon.rightAnchor, bottom: itemIcon.bottomAnchor, right: rightAnchor, topConstant: 0, leftConstant: 8, bottomConstant: 0, rightConstant: 8, widthConstant: 0, heightConstant: 20)
        
        line.anchor(nil, left: itemName.leftAnchor, bottom: bottomAnchor, right: itemName.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 8, widthConstant: 0, heightConstant: 0.5)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
