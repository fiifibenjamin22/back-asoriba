//
//  Keys.swift
//  asoriba
//
//  Created by Bright Ahedor on 15/12/2016.
//  Copyright © 2016 Asoriba Inc. All rights reserved.
//

import Foundation
struct AllKeys{
   static var  primaryHexCode = "#12A5F4"
   static var feedDetailController = "FeedDetailController"
   static var newFeedController = "NewFeedController"
   static var  feeddetailSegue = "FeedDetailSegue"
   static var churchProfileController = "ChurchProfileController"
   static var userProfile = "UserProfileSegue"
   static var userProfileController = "ProfileViewController"
   static var editProfileSegue = "EditProfileSegue"
    static var EditProfileController = "EditProfileController"
    static var aboutChurch = "UIViewController-y2M-rc-mZn"
    static var givingScene = "givingScene"
    static var helperVC = "helperVC"
    static var findUsVC = "findUsVC"
   static var zoomImageController = "ZoomImageController"
   static var onboardViewController = "OnboardViewController"
   static var updateNameController = "UpdateNameController"
   static var searchSegue = "SearchSegue"
   static var chooseChurchController = "ChooseChurchController"
   static var aboutChurchSegue  = "AboutChurchSegue"
   static var chooseChurchSegue = "ChooseChurchSegue"
   static var paymentPopOverController = "PaymentPopOverController"
   static var paymentCheckOutController = "PaymentCheckoutController"
   static var failureMessage = "an error occured during the process, this may be due to internet connection"
   static var updatedUser = "UserUpdated"
   static var micsStoryBoard = "Mics"
   static var mainController = "MainTabController"
   static var paymentHistorySegue = "PaymentHistorySegue"
   static var paymentHistoryController = "PaymentHistoryController"
   static let birthDateViewController = "BirthDateViewController"
   static var videoViewController = "VideoViewController"
   static var youtubePlayer = "YoutubeVideoPlayer"
   static var versePickerController = "VersePickerController"
   static var searchFilterViewController = "SearchFilterViewController"
   static var churchSegue = "ChurchSegue"
   static var searchChurchController = "SearchChurchController"
   static var searchViewController = "SearchViewController"
   static var actionPerformersController = "ActionPerformersController"
   static var audioControllerViewController = "AudioControllerViewController"
   static let checkInsViewController = "CheckInsViewController"
   static let recordAudioController = "RecordAudioController"
   static let scannerViewController = "ScannerViewController"
   static var mainStoryBoard = "Main"
   static var youtubeVideoIdLength = -11
   static var youtubeVideoIdLengthPositive = 11
   static let cellSpacing = 8.0
   static let checkOutRedirect = "https://www.asoriba.com/android/"
   static let verseNotification = "LocalVerseNotification"
   static let filterNotification = "FilterNotification"
   static let audioNotification = "AudioNotification"
   static let notificationCount = "NotificationCount"
   static let newPostNotification = "NewPostNotification"
   static let bibleVerse = "https://www.biblegateway.com/passage/?search="
   static let bibleVerseViewController = "BibleVerseViewController"
   static let aboutDetailViewController = "AboutDetailViewController"
   static let selectTabNotificationState = "selectTabNotificationState"
   
}
