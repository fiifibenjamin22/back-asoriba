//
//  TabBarMoreCell.swift
//  asoriba
//
//  Created by Benjamin Acquah on 26/09/2017.
//  Copyright © 2017 Asoriba Inc. All rights reserved.
//

import UIKit
import LBTAComponents
import AlamofireImage

class myFollowersVC: UICollectionViewController, UICollectionViewDelegateFlowLayout,ChurchMembersServiceProtocol {
    
    var api:ApiService!
    var followersData = [Member]()
    var loadedPages = [String]()
    var followersNextUrl = ""
    var requestId = ""
    let currentUser = DataOperation.getUser()
    var user:Member!
    var titleText = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        self.collectionView?.backgroundColor = .white
        
        
        self.navigationItem.title = "Following"
        
        self.collectionView?.register(myFollowersCell.self, forCellWithReuseIdentifier: cellIdentify)
        
        //api instance
        api = ApiService()
        api.churchMembersDelegate = self
        self.getDataFollowings(page: "page=1")

    }
    
    func getDataFollowings(page: String)  {
        loadedPages.append(page)
        api.getMemberFollowers(userId: requestId, next: page)
        print("print requestID :", requestId)
    }
    
    //conform to members protocol to facilitate my follower fetch
    func didReceiveChurchMembersError(results: String) {
        //error
    }
    
    func didReceiveChurchMemberNextUrl(results: String) {
        self.followersNextUrl = results
    }
    
    func didReceiveChurchMembersDetail(results: String) {
        //something happened
    }
    
    func didReceiveChurchMembersSuccess(results: [Member]) {
        self.followersData.append(contentsOf: results)
        self.collectionView?.reloadData()
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return followersData.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let myFollowers = followersData[indexPath.row]
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentify, for: indexPath) as! myFollowersCell
        cell.feed = myFollowers
        cell.followActionButton.isHidden = true
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.view.frame.size.width, height: 100)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    //MARK: - Following user
    @IBAction func followUser(_ sender: UIButton) {
        
        let path = Bundle.main.path(forResource: "configuration", ofType: "plist")
        let root = NSDictionary(contentsOfFile: path!)
        
        let headerBackground = (root?["headerBackgroundColor"] as! String)
        let headerYellowBackground = (root?["tint_white"] as! String)
        
        titleText = sender.currentTitle!
        let feed = self.followersData[sender.tag]
        if (titleText == "Follow") {
            api.followMeber(userId: feed.id)
            sender.setTitle("Unfollow", for: .normal)
            feed.isFollowing = true
            sender.backgroundColor = UIColor().HexToColor(hexString: headerBackground)
            sender.setTitleColor(UIColor().HexToColor(hexString: headerYellowBackground), for: UIControlState.normal)
        } else {
            api.unFollowMeber(userId: feed.id)
            sender.setTitle("Follow", for: .normal)
            feed.isFollowing = false
            sender.backgroundColor = UIColor().HexToColor(hexString: headerYellowBackground)
            sender.setTitleColor(UIColor().HexToColor(hexString: headerBackground), for: UIControlState.normal)
            
        }
    }
    
    //MARK: -- member profile
    func startDetail(_ sender: UITapGestureRecognizer)  {
        
        let layout = UICollectionViewFlowLayout()
        let vc = profileVC(collectionViewLayout: layout)
        let position = (sender.view?.tag)!
        let member = followersData[position]
        print("print selected member: ", member)
        vc.user = member
        self.navigationController?.pushViewController(vc, animated: true)
    }

}


//cell
class myFollowersCell: UICollectionViewCell {

    let userImageView : UIImageView = {
        let img = UIImageView()
        img.image = UIImage(named: "default")?.withRenderingMode(.alwaysOriginal)
        img.translatesAutoresizingMaskIntoConstraints = false
        img.layer.cornerRadius = 37
        img.layer.masksToBounds = true
        return img
    }()
    
    let userNameLabel : UILabel = {
        let lbl = UILabel()
        lbl.text = "Benjamin Acquah"
        lbl.font = UIFont.systemFont(ofSize: 16)
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let userFollowersCountLabel : UILabel = {
        let lbl = UILabel()
        lbl.text = "5 • Followers"
        lbl.font = UIFont.boldSystemFont(ofSize: 12)
        lbl.textColor = UIColor.gray
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let followActionButton : UIButton = {
        let btn = UIButton(type: UIButtonType.system)
        btn.setTitle("+ Follow", for: .normal)
        btn.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        btn.backgroundColor = UIColor.red
        btn.setTitleColor(UIColor.white, for: .normal)
        btn.layer.cornerRadius = 5
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.layer.shadowColor = UIColor.lightGray.cgColor
        btn.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        btn.layer.shadowRadius = 2.0
        btn.layer.shadowOpacity = 1.0
        btn.layer.masksToBounds = false
        return btn
    }()
    
    private var placeHolderImage = UIImage(named: "default")
    
    let user = DataOperation.getUser()


    var noAction : Bool!{
        didSet {
            self.followActionButton.isHidden = true
        }
    }


    var feed: Member!{
        didSet{
            updateUI()
        }
    }

    func updateUI()   {

        let path = Bundle.main.path(forResource: "configuration", ofType: "plist")
        let root = NSDictionary(contentsOfFile: path!)

        let headerBackground = (root?["headerBackgroundColor"] as! String)
        let headerYellowBackground = (root?["tint_white"] as! String)

        self.followActionButton.layer.borderWidth = 2
        self.followActionButton.layer.borderColor = UIColor().HexToColor(hexString: headerBackground).cgColor

        //setting counts
        let followers = feed.numberOfFollowing
        let followingShowable = followers == 1 ? "\(followers) Follower" : "\(followers) Followers"
        self.userFollowersCountLabel.text = followingShowable
        self.userNameLabel.text = feed.lastName + " " + feed.firstName

        if (feed.isFollowing){
            self.followActionButton.setTitle("Following", for: .normal)
            self.followActionButton.backgroundColor = UIColor().HexToColor(hexString: headerBackground)

        }else {
            self.followActionButton.setTitle("Follow", for: .normal)
            self.followActionButton.backgroundColor = UIColor().HexToColor(hexString: headerYellowBackground)
            self.followActionButton.setTitleColor(UIColor().HexToColor(hexString: headerBackground), for: UIControlState.normal)
        }
        //content image
        if  feed.avatar != "" {
            self.userImageView.af_setImage(
                withURL: URL(string: feed.avatar)!,
                placeholderImage: placeHolderImage,
                imageTransition: .crossDissolve(0.2)
            )
        }else {
            self.userImageView.image = placeHolderImage
        }


        if self.feed.id == self.user.id {
            self.followActionButton.isHidden = true
        } else{
            self.followActionButton.isHidden = false
        }

    }

    override func prepareForReuse() {
        super.prepareForReuse()
        self.userImageView.af_cancelImageRequest()
        self.userImageView.layer.removeAllAnimations()
        self.userImageView.image = nil
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .white
        setUpViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setUpViews(){
        
        addSubview(userImageView)
        addSubview(userNameLabel)
        addSubview(userFollowersCountLabel)
        addSubview(followActionButton)

        userImageView.anchor(topAnchor, left: leftAnchor, bottom: nil, right: nil, topConstant: 10, leftConstant: 12, bottomConstant: 0, rightConstant: 0, widthConstant: 80, heightConstant: 80)
        userNameLabel.anchor(userImageView.topAnchor, left: userImageView.rightAnchor, bottom: nil, right: followActionButton.leftAnchor, topConstant: 5, leftConstant: 8, bottomConstant: 0, rightConstant: 8, widthConstant: 0, heightConstant: 25)
        userFollowersCountLabel.anchor(userNameLabel.bottomAnchor, left: userNameLabel.leftAnchor, bottom: nil, right: userNameLabel.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 20)
        followActionButton.anchor(userNameLabel.bottomAnchor, left: nil, bottom: nil, right: rightAnchor, topConstant: 8, leftConstant: 0, bottomConstant: 0, rightConstant: 8, widthConstant: 80, heightConstant: 45)
    }
}
