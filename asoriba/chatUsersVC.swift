//
//  chatUsersVC.swift
//  asoriba
//
//  Created by Benjamin Acquah on 11/09/2017.
//  Copyright © 2017 Asoriba Inc. All rights reserved.
//

import UIKit
import LBTAComponents
import Alamofire
import AlamofireImage
import Photos
import SKPhotoBrowser
import Toaster
import Crashlytics
import Firebase
import RealmSwift

class chatUsersVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, ChurchMembersServiceProtocol, UISearchBarDelegate {
    
    lazy var collectionView : UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.delegate = self
        cv.dataSource = self
        cv.alwaysBounceVertical = true
        cv.showsVerticalScrollIndicator = false
        cv.backgroundColor = .white
        return cv
    }()
    
    let titlebar : UILabel = {
        let tlt = UILabel()
        tlt.text = "New Chat"
        tlt.font = UIFont.boldSystemFont(ofSize: 14)
        tlt.textAlignment = .center
        tlt.textColor = .white
        tlt.translatesAutoresizingMaskIntoConstraints = false
        return tlt
    }()
    
    lazy var searchBar : UISearchBar = {
        let sb = UISearchBar()
        sb.placeholder = "Enter Name to search ..."
        sb.barTintColor = UIColor.groupTableViewBackground//(r: 28, g: 90, b: 65)
        sb.translatesAutoresizingMaskIntoConstraints = false
        sb.delegate = self
        UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).backgroundColor = UIColor(r: 28, g: 90, b: 65)
        return sb
    }()
    
    let greenView : UIView = {
        let searchView = UIView()
        searchView.translatesAutoresizingMaskIntoConstraints = false
        return searchView
    }()
    
    var api:ApiService!
    
    var positionSelected = 0
    var membersData = [Member]()
    var groupsData = [Group]()
    var feedsData = [Feed]()
    var userData = DataOperation.getUser()
    var chatUserData = [ChatUser]()
    var churchItem: Church!
    var feedNextUrl = ""
    var membersNextUrl = ""
    var groupsNextUrl = ""
    var loadedPages = [String]()
    var text = ""
    var activityViewIndicator:UIActivityIndicatorView = UIActivityIndicatorView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.collectionView.contentInset = UIEdgeInsetsMake(50, 0, 0, 0)
        
        //api instance
        api = ApiService()
        self.api.churchMembersDelegate = self
        
        self.navigationItem.title = "New Chat"
        self.collectionView.register(usersCell.self, forCellWithReuseIdentifier: cellIdentify)
        
        if !userData.churchId.isEmpty {
            self.getMembers(url: "page=1",churchId: userData.churchId)            
        }
        
        setUpView()
    }
    
    func startProgressBar()  {
        activityViewIndicator.center = self.view.center
        activityViewIndicator.hidesWhenStopped = true
        activityViewIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray
        self.view.addSubview(activityViewIndicator)
        activityViewIndicator.startAnimating()
    }
    
    func getData(page:String)  {
        self.api.searchChurcheMembers(url: "connections/search/?type=users&\(page)&page_size=500&keyword=\(text)")
    }
    
    func getMembers(url:String,churchId:String) {
        api.getChurchMembers(branchId: churchId,next:url)
    }

    func didReceiveChurchMembersSuccess(results: [Member]) {        
        self.membersData.removeAll()
        self.collectionView.reloadData()
        self.membersData.append(contentsOf: results)
        print("print search results: \(results)")
    }
    
    func didReceiveChurchMembersError(results: String) {
        //
    }
    
    func didReceiveChurchMembersDetail(results: String) {
        //
    }
    
    func didReceiveChurchMemberNextUrl(results: String) {
        //
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false

    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
    }
    func setUpView(){
        
        view.addSubview(collectionView)
        view.addSubview(searchBar)
        
        collectionView.anchor(view.topAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        searchBar.anchor(view.topAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, topConstant: 64, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 50)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return membersData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentify, for: indexPath) as! usersCell
        let users = membersData[indexPath.row]
        cell.users = users
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.view.frame.size.width, height: 60)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 1.0
    }

    //navigate to chat log Controller
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let user2_lastname = membersData[indexPath.row].lastName
        let user2_firstname = membersData[indexPath.row].firstName
        let chat2_userName = membersData[indexPath.row].otherName
        let user2_id = membersData[indexPath.row].id
        let user2_profileImage = membersData[indexPath.row].avatar
        
        let chat2_numberOfFollowers : Int = membersData[indexPath.row].numberOfFollowing
        let chat2_numberOfGroups : Int = membersData[indexPath.row].numberOfGroups
        
        let toChatLogCV = chatLogCV(collectionViewLayout: UICollectionViewFlowLayout())
        toChatLogCV.chatUser_2Id = user2_id
        toChatLogCV.chatUser_2firstName = user2_firstname
        toChatLogCV.chatUser_2lastname = user2_lastname
        toChatLogCV.chatuser_2avatar = user2_profileImage
        toChatLogCV.chatUserName = chat2_userName
        
        toChatLogCV.chat2_numberOfFollowers = chat2_numberOfFollowers
        toChatLogCV.chat2_numberOfGroups = chat2_numberOfGroups
        
        self.navigationController?.pushViewController(toChatLogCV, animated: true)
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        self.searchBar.showsCancelButton = true
        self.searchBar.becomeFirstResponder()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.navigationController?.navigationItem.hidesBackButton = false
        self.searchBar.endEditing(true)
        self.searchBar.showsCancelButton = false
        resignFirstResponder()
        self.searchBar.text = ""
        
        DispatchQueue.main.async {
            self.collectionView.reloadData()
        }
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        self.searchBar.showsCancelButton = false
        self.searchBar.resignFirstResponder()
    }
    
    //let username = "\(Member().firstName) + \(Member().lastName)"
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        print("searching")
        self.navigationController?.navigationItem.hidesBackButton = true
        
        
        if searchText.isEmpty {
            self.membersData.removeAll()
            self.getMembers(url: "page=1", churchId: userData.churchId)
        }else{
            
            if (searchText.count > 2){
                
                self.membersData.removeAll()
                
                startProgressBar()
                
                self.text = searchText
                self.getData(page: "page=1")
                self.activityViewIndicator.stopAnimating()
                
                
                DispatchQueue.main.async {
                    self.collectionView.reloadData()
                    self.activityViewIndicator.isHidden = true
                }
            }else{
                
                self.membersData.removeAll()
                
                DispatchQueue.main.async {
                    self.collectionView.reloadData()
                }
            }
        }
        
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.searchBar.resignFirstResponder()
    }
    
    func savedVaribles(){
        
    }
}
