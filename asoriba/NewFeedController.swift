//
//  NewFeedController.swift
//  asoriba
//
//  Created by Bright Ahedor on 18/01/2017.
//  Copyright © 2017 Asoriba Inc. All rights reserved.
//

import UIKit
import AlamofireImage
import BSImagePicker
import Photos
import MobileCoreServices
import AVKit
import AVFoundation
import MediaPlayer
import PopupDialog
import Toaster
import SConnection
import MRProgress


class NewFeedController: UIViewController,UITextViewDelegate,PostServiceProtocol,UINavigationControllerDelegate,UIImagePickerControllerDelegate,MPMediaPickerControllerDelegate,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
        
    var api:ApiService!
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var contentTextView: UITextView!
    let hintMessage = "Share God's word  today ..."
    let user = DataOperation.getUser()
    var message = ""
    var videoPath:NSURL?
    var audioPath:URL?
    var image:UIImage?
    let imagePicker = UIImagePickerController()
    var mediaPicker: MPMediaPickerController?
    @IBOutlet weak var singleImagePreview: UIImageView!
    let vcImagePicker = BSImagePickerViewController()
    
    var feedArray = [UIImage]()
    @IBOutlet weak var addPhotoImageView: UIImageView!
    @IBOutlet weak var addVideoImageView: UIImageView!
    @IBOutlet weak var addAudioImageView: UIImageView!
    @IBOutlet weak var addBibleVerseImageView: UIImageView!
    @IBOutlet weak var feedCollectionView: UICollectionView!
    let numberOfItemsPerRow : CGFloat = 1.0
    
    var newMedia : Bool = false
    var quotation = Quotation()
    
    @IBOutlet weak var videoIconImageView: UIImageView!
    @IBOutlet weak var singleImageVisualEffect: UIVisualEffectView!
    @IBOutlet weak var quotationLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let tag = "New Post"
        self.navigationItem.title = tag
        
        self.contentTextView.delegate = self
        self.contentTextView.textColor = UIColor.lightGray
        self.contentTextView.text = hintMessage
        self.singleImageVisualEffect.isHidden = true
        self.videoIconImageView.isHidden = true
        self.feedCollectionView.isHidden = true
        self.quotationLabel.isHidden = true
        
        self.singleImagePreview.contentMode = .scaleAspectFill
        
        api = ApiService()
        self.api.postServiceProtocol = self
        self.feedCollectionView.delegate = self
        self.feedCollectionView.dataSource = self
        self.imagePicker.delegate = self
        
        //register cell
        self.feedCollectionView.register(UINib(nibName: CellIdentifiers.imageCollectionViewCell.rawValue, bundle: nil), forCellWithReuseIdentifier: CellIdentifiers.imageCollectionViewCell.rawValue)
        //end section here
        
        
        updateUI()
        
        
        self.setMenuItems()
        
       
        //image picker recognition
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.tapImagePicker))
        self.addPhotoImageView.addGestureRecognizer(tap)
        self.addPhotoImageView.isUserInteractionEnabled = true
        
        //video picker recognition
        let tapVideo = UITapGestureRecognizer(target: self, action: #selector(self.tapVideoPicker))
        self.addVideoImageView.addGestureRecognizer(tapVideo)
        self.addVideoImageView.isUserInteractionEnabled = true
        
      
        //audio picker instead
        let tapVerse = UITapGestureRecognizer(target: self, action: #selector(self.tapAudioPicker))
        self.addBibleVerseImageView.addGestureRecognizer(tapVerse)
        self.addBibleVerseImageView.isUserInteractionEnabled = true
        
        //verse picker
        let tapAudio = UITapGestureRecognizer(target: self, action: #selector(self.tapVersePicker))
        self.addAudioImageView.addGestureRecognizer(tapAudio)
        self.addAudioImageView.isUserInteractionEnabled = true
        
    
        imagePicker.delegate = self
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(NewFeedController.receivedQuotationNotification(notification:)), name: Notification.Name(AllKeys.verseNotification), object: nil)
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(NewFeedController.receivedAudioNotification(notification:)), name: Notification.Name(AllKeys.audioNotification), object: nil)

        //switch navigation Bar
        let path = Bundle.main.path(forResource: "configuration", ofType: "plist")
        let root = NSDictionary(contentsOfFile: path!)
        
        let naBarColor = (root?["headerBackgroundColor"] as! String)
        
        self.navigationController?.navigationBar.barTintColor = UIColor().HexToColor(hexString: naBarColor)
        
        endEditing()
   }
    
    func endEditing(){
        self.view.endEditing(true)
        resignFirstResponder()
    }
    
    func receivedQuotationNotification(notification: Notification){
        let quotation = notification.object as! Quotation
        //Take Action on Notification
        print("Received Quotation\(quotation)")
        
        self.quotation = quotation
        self.quotationLabel.isHidden = false
        self.quotationLabel.text = quotation.book + " " + quotation.chapter + ":" + quotation.verse
        
    }
    
    func receivedAudioNotification(notification: Notification){
        let audio = notification.object
        if audio != nil {
            //Take Action on Notification
            self.audioPath = audio as? URL
            self.setAudioView()
        }
  
    }
    
    
    //MARK:- collection view delegate
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return feedArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let feed = self.feedArray[indexPath.row]
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CellIdentifiers.imageCollectionViewCell.rawValue, for: indexPath) as! ImageCollectionViewCell
        cell.backgroundColor = UIColor.white
        cell.feed = feed
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, shouldHighlightItemAt indexPath: IndexPath) -> Bool {
        return false
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return CGFloat(AllKeys.cellSpacing)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return CGFloat(AllKeys.cellSpacing)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: CGFloat(AllKeys.cellSpacing), left: 0.0, bottom: 0.0, right: 0.0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
        let contentWidth = collectionView.bounds.width
        let totalSpace = flowLayout.sectionInset.left
            + flowLayout.sectionInset.right
            + (flowLayout.minimumInteritemSpacing * CGFloat(numberOfItemsPerRow - 1))
        let size = Int((contentWidth - totalSpace) / CGFloat(numberOfItemsPerRow))
        let _ = self.feedArray[indexPath.row]
        return CGSize(width: size - 100, height: 190)
        
    }
    
    


    //MARK: - image picker
    func tapImagePicker()
    {
       self.showAction(isPhoto: true)
        
    }
    
    //MARK: - video picker
    func tapVideoPicker()
    {
        self.showAction(isPhoto: false)
        
    }
    
    //MARK: - audio picker
    func tapAudioPicker()
    {
       //self.audioShowAction()
        self.startAudioRecording()
        
    }
    
    
    //MARK: - verse picker
    func tapVersePicker()
    {
        let popOverVC = UIStoryboard(name: AllKeys.mainStoryBoard, bundle: nil).instantiateViewController(withIdentifier: AllKeys.versePickerController) as! VersePickerController
        self.addChildViewController(popOverVC)
        popOverVC.view.frame = self.view.frame
        self.view.addSubview(popOverVC.view)
        popOverVC.didMove(toParentViewController: self)
        
    }
    
    
    func startAudioRecording()  {
        let popOverVC = UIStoryboard(name: AllKeys.mainStoryBoard, bundle: nil).instantiateViewController(withIdentifier: AllKeys.recordAudioController) as! RecordAudioController
        self.addChildViewController(popOverVC)
        popOverVC.view.frame = self.view.frame
        self.view.addSubview(popOverVC.view)
        popOverVC.didMove(toParentViewController: self)
    }
    
    //MARK- audio recording action prompt
    func audioShowAction() {
        
        let alertActionControl = UIAlertController.init(title: nil, message: "Complete Action", preferredStyle: .actionSheet)
        
        let alertActionCamera = UIAlertAction.init(title: "Record", style: .default) {(Alert:UIAlertAction!) -> Void in
             self.startAudioRecording()
        }
        let alertActionGallery = UIAlertAction.init(title: "Gallery", style: .default) {(Alert:UIAlertAction!) -> Void in
           self.pickAudio()
        }
        
        let alertActionCancel = UIAlertAction.init(title: "Cancel", style: .cancel) {(Alert:UIAlertAction!) -> Void in
            
        }
        
        alertActionControl.addAction(alertActionCamera)
        alertActionControl.addAction(alertActionGallery)
        alertActionControl.addAction(alertActionCancel)
        
        self.present(alertActionControl,animated:true,completion:nil)
        
    }
    
    //MARK - pick audio
    func pickAudio()  {
        let mediaPicker = MPMediaPickerController.self(mediaTypes:MPMediaType.anyAudio)
        mediaPicker.allowsPickingMultipleItems = false
        mediaPicker.delegate = self
        self.present(mediaPicker, animated: true, completion: nil)
    }
    

    
    //Audio picker delegate
    func mediaPickerDidCancel(_ mediaPicker: MPMediaPickerController) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func mediaPicker(_ mediaPicker: MPMediaPickerController, didPickMediaItems mediaItemCollection: MPMediaItemCollection) {
        self.dismiss(animated: true, completion: nil)
        for mediaItem in mediaItemCollection.items {
          print("Media \(mediaItem.assetURL!.absoluteURL)")
          self.audioPath = mediaItem.assetURL!.absoluteURL
          self.setAudioView()
        }
    }
    //end of delegate
    
    
    func showAction(isPhoto:Bool) {
        
        let alertActionControl = UIAlertController.init(title: nil, message: "Pick from", preferredStyle: .actionSheet)
        
        let alertActionCamera = UIAlertAction.init(title: "Camera", style: .default) {(Alert:UIAlertAction!) -> Void in
            if (isPhoto){
              self.takePhoto()
            } else {
              self.takeVideo()
            }
        }
        let alertActionGallery = UIAlertAction.init(title: "Gallery", style: .default) {(Alert:UIAlertAction!) -> Void in
            if (isPhoto){
                self.pickPhotos()
            } else {
                self.pickVideo()
            }
        }
        
        let alertActionCancel = UIAlertAction.init(title: "Cancel", style: .cancel) {(Alert:UIAlertAction!) -> Void in
            
        }
        
        alertActionControl.addAction(alertActionCamera)
        alertActionControl.addAction(alertActionGallery)
        alertActionControl.addAction(alertActionCancel)
        
        self.present(alertActionControl,animated:true,completion:nil)
        
    }
    
    func pickPhotos()  {
        let vc = BSImagePickerViewController()
        vc.maxNumberOfSelections = 5
        vc.takePhotoIcon = UIImage(named: "AppIcon")
        vc.albumButton.tintColor = UIColor.white
        vc.cancelButton.tintColor = UIColor.white
        vc.doneButton.tintColor = UIColor.white
        vc.selectionCharacter = "✓"
        vc.selectionFillColor = Mics.hexStringToUIColor(hex: AllKeys.primaryHexCode)
        vc.selectionStrokeColor = UIColor.white
        vc.selectionShadowColor = UIColor.white
        vc.selectionTextAttributes[NSForegroundColorAttributeName] = UIColor.white
        vc.cellsPerRow = {(verticalSize: UIUserInterfaceSizeClass, horizontalSize: UIUserInterfaceSizeClass) -> Int in
            switch (verticalSize, horizontalSize) {
            case (.compact, .regular): // iPhone5-6 portrait
                return 2
            case (.compact, .compact): // iPhone5-6 landscape
                return 2
            case (.regular, .regular): // iPad portrait/landscape
                return 3
            default:
                return 2
            }
        }
        
        bs_presentImagePickerController(vc, animated: true,
                                        select: { (asset: PHAsset) -> Void in
                                            print("Selected: \(asset)")
                                            
        }, deselect: { (asset: PHAsset) -> Void in
            print("Deselected: \(asset)")
        }, cancel: { (assets: [PHAsset]) -> Void in
            print("Cancel: \(assets)")
        }, finish: { (assets: [PHAsset]) -> Void in
               print("Finish: \(assets)")
               self.resetImagesView()
               self.getAllImages(assets: assets)
        }, completion: nil)
        
    }
    
    func resetImagesView()  {
        
        //the single image view is called on the main thread to remove delay
        DispatchQueue.main.async {
            self.singleImagePreview.isHidden = true
            self.singleImageVisualEffect.isHidden = true
            self.videoIconImageView.isHidden = true
            self.feedCollectionView.isHidden = false
            self.feedArray.removeAll()
            self.feedCollectionView.reloadData()

        }
        
        //self.image = nil
        //self.videoPath = nil
        
    }
    
    func setVideoView()  {
       self.videoIconImageView.isHidden = false
       self.singleImagePreview.isHidden = false
       self.singleImageVisualEffect.isHidden = false
       self.feedCollectionView.isHidden = true
        
        self.feedArray.removeAll()
    }
    
    func setAudioView()  {
        self.videoIconImageView.image = UIImage(named:"musical-note")
        self.videoIconImageView.isHidden = false
        self.singleImagePreview.isHidden = false
        self.singleImageVisualEffect.isHidden = false
        self.feedCollectionView.isHidden = true
        
        self.feedArray.removeAll()
    }
    
    func setSingleImageView()  {
        
        self.videoIconImageView.isHidden = true
        self.singleImagePreview.isHidden = false
        self.singleImageVisualEffect.isHidden = true
        self.feedCollectionView.isHidden = true
        
        //self.feedArray.removeAll()
        self.videoPath = nil
    }
    
    func getAllImages(assets:[PHAsset]) -> Void {
        self.feedArray.removeAll()
        DispatchQueue.main.async {
            for i in 0..<assets.count{
                let thumbnail = self.getAssetThumbnail(asset: assets[i])
                self.feedArray.append(thumbnail)
            }
             self.feedCollectionView.reloadData()
             self.navigationItem.rightBarButtonItem?.isEnabled = true
        }
    }
    
    
    func getAssetThumbnail(asset: PHAsset) -> UIImage {
        let manager = PHImageManager.default()
        let option = PHImageRequestOptions()
        var thumbnail = UIImage()
        option.isSynchronous = true
        manager.requestImage(for: asset, targetSize: CGSize(width: 350, height: 350), contentMode: .aspectFit, options: option, resultHandler: {(result, info)->Void in
            thumbnail = result!
        })
        return thumbnail
    }
    


    //take photo only if the cammera is available
    func takePhoto()  {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera){
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.allowsEditing = true
            imagePicker.sourceType = .camera
            imagePicker.mediaTypes = [(kUTTypeImage as NSString) as String]
            present(imagePicker, animated: true, completion: nil)
        }else{
           ControllerHelpers.showDialog(targetVC: self, title: "Asoriba", message: "Your device has no camera")
        }
    }
    
    //take photo only if the cammera is available
    func pickVideo()  {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.savedPhotosAlbum){
            imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
            imagePicker.mediaTypes = [(kUTTypeMovie as NSString) as String]
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
            newMedia = false
            
        }else {
          ControllerHelpers.showDialog(targetVC: self, title: "Asoriba", message: "No video content to pick from ")
        }
        
    }
    
    
    //take photo only if the cammera is available
    func takeVideo()  {
        if (UIImagePickerController.isSourceTypeAvailable(.camera)) {
            if UIImagePickerController.availableCaptureModes(for: .rear) != nil {
                imagePicker.sourceType = .camera
                imagePicker.mediaTypes = [(kUTTypeMovie as NSString) as String]
                imagePicker.allowsEditing = false
                present(imagePicker, animated: true, completion: {})
                
                newMedia = true
                
            } else {
                 ControllerHelpers.showDialog(targetVC: self, title: "Asoriba", message: "Your device has no camera")
            }
        } else {
             ControllerHelpers.showDialog(targetVC: self, title: "Asoriba", message: "Your device has no camera")
        }
        
    }
    
    func updateUI() {
        let avatar = self.user.avatar
        self.userNameLabel.text = self.user.lastName + " " + self.user.firstName
        if  !(avatar.isEmpty) {
            self.userImageView.af_setImage(
                withURL: URL(string: (avatar))!,
                placeholderImage: UIImage(named: "default"),
                imageTransition: .crossDissolve(0.2)
            )
        }else
        {
            self.userImageView.image = UIImage(named: "default")
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        imagePicker.dismiss(animated: true, completion: nil)
     
        let mediaType = info[UIImagePickerControllerMediaType] as! NSString
        
        if mediaType.isEqual(to: (kUTTypeImage as NSString) as String)
        {
            let image = info[UIImagePickerControllerOriginalImage] as! UIImage
            
            if (newMedia == true)
            {
                
                UIImageWriteToSavedPhotosAlbum(image, self, Selector(("image:didFinishSavingWithError:contextInfo:")), nil)

            }
            
            self.setSingleImageView()
            self.singleImagePreview.transform = CGAffineTransform(scaleX: -1, y: 1)
            self.singleImagePreview.image = image
            
        }
        else if mediaType.isEqual(to: (kUTTypeMovie as NSString) as String)
        {
            _ = info[UIImagePickerControllerMediaURL] as! NSURL
                
                if(newMedia == true)
                {
                    //UISaveVideoAtPathToSavedPhotosAlbum(videoPath as String, self, Selector(("image:didFinishSavingWithError:contextInfo:")), nil)
                }
            
            self.videoPath = info[UIImagePickerControllerMediaURL] as? NSURL
            self.getThumbnail(filePath: self.videoPath! as URL)
            
        }
        
    }
    
    func getThumbnail(filePath:URL) {
        
        do {
            let asset = AVURLAsset(url: filePath , options: nil)
            let imgGenerator = AVAssetImageGenerator(asset: asset)
            imgGenerator.appliesPreferredTrackTransform = true
            let cgImage = try imgGenerator.copyCGImage(at: CMTimeMake(0, 1), actualTime: nil)
            let thumbnail = UIImage(cgImage: cgImage)
            
            self.setVideoView()
            self.singleImagePreview.image = thumbnail
            
        } catch _ as NSError {
            ControllerHelpers.showDialog(targetVC: self, title: "Asoriba", message: "Failed to prepare video for upload")
            
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        imagePicker.dismiss(animated: true, completion: nil)
    }
    
    //setup the menu items for current user
    func setMenuItems() {
        let postButtonItem = UIBarButtonItem.init(title: "Post", style: .plain, target: self, action: #selector(handleClick))
        self.navigationItem.rightBarButtonItem = postButtonItem
        
        let cancelButtonItem = UIBarButtonItem.init(title: "Cancel", style: .plain, target: self, action: #selector(handleCancelClick))
        self.navigationItem.leftBarButtonItem = cancelButtonItem
    }
    
    func handleCancelClick()  {
        self.contentTextView.resignFirstResponder()
        self.dismiss(animated: true, completion: nil)
    }
    
    func handleClick()  {
        self.contentTextView.resignFirstResponder()
        if (SConnection.isConnectedToNetwork()){
            if contentTextView.textColor != UIColor.lightGray {
                self.message = self.contentTextView.text
            }else {
                self.message = ""
            }
           
            
            let image = self.singleImagePreview.image
            
            if self.message.isEmpty  {
                ControllerHelpers.showDialog(targetVC: self, title: "Asoriba", message: "Add a message at least")
                return
            }
            
            Toast(text: "Posting...").show()
            MRProgressOverlayView.showOverlayAdded(to: self.view, title: "Posting", mode: MRProgressOverlayViewMode.checkmark, animated: true)
            
            
            if !self.feedArray.isEmpty {
                self.api.addPostImage(images: self.feedArray, content: message,quotation:self.quotation)
                self.dismiss(animated: true, completion: nil)
            } else if self.videoPath != nil  {
                self.api.addPostVideo(image: image!, videoPath: self.videoPath! as URL, content: message,quotation:self.quotation)
                self.dismiss(animated: true, completion: nil)
            } else if image != nil && self.videoPath == nil{
                self.feedArray.append(image!)
                self.api.addPostImage(images: self.feedArray, content: message,quotation:self.quotation)
                self.dismiss(animated: true, completion: nil)
            } else if self.audioPath != nil {
                self.api.addPostAudio(audioPath: self.audioPath!, content: message, quotation: self.quotation)
                self.dismiss(animated: true, completion: nil)
            } else {
                self.api.addPost(message: message,quotation:self.quotation)
                self.dismiss(animated: true, completion: nil)
            }
        }else{
            
            ControllerHelpers.showDialog(targetVC: self, title: "Ooops😭", message: "Your device apears to be offline, Check your internet connection")
        }
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = hintMessage
            textView.textColor = UIColor.lightGray
        }
    }
    
    func textViewShouldEndEditing(_ textView: UITextView) -> Bool {
        message = contentTextView.text
        contentTextView.resignFirstResponder()
        return true
    }
    
    //
    
    //post delegate here
    func didReceivePostResult(result: Feed) {
        
    }
    
    func didReceivePostError(result: String) {
        
    }

}
