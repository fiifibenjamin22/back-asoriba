//
//  CommentCollectionViewCell.swift
//  asoriba
//
//  Created by Bright Ahedor on 15/02/2017.
//  Copyright © 2017 Asoriba Inc. All rights reserved.
//

import UIKit
import AlamofireImage

class CommentCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var commentTimeLabel: UILabel!
    @IBOutlet weak var deletedCommentButton: UIButton!
    @IBOutlet weak var commentLabel: UILabel!
    @IBOutlet weak var commentLabelConstraint: NSLayoutConstraint!
    
    
    let user = DataOperation.getUser()
    
    var comment: Comment! {
        didSet {
            self.updateUI()
        }
    }
    
    func updateUI()  {
        
        //New
        self.layer.shadowColor = UIColor.lightGray.cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        self.layer.shadowRadius = 2.0
        self.layer.shadowOpacity = 1.0
        self.layer.masksToBounds = false
        self.layer.shadowPath = UIBezierPath(roundedRect: self.bounds, cornerRadius: self.contentView.layer.cornerRadius).cgPath
        
        self.commentTimeLabel.translatesAutoresizingMaskIntoConstraints = false
        self.userNameLabel.translatesAutoresizingMaskIntoConstraints = false

        userNameLabel.topAnchor.constraint(equalTo: topAnchor, constant: 5).isActive = true
        
        commentTimeLabel.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        commentTimeLabel.leftAnchor.constraint(equalTo: commentLabel.leftAnchor).isActive = true
        commentTimeLabel.heightAnchor.constraint(equalToConstant: 20).isActive = true
        commentTimeLabel.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
        
        commentTimeLabel.textColor = .black
        
        
        let member = comment.member
        
        let time = comment.date
        if (time.lowercased() != "now"){
            self.commentTimeLabel.text = Mics.setDateElapsed(dateString: comment.date)
        }
        
        self.userNameLabel.text = "\(member!.lastName)  \(member!.firstName)"
        
//        if (comment.id.isEmpty) || (self.user.id == (member?.id)!) {
//            self.deletedCommentButton.isHidden = false
//        }else {
//            self.deletedCommentButton.isHidden = true
//        }
        
        self.commentLabel.text = comment.content
        
        let avatar = member?.avatar
        //content image
        if  !(avatar?.isEmpty)! {
            self.userImageView.af_setImage(
                withURL: URL(string: (avatar)!)!,
                placeholderImage: UIImage(named: "default"),
                imageTransition: .crossDissolve(0.2)
            )
        }else
        {
            self.userImageView.image = UIImage(named: "default")
        }
        
        self.commentLabel.numberOfLines = 5
        self.commentLabelConstraint.constant = Helpers.getHeightOfLabel(text: comment.content, fontSize: CGFloat(17), width: UIScreen.main.bounds.width-CGFloat(16),numberOfLines: 5)
        
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        self.userImageView.af_cancelImageRequest()
        self.userImageView.layer.removeAllAnimations()
        self.userImageView.image = nil
    }

}
