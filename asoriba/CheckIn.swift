//
//  CheckIn.swift
//  asoriba
//
//  Created by Bright Ahedor on 16/03/2017.
//  Copyright © 2017 Asoriba Inc. All rights reserved.
//

import Foundation

class CheckIn {
    var id = ""
    var device = ""
    var attempts = 0
    var date = ""
    var checkInTime = ""
    var program = ""
}
