//
//  FeedCollectionHeader.swift
//  asoriba
//
//  Created by Bright Ahedor on 15/02/2017.
//  Copyright © 2017 Asoriba Inc. All rights reserved.
//

import UIKit
import AlamofireImage
import Firebase
import LBTAComponents

class FeedCollectionHeader: UICollectionReusableView {

    @IBOutlet weak var buttonShareWord: UIButton!
    @IBOutlet weak var userImageView: UIImageView!
    
    var user: User!{
        didSet{
            updateUI()
        }
    }
    
    
    func updateUI() {
        
//        let path = Bundle.main.path(forResource: "configuration", ofType: "plist")
//        let root = NSDictionary(contentsOfFile: path!)
//        
//        let background = (root?["headerBackgroundColor"] as! String)
//        let whitecolor = (root?["tint_white"] as! String)
//        buttonShareWord.setTitleColor(UIColor().HexToColor(hexString: whitecolor), for: UIControlState.normal)
//        backgroundColor = UIColor().HexToColor(hexString: background)
        
        self.userImageView.translatesAutoresizingMaskIntoConstraints = false
        self.buttonShareWord.translatesAutoresizingMaskIntoConstraints = false
        
        self.userImageView.leftAnchor.constraint(equalTo: leftAnchor, constant: 8).isActive = true
        self.userImageView.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        self.userImageView.widthAnchor.constraint(equalToConstant: 50).isActive = true
        self.userImageView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        self.userImageView.layer.cornerRadius = 25
        self.userImageView.layer.masksToBounds = true
        
        buttonShareWord.anchor(userImageView.topAnchor, left: userImageView.rightAnchor, bottom: userImageView.bottomAnchor, right: rightAnchor, topConstant: 0, leftConstant: 8, bottomConstant: 0, rightConstant: 8, widthConstant: 0, heightConstant: 0)
        
        //content image
        let avatar = self.user.avatar
        
        if  !(avatar.isEmpty) {
            self.userImageView.af_setImage(
                withURL: URL(string: (avatar))!,
                placeholderImage: UIImage(named: "default"),
                imageTransition: .crossDissolve(0.2)
            )
        }else
        {
            self.userImageView.image = UIImage(named: "default")
        }
        
        self.buttonShareWord.addTarget(self, action: #selector(shareInteract), for: UIControlEvents.touchUpInside)
    }
    
    func shareInteract(){
        
        Analytics.logEvent("Start_shareBtn_Clicked", parameters: nil)
    }
    

    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        self.userImageView.af_cancelImageRequest()
        self.userImageView.layer.removeAllAnimations()
        self.userImageView.image = nil
    }
}
