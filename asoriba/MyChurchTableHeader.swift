//
//  MyChurchTableHeader.swift
//  asoriba
//
//  Created by Bright Ahedor on 20/12/2016.
//  Copyright © 2016 Asoriba Inc. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage

var churchBranchname = ""

class MyChurchTableHeader: UITableViewHeaderFooterView {
    
    @IBOutlet weak var churchBackDropImageView: UIImageView!
    @IBOutlet weak var churchLogoImageView: UIImageView!
    @IBOutlet weak var churchNameLabel: UILabel!
    @IBOutlet weak var churchStatsLabel: UILabel!
    @IBOutlet weak var joinButton: UIButton!
    
    @IBOutlet weak var myChurchSegment: UISegmentedControl!
    @IBAction func churchSegmentButtonClick(_ sender: UISegmentedControl) {
        
    }
    @IBOutlet weak var branchLabel: UILabel!
    
    var churchItem: Church!{
        didSet {
            //self.updateAllData(churchData: churchItem)
        }
    }
    var userData: User! {
        didSet {
            self.updateUI()
        }
    }
    
    var api:ApiService!
 
    override func awakeFromNib() {
        //api instance
        api = ApiService()
    }

    func updateUI()  {
        
        let path = Bundle.main.path(forResource: "configuration", ofType: "plist")
        let root = NSDictionary(contentsOfFile: path!)
        
        let headerBackground = (root?["headerBackgroundColor"] as! String)
        //let headerYellowBackground = (root?["headerYellowColor"] as! String)
        let tint_white = (root?["tint_white"] as! String)

        var numberOfFollowers = 0
        var numberOfGroups = 0
        
        if (self.churchItem != nil){
            numberOfFollowers = churchItem.numberOfFollowers
            numberOfGroups = churchItem.numberOfGroups
        } else {
            numberOfFollowers = userData.numberOfFollowers
            numberOfGroups = userData.numberOfGroups
        }
        
        //let followsShowable = numberOfFollowers == 1 ? "\(numberOfFollowers) Follower" : "\(numberOfFollowers) Followers";
        //let groupsShowable = numberOfGroups == 1 ? "\(numberOfGroups) Group" : "\(numberOfGroups) Groups";
        
        
        //branch issues must be resolved here//
        
        if userData.churchName.isEmpty {
            //self.churchNameLabel.text = "----"
            //self.branchLabel.isHidden = true
        }else if churchItem != nil {
            //self.branchLabel.isHidden = false
//            self.branchLabel.text = churchItem.branchName
//            self.churchNameLabel.text = userData.churchName
            
            //print("print church details \(userData.churchName) + \(churchItem.branchName)")
            
        }else {
           // self.churchNameLabel.text = userData.churchName
           // self.branchLabel.isHidden = true
            
            print("print church details \(userData.churchName)")
        }
        
//        self.joinButton.setTitle("CHECK IN", for: UIControlState.normal)
//        self.joinButton.backgroundColor = UIColor().HexToColor(hexString: headerBackground)
//        self.joinButton.layer.cornerRadius = 3
//        self.joinButton.layer.borderWidth = 2
//        self.joinButton.layer.borderColor = UIColor().HexToColor(hexString: tint_white).cgColor
        
        self.myChurchSegment.backgroundColor = UIColor().HexToColor(hexString: tint_white)
        self.myChurchSegment.tintColor = UIColor().HexToColor(hexString: headerBackground)
        
        //self.churchStatsLabel.text = "\(followsShowable) • \(groupsShowable)"
        //content image
//        Alamofire.request((userData.churchBackDrop)).responseImage { response in
//            if let image = response.result.value {
//                self.churchBackDropImageView.image = image
//            }else{
//                self.churchBackDropImageView.image = UIImage(named: "default")
//            }
//        }
        
        //user image
//        Alamofire.request((userData.churchLogo)).responseImage { response in
//            if let image = response.result.value {
//                self.churchLogoImageView.image = image
//            }else{
//                self.churchLogoImageView.image = UIImage(named: "default")
//            }
//        }
        
//        self.churchLogoImageView.isUserInteractionEnabled = true
        
    }
    

//    func updateAllData(churchData: Church)  {
//        self.userData = User()
//        self.userData.churchBackDrop = churchData.image
//        self.userData.churchLogo = churchData.churchLogo
//        self.userData.numberOfFollowers = churchData.numberOfFollowers
//        self.userData.numberOfGroups = churchData.numberOfGroups
//        self.userData.churchName = churchData.branchName
//        self.userData.churchLogo = churchData.churchLogo
//
//        self.updateUI()
//    }

}
