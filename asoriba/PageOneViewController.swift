//
//  PageOneViewController.swift
//  asoriba
//
//  Created by Bright Ahedor on 14/12/2016.
//  Copyright © 2016 Asoriba Inc. All rights reserved.
//

import UIKit
import Fabric
import Crashlytics

//page one
class PageOneViewController: UIViewController {

    @IBOutlet weak var walk1Image: UIImageView!
    @IBOutlet weak var pageOneStartBtn : UIButton!
    @IBOutlet weak var pageOneDecsTxt : UILabel!
    @IBOutlet weak var pageOneTitle : UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let path = Bundle.main.path(forResource: "configuration", ofType: "plist")
        let root = NSDictionary(contentsOfFile: path!)
        
        //let walkBackgroundColor = (root?["headerYellowColor"] as! String)
        let walkContentColor = (root?["headerBackgroundColor"] as! String)
        let walk1Img = (root?["walk1-image"] as! String)
        let walk1Title = (root?["walk1-title"] as! String)
        let walk1Desc = (root?["walk1-Desc"] as! String)
        let walkOneBackColor = (root?["walk1-BackColor"] as! String)
        
        view.backgroundColor = UIColor().HexToColor(hexString: walkOneBackColor)
        pageOneStartBtn.backgroundColor = UIColor().HexToColor(hexString: "#FFFFFF")
        pageOneStartBtn.setTitleColor(UIColor().HexToColor(hexString: walkContentColor), for: .normal)
        pageOneTitle.textColor = UIColor().HexToColor(hexString: "#FFFFFF")
        pageOneDecsTxt.textColor = UIColor().HexToColor(hexString: "#FFFFFF")
        
        walk1Image.image = UIImage(named: walk1Img)
        pageOneTitle.text = walk1Title
        pageOneTitle.font = UIFont.boldSystemFont(ofSize: 20)
        
        pageOneDecsTxt.text = walk1Desc
        
        
        Answers.logCustomEvent(withName: "NEW_INSTALLATION_HAS_STARTED", customAttributes: nil)
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
}


//page two
class PageTwoViewController: UIViewController {
    
    @IBOutlet weak var walk2Image: UIImageView!
    @IBOutlet weak var pageTwoStartBtn : UIButton!
    @IBOutlet weak var pageTwoDecsTxt : UILabel!
    @IBOutlet weak var pageTwoTitle : UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        let path = Bundle.main.path(forResource: "configuration", ofType: "plist")
        let root = NSDictionary(contentsOfFile: path!)
        
        //let walkBackgroundColor = (root?["headerBackgroundColor"] as! String)
        let walk2Img = (root?["walk2-image"] as! String)
        let walk2Title = (root?["walk2-title"] as! String)
        let walk2Desc = (root?["walk2-Desc"] as! String)
        let walkTwoBackColor = (root?["walk2-BackColor"] as! String)
        
        view.backgroundColor = UIColor().HexToColor(hexString: walkTwoBackColor)
        
        walk2Image.image = UIImage(named: walk2Img)
        pageTwoTitle.text = walk2Title
        pageTwoTitle.font = UIFont.boldSystemFont(ofSize: 20)
        
        pageTwoDecsTxt.text = walk2Desc

    }
}


//page three
class PageThreeViewController: UIViewController {
    
    @IBOutlet weak var pageThreeDecsTxt : UILabel!
    @IBOutlet weak var pageThreeTitle : UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        let path = Bundle.main.path(forResource: "configuration", ofType: "plist")
        let root = NSDictionary(contentsOfFile: path!)
        
        //let walkBackgroundColor = (root?["headerBackgroundColor"] as! String)
        let walk3Title = (root?["walk3-title"] as! String)
        let walk3Desc = (root?["walk3-Desc"] as! String)
        let walkThreeBackColor = (root?["walk3-BackColor"] as! String)
        
        view.backgroundColor = UIColor().HexToColor(hexString: walkThreeBackColor)
        
        pageThreeTitle.text = walk3Title
        pageThreeTitle.font = UIFont.boldSystemFont(ofSize: 17)
        
        pageThreeDecsTxt.text = walk3Desc

    }
}
