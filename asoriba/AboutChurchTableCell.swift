//
//  AboutChurchTableCell.swift
//  asoriba
//
//  Created by Bright Ahedor on 22/12/2016.
//  Copyright © 2016 Asoriba Inc. All rights reserved.
//

import UIKit

class AboutChurchTableCell: UITableViewCell {

    
    @IBOutlet weak var contentTitle: UILabel!
    
    @IBOutlet weak var contentSubtitle: UILabel!
    @IBOutlet weak var contentConstraint: NSLayoutConstraint!
    
    var feed: About! {
        didSet {
          updateUI()
        }
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func updateUI() {
        
        self.contentTitle.text = feed.title
        let message = feed.subTitle + "\n\n" + feed.content
        self.contentSubtitle.text = message
        self.contentSubtitle.numberOfLines = 5
        self.contentConstraint.constant = Helpers.getHeightOfLabel(text: message, fontSize: CGFloat(17), width: UIScreen.main.bounds.width-CGFloat(17),numberOfLines: 3)

    }
}
